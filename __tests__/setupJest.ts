/**
 * setupJest.js global 'fetch'
 * to run: yarn jest --coverage
 * to setup:  in package.json
 */
global.fetch = require('jest-fetch-mock');