import {api} from '../../App/api/api';

const loginRequest = require('./login/Login_request.json')

const loginResponse404 = require('./login/Login_response_error404.json')
const loginResponse200 = require('./login/Login_response_success200.json')

beforeEach(() => {
    fetch.resetMocks();
});




// Mock http test that returns 200
test("test login returns 200", () => {
    fetch.mockResponseOnce(JSON.stringify(loginResponse200));

    const onResponse = jest.fn();
    const onError = jest.fn();


    return api("/account/auth/" , loginRequest)
        .then(onResponse)
        .catch(onError)
        .finally(() => {
            expect(onResponse).toHaveBeenCalledTimes(1);
            expect(onError).not.toHaveBeenCalled();

        });
});
// Mock http test that returns 200 and validated correctly
test("test login returns 200 and validated correctly", async () => {
    fetch.mockResponseOnce(JSON.stringify(loginResponse200));

    const result = await api("/account/auth/", loginRequest)
    const data = await result.json()

    expect(data.accountId).toBe(1111)
    expect(data.verified).toBe(0)
    expect(data.authData).toBe("cmNyLXRlc3QwMDdAcG5nYW1pbmcuY29tOjEyMzQ=")
});


// Mock http test that returns 200
test("test login returns 400", () => {
    fetch.mockRejectOnce(JSON.stringify(loginResponse404));

    const onResponse = jest.fn();
    const onError = jest.fn();


    return api("/account/auth/" , loginRequest)
        .then(onResponse)
        .catch(onError)
        .finally(() => {
            expect(onResponse).not.toHaveBeenCalled;
            expect(onError).toHaveBeenCalledTimes(1);

        });
});

// Mock http test that returns 200 and validated correctly
test("test login returns 404 and validated correctly", async () => {
    fetch.mockResponseOnce(JSON.stringify(loginResponse404));

    const result = await api("/account/auth/", loginRequest)
    const data = await result.json()

    expect(data.status).toBe("404")
    expect(data.message).toBe("Account could not be found with the provided information")
});