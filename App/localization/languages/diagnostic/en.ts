export default {
    diagnostic_success: 'Thank you for the feedback',
    diagnostic_success_msg: 'We\'ll get back to you in the next 24-48 hours. Please try the following troubleshooting tips while we look into the issue:\n'
    +"\n\u2022 Bluetooth is on"
    +"\n\u2022 Location Services are set to Always"
    +"\n\u2022 Phone data is available",
    diagnostic_error: 'Error',
    diagnostic_help_and_support: 'Help & Support',
    diagnostic_having_issues_checking_in: 'Having Issues Checking In?',
    diagnostic_instruction_subtext: 'Click "Send" to share your app diagnostics with the RCR Team and we will help you get checked in as soon as possible! Thanks',
    diagnostic_send_button: 'SEND',
    genericError: 'Something went wrong please try again',
}