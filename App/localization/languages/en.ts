import Login from './login'
import Register from './register'
import Profile from './profile'
import Commons from './commons'
import Notification from './notification'
import Dashboard from './dashboard'
import ChangePin from './changepin'
import Referral from './referral'
import ChangeEmail from './changeemail'
import DrawingDetails from './drawingDetails'
import Onboarding from './onboarding'
import Deactivate from './deactivate'
import Forgotpin from './forgotpin'
import Permission from './permission'
import Diagnostic from './diagnostic'

export default {
    dashboard_title: "Dashboard",
    required_text: "Required",
    tos_title: "Terms and Conditions",
    proceed_btn_proceed: "Proceed",



    ...Login.en,
    ...Register.en,
    ...Profile.en,
    ...Commons.en,
    ...Notification.en,
    ...Dashboard.en,
    ...ChangePin.en,
    ...Referral.en,
    ...ChangeEmail.en,
    ...DrawingDetails.en,
    ...Onboarding.en,
    ...Forgotpin.en,
    ...Deactivate.en,
    ...Permission.en,
    ...Diagnostic.en,
}
