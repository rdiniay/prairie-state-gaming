export default {

    onboarding_title: "Use your location",
    onboarding_message: "Red Carpet Rewards uses location services to automatically check you into RCR locations to receive promotional entries every two minutes!  When you leave you are automatically checked-out!\n\n" +
                        "Once registered with Bluetooth, Location Services and Precise Location turned on you should never have to worry about checking in for great chances to win huge prizes at all RCR locations!",
    onboarding_message_android: "Red Carpet Rewards uses location services to automatically check you into RCR locations to receive promotional entries every two minutes!  When you leave you are automatically checked-out!\n\n" +
        "Once registered with bluetooth and location services turned on you should never have to worry about checking in for great chances to win huge prizes at all RCR locations!\n\nRed Carpet Rewards collects location data to enable features check-in and check-out even when the app is closed or not in use as long as you're currently logged in.",
    onboarding_button_skip: "SKIP",
    onboarding_button_turn_on: "Turn On",
    onboarding_permission_rationale_title: "Allow RCR app permission/s",
    onboarding_permission_rationale_message: "Allow RedCarpet Rewards app to enable location permission.",
    onboarding_button_settings: "App Settings"

}
