export default {
    drawing_title: 'Drawing Details',
    drawing_rules: 'Drawing Rules',
    drawing_directions: 'Directions',
    drawing_earned_today: (value: Number) => ((0 < value ? `You have earned ${value}` : `You have no`) + " entries during this earning session. Stay checked in to continue earning. "),
    drawing_location_checked_in: (value: Number, locationName: String) => "This location does not have a local drawing. " +
                                                                             ((0 < value ? `You have earned ${value}` : `You have no`) + `for the statewide drawing during this earning session at (${locationName}).`),
    drawing_location_checked_out:  `This location does not have a local drawing. Head into this location to earn for our statewide drawing. Details below.`,

}