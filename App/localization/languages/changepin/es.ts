export default {
    change_pin_title: 'change pin',
    change_pin_header_title_current_pin: 'Current PIN',
    change_pin_header_title_new_pin: 'New 4 digit PIN',
    change_pin_button_text: 'Change PIN',
}