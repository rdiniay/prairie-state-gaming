export default {
    dashboard_promo_item_title_1: 'This Month’s Promotion',
    dashboard_promo_na_item_title_1: 'No Month’s Promotion',

    dashboard_promo_item_title_2: 'Promotions Near Me',
    dashboard_promo_item_subtitle_2: 'What’s Playing Close to Home',
    dashboard_promo_na_item_title_2: 'No Promotions Near Me',

    dashboard_promo_item_title_3: 'My Entries',
    dashboard_promo_na_item_title_3: 'No Entries',
    dashboard_promo_item_subtitle_3: 'See How Many You Have',

    dashboard_empty_message: 'Oops! promotion is empty. Please check back later or tap retry button.',

    dashboard_no_monthly_promo: 'No monthly promotion is available at this time. Please try again',
    dashboard_monthly_promo: 'Monthly Promotion',
    dashboard_local_promo: 'Local Promo',
    dashboard_state_promo: 'State Promo',

    dashboard_verification_sent: 'Email Sent and SMS Sent',
    dashboard_verification_description_sent: `We sent a code to your email address.\nEnter the code below.`,

    dashboard_storage_permission_rationale_title: "Files/Media Request Permission",
    dashboard_storage_permission_rationale_message: "App optionally needs to save files / images to cache and reused later in the app for better performance",

    dashboard_set_location_to_always: "Set Location to Always",
    dashboard_location_always_description: "Please set location to Always for the best experience",
    dashboard_location_settings: "Settings",

}
