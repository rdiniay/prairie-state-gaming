export default {
    change_email_title: 'change email',
    change_email_heading_title: `Now we just need to make sure we can reach you. So we'll send and email to your address below with a code.`,
    change_email_label: 'Email',
    change_email_current_pin_label: 'Current Pin',
    change_email_button_text: 'Change Email',
    change_email_success: "Success",
    change_email_message: "Your Email has been changed successfully",
}