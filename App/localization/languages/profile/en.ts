export default {
    profile_title: "Profile",

    profile_information: "My Information",
    profile_firstname_placeholder: "First Name",
    profile_lastname_placeholder: "Last Name",
    profile_change_pin_placeholder: "Change your PIN",
    profile_telephone_placeholder: "Telephone",

    profile_account: "Account",
    profile_settings: "Settings",
    profile_email_promoos: "Email promos",
    profile_earning_reminders: "SMS earning reminders",
    profile_deactivate_account: "Deactivate Account",
    profile_deactivate_account_description: "Are you sure you want to deactivate your account?",

    profile_promos_referrals: "Promos & Referrals",
    profile_promo_code: "Promo code",

    profile_yes_button_text: "Yes",
    profile_no_button_text: "No",
    profile_save_button_text: "Save",
    profile_cancel_button_text: "Cancel",
}