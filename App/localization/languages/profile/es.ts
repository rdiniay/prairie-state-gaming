export default {
    profile_title: "Perfil",

    profile_information: "Mi informacion",
    profile_firstname_placeholder: "Primer nombre",
    profile_lastname_placeholder: "Apellido",
    profile_change_pin_placeholder: "Cambia tu PIN",
    profile_telephone_placeholder: "Teléfono",

    profile_account: "Cuenta",
    profile_settings: "Ajustes",
    profile_email_promoos: "Promoción por correo electrónico",
    profile_earning_reminders: "Recordatorios de obtención de SMS",
    profile_deactivate_account: "Desactivar Cuenta",
    profile_deactivate_account_description: "¿Estás segura de que quieres desactivar tu cuenta?",

    profile_promos_referrals: "Promociones y referencias",
    profile_promo_code: "Código promocional",

    profile_yes_button_text: "Sí",
    profile_no_button_text: "No",
    profile_save_button_text: "Ahorrar",
    profile_cancel_button_text: "Cancelar",
}