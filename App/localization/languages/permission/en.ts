export default {
    permission_gps_title: 'GPS',
    permission_bluetooth_title: 'Bluetooth',
    permission_notifications_title: 'Notifications',
    permission_data_access_title: 'Data Access',
    permission_open_gps_title: 'Open GPS',
    permission_open_nearby_devices_title: 'Open Nearby Devices',
    permission_open_bluetooth_title: 'Open Bluetooth',
    permission_open_notification_title: 'Open Notification',

    permission_bluetooth_requires_nearby_devices_title: 'Bluetooth requires nearby devices',
    permission_bluetooth_requires_nearby_devices_description: 'Bluetooth requires nearby devices to be enabled first.',

    permission_open_nearby_devices_description: 'Open Nearby Devices can find, connect to and determine the relative position of nearby devices',
    permission_open_bluetooth_description: 'Open Bluetooth to quicken the connection speed.',
    permission_open_notification_description: 'Open Notification to receive a check-in notification.',

    permission_incorrect_gps_description: 'Incorrect GPS Settings. Change the settings from Approximate Location to Precise Location',
    permission_incorrect_nearby_devices_description: 'Incorrect Nearby Devices setting. In Settings, locate the RCR app and Allow Nearby Devices',
    permission_incorrect_bluetooth_description: 'Incorrect Bluetooth setting. Enable Bluetooth on your device',
    permission_incorrect_data_access_description: 'Please make sure you are connected to the internet with cellular data or wi-fi.',
    permission_incorrect_notification_description: 'Incorrect Notifications setting. In Settings, locate the RCR app and turn on Notifications',

    permission_correct_gps_description: 'You have the correct setting for GPS to function in RCR app',
    permission_correct_nearby_devices_description: 'You have the correct setting for nearby devices to function in RCR app',
    permission_correct_bluetooth_description: 'You have the correct setting for bluetooth to function in RCR app',
    permission_correct_data_access_description: `You're connected to the internet`,
    permission_correct_notification_description: 'You have the correct setting for notifications to function in RCR app',

    // android location permission instructions
    permission_follow_steps_provided: 'Follow steps provided for the app to work properly',
    permission_step_1: 'Step 1',
    permission_step_2: 'Step 2',
    permission_step_3: 'Step 3',
    permission_step_4: 'Step 4',
    permission_step_select_precise: 'Select Precise',
    permission_step_select_while_using_the_app: 'Select While using the app',
    permission_step_select_allow_only_while_using_the_app: 'Select Allow only while using the app',
    permission_step_select_allow: 'Select Allow',
    permission_step_select_enable_geolocation: 'If device location dialog is shown, click `OK` to enable',
    permission_continue_if_step_is_done: 'If this step is done, click `Next` to continue',
    permission_click_turn_on_button: 'Click \'Turn On\' button to perform the steps and complete the process',

    //ios location permission instructions
    permission_step_select_precise_on: 'Select Precise On',
    permission_ios_step_select_while_using_the_app: 'Select Allow While Using App',

    // nearby devices instructions
    permission_step_under_app_settings: 'Under `App Settings`',
    permission_step_locate_find_permissions: 'Locate / Find `Permissions` item in the `App Settings` list.',
    permission_step_select_nearby_devices: 'Select Nearby devices under `Not Allowed`',
    permission_step_allow_quoted: 'Select `Allow`',

    // open settings instructions
    permission_allow_precise_loction_in_app_settings: 'Allow location in app setting to make app work properly',
    permission_click_permissions_in_app_info: 'Click Permissions in App Info',
    permission_look_for_location_in_allowed_section: 'Look for Location in Allowed/Not Allowed Section then click it',
    permission_choose_allow_all_the_time: 'Choose Allow all the time and if `Use precise location` setting is shown, enable it',
    permission_click_location_in_app_Settings: 'Click Location in App Settings',
    permission_select_always_in_allow_location_access_and_enable_precise_location: 'Select Always in Allow Location Access and Enable Precise Location',
}
