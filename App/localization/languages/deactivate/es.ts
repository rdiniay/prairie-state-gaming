export default {
    deactivate_account_description: "¿Estás segura de que quieres eliminar tu cuenta?",
    deactivate_button_cancel: "CANCEL",
    deactivate_button_deactivate: "ELIMINAR",
    deactivate_button_proceed: "PROCEED",
}
