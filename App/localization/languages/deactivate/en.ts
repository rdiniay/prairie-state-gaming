export default {

    deactivate_account_description: "Are you sure you want to delete your account?",
    deactivate_button_cancel: "CANCEL",
    deactivate_button_deactivate: "DELETE",
    deactivate_button_proceed: "PROCEED",

}
