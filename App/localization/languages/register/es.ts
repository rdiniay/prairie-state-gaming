export default {
    register_title: "Create Account",

    register_header_title: "We just need a couple of things",
    register_firstname_placeholder: "First Name*",
    register_lastname_placeholder: "Last Name*",
    register_email_placeholder: "Email*",
    register_pin_placeholder: "Pin*",
    register_referral_code_placeholder: "Referral Code",
    register_phone_number_placeholder: "Phone Number",
    register_earning_reminders_chk_text: "Earning Reminders?",
    register_register_button_text: "Register",
    register_footer_text: "By clicking above you agree to our",
    register_footer_text_link: "Terms of Service",
    register_footer_text_hyperlink: "http://www.fullscale.io"
}