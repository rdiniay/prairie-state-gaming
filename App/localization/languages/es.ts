import Login from './login'
import Register from './register'
import Profile from './profile'
import Commons from './commons'
import Notification from './notification'
import Dashboard from './dashboard'
import ChangePin from './changepin'
import Referral from './referral'
import ChangeEmail from './changeemail'
import DrawingDetails from './drawingDetails'
import Onboarding from './onboarding'
import Deactivate from './deactivate'
import Forgotpin from './forgotpin'
import Permission from "./permission";
import Diagnostic from './diagnostic'

export default {
    dashboard_title: "Tablero",
    required_text: "Required",
    tos_title: "Terms and Conditions",
    proceed_btn_proceed: "Proceed",

    ...Login.es,
    ...Register.es,
    ...Profile.es,
    ...Commons.es,
    ...Notification.es,
    ...Dashboard.es,
    ...ChangePin.es,
    ...Referral.es,
    ...ChangeEmail.es,
    ...DrawingDetails.es,
    ...Onboarding.es,
    ...Forgotpin.es,
    ...Deactivate.es,
    ...Permission.en,
    ...Diagnostic.es,
}
