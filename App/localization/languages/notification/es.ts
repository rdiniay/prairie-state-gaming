export default {
    checkin_notification_name: "Prairie State Gaming RCR-Channel",
    checkin_notification_title: "Prairie State Gaming",
    checkin_notification_message: "You have been checked in. To stay checked in, keep Location Services set to Always or While Using App. Precise Location must also be on.",
    checkout_notification_message: "You have been checked out.",
    checkout_schedule_notification_message: `You closed RCR… You’ve been checked out.\nIf you want to stay checked in with the RCR app closed change location settings to “Always"`,
    bluetooth_enable_notification_message: "Please re-enable Bluetooth to continue earning entries.",
    bluetooth_enable_notification_android_message: "Please re-enable Bluetooth to continue earning entries or visit app for better experience.",
    android_channel_id: "RCR-Channel",
    android_background_service_title: "RCR Service Manager",
    android_background_service_message: "RCR Service is running for the best experience."
}
