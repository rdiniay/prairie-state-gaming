export default {
    promos_referrals_title: 'promos & referrals',
    promos_referrals_heading_title: 'Have a referral code?',
    promos_referrals_content_title: 'Enter it below!',
    promos_referrals_button_text: 'Send',
    promos_referrals_code_label: 'Code',
    promos_referrals_already_claimed: `The referral code you enter has been already claimed or invalid.`,
    promos_referrals_earned_entries: (value: Number) => `You’ve earned ${value} entries!`,
}