
const lotties = {
    refresh: () => {
        let myAnimationSource = require('../assets/lottie/icon-refresh.json')
        myAnimationSource.layers[0].shapes[0].it[1].c.k = [1,1,1,1]
        myAnimationSource.layers[1].shapes[0].it[1].c.k = [1,1,1,1]
        myAnimationSource.layers[2].shapes[0].it[1].c.k = [1,1,1,1]
        myAnimationSource.layers[3].shapes[0].it[1].c.k = [1,1,1,1]
        return myAnimationSource
    },
    verification: require('../assets/lottie/icon-verification.json'),
    catchVerification: require('../assets/lottie/catchy-verification.json'),
    onBoardingLocation: require('../assets/lottie/onboarding-location.json'),
    deactivateAccountLocation: require('../assets/lottie/deactivate-account-location.json'),
}

export default lotties