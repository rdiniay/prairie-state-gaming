const images = {
    hamburgerMenu: require('../assets/hamburger-menu.png'),
    helpIcon: require('../assets/help-icon.png'),
    verifyStar: require('../assets/verify_star.png'),
    drawingStar: require('../assets/drawing_star.png'),
    contests: require('../assets/contests.png'),
    locations: require('../assets/locations.png'),
    profile: require('../assets/profile.png'),
    more: require('../assets/more.png'),
    lottieIncreaseAnimation: require('../assets/lottie/increase_earning_lottie_file.json'),

    onBoardingLocation: require('../assets/onboarding-location.png'),
    entries: require('../assets/entries.png'),
    promoMonthly: require('../assets/promo-monthly.png'),
    promoNearby: require('../assets/promo-nearby.png'),
    chevronSmall: require('../assets/chevron-small.png'),

    crossIcon: require('../assets/cross-icon.png'),
    checkIcon: require('../assets/check-icon.png'),
    locationPinIcon: require('../assets/location-pin-icon.png'),
    bluetoothIcon: require('../assets/bluetooth-icon.png'),
    dataAccessIcon: require('../assets/data-access-icon.png'),
    notificationIcon: require('../assets/notification-icon.png'),
    nearbyDevicesIcon: require('../assets/nearby-devices-icon.png'),

    // android location permission images
    locationPermissionA8: require('../assets/android8-location-permission.png'),
    locationPermissionA10: require('../assets/android10-location-permission.png'),
    locationPermissionA11: require('../assets/android11-location-permission.png'),
    locationPermissionA12Step1: require('../assets/android12-location-permission-step1.png'),
    locationPermissionA12Step2: require('../assets/android12-location-permission-step2.png'),

    // android geolocatio  permisison image
    geolocationPermissionStep: require('../assets/android-geolocation-step.png'),

    // android nearby devices permission images
    nearbyDevicesPermission12Step1: require('../assets/android12-nearby-permission-step1.png'),
    nearbyDevicesPermission12Step2: require('../assets/android12-nearby-permission-step2.png'),
    nearbyDevicesPermission12Step3: require('../assets/android12-nearby-permission-step3.png'),

    // ios location permission images
    locationPermissionI15Step1: require('../assets/ios15-location-permission-step1.png'),
    locationPermissionI15Step2: require('../assets/ios15-location-permission-step2.png'),
    locationPermissionI13: require('../assets/ios13-location-permission.png'),

    // android open settings
    openSettingsAStep1: require('../assets/android-open-settings-step1.png'),
    openSettingsAStep2: require('../assets/android-open-settings-step2.png'),
    openSettingsAStep3: require('../assets/android-open-settings-step3.png'),

    // ios open settings
    openSettingsIStep1: require('../assets/ios-open-settings-step1.png'),
    openSettingsIStep2: require('../assets/ios-open-settings-step2.png'),

    // ios bluetooth connection
    bluetoothConnectioniOS: require('../assets/ios-bluetooth-connection.png'),

    // ios open settings - internet
    internetConnectioniOS: require('../assets/ios-internet-data.png'),

    // android open settings - internet
    internetConnectionAndroid: require('../assets/android-internet-data.png'),
}

export default images
