enum Proximity { 
    INSIDE = 'inside',
    OUTSIDE =  'outside',
    UNKNOWN = 'unknown',
}

enum AppState { 
    NA = 'NA',
    ACTIVE = 'active',
    BACKGROUND = 'background'
}

export default { Proximity, AppState } 