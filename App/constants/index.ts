import images from './images'
import globals from './globals'
import lotties from './lotties'

export {
    images,
    globals,
    lotties,
}