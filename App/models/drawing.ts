import { Rules } from './rules'
import { Multiplier } from './multiplier'
import { Beacon } from './beacon'
import { DrawingDate } from './drawingDate'
type DrawingType = {
    id?: Number,
    locationId?: Number,
    title?: String,
    description?: String,
    weeklyPrize?: String,
    startDate?: String,
    endDate?: String,
    grandPrize?: String,
    grandPrizeUrl?: String,
    status?: String,
    multiplier?: Multiplier,
    rules?: Rules,
    beacons?: Beacon[],
    drawingDates?: DrawingDate[],
    isExpired?: () => Boolean,
}
export class Drawing implements DrawingType {
    id?: Number
    locationId?: Number
    title?: String
    description?: String
    weeklyPrize?: String
    startDate?: String
    endDate?: String
    grandPrize?: String
    grandPrizeUrl?: String
    status?: String
    multiplier?: Multiplier
    rules?: Rules
    beacons?: Beacon[]
    drawingDates?: DrawingDate[]
    isExpired?: () => Boolean

    constructor() {
        this.isExpired = () => {
            let endDateTime = Number(new Date(`${this.endDate}`)) // 2022-05-05T13:00:00.000Z 
            const currentTime = Number(Date.now())
            const elapse = (currentTime - endDateTime) / 1000
            return elapse <= 0
        }
    }
}