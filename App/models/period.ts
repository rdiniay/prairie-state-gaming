export type Period = {
    locationId?: Number,
    checkinId?: Number,
    start?: String,
    end?: String,
}