export type Multiplier = {
    id?: Number,
    amount?: Number,
    description?: String,
    startDate?: String,
    endDate?: String,
}