
export type ContactUs = {
    phone_no?: string,
    email?: string,
}
  