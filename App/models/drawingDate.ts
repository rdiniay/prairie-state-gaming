
export type DrawingDate = {
    id?: Number,
    drawingId?: Number,
    runDate?: String,
    numWinners?: Number,
    type?: String,
    complete?: Number,
}