export type Beacon = {
    uuid?: String,
    major: String,
    minor: String,
    proximity?: String,
    rssi?: Number,
    accuracy?: Number,
    distance?: Number,
}