export type LocationCheckin = {
    id?: Number,
    lastCheckInPeriodTime?: Number,
    lastCheckOutPeriodTime?: Number,
}