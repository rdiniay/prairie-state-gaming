import { Location } from './location'
import { Drawing } from './drawing'
import { Entry } from './entry'
export type Promotion = {
    id?: Number,
    location?: Location,
    drawing?: Drawing,
    entry?: Entry,
    earningTickPercentage?: Number,
    isCheckedin?: Boolean,
}