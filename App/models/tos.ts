import { Rules } from './rules'
export type TOS = {
    id?: Number,
    rules?: Rules,
}