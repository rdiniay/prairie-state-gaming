import { Coordinate } from './coordinate'
import { Beacon } from './beacon'
export type Location = {
    id?: number,
    name?: string,
    active?: number,
    address1?: string,
    address2?: string,
    city?: string,
    clientExcluded?: boolean,
    companyId?: number,
    drawingEnabled?: boolean,
    phoneNumber?: number,
    points?: Coordinate[],
    reportingId?: number,
    state?: string,
    zipCode?: string,
    completeAddress?: string,
    coordinate?: Coordinate,
    beacons?: Beacon[],
    deleted?: boolean,
    distance: number,
    isNear?: Boolean,
}