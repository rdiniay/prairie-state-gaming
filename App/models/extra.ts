import {Beacon} from "./beacon";
export type Extra = {
    event?: String,
    isBluetoothEnabled?: Boolean,
    isSilent?: Boolean,
    isForce?: Boolean,
    beacon?: Beacon,
}
