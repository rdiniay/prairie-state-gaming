import { Multiplier } from './multiplier'
export type Entry = {
    locationId?: Number,
    drawingId?: Number,
    earnedEntryCount?: Number,
    entriesBanked?: Number,
    isActiveCheckinInvalid?: Boolean,
    clientCheckinIsActiveOne?: Boolean,
    multiplier?: Multiplier,
}