import { Notification } from './notification'
export type Message = {
    to?: String,
    notification?: Notification,
}