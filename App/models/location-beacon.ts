import { Coordinate } from './coordinate'
import { Beacon } from './beacon'
export type LocationBeacon = {
    id?: number,
    clientExcluded?: boolean,
    beacon?: Beacon,
    points?: Coordinate[],
}