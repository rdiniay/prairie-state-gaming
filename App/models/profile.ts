import { Settings } from './settings'
export type Profile = {
    id: Number,
    firstname: string,
    lastname: string,
    emailAddress?: string,
    phoneNumber: string,
    flags: number,
    verified: Boolean,
    settings: Settings,
}

