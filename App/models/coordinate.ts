import { Status } from './status'
export type Coordinate = {
    latitude?: number,
    longitude?: number,
    accuracy?: number,
    altitude?: number,
    status?: Status,
}
