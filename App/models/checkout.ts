import {Beacon} from "./beacon";

export type Checkout = {
    value: true,
    data: {
        event: String,
        isBluetoothEnabled: Boolean,
        isSilent: Boolean,
        isForce: Boolean,
        beacon: Beacon,
    }
}
