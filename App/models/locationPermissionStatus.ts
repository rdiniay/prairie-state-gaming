export const LocationPermissionStatus = {
    Never: 2,
    AskNextTime: 0,
    WhileUsingTheApp: 4,
    Always: 3,
}