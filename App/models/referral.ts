export type Referral = {
    code?: string,
    award?: number,
}