export type Rules = {
    id?: Number,
    value?: string,
    code?: string,
}