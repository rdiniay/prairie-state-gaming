import { Coordinate } from './coordinate'
import { Beacon } from './beacon'
import { Drawing } from './drawing'
import {Extra} from "./extra";
export type Mobile = {
    drawing?: Drawing,
    accountId: String,
    keychain: String,
    action?: String,
    beacon?: Beacon,
    coordinate: Coordinate,
    existingCheckinId: Number,
    email?: String,
    pin?: String,
    checkinId?: Number,
    locationId?: Number,
    extra?: Extra,
}
