export type Notification = {
    title?: String,
    body?: String,    
}