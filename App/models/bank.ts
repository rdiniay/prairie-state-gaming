import { Coordinate } from './coordinate'
import { Period } from './period'
import { Entry } from './entry'
export type Bank = {
    periods: Period[],
    coordinate: Coordinate,
    checkinId: Number,
    continueSession: Boolean,
    entries?: Entry[],
    refresh?: Boolean
}