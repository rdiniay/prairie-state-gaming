export type Status = {
    accuracyAuthorization: number,
    enabled: boolean,
    gps: boolean,
    network: boolean,
    status: number
}
