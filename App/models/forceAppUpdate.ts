export type ForceAppUpdate = {
    status?: string,
    message?: string,
    url: string
}