export type Settings = {
    emailPromos: Boolean,
    smsReminders: Boolean,
    deactivateAccount: Boolean,
}