import {LocationBeacon} from "./location-beacon";

export type Checkin = {
    locationBeacon: LocationBeacon
    dateTime: Number
}
