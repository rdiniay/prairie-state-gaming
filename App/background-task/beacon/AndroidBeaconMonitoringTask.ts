import BackgroundFetch from "react-native-background-fetch";
import BackgroundGeolocation, {Config, Location, ProviderChangeEvent} from "react-native-background-geolocation";
import BackgroundService from 'react-native-background-actions';
import Beacons from '../../services/shared/react-native-beacons-manager'
import {Auth, Beacon, Coordinate, LocationPermissionStatus} from '../../models'
import { strings } from '../../localization/strings';
import BluetoothStateManager from 'react-native-bluetooth-state-manager';
import * as GLOBAL from '../../constants'
import { RESULTS } from "react-native-permissions";
import crashlytics from '@react-native-firebase/crashlytics';
import Utils from "../../services/shared/utils/Utils";
import NetInfo from "@react-native-community/netinfo";
import Geolocation from 'react-native-geolocation-service'
import {env} from "../../config";
import {FirebaseMessagingTypes} from "@react-native-firebase/messaging";
import { Alert } from "react-native";

// You can do anything in your task such as network requests, timers and so on,

// as long as it doesn't touch UI. Once your task completes (i.e. the promise is resolved),

// React Native will go into "paused" mode (unless there are other tasks running,

// or there is a foreground app).
let onBackgroundFetchListener = (taskId: any) => null

let onLocationChanged = (newLocation: Location) => null, onLocationUnsub: any = () => null

let onChangeBeaconsDidRange = () => null

let regionDidEnterSubscription = null, regionDidExitSubscription = null, regionDidDetermineStateSubscription =  null

let forceExitSubscription = () => null

let subscriptionBeaconsDidRange = null

let netInfoUnsubscribe = () => null,bToothStateUnsub:any = () => null, providerChangeUnsub: any = () => null

let MAX_HEARTBEAT = 60

const region = {
    identifier: 'RcrRegion',
    uuid: '11111111-1111-1111-1111-111111111111'
};

const conf: Config = {
    locationAuthorizationRequest: 'Any',
    backgroundPermissionRationale: {
        title: strings.location_permission_always_rationale_title,
        message: strings.location_permission_always_rationale_message,
        positiveAction: strings.location_permission_always_rationale_positive_button
    },
    disableLocationAuthorizationAlert: true,
    desiredAccuracy: BackgroundGeolocation.DESIRED_ACCURACY_NAVIGATION,
    // // Distance In Meters
    // // Default: 10.
    // // The minimum distance (measured in meters) a device must move horizontally
    // // before an update event is generated.
    distanceFilter: 10,
    disableMotionActivityUpdates: true, //(__DEV__)? false: true,
    logLevel: (__DEV__)? BackgroundGeolocation.LOG_LEVEL_VERBOSE: BackgroundGeolocation.LOG_LEVEL_OFF,
    stopOnTerminate: false,     // <-- Allow the background-service to continue tracking when user closes the app.
    startOnBoot: true,          // <-- Auto start tracking when device is powered-up.
    enableHeadless: true,
    foregroundService: true,
    allowIdenticalLocations: false,
}

const BeginMonitoringTask = async (eventEmitter: Function, auth: Auth) => {

    console.log("BeaconMonitoringTask begin....: ");

    await initBackgroundFetch(auth)

    if(__DEV__){
        Beacons.setDebug(true)
    }

    Beacons.detectCustomBeaconLayout('m:0-3=ad7700c6')
    Beacons.detectIBeacons()

    const notifData = {
        channelId: "RCR-Channel",
        notificationId: 11111,
        contentTitle: strings.auto_mode_foreground_service_title,
        contentMessage: strings.auto_mode_foreground_service_message,
        drawableIcon: "ic_launcher",
        showWhen: true,
        priority: "max",
        visibility: "",

    }


    console.log("Initiating foreground service... " , notifData)
    Beacons.startForegroundService(notifData)
    .then(result => {
        console.log("Foreground service started successfully: ", result)
        Beacons.startMonitoringForRegion(region)
        .then(async () => {
            console.log('Beacons monitoring started succesfully')
            await new Promise( async (resolve) => {

                onChangeBeaconsDidRange = async () => {
                        let heartbeat = 0
                        let fireEvent = false
                        await disableRangingSubscriptionAsApplicable()
                        await enableRangingAsApplicable()
                        subscriptionBeaconsDidRange = await Beacons.BeaconsEventEmitter.addListener('beaconsDidRange',
                            async (data: any) => {
                                const beacons:Beacon[] = data.beacons;
                                heartbeat++
                                if (beacons) {
                                    await disableRangingSubscriptionAsApplicable()
                                    fireEvent = true
                                }
                                else if (heartbeat >= MAX_HEARTBEAT) {
                                    await disableRangingSubscriptionAsApplicable()
                                    fireEvent = true
                                }

                                if (fireEvent){
                                    fireEvent = false
                                    BluetoothStateManager.getState().then( (bluetoothState) => {
                                        console.log("onChangeBeaconsDidRange: Region Did Range...and btooth state: ", bluetoothState, data.beacons)
                                        var isBluetoothEnabled = false
                                        switch (bluetoothState) {
                                            case 'PoweredOn': {
                                                isBluetoothEnabled = true;
                                                eventEmitter({ beacons, isBluetoothEnabled, event: 'beaconsDidRange' })
                                                break;
                                            }
                                            case 'PoweredOff':
                                                isBluetoothEnabled = false;
                                                eventEmitter({ beacons, isBluetoothEnabled, event: 'beaconsDidRange' })
                                                break;
                                        }
                                    })
                                }
                            })


                }

                forceExitSubscription = async () => {
                    await disableRangingSubscriptionAsApplicable()

                    BluetoothStateManager.getState().then( (bluetoothState) => {
                        console.log("forceExitSubscription: Region Did Range...and btooth state: ", bluetoothState)
                        let isBluetoothEnabled = false
                        switch (bluetoothState) {
                            case 'PoweredOn': {
                                isBluetoothEnabled = true;
                                eventEmitter({  isBluetoothEnabled, event: 'regionStateOutside' })
                                break;
                            }
                            case 'PoweredOff':
                                isBluetoothEnabled = false;
                                eventEmitter({  isBluetoothEnabled, event: 'regionStateOutside', force: true })
                                break;
                        }
                    })

                }

                onLocationChanged = async (newLocation: Location) => {
                    let heartbeat = 0
                    let fireEvent = false
                    await disableRangingSubscriptionAsApplicable()
                    await enableRangingAsApplicable()
                    subscriptionBeaconsDidRange = await Beacons.BeaconsEventEmitter.addListener('beaconsDidRange',
                        async (data: any) => {
                            const beacons:Beacon[] = data.beacons;
                            heartbeat++
                            if (beacons) {
                                await disableRangingSubscriptionAsApplicable()
                                fireEvent = true
                            }
                            else if (heartbeat >= 30) { // 30 seconds interval
                                fireEvent = true
                                // do not stop ranging to continue firing event on 60 seconds interval
                            }
                            else if (heartbeat >= MAX_HEARTBEAT) { // 60 seconds interval
                                await disableRangingSubscriptionAsApplicable()
                                fireEvent = true
                            }

                            if (fireEvent) {
                                fireEvent = false
                                BluetoothStateManager.getState().then( (bluetoothState) => {
                                    console.log("onLocationChanged: Region Did Range...and btooth state: ", bluetoothState, data.beacons);
                                    var isBluetoothEnabled = false
                                    const beaconProximity = (beacons && beacons.length > 0 ) ? GLOBAL.globals.Proximity.INSIDE : GLOBAL.globals.Proximity.OUTSIDE
                                    switch (bluetoothState) {
                                        case 'PoweredOn': {
                                            isBluetoothEnabled = true;
                                            eventEmitter({ beacons, isBluetoothEnabled, event: 'locationChanged', newLocation: newLocation, beaconProximity })
                                            break;
                                        }
                                        case 'PoweredOff':
                                            isBluetoothEnabled = false;
                                            eventEmitter({ beacons, isBluetoothEnabled, event: 'locationChanged', newLocation: newLocation, beaconProximity })
                                            break;
                                    }
                                })
                            }
                        })


                }

                if (regionDidExitSubscription != null){
                    regionDidExitSubscription?.remove()
                    regionDidExitSubscription = null
                }
                regionDidExitSubscription = await Beacons.BeaconsEventEmitter.addListener('regionDidExit',
                    async (data: any) => {
                        Utils.setBeaconProximity(GLOBAL.globals.Proximity.OUTSIDE)
                        BluetoothStateManager.getState().then( (bluetoothState) => {
                            console.log("regionDidExitSubscription: Region Did Range...and btooth state: ", bluetoothState, data)
                            let isBluetoothEnabled = false
                            const beacons:Beacon[] = data?.beacons
                            switch (bluetoothState) {
                                case 'PoweredOff':{
                                    isBluetoothEnabled = false;
                                    eventEmitter({ beacons, isBluetoothEnabled, event: 'regionDidExit' })
                                    break;
                                }
                                case 'PoweredOn': {
                                    isBluetoothEnabled = true;
                                    eventEmitter({ beacons, isBluetoothEnabled, event: 'regionDidExit' })
                                    break;
                                }
                                default:

                                    break;
                            }
                        })

                    })

                if (regionDidEnterSubscription != null){
                    regionDidEnterSubscription?.remove()
                    regionDidEnterSubscription = null
                }
                regionDidEnterSubscription = await Beacons.BeaconsEventEmitter.addListener('regionDidEnter',
                    async (dataEnter: any) => {
                        let heartbeat = 0
                        let fireEvent = false
                        Utils.setBeaconProximity(GLOBAL.globals.Proximity.INSIDE)
                        await disableRangingSubscriptionAsApplicable()
                        await enableRangingAsApplicable()
                        subscriptionBeaconsDidRange = await Beacons.BeaconsEventEmitter.addListener('beaconsDidRange',
                            async (data: any) => {
                                const beacons:Beacon[] = data.beacons;
                                heartbeat++
                                if (beacons) {
                                    await disableRangingSubscriptionAsApplicable()
                                    fireEvent = true
                                }
                                else if (heartbeat >= MAX_HEARTBEAT) {
                                    await disableRangingSubscriptionAsApplicable()
                                    fireEvent = true
                                }

                                if (fireEvent) {
                                    fireEvent = false
                                    BluetoothStateManager.getState().then( (bluetoothState) => {
                                        console.log("RegionDidEnter & DidRange...and btooth state: ", bluetoothState , dataEnter, data);
                                        var isBluetoothEnabled = false;
                                        switch (bluetoothState) {
                                            case 'PoweredOn': {
                                                isBluetoothEnabled = true;
                                                eventEmitter({ beacons, isBluetoothEnabled, event: 'beaconsDidRange' })
                                                break;
                                            }
                                            case 'PoweredOff':
                                                isBluetoothEnabled = false;
                                                eventEmitter({ beacons, isBluetoothEnabled, event: 'beaconsDidRange' })
                                                break;
                                        }
                                    })
                                }
                            })

                    })

                netInfoUnsubscribe = NetInfo.addEventListener(async (networkState) => {
                    if ((networkState.isConnected || !networkState.isConnected) || (networkState.isInternetReachable || !networkState.isInternetReachable)) {
                        let heartbeat = 0
                        let fireEvent = false
                        await disableRangingSubscriptionAsApplicable()
                        await enableRangingAsApplicable()
                        subscriptionBeaconsDidRange = await Beacons.BeaconsEventEmitter.addListener('beaconsDidRange',
                            async (data: any) => {
                            const beacons:Beacon[] = data.beacons;
                            heartbeat++
                            if (beacons) {
                                await disableRangingSubscriptionAsApplicable()
                                fireEvent = true
                            }
                            else if (heartbeat >= MAX_HEARTBEAT) {
                                await disableRangingSubscriptionAsApplicable()
                                fireEvent = true
                            }
                            
                            if (fireEvent) {
                                fireEvent = false
                                BluetoothStateManager.getState().then( (bluetoothState) => {
                                    console.log("NetInfo RegionDidEnter & DidRange...and btooth state: ", bluetoothState);
                                    var isBluetoothEnabled = false;
                                    switch (bluetoothState) {
                                        case 'PoweredOn': {
                                            isBluetoothEnabled = true;
                                            eventEmitter({ beacons, isBluetoothEnabled, event: 'beaconsDidRange' })
                                            break;
                                        }
                                        case 'PoweredOff':
                                            isBluetoothEnabled = false;
                                            eventEmitter({ beacons, isBluetoothEnabled, event: 'beaconsDidRange'})
                                            break;
                                    }
                                })
                            }
                        })
                    }
                });
                bToothStateUnsub = BluetoothStateManager.onStateChange(async (bluetoothState) => {
                    switch(bluetoothState){
                        case 'PoweredOn': {
                            let heartbeat = 0
                            let fireEvent = false
                            await disableRangingSubscriptionAsApplicable()
                            await enableRangingAsApplicable()
                            subscriptionBeaconsDidRange = await Beacons.BeaconsEventEmitter.addListener('beaconsDidRange',
                                async (data: any) => {
                                    const beacons:Beacon[] = data.beacons;
                                    heartbeat++
                                    if (beacons) {
                                        await disableRangingSubscriptionAsApplicable()
                                        fireEvent = true
                                    }
                                    else if (heartbeat >= MAX_HEARTBEAT) {
                                        await disableRangingSubscriptionAsApplicable()
                                        fireEvent = true
                                    }
                                    if (fireEvent) {
                                        fireEvent = false
                                        let isBluetoothEnabled = true;
                                        eventEmitter({ beacons, isBluetoothEnabled, event: 'beaconsDidRange' })
                                    }
                                })
                            break
                        }
                        case 'PoweredOff':
                            let isBluetoothEnabled = false;
                            let beacons = null
                            eventEmitter({ beacons, isBluetoothEnabled, event: 'regionStateOutside', force: true })
                            break
                        default: break
                    }



                }, true)




            })

        })
        .catch(error => {
            console.log(`Beacons monitoring not started, error: ${error}`);
            crashlytics().recordError(error)
        });
    })
    .catch( err => {
        console.log("Error: ", err)
    })

    Beacons.checkTransmissionSupported()
    .then( supported => {
        console.log("Transmission supported? " , supported)
    } )
    .catch( e => {
        console.log("Transmission supported crash reported.")
        crashlytics().recordError(e);
    })




};

async function disableRangingSubscriptionAsApplicable() {
    if (subscriptionBeaconsDidRange != null) {
        console.log('Stopping Ranging...')
        await Beacons.stopRangingBeaconsInRegion(region)
        subscriptionBeaconsDidRange?.remove()
        subscriptionBeaconsDidRange = null
    }
}

async function enableRangingAsApplicable(){
    if(subscriptionBeaconsDidRange == null){
        await Beacons.startRangingBeaconsInRegion(region)
    }
}


function triggerRegionState(){
    Beacons.requestStateForRegion(region);
}

function getMonitoredRegions(){
    Beacons.getMonitoredRegions()
    .then(monitoredRegions => {
        console.log("Monitored regions: ", monitoredRegions)
        if(!monitoredRegions){
            const ranging = Beacons.BeaconsEventEmitter.addListener('beaconsDidRange' ,
            async (data: any) => {

                console.log('Monitored Regions and ranging: ' , data)
                if(data.beacons && ranging != null){
                    ranging?.remove()
                }
            } )
        }
    } )
}

function getRangedRegions(){
    Beacons.getRangedRegions()
    .then(rangedRegions => {
        console.log("Ranged regions: ", rangedRegions)
    })
}

const StopRangingTask = async () => {
    const region = {
        identifier: 'RcrRegion',
        uuid: '11111111-1111-1111-1111-111111111111'
    };
    await Beacons.stopRangingBeaconsInRegion(region)
}

const StopMonitoringTask = async () => {
    const region = {
        identifier: 'RcrRegion',
        uuid: '11111111-1111-1111-1111-111111111111'
    };

    if(onLocationUnsub != null){
        onLocationUnsub.remove()
        onLocationUnsub = null
    }

    if(providerChangeUnsub != null){
        providerChangeUnsub.remove()
        providerChangeUnsub = null
    }

    if(netInfoUnsubscribe != null){
        netInfoUnsubscribe = null
    }

    if(bToothStateUnsub != null){
        bToothStateUnsub = null
    }
    
    if (regionDidEnterSubscription != null){
        regionDidEnterSubscription?.remove()
        regionDidEnterSubscription = null
    }


    if (regionDidExitSubscription != null){
        regionDidExitSubscription?.remove()
        regionDidExitSubscription = null
    }

    // if(regionDidDetermineStateSubscription != null){
    //     regionDidDetermineStateSubscription?.remove()
    //     regionDidDetermineStateSubscription = null
    // }

    if (subscriptionBeaconsDidRange != null){
        subscriptionBeaconsDidRange?.remove()
        subscriptionBeaconsDidRange = null
    }

    if (onBackgroundFetchListener != null) {
        onBackgroundFetchListener = null
    }

    if(onChangeBeaconsDidRange != null) {
        onChangeBeaconsDidRange = null
    }

    if(onLocationChanged != null){
        onLocationChanged = null
    }

    if(forceExitSubscription != null){
        forceExitSubscription = null
    }

    await Beacons.stopRangingBeaconsInRegion(region)
    await Beacons.stopMonitoringForRegion(region)
    .then(async () => {
        await BackgroundFetch.stop();
        await BackgroundGeolocation.stop()
    })
   .catch(error => {
        crashlytics().recordError(error)
        console.log(`Beacons ranging not stopped, error: ${error}`)

    });
};

const StopForegroundService = async () => {
    console.log('Stopping beacon-foreground service!!')
    Beacons.stopForegroundService().then( () => {
        console.log('Foreground service stopped successfully.')
    })
    .catch(error => {
        crashlytics().recordError(error)
        console.log(`Foreground service not stopped, error: ${error}`)
    })
}


const initBackgroundFetch = async (auth: Auth) => {
    // Step 1:  Configure BackgroundFetch as usual.
    let status = await BackgroundFetch.configure({
        minimumFetchInterval: 15,
        stopOnTerminate: false,
        requiredNetworkType: 1,
        requiresBatteryNotLow: false,
        requiresCharging: false,
        requiresDeviceIdle: false,
        requiresStorageNotLow: false,
        startOnBoot: true,
    }, async (taskId) => {  // <-- Event callback
        // This is the fetch-event callback.
        console.log("[BackgroundFetch] taskId: ", taskId);

        await onBackgroundFetchListener(taskId)

        BackgroundFetch.finish(taskId);
    }, async (taskId) => {  // <-- Task timeout callback
        // This task has exceeded its allowed running-time.
        // You must stop what you're doing and immediately .finish(taskId)
        console.log("[BackgroundFetch] taskId timeOut!: ", taskId);
        BackgroundFetch.finish(taskId);
    });

    console.log('status-background-fetch: ', status)
    // Step 2:  Schedule a custom "oneshot" task "com.foo.customtask" to execute 5000ms from now.
    await BackgroundFetch.scheduleTask({
        taskId: "com.redcarpetrewards.penn",
        forceAlarmManager: true,
        stopOnTerminate: false,
        delay: __DEV__ ? 2 * 60 * 1000 : 6 * 60 * 1000,  // <-- milliseconds, // 12 minutes to execute bg fetch or 6 earnings
        periodic: true,
    });

    /////////////////////////////////////////////////////////

    // This handler fires whenever bgGeo receives a location update.
    onLocationUnsub = BackgroundGeolocation.onLocation(onLocation, onError);

    // This handler fires when movement states changes (stationary->moving; moving->stationary)
    // if (__DEV__){
    //     BackgroundGeolocation.onMotionChange(onMotionChange);
    // }

    // // This event fires when a change in motion activity is detected
    // if (__DEV__){
    //     BackgroundGeolocation.onActivityChange(onActivityChange);
    // }

    // This event fires when the user toggles location-services authorization
    providerChangeUnsub = BackgroundGeolocation.onProviderChange(async (event: ProviderChangeEvent) => {

        console.log("Monitoring: onProviderChange: ", event)

        if(__DEV__ || env.IS_DEBUG){
            const deviceInfo = await BackgroundGeolocation.getDeviceInfo()
            const deviceUser = deviceInfo.manufacturer + '-' + auth?.authData
           
            try {
                conf.transistorAuthorizationToken = await BackgroundGeolocation.findOrCreateTransistorAuthorizationToken(env.TRACKER_ORG, deviceUser , env.TRACKER_HOST)
            } catch (e) {
                console.log('transistorAuthorizationToken: ' + JSON.stringify(e))
            }
        }
        // 1. Always - keep as is or reset configuration to Always
        // 2. While in use - Tentative for Android as it seems like it falls to Always
        // 3. Ask once - Should checkout if earning
        // 4. Never - Should checkout if earning

        switch(event?.status){
            case LocationPermissionStatus.Always:
                conf.locationAuthorizationRequest = 'Always'
                await BackgroundGeolocation.reset(conf)
                break;
            case LocationPermissionStatus.WhileUsingTheApp:
                conf.locationAuthorizationRequest = 'WhenInUse'
                BackgroundGeolocation.reset(conf)
                    .then( async result => {
                        await BackgroundGeolocation.changePace(true)
                    })

                break;
            default:
                console.log('Force exit !!!')
                // END CHECKOUT OR STOP MONITORING / EARNING
                if(forceExitSubscription != null){
                    forceExitSubscription()
                }
                break;

        }
    });


    if(__DEV__ || env.IS_DEBUG){
        const deviceInfo = await BackgroundGeolocation.getDeviceInfo()
        const deviceUser = deviceInfo.manufacturer + '-' + auth?.authData
        try {
            conf.transistorAuthorizationToken = await BackgroundGeolocation.findOrCreateTransistorAuthorizationToken(env.TRACKER_ORG, deviceUser , env.TRACKER_HOST)
        } catch (e) {
            console.log('transistorAuthorizationToken: ' + JSON.stringify(e))
        }
    }
    ////
    // 2.  Execute #ready method (required)
    //
    await BackgroundGeolocation.ready(conf, (state) => {
        console.log("- BackgroundGeolocation is configured and ready: ", state.enabled);

        if (!state.enabled) {

            // Attempt to get three(3) samples with maximumAge of five(5) seconds current coordinates / location
            const options = {
                timeout: 60,          // Default 30 second timeout to fetch location
                maximumAge: 5000,     // Accept the last-known-location if not older than 5000 ms.
                desiredAccuracy: 10,  // Try to fetch a location with an accuracy of `10` meters.
                // samples: 3, // Default is 3 - How many location samples to attempt.
            }
            BackgroundGeolocation.getCurrentPosition(options)
            ////
            // 3. Start tracking!
            //
            BackgroundGeolocation.start(function() {
            console.log("- Start success");
            });
        }
    });
}

const onLocation = (location: any) => {
    console.log("Monitoring: Location: ", location);
    console.log("Monitoring: Location Coordinate: ", location.coords);
    // if development mode then proceed otherwise proceed only when location is not mock
    if(__DEV__&& onLocationChanged != null){
        onLocationChanged(location)
    }else if(!location.mock && onLocationChanged != null){
        console.log("Monitoring: Location is not mocked then we proceed...")
        onLocationChanged(location)
    }

}
const onError = (error: any) => {
    console.log("Monitoring: onError: ", error);
 }

 // TODO : Remove unused code
const onProviderChange  = async (event: ProviderChangeEvent) => {
    console.log("Monitoring: onProviderChange: ", event);
    // 1. Always - keep as is or reset configuration to Always
    // 2. While in use - Tentative for Android as it seems like it falls to Always
    // 3. Ask once - Should checkout if earning
    // 4. Never - Should checkout if earning
    switch(event.status){
        case BackgroundGeolocation.AUTHORIZATION_STATUS_ALWAYS:
            await BackgroundGeolocation.reset(conf)
            break;
        case BackgroundGeolocation.AUTHORIZATION_STATUS_WHEN_IN_USE:

            break;
        default:
            break;

    }
    // if(onChangeBeaconsDidRange != null)
    //     onChangeBeaconsDidRange()
    // BackgroundGeolocation.getProviderState(data => {
    //     const permissionState = data?.status
    //     if (permissionState === LocationPermissionStatus.WhileUsingTheApp){
    //         BackgroundGeolocation.changePace(true)
    //         console.log("We are now changing pace... since Location permission is set to only while using the app")
    //     }
    //
    //     switch(permissionState){
    //         case LocationPermissionStatus.WhileUsingTheApp:
    //             BackgroundGeolocation.changePace(true)
    //             break;
    //
    //     }
    // })

    // TODO : Apply providerChange event changed
    // 1. Always - keep as is or reset configuration to Always
    // 2. While in use - Tentative for Android as it seems like it falls to Always
    // 3. Ask once - Should checkout if earning
    // 4. Never - Should checkout if earning
    // providerChangeUnsub = BackgroundGeolocation.onProviderChange(async (event) => {
    //     console.log("[onProviderChange: ", event);
    //
    //     switch(event.status) {
    //         case BackgroundGeolocation.AUTHORIZATION_STATUS_ALWAYS:
    //             // Android & iOS
    //             if(__DEV__ || env.IS_DEBUG){
    //                 const deviceInfo = await BackgroundGeolocation.getDeviceInfo()
    //                 const deviceUser = deviceInfo.manufacturer + '-' + auth?.authData
    //                 conf.transistorAuthorizationToken = await BackgroundGeolocation.findOrCreateTransistorAuthorizationToken(env.TRACKER_ORG, deviceUser , env.TRACKER_HOST)
    //             }
    //             await BackgroundGeolocation.reset(conf)
    //             break;
    //         case BackgroundGeolocation.AUTHORIZATION_STATUS_WHEN_IN_USE:
    //             // iOS only
    //             if(__DEV__ || env.IS_DEBUG){
    //                 const deviceInfo = await BackgroundGeolocation.getDeviceInfo()
    //                 const deviceUser = deviceInfo.manufacturer + '-' + auth?.authData
    //                 conf.transistorAuthorizationToken = await BackgroundGeolocation.findOrCreateTransistorAuthorizationToken(env.TRACKER_ORG, deviceUser , env.TRACKER_HOST)
    //                 conf.locationAuthorizationRequest = 'WhenInUse'
    //             }
    //             await BackgroundGeolocation.reset(conf)
    //             await BackgroundGeolocation.changePace(true)
    //
    //             break;
    //
    //         default:
    //             BluetoothStateManager.getState().then( (bluetoothState) => {
    //                 console.log("regionDidExitSubscription: Region Did Range...and btooth state: ", bluetoothState)
    //                 let isBluetoothEnabled = false
    //                 const beacons:Beacon[] = []
    //                 switch (bluetoothState) {
    //                     case 'PoweredOff':{
    //                         isBluetoothEnabled = false;
    //                         eventEmitter({ beacons, isBluetoothEnabled, event: 'regionDidExit' })
    //                         break;
    //                     }
    //                     case 'PoweredOn': {
    //                         isBluetoothEnabled = true;
    //                         eventEmitter({ beacons, isBluetoothEnabled, event: 'regionDidExit' })
    //                         break;
    //                     }
    //                     default:
    //
    //                         break;
    //                 }
    //             })
    //             break;
    //     }
    // })
}

const onBackgroundFetch = async (eventEmitter: Function) => {
    onBackgroundFetchListener = async () => {
        return new Promise((resolve, reject) => {
            eventEmitter()
            setTimeout(() => {
                // ONCE RESOLVE
                // BackgroundFetch.finish(taskId); will trigger
                Promise.resolve()
            }, 5000);
        })
    }
}



export default {
    Config: conf,
    BeginMonitoringTask,
    StopMonitoringTask,
    StopRangingTask,
    StopForegroundService,
    onBackgroundFetch,
}
