import BackgroundFetch from "react-native-background-fetch";
import BackgroundGeolocation, {Config} from "react-native-background-geolocation";
import BackgroundService from 'react-native-background-actions';
import Beacons from '../../services/shared/react-native-beacons-manager'
import {Auth, Beacon, Coordinate, LocationPermissionStatus} from '../../models'
import { strings } from '../../localization/strings';
import locationService from "../../services/shared/location-service";
import BluetoothStateManager from 'react-native-bluetooth-state-manager';
import {Location} from 'react-native-background-geolocation'
import Utils from "../../services/shared/utils/Utils";
import * as GLOBAL from '../../constants'
import { RESULTS } from "react-native-permissions";
import crashlytics from '@react-native-firebase/crashlytics';
import { DeviceEventEmitter } from "react-native";
import NetInfo from "@react-native-community/netinfo";
import Geolocation from 'react-native-geolocation-service'
import PushNotification from 'react-native-push-notification';
import {env} from "../../config";

// You can do anything in your task such as network requests, timers and so on,

// as long as it doesn't touch UI. Once your task completes (i.e. the promise is resolved),

// React Native will go into "paused" mode (unless there are other tasks running,

// or there is a foreground app).
let onLocationProviderChangedListener = (provider: any) => null

let onBackgroundFetchListener = (taskId: any) => null

let onChangeBeaconsDidRange = () => null

let onLocationChanged = (newLocation: Location) => null

let regionDidEnterSubscription = null, regionDidExitSubscription = null, regionDidDetermineStateSubscription =  null

let netInfoUnsubscribe = null,bToothStateUnsub = null, providerChangeUnsub = null, onLocationUnsub = null

let MAX_HEARTBEAT = 60

const region = {
    identifier: 'RcrRegion',
    uuid: '11111111-1111-1111-1111-111111111111'
};


const conf: Config = {
    locationAuthorizationRequest: 'Any',
    desiredAccuracy: BackgroundGeolocation.DESIRED_ACCURACY_NAVIGATION,
    // Distance In Meters
    // Default: 10.
    // The minimum distance (measured in meters) a device must move horizontally
    // before an update event is generated.
    disableLocationAuthorizationAlert: true,
    disableMotionActivityUpdates: true, //(__DEV__)? false: true,
    logLevel: (__DEV__)? BackgroundGeolocation.LOG_LEVEL_VERBOSE: BackgroundGeolocation.LOG_LEVEL_OFF,
    stopOnTerminate: false,     // <-- Allow the background-service to continue tracking when user closes the app.
    startOnBoot: true,          // <-- Auto start tracking when device is powered-up.
    allowIdenticalLocations: false,
}

const BeginMonitoringTask = async (eventEmitter: Function, auth: Auth) => {



    initBackgroundFetch(auth)

    Beacons.allowsBackgroundLocationUpdates(true)

    // Request for authorization while the app is open
    Beacons.requestAlwaysAuthorization();

    // This call is needed for monitoring beacons and gets the initial position of the device.
    // Beacons.startUpdatingLocation()

    // TODO: Lets find alternative to execute timeout when there is no beacon ranged e.g, timeout scenario
    // Beacons.shouldDropEmptyRanges(true)

    // Range for beacons inside the region
    // Beacons.startRangingBeaconsInRegion(region)
    Beacons.startMonitoringForRegion(region)
    .then(() => {
        /*
            IMPORTANT:
            On ios, there's about a 150 second delay after the device restarts and the app is launched for CoreLocation
            services to become available. If the region status is requested before that then a monitoring failed error will
            be received (once CoreLocation becomes available it will send the status down the pipe though).
         */
        console.log('Beacons monitoring started succesfully')

        triggerRegionState()
    })
    .catch(error => {
        crashlytics().recordError(error)
        console.log(`Beacons monitoring not started, error: ${error}`)

    });

    // Listen for beacon changes
    // For iOS this will be ignored
    const options = {
        taskName: strings.checkin_notification_name,
        taskTitle: '',
        taskDesc: '',
        taskIcon: {
            name: 'ic_launcher',
            type: 'mipmap',
        },
        // color: '#ff00ff',
        // linkingURI: 'yourSchemeHere://chat/jane', // See Deep Linking for more info
        // parameters: {
        //     delay: 1000,
        // },
    };
    /*
      -so we'll ask for always permission
      -then we'll start monitoring the region
      -if we enter the region then we start ranging for the beacon
      -if we find (range) the beacon then we'll checkin and stop ranging for the beacon
      -if we exit the region then we debounce by a second or so and then checkout if status has not changed and stop ranging for beacon

      -if ranging is not available then we'll skip the ranging and just check them in solely on entering the region
      -handle possible errors
      */


    await BackgroundService.start(async () => {
        await new Promise(async (resolve) => {
            let onProviderStateChanged = async (event: any) => {
                console.log("[onProviderChange: ", event);
                if (onLocationProviderChangedListener != null) {
                    onLocationProviderChangedListener(event)
                }
            }
            let subscriptionBeaconsDidRange = null

            onChangeBeaconsDidRange = async () => {
                    let heartbeat = 0
                    let fireEvent = false
                    Beacons.stopRangingBeaconsInRegion(region)
                    Beacons.startRangingBeaconsInRegion(region)
                    if (subscriptionBeaconsDidRange != null){
                        subscriptionBeaconsDidRange.remove()
                        subscriptionBeaconsDidRange = null
                    }
                    subscriptionBeaconsDidRange = await Beacons.BeaconsEventEmitter.addListener('beaconsDidRange',
                    async (data: any) => {
                        BackgroundGeolocation.getProviderState(onProviderStateChanged)
                        const beacons:Beacon[] = data.beacons;
                        heartbeat++
                        if (beacons) {
                            subscriptionBeaconsDidRange?.remove()
                            subscriptionBeaconsDidRange = null
                            Beacons.stopRangingBeaconsInRegion(region)
                            fireEvent = true
                        }
                        else if (heartbeat >= MAX_HEARTBEAT) {
                            subscriptionBeaconsDidRange?.remove()
                            subscriptionBeaconsDidRange = null
                            Beacons.stopRangingBeaconsInRegion(region)
                            fireEvent = true
                        }

                        if (fireEvent) {
                            fireEvent = false
                            BluetoothStateManager.getState().then( (bluetoothState) => {
                                console.log("Region Did Range...and btooth state: ", bluetoothState);
                                var isBluetoothEnabled = false;
                                switch (bluetoothState) {
                                    case 'PoweredOn': {
                                        isBluetoothEnabled = true;
                                        eventEmitter({ beacons, isBluetoothEnabled, event: 'beaconsDidRange' })
                                        break;
                                    }
                                    case 'PoweredOff':
                                        isBluetoothEnabled = false;
                                        eventEmitter({ beacons, isBluetoothEnabled, event: 'beaconsDidRange' })
                                        break;
                                    default:break;
                                }
                            })
                        }
                    })
            }

            onLocationChanged = async (newLocation: Location) => {
                let heartbeat = 0
                let fireEvent = false
                if (subscriptionBeaconsDidRange != null){
                    subscriptionBeaconsDidRange.remove()
                    subscriptionBeaconsDidRange = null
                }
                Beacons.stopRangingBeaconsInRegion(region)
                Beacons.startRangingBeaconsInRegion(region)
                subscriptionBeaconsDidRange = await Beacons.BeaconsEventEmitter.addListener('beaconsDidRange',
                    async (data: any) => {
                        BackgroundGeolocation.getProviderState(onProviderStateChanged)
                        const beacons:Beacon[] = data.beacons;
                        heartbeat++
                        if (beacons) {
                            subscriptionBeaconsDidRange?.remove()
                            subscriptionBeaconsDidRange = null
                            Beacons.stopRangingBeaconsInRegion(region)
                            fireEvent = true
                        }
                        else if (heartbeat >= 30) { // 30 seconds interval
                            fireEvent = true
                            // do not stop ranging to continue firing event on 60 seconds interval
                        }
                        else if (heartbeat >= MAX_HEARTBEAT) { // 60 seconds interval
                            subscriptionBeaconsDidRange?.remove()
                            subscriptionBeaconsDidRange = null
                            Beacons.stopRangingBeaconsInRegion(region)
                            fireEvent = true
                        }

                        if (fireEvent) {
                            fireEvent = false
                            BluetoothStateManager.getState().then( (bluetoothState) => {
                                console.log("onLocationChanged: Region Did Range...and btooth state: ", bluetoothState, data.beacons);
                                var isBluetoothEnabled = false
                                const beaconProximity = (beacons && beacons.length > 0 ) ? GLOBAL.globals.Proximity.INSIDE : GLOBAL.globals.Proximity.OUTSIDE
                                switch (bluetoothState) {
                                    case 'PoweredOn': {
                                        isBluetoothEnabled = true;
                                        eventEmitter({ beacons, isBluetoothEnabled, event: 'locationChanged', newLocation: newLocation, beaconProximity })
                                        break;
                                    }
                                    case 'PoweredOff':
                                        isBluetoothEnabled = false;
                                        eventEmitter({ beacons, isBluetoothEnabled, event: 'locationChanged', newLocation: newLocation, beaconProximity })
                                        break;
                                }
                            })
                        }
                    })
            }

            if (regionDidExitSubscription != null){
                regionDidExitSubscription.remove()
                regionDidExitSubscription = null
            }
            regionDidExitSubscription = await Beacons.BeaconsEventEmitter.addListener('regionDidExit',
                async (data: any) => {
                    BackgroundGeolocation.getProviderState(onProviderStateChanged)

                    Utils.setBeaconProximity(GLOBAL.globals.Proximity.OUTSIDE)
                    BluetoothStateManager.getState().then( (bluetoothState) => {
                        console.log("regionDidExitSubscription: Region Did Exit...and btooth state: ", bluetoothState, data?.beacons);
                        var isBluetoothEnabled = false;
                        const beacons:Beacon[] = data && data?.beacons ? data?.beacons : [];
                        switch (bluetoothState) {
                            case 'PoweredOff':{
                                isBluetoothEnabled = false;
                                eventEmitter({ beacons, isBluetoothEnabled, event: 'regionDidExit' })
                                break;
                            }
                            case 'PoweredOn': {
                                isBluetoothEnabled = true;
                                eventEmitter({ beacons, isBluetoothEnabled, event: 'regionDidExit' })
                                break;
                            }
                            default:

                                break;
                        }
                    })
                })

            if (regionDidEnterSubscription != null){
                regionDidEnterSubscription.remove()
                regionDidEnterSubscription = null
            }

            regionDidEnterSubscription = await Beacons.BeaconsEventEmitter.addListener('regionDidEnter',
                async (dataEnter: any) => {
                    let heartbeat = 0
                    let fireEvent = false
                    BackgroundGeolocation.getProviderState(onProviderStateChanged)
                    
                    Utils.setBeaconProximity(GLOBAL.globals.Proximity.INSIDE)
                    Beacons.stopRangingBeaconsInRegion(region)
                    Beacons.startRangingBeaconsInRegion(region)

                    if (subscriptionBeaconsDidRange != null){
                        subscriptionBeaconsDidRange.remove()
                        subscriptionBeaconsDidRange = null
                    }
                    subscriptionBeaconsDidRange = await Beacons.BeaconsEventEmitter.addListener('beaconsDidRange',
                    async (data: any) => {
                        BackgroundGeolocation.getProviderState(onProviderStateChanged)
                        const beacons:Beacon[] = data.beacons;
                        heartbeat++

                        if (beacons) {
                            subscriptionBeaconsDidRange?.remove()
                            subscriptionBeaconsDidRange = null
                            Beacons.stopRangingBeaconsInRegion(region)
                            fireEvent = true
                        }
                        else if (heartbeat >= MAX_HEARTBEAT) {
                            subscriptionBeaconsDidRange?.remove()
                            subscriptionBeaconsDidRange = null
                            Beacons.stopRangingBeaconsInRegion(region)
                            fireEvent = true
                        }

                        if (fireEvent) {
                            fireEvent = false
                            BluetoothStateManager.getState().then( (bluetoothState) => {
                                console.log("RegionDidEnter & DidRange...and btooth state: ", bluetoothState);
                                var isBluetoothEnabled = false;
                                switch (bluetoothState) {
                                    case 'PoweredOn': {
                                        isBluetoothEnabled = true;
                                        eventEmitter({ beacons, isBluetoothEnabled, event: 'beaconsDidRange' })
                                        break;
                                    }
                                    case 'PoweredOff':
                                        isBluetoothEnabled = false;
                                        eventEmitter({ beacons, isBluetoothEnabled, event: 'beaconsDidRange' })
                                        break;
                                    default:break;
                                }
                            })
                        }
                    })

                })


            if (regionDidDetermineStateSubscription != null){
                regionDidDetermineStateSubscription.remove()
                regionDidDetermineStateSubscription = null
            }

            regionDidDetermineStateSubscription =await Beacons.BeaconsEventEmitter.addListener('didDetermineState',
                async (regionState: any) => {
                    console.log("RegionState: ", regionState);
                    new Promise( (resolve, reject) => {
                        if(regionState.state === 'inside' || regionState.state === 'outside'){
                            resolve(regionState.state);
                        }
                        else{
                            reject(regionState.state);
                        }
                        // Do nothing if undefined

                    })
                    .then( async (state) => {
                        // TODO: To execute function only that we're inside and well start ranging once to determine bluetooth details

                        if (state === 'inside'){
                            Utils.setBeaconProximity(GLOBAL.globals.Proximity.INSIDE)
                            console.log("Region state is inside -- performing listeners to range beacon and subscription object: ", subscriptionBeaconsDidRange);
                            let heartbeat = 0
                            let fireEvent = false
                            Beacons.stopRangingBeaconsInRegion(region)
                            Beacons.startRangingBeaconsInRegion(region)

                            // If we encountered null ranging subscription then we create one and remove when it returned one beacon range
                            if(subscriptionBeaconsDidRange == null){
                                subscriptionBeaconsDidRange = await Beacons.BeaconsEventEmitter.addListener('beaconsDidRange',
                                async (data: any) => {
                                    const beacons:Beacon[] = 0 < data.beacons.length ? data.beacons : null;
                                    heartbeat++

                                    if (beacons) {
                                        subscriptionBeaconsDidRange?.remove()
                                        subscriptionBeaconsDidRange = null
                                        Beacons.stopRangingBeaconsInRegion(region)
                                        fireEvent = true
                                    }
                                    else if (heartbeat >= MAX_HEARTBEAT) {
                                        subscriptionBeaconsDidRange?.remove()
                                        subscriptionBeaconsDidRange = null
                                        Beacons.stopRangingBeaconsInRegion(region)
                                        fireEvent = true
                                    }
                                    
                                    if (fireEvent) {
                                        fireEvent = false
                                        BluetoothStateManager.getState().then( (bluetoothState) => {
                                            console.log("Region Did Range...and btooth state: ", bluetoothState);
                                            var isBluetoothEnabled = false;
                                            
                                            switch (bluetoothState) {
                                                case 'PoweredOn': {
                                                    isBluetoothEnabled = true;
                                                    eventEmitter({ beacons, isBluetoothEnabled, event: 'beaconsDidRange' })
                                                    break;
                                                }
                                                case 'PoweredOff':
                                                    isBluetoothEnabled = false;
                                                    eventEmitter({ beacons, isBluetoothEnabled, event: 'beaconsDidRange' })
                                                    break;
    
                                                default:break;
                                            }
                                        })
                                    }
                                })
                            }

                        }else{
                            Utils.setBeaconProximity(GLOBAL.globals.Proximity.OUTSIDE)
                            console.log("Region state is outside -- performing removal of listeners");
                            // TODO: To execute function only that we're outside and provide alternative way to determine if we can check out
                            Beacons.stopRangingBeaconsInRegion(region)
                            BluetoothStateManager.getState().then( (bluetoothState) => {
                                var isBluetoothEnabled = false;
                                let beacons = null;
                                switch (bluetoothState) {
                                    case 'PoweredOn': {
                                        isBluetoothEnabled = true;
                                        eventEmitter({ beacons, isBluetoothEnabled, event: 'regionStateOutside' })
                                        break;
                                    }
                                    case 'PoweredOff':
                                        isBluetoothEnabled = false;
                                        eventEmitter({ beacons, isBluetoothEnabled, event: 'regionStateOutside' })
                                        break;

                                    default:break;
                                }
                            })
                        }


                    } )
                    .catch( (rejectErr) => {
                        crashlytics().recordError(rejectErr)
                        Utils.setBeaconProximity(GLOBAL.globals.Proximity.UNKNOWN)
                        console.log("Reject determinedState " , rejectErr)
                        // Determined state is possibly rejected or other then we send a checkedout since we don't know the status of the beacon
                        Beacons.stopRangingBeaconsInRegion(region)
                            BluetoothStateManager.getState().then( (bluetoothState) => {
                                var isBluetoothEnabled = false;
                                let beacons = null;
                                switch (bluetoothState) {
                                    case 'PoweredOn': {
                                        isBluetoothEnabled = true;
                                        eventEmitter({ beacons, isBluetoothEnabled, event: 'regionStateRejectedUndefined' })
                                        break;
                                    }
                                    case 'PoweredOff':
                                        isBluetoothEnabled = false;
                                        eventEmitter({ beacons, isBluetoothEnabled, event: 'regionStateRejectedUndefined' })
                                        break;

                                    default:break;
                                }
                            })
                    })
                }
            )

            netInfoUnsubscribe =  NetInfo.addEventListener(async (networkState) => {
                if ((networkState.isConnected || !networkState.isConnected) || (networkState.isInternetReachable || !networkState.isInternetReachable)) {
                    let heartbeat = 0
                    let fireEvent = false
                    BackgroundGeolocation.getProviderState(onProviderStateChanged)

                    Beacons.stopRangingBeaconsInRegion(region)
                    Beacons.startRangingBeaconsInRegion(region)
                    if (subscriptionBeaconsDidRange != null){
                        subscriptionBeaconsDidRange.remove()
                        subscriptionBeaconsDidRange = null
                    }
                    subscriptionBeaconsDidRange = await Beacons.BeaconsEventEmitter.addListener('beaconsDidRange',
                        async (data: any) => {
                        const beacons:Beacon[] = data.beacons;
                        heartbeat++

                        if (beacons) {
                            subscriptionBeaconsDidRange?.remove()
                            subscriptionBeaconsDidRange = null
                            Beacons.stopRangingBeaconsInRegion(region)
                            fireEvent = true
                        }
                        else if (heartbeat >= MAX_HEARTBEAT) {
                            subscriptionBeaconsDidRange?.remove()
                            subscriptionBeaconsDidRange = null
                            Beacons.stopRangingBeaconsInRegion(region)
                            fireEvent = true
                        }

                        if (fireEvent) {
                            fireEvent = false
                            BluetoothStateManager.getState().then( (bluetoothState) => {
                                console.log("RegionDidEnter & DidRange...and btooth state: ", bluetoothState);
                                var isBluetoothEnabled = false;
                                switch (bluetoothState) {
                                    case 'PoweredOn': {
                                        isBluetoothEnabled = true;
                                        eventEmitter({ beacons, isBluetoothEnabled, event: 'beaconsDidRange' })
                                        break;
                                    }
                                    case 'PoweredOff':
                                        isBluetoothEnabled = false;
                                        eventEmitter({ beacons, isBluetoothEnabled, event: 'beaconsDidRange' })
                                        break;
                                }
                            })
                        }
                    })
                }
            });

            bToothStateUnsub = BluetoothStateManager.onStateChange((bluetoothState) => {
                switch(bluetoothState){
                    case 'PoweredOn': {
                        let heartbeat = 0
                        let fireEvent = false
                        Beacons.stopRangingBeaconsInRegion(region)
                        Beacons.startRangingBeaconsInRegion(region)

                        if (subscriptionBeaconsDidRange != null){
                            subscriptionBeaconsDidRange.remove()
                            subscriptionBeaconsDidRange = null
                        }
                        subscriptionBeaconsDidRange = Beacons.BeaconsEventEmitter.addListener('beaconsDidRange',
                            async (data: any) => {
                                const beacons:Beacon[] = data.beacons;
                                heartbeat++
                                if (beacons) {
                                    subscriptionBeaconsDidRange?.remove()
                                    subscriptionBeaconsDidRange = null
                                    Beacons.stopRangingBeaconsInRegion(region)
                                    fireEvent = true
                                }
                                else if (heartbeat >= MAX_HEARTBEAT) {
                                    subscriptionBeaconsDidRange?.remove()
                                    subscriptionBeaconsDidRange = null
                                    Beacons.stopRangingBeaconsInRegion(region)
                                    fireEvent = true
                                }

                                if (fireEvent){
                                    fireEvent = false
                                    let isBluetoothEnabled = true;
                                    eventEmitter({ beacons, isBluetoothEnabled, event: 'beaconsDidRange' })
                                }
                            })
                        break
                    }
                    case 'PoweredOff':
                        let isBluetoothEnabled = false;
                        let beacons = null
                        eventEmitter({ beacons, isBluetoothEnabled, event: 'regionStateOutside', force: true })
                        break
                    default: break
                }
            }, true)

            // TODO : Apply providerChange event changed
            // 1. Always - keep as is or reset configuration to Always
            // 2. While in use - Tentative for Android as it seems like it falls to Always
            // 3. Ask once - Should checkout if earning
            // 4. Never - Should checkout if earning
            providerChangeUnsub = BackgroundGeolocation.onProviderChange(onProviderStateChanged)
        });
    }, options);
};

function triggerRegionState(){
    Beacons.requestStateForRegion(region);
}


const StopRangingTask = async () => {
    const region = {
        identifier: 'RcrRegion',
        uuid: '11111111-1111-1111-1111-111111111111'
    };
    await Beacons.stopRangingBeaconsInRegion(region)
}

const StopMonitoringTask = async () => {
    const region = {
        identifier: 'RcrRegion',
        uuid: '11111111-1111-1111-1111-111111111111'
    };

    if(providerChangeUnsub != null){
        providerChangeUnsub()
        providerChangeUnsub?.remove()
    }

    if (onLocationUnsub != null) {
        onLocationUnsub()
        onLocationUnsub?.remove()
    }

    if(netInfoUnsubscribe != null){
        netInfoUnsubscribe()
        netInfoUnsubscribe?.remove()
    }

    if(bToothStateUnsub != null){
        bToothStateUnsub()
        bToothStateUnsub?.remove()
    }

    if (regionDidDetermineStateSubscription != null){
        regionDidDetermineStateSubscription.remove()
        regionDidDetermineStateSubscription = null
    }

    if (regionDidEnterSubscription != null){
        regionDidEnterSubscription.remove()
        regionDidEnterSubscription = null
    }


    if (regionDidExitSubscription != null){
        regionDidExitSubscription.remove()
        regionDidExitSubscription = null
    }

    if (onBackgroundFetchListener != null) {
        onBackgroundFetchListener = null
    }

    if (onChangeBeaconsDidRange != null) {
        onChangeBeaconsDidRange = null
    }

    if (onLocationProviderChangedListener != null) {
        onLocationProviderChangedListener = null
    }

    // This method should be called when you don’t need to receive location-based information and want to save battery power.
    // Beacons.stopUpdatingLocation()
    await BackgroundGeolocation.stop()
    await Beacons.stopRangingBeaconsInRegion(region)
    await Beacons.stopMonitoringForRegion(region)
    .then(async () => {
        // await BackgroundService.updateNotification({taskDesc: 'New ExampleTask description'}); // Only Android, iOS will ignore this call
        // iOS will also run everything here in the background until .stop() is called
        await BackgroundService.stop();
        await BackgroundFetch.stop();
    })
   .catch(error => {
        crashlytics().recordError(error)
        console.log(`Beacons ranging not stopped, error: ${error}`)

    });
};

const initBackgroundFetch = async (auth: Auth) => {
    // Step 1:  Configure BackgroundFetch as usual.
    let status = await BackgroundFetch.configure({
        minimumFetchInterval: 15,
    }, async (taskId) => {  // <-- Event callback
        // This is the fetch-event callback.
        console.log("[BackgroundFetch] taskId: ", taskId);

        await onBackgroundFetchListener(taskId)

        BackgroundFetch.finish(taskId);
    }, async (taskId) => {  // <-- Task timeout callback
        // This task has exceeded its allowed running-time.
        // You must stop what you're doing and immediately .finish(taskId)
        await onBackgroundFetchListener(taskId)

        BackgroundFetch.finish(taskId);
    });

    console.log('status-background-fetch: ', status)
    // Step 2:  Schedule a custom "oneshot" task "com.foo.customtask" to execute 5000ms from now.
    BackgroundFetch.scheduleTask({
        taskId: "com.rcrewards.penn",
        forceAlarmManager: true,
        stopOnTerminate: false,
        delay: 3000,  // <-- milliseconds,
        periodic: true
    });

    /////////////////////////////////////////////////////////

    // This handler fires whenever bgGeo receives a location update.
    onLocationUnsub = BackgroundGeolocation.onLocation(onLocation, onError);

    // This handler fires when movement states changes (stationary->moving; moving->stationary)
    // if (__DEV__){
    //     BackgroundGeolocation.onMotionChange(onMotionChange);
    // }

    // // This event fires when a change in motion activity is detected
    // if (__DEV__){
    //     BackgroundGeolocation.onActivityChange(onActivityChange);
    // }

    // This event fires when the user toggles location-services authorization
    //BackgroundGeolocation.onProviderChange(onProviderChange);

    if(__DEV__ || env.IS_DEBUG){
        const deviceInfo = await BackgroundGeolocation.getDeviceInfo()
        const deviceUser = deviceInfo.manufacturer + '-' + auth?.authData
        const org = env.TRACKER_ORG ? env.TRACKER_ORG : 'psg'
        const host = env.TRACKER_HOST ? env.TRACKER_HOST : 'https://tracker.transistorsoft.com'
        //console.log("TRACKER : " , org, host)
        //conf.transistorAuthorizationToken = await BackgroundGeolocation.findOrCreateTransistorAuthorizationToken(org, deviceUser , host)
    }
    ////
    // 2.  Execute #ready method (required)
    // Added other configuration and removed motion & fitness permission
    BackgroundGeolocation.ready(conf, (state) => {
        console.log("- BackgroundGeolocation is configured and ready: ", state.enabled);

        if (!state.enabled) {

            // Attempt to get three(3) samples with maximumAge of five(5) seconds current coordinates / location
            const options = {
                timeout: 60,          // Default 30 second timeout to fetch location
                maximumAge: 5000,     // Accept the last-known-location if not older than 5000 ms.
                desiredAccuracy: 10,  // Try to fetch a location with an accuracy of `10` meters.
                // samples: 3, // Default is 3 - How many location samples to attempt.
            }
            BackgroundGeolocation.getCurrentPosition(options)
            ////
            // 3. Start tracking!
            //
            BackgroundGeolocation.start(function() {
            console.log("- Start success");
            });
        }
    });
}

const onLocation = (location: any) => {
    console.log("Monitoring: Location: ", location);
    console.log("Monitoring: Location Coordinate: ", location.coords);
    if (onLocationChanged != null){
        onLocationChanged(location)
    }
}
const onError = (error: any) => {
    console.log("Monitoring: onError: ", error);
 }
const onMotionChange = (event: any) => {
    console.log("Monitoring: onMotionChange: ", event);
 }
const onProviderChange = (provider: any) => {
    console.log("Monitoring: onProviderChange: ", provider);
    if (onChangeBeaconsDidRange != null)
        onChangeBeaconsDidRange()
}
const onActivityChange = (event: any) => {
    console.log("Monitoring: onActivityChange: ", event);
    onChangeBeaconsDidRange()
}
const onBackgroundFetch = async (eventEmitter: Function) => {
    onBackgroundFetchListener = async () => {
        return new Promise((resolve, reject) => {
            eventEmitter()
            BackgroundGeolocation.getProviderState(data => {
                const permissionState = data?.status
                if (permissionState === LocationPermissionStatus.WhileUsingTheApp){
                    BackgroundGeolocation.changePace(true)
                }
            })
            setTimeout(() => {
                // ONCE RESOLVE
                // BackgroundFetch.finish(taskId); will trigger
                Promise.resolve()
            }, 5000);
        })
    }
}

const onLocationProviderChanged = async (eventEmitter: Function) => {
    onLocationProviderChangedListener = (data: any) => {
        return new Promise((resolve, reject) => {
            eventEmitter(data)
            setTimeout(() => {
                // ONCE RESOLVE
                // BackgroundFetch.finish(taskId); will trigger
                Promise.resolve()
            }, 1000);
        })
    }
}

export default {
    BeginMonitoringTask,
    StopMonitoringTask,
    StopRangingTask,
    onBackgroundFetch,
    onLocationProviderChanged,
}
