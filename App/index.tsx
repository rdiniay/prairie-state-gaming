import * as React from 'react'
import { Provider } from 'react-redux'
import DebugConfig from './config/debugConfig'
import store from './redux'
import RootNavigators from './navigation'
import Toast from 'react-native-toast-message';
import { View, Text } from 'react-native'

const toastConfig = {
  // success: ({ text1, props, ...rest }) => (
  //   <View style={{ flex: 1, width: '95%', justifyContent: 'center', paddingVertical: 15, paddingHorizontal: 15, backgroundColor: 'black', borderRadius: 5, }}>
  //     <Text style={{ color: 'white', fontSize: 16,  }}>{text1}</Text>
  //     {/* <Text>{props.guid}</Text> */}
  //   </View>
  // ),
  // error: () => {},
  info: () => {},
  custom: ({ text1, props, ...rest }) => (
    <View style={{ flex: 1, width: '95%', justifyContent: 'center', paddingVertical: 15, paddingHorizontal: 20, backgroundColor: 'black', borderRadius: 5, }}>
      <Text style={{ color: 'white', fontSize: 16,  }}>{text1}</Text>
      {/* <Text>{props.guid}</Text> */}
    </View>
  )
};

const App = () => (
  <Provider store={store}>
    <RootNavigators />
    <Toast config={toastConfig} ref={(ref) => Toast.setRef(ref)} />
  </Provider>
)

// export default App
export default DebugConfig.useReactotron
                ? console.tron.overlay(App)
                : App
