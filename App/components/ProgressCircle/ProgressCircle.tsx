import React, { useEffect, useState} from 'react';
import { 
  Modal, 
  View, 
  Text,
  ActivityIndicator, 
  Button,
  Platform,
  Dimensions,
} from 'react-native';
import styles from './Styles'
import ProgressCircle from 'react-native-progress-circle'
import { Colors } from '../../styles'
  
type Props = {
  children?: any,
  title?: string,
  height?: number,
  percent?: number,
  borderWidth?: number,
  theme?: string,
  color?: string,
  style?: any,
}

const CustomProgressCircle = ({ 
  children, 
  style, 
  title="", 
  height=null, 
  percent=50, 
  borderWidth=20, 
  theme="light", 
  color=null,
}: Props) => {

  return (
    <View style={styles.container}>
        <View style={styles.circleContent}>
            <ProgressCircle
                    percent={percent}
                    radius={height}
                    borderWidth={borderWidth}
                    color={color}
                    shadowColor={theme === "dark" ? Colors.darkRed : Colors.gray}
                    bgColor={theme === "dark" ? Colors.red : Colors.lightBrown} />
        </View>
        <Text style={[styles.titleText, { color: theme === "dark" ? Colors.white : Colors.black }]}>
          {`${title}`}
        </Text>
    </View>
  )
}
  
export default CustomProgressCircle