import { StyleSheet } from 'react-native'
import { Colors } from '../../styles'
import {  Dimensions, } from  'react-native'
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        alignItems: 'center', 
        borderRadius: 20, 
        backgroundColor: Colors.red
    },
    loadingContent: {
        zIndex: 999,
        position: 'absolute',
        top: height/4, 
        alignSelf: 'center'
    },
    imageContent: {
        flex: 1, 
        backgroundColor: Colors.red
    },
    imageBackgroundRadiusContent: {
        position: 'absolute', 
        width: '100%', 
        height: 30, 
        backgroundColor: Colors.white 
    },
    imageRadius: {
        borderRadius: 100,
    },
    image: {
        backgroundColor: Colors.lightBrown, 
        borderRadius: 30,  
        width: width 
    },

    detailsContent: {
        paddingTop: 30, 
        paddingBottom: 90, 
        backgroundColor: Colors.white
    },
    detailsContentPadding: {
        flex: 1, 
        padding: 10, 
        justifyContent: 'center',
    },
    detailsSpaceBetween: { 
        justifyContent: 'space-between',
        flexDirection: 'row', 
        paddingVertical: 2,
        paddingHorizontal: 20, 
    },
    detailsLineSperator: {
        height: 1,
        borderTopWidth: .5,
        borderTopColor: Colors.black,
        marginTop: 12,
    },
    detailsLeftBullet: {
        width: 16, 
        height: 16, 
        borderRadius: 8, 
        backgroundColor: Colors.red, 
        marginRight: 10, 
    },
    detailsLeftTextContent: {
        flex: .74, 
        flexDirection: 'row', 
        alignItems: 'center', 
        textAlign: 'justify',
    },
    detailsRightTextContent: {
        flex: .26, 
        flexDirection: 'row',
    },
    detailsLeftTitleText: {
        flex: 1, 
        color: Colors.black, 
        fontSize: 17, 
    },
    detailsLeftTextBold: {
        flex: 1, 
        color: Colors.red, 
        fontSize: 20, 
        fontWeight: 'bold',
    },
    detailsLeftText: {
        flex: 1, 
        color: Colors.lightgray, 
        textAlign: 'left',
        fontSize: 16.5, 
    },
    detailsRightText: {
        flex: 1, 
        color: Colors.red, 
        textAlign: 'right',
        fontSize: 16.5, 
    }
});

export default styles;