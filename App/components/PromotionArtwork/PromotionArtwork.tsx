import React, { useState, useEffect } from 'react';
import { 
    View,
    TouchableHighlight,
    Image,
    ActivityIndicator,
    Dimensions,
  } from 'react-native'
import { Text } from '../../components'
import DateString from 'moment'
import { Colors } from '../../styles'
import styles from './Styles'
import { Promotion, Entry, Drawing, Location, DrawingDate } from '../../models'
const { width, height } = Dimensions.get('window')

type Props = {
    style?: any,
    theme?: string,
    promotion?: Promotion,
    onPress?: () => void,
}

const PromotionArtwork = ({
    theme,
    onPress,
    promotion,
    style,
}: Props) => {

    let weeklyCount = 0
    const [isLoading, setIsLoading] = useState(true)
    const [localUrl, setLocalUrl] = useState(null)
    const [promotionSize, setPromotionSize] = useState({ width: width, height: height*2.5 })
    const weeklyDates: DrawingDate[] = promotion?.drawing?.drawingDates.map(item => {
        if (item.type === "weekly") {
            const endTime = Number(new Date(`${item.runDate}`))
            const currentTime = Number(Date.now()) // `2022-05-15T13:00:00.000Z`
            const elapse = (endTime - currentTime) / 1000 
            if (0 < elapse && weeklyCount < 2) {
                weeklyCount++
                return item
            }
        }
        return []
    }).flat()

    useEffect(() => {
        if (promotion.drawing) {
            const url = promotion.drawing.grandPrizeUrl

            if (url && 0 < url.length) {
            
                if (localUrl && url !== localUrl) {
                    setTimeout(() => {
                        Image.getSize(`${url}`, (width, height) => {
                            setPromotionSize({
                                width: width * (promotionSize.height / height),
                                height: height * (promotionSize.width / width)
                            });
                        });
                    }, 500);
                }
                else {
                    Image.getSize(`${url}`, (width, height) => {
                        setPromotionSize({
                            width: width * (promotionSize.height / height),
                            height: height * (promotionSize.width / width)
                        });
                    });
                    setLocalUrl(url)
                }
            }
        }
    }, [promotion.drawing])
    
    return promotion?.drawing ? (
        <View style={styles.container} >
            {isLoading &&
                <ActivityIndicator style={styles.loadingContent} size="large" color={'gray'}/>
            }
            {localUrl && 0 < promotionSize.height &&
                <TouchableHighlight  underlayColor="transparent">
                    <View style={styles.imageContent}>
                        <View style={[styles.imageBackgroundRadiusContent, {top: promotionSize.height-30,}]} />
                        <View style={styles.imageRadius}>
                            <Image resizeMode="cover" 
                                    onLoadEnd={() => { setIsLoading(false) }}
                                    style={[styles.image, {height: promotionSize.height,}]} source={{ uri: `${localUrl}` }} />
                            {/* require('../../assets/secondary-drawing.png') https://psgapp-rewards-artwork.s3.amazonaws.com/Electric Bag.png */}
                        </View>

                        <View style={styles.detailsContent}>
                            {weeklyDates?.map((drawingDate: DrawingDate) => (
                                <View style={styles.detailsContentPadding}>
                                    <View style={styles.detailsSpaceBetween}>
                                        <View style={styles.detailsLeftTextContent}>
                                            <View style={styles.detailsLeftBullet} />
                                            <Text style={styles.detailsLeftTitleText}>
                                                {`Weekly Drawing On:`}
                                            </Text>
                                        </View>
                                        <View style={[styles.detailsRightTextContent, {alignItems: 'center'}]}>
                                            <Text style={styles.detailsRightText}>
                                                {DateString(`${drawingDate.runDate}`) .format("MM/DD/YY")}
                                            </Text>
                                        </View>
                                    </View>
                                    <View style={styles.detailsSpaceBetween}>
                                        <View style={styles.detailsLeftTextContent} />
                                        <View style={[styles.detailsRightTextContent, {alignItems: 'flex-start',}]}>
                                            <Text style={styles.detailsRightText}>
                                                {DateString(`${drawingDate.runDate}`).format("hh:mm:A")}
                                            </Text>
                                        </View>
                                    </View>
                                    <View style={styles.detailsSpaceBetween}>
                                        <Text style={styles.detailsRightText}>
                                            {`Drawing ` +DateString(`${drawingDate.runDate}`).format("MM/DD")}
                                        </Text>
                                    </View>
                                    <View  style={styles.detailsLineSperator} />
                                </View>
                            ))}
                            

                            <View style={styles.detailsContentPadding}>
                                <View style={styles.detailsSpaceBetween}>
                                    <View style={styles.detailsLeftTextContent}>
                                        <Text style={styles.detailsLeftTextBold}>
                                            {`GRAND PRIZE`}
                                        </Text>
                                    </View>
                                    <View style={[styles.detailsRightTextContent, {alignItems: 'center'}]}>
                                        <Text style={styles.detailsRightText}>
                                            {DateString(`${promotion?.drawing.endDate}`) .format("MM/DD/YY")}
                                        </Text>
                                    </View>
                                </View>
                                <View style={styles.detailsSpaceBetween}>
                                    <View style={styles.detailsLeftTextContent}>
                                        <Text style={styles.detailsLeftText}>
                                            {`${promotion?.drawing.grandPrize.toUpperCase()}`}
                                        </Text>
                                    </View>
                                    <View style={[styles.detailsRightTextContent, {alignItems: 'flex-start',}]}>
                                        <Text style={styles.detailsRightText}>
                                            {DateString(`${promotion?.drawing.endDate}`).format("hh:mm:A")}
                                        </Text>
                                    </View>
                                </View>
                                
                            </View>
                        </View>
                    </View>
                </TouchableHighlight>
            }
        </View>
    ) 
    : (<View />)
}

export default PromotionArtwork;

