import { StyleSheet, PixelRatio, } from  'react-native'
import { Colors } from '../../styles'

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        paddingTop: 23,
        paddingBottom: 8,
        backgroundColor: 'white',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        shadowRadius: 2,
        elevation: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        flex: 1,
        fontSize: 22,
        textAlign: 'center',
    },
    promotionItemMainContent: {
        flexDirection: 'row',
    },
    promotionItemBottomContent: {
        paddingHorizontal: 25, 
        paddingTop: 20, 
        paddingBottom: 5,  
        flexDirection: 'row',
    },
    promotionItemBottomRoundContent: {
        flex: 1, 
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center', 
        paddingVertical: 5,  
        paddingHorizontal: 15,  
        borderRadius: 10,
    },
    promotionItemBottomRoundLeftText: {
        flex: 0.45,
        paddingRight: 2,
        textAlign: 'left',
        color: Colors.white, 
        fontSize: 14 / PixelRatio.getFontScale(),
    },
    promotionItemBottomRoundRightText: {
        flex: 0.55,
        paddingLeft: 2,
        textAlign: 'right',
        color: Colors.white, 
        fontSize: 14 / PixelRatio.getFontScale(),
    },
    promotionItemLeftContent: {
        flex: .25,
        flexDirection: 'row',
        alignItems: 'flex-start',
    },
    promotionItemCenterContent: {
        flex: .75,  
        justifyContent: 'center',
    },
    promotionItemCenterText: {
        padding: 3, 
        paddingRight: 8,
        fontSize: 18, 
        color: Colors.lightgray, 
    },
    promotionItemRightContent: {
        flex: .05,
        justifyContent: 'center', 
        alignItems: 'center',
    },
    promotionItemRightImage: {
        marginRight: 10, 
        height: 12, 
        width: 12, 
    },
    promotionItemButtom: {
        flex: 1,
        justifyContent: 'center', 
        alignItems: 'center' , 
        borderRadius: 15, borderColor: Colors.lightgray, 
    },
    promotionItemPromoSubContent: {
        flexDirection: 'row', 
        alignItems: 'center'
    },
    promotionItemPromo: {
        flex: .4, 
        padding: 3, 
        paddingHorizontal: 5,  
        borderRadius: 10, 
        backgroundColor: Colors.red 
    },
    promotionItemPromoText: {
        color: Colors.white, 
        fontSize: 13, 
        textAlign: 'center', 
    },
    promotionItemPromoLocation: {
        paddingLeft: 5,
        fontSize: 16,
    },
    promotionItemTitle: {
        padding: 2, 
        fontSize: 18,
    },
    promotionItemDescription: {
        fontSize: 12,
    },
    diagnosticContent: {
        margin: 15, 
        marginTop: 25, 
        marginBottom: 0, 
        borderColor: Colors.lightgray, 
        borderWidth: 0.5, 
        borderRadius: 15, 
        paddingVertical: 15, 
        paddingHorizontal: 10, 
        backgroundColor: 'white',
    },
    loadingView: {
        //flex:0.7, 
        paddingTop: 27, 
        justifyContent: 'center', 
    }
});

export default styles;