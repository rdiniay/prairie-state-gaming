import React, { useEffect, useState } from 'react';
import { 
    View,
    TouchableOpacity,
    TouchableHighlight,
    Image,
    ActivityIndicator,
  } from 'react-native'
import { Colors } from '../../styles'
import { images } from '../../constants'
import { strings } from '../../localization/strings';
import { Text, ProgressCircle, Button } from '../../components'
import { Promotion, Entry, Multiplier, Drawing, Location } from '../../models'
import Utils from '../../services/shared/utils/Utils'
import { abbreviateNumber } from 'js-abbreviation-number'
import styles from './Styles'
import DateString from 'moment'

type Props = {
    style?: any,
    truncated?: boolean,
    theme?: string,
    promotion?: Promotion,
    showDiagnostic?: boolean,
    onPress?: () => void,
    onSendDiagnostic?: () => void,
    isDiagnosticLoading?: boolean,
}

const PromotionItem = ({
    truncated,
    theme,
    onPress,
    onSendDiagnostic,
    promotion,
    showDiagnostic,
    isDiagnosticLoading,
    style,
}: Props) => {

    const [multiplierDuration, setMultiplierDuration] = useState('')

    const [multiplierDescription, setMultiplierDescription] = useState('')

    const [shouldShowMultiplier, setShouldMultiplier] = useState(false)

    const shouldShowDiagnostic: Boolean = showDiagnostic

    const isThemeDark: Boolean = theme === "dark"

    const isMonthlyPromotion: Boolean = promotion.location ? false : true

    const onPromotionEntry = () => {
        if (promotion?.entry?.earnedEntryCount) {
            const drawingEntry: Entry = promotion.entry
            if (drawingEntry.earnedEntryCount === 0)
                return ``
            if(drawingEntry.earnedEntryCount >= 10000){
                return abbreviateNumber(drawingEntry.earnedEntryCount as number , 1)
            }
            let earnedEntryCount = `${parseFloat(`${drawingEntry.earnedEntryCount}`).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}`
            return earnedEntryCount.replace(".00", "")
        }
        
        return ``
    }

    useEffect(() => {
        let multiplier: Multiplier = isMonthlyPromotion ? promotion?.drawing?.multiplier : promotion?.entry?.multiplier
        if (multiplier) {
            let startDate = multiplier.startDate
            let endDate = multiplier.endDate
            let entryStartDate = new Date(`${startDate}`)
            let entryEndDate = new Date(`${endDate}`)
            let multiplierStartDate = DateString(`${entryStartDate}`).format("MMMM D")
            let multiplierEndDate = entryStartDate.getMonth() === entryEndDate.getMonth() ?
                                                    DateString(entryEndDate).format("D") : 
                                                    DateString(entryEndDate).format("MMMM D")
            setMultiplierDuration(`${multiplierStartDate} - ${multiplierEndDate}`)
            setMultiplierDescription(`${multiplier?.description}`)
            setShouldMultiplier(1 < multiplier?.amount)
        }
    }, [promotion])
    
    return (
        <TouchableHighlight activeOpacity={0.6}
                            underlayColor={Colors.blackOpacity} 
                            key={`${promotion.id}`}  
                            style={[styles.promotionItemButtom, { paddingVertical: isThemeDark ? 25 : 20, borderBottomWidth: isThemeDark ? 0 : 0.5, } ]}
                                            disabled={!onPress}
                                            onPress={Utils.multipleTapHandler(() => onPress())}> 
            <>                       
                <View style={styles.promotionItemMainContent}>
                        <View style={styles.promotionItemLeftContent}>
                            <ProgressCircle 
                                title={`${onPromotionEntry()}`}
                                percent={Number(promotion.drawing?.id && promotion.earningTickPercentage ? promotion.earningTickPercentage : 0)}
                                height={35}
                                borderWidth={8}
                                color={isThemeDark ? Colors.white : promotion.isCheckedin ? Colors.red : Colors.black}
                                theme={theme}
                                  />
                        </View>
                        
                        {promotion.drawing && 
                            <View style={styles.promotionItemCenterContent}>
                                    <View style={styles.promotionItemPromoSubContent}>
                                        {!isMonthlyPromotion && 
                                            <View style={[styles.promotionItemPromo, { backgroundColor: isThemeDark ? Colors.darkRed : Colors.blue }]}>
                                                <Text numberOfLines={1} style={styles.promotionItemPromoText}>
                                                    {strings.dashboard_local_promo.toUpperCase()}
                                                </Text>
                                            </View>
                                        }
                                        <Text numberOfLines={1} fontWeight='bold' style={[styles.promotionItemPromoLocation, 
                                                                        { flex: isMonthlyPromotion ? 1 : .6 , color: isThemeDark ? Colors.lightPink : Colors.lightgray, }]}>
                                            {isMonthlyPromotion ? strings.dashboard_monthly_promo.toUpperCase() : promotion.location?.name.toUpperCase()}
                                        </Text>
                                    </View>
                                    
                                    <Text numberOfLines={truncated ? 2 : 0} fontWeight='bold' style={[styles.promotionItemTitle, { color: isThemeDark ? Colors.white : Colors.black, } ]}> 
                                        {`${promotion.drawing?.title?.toUpperCase()}`}
                                    </Text>
                                
                                    <Text numberOfLines={1} style={[styles.promotionItemDescription, {  color: isThemeDark ? Colors.white : Colors.lightgray, } ]}>
                                        {`${promotion.drawing.description}`}
                                    </Text>
                                
                            </View>
                        }
                        {!promotion.drawing && 
                            <View style={styles.promotionItemCenterContent}>
                                <Text numberOfLines={truncated ? 1 : 0} fontWeight='bold' style={[styles.promotionItemCenterText, { color: isThemeDark && !isMonthlyPromotion  ? Colors.white : Colors.black, } ]}>
                                    {`${promotion.location?.name.toUpperCase()}`}
                                </Text>
                                <View style={styles.promotionItemPromoSubContent}>
                                    <View style={[styles.promotionItemPromo, { backgroundColor: isThemeDark ? Colors.darkRed : Colors.red }]}>
                                        <Text numberOfLines={1} style={styles.promotionItemPromoText}>
                                            {strings.dashboard_state_promo.toUpperCase()}
                                        </Text>
                                    </View>
                                </View>
                            </View>
                        }
                        <View style={styles.promotionItemRightContent}>
                            <Image style={styles.promotionItemRightImage} source={images.chevronSmall} />
                        </View>
                </View>
                {shouldShowMultiplier && 
                    <View style={styles.promotionItemBottomContent}>
                        <View style={[styles.promotionItemBottomRoundContent, { backgroundColor: isThemeDark ? Colors.darkRed :  Colors.blue  }]}>
                            <Text numberOfLines={1} fontWeight='bold' style={styles.promotionItemBottomRoundLeftText}>
                                {`${multiplierDescription.toUpperCase()} ENTRIES`}
                            </Text>
                            <Text numberOfLines={1} fontWeight='bold' style={styles.promotionItemBottomRoundRightText}>
                                {multiplierDuration}
                            </Text>
                        </View>
                    </View>
                }
                { shouldShowDiagnostic &&
                    <View style={styles.diagnosticContent}>
                            <View style={{  paddingBottom: 10, paddingHorizontal: 10, }}>
                                <Text style={{ color: Colors.black, fontSize: 16, fontWeight: 'bold' }}>
                                    {strings.diagnostic_having_issues_checking_in}
                                </Text>
                            </View>
                            <View style={{paddingHorizontal: 10, paddingBottom: 20}}>
                                <Text style={{ color: Colors.lightgray, fontSize: 14 }}>
                                    {strings.diagnostic_instruction_subtext}
                                </Text>
                            </View>
                            {!isDiagnosticLoading &&
                            <Button style={{ backgroundColor: Colors.red, 
                                            padding: 15, borderRadius: 10, borderWidth: 2, 
                                            borderColor: Colors.white }}
                                            titleStyle={{ fontWeight: 'bold', fontSize: 16 }}
                                            title={strings.diagnostic_send_button}
                                            onPress={onSendDiagnostic}/>
                            }
                            {isDiagnosticLoading &&
                                <View style={styles.loadingView}>
                                    <ActivityIndicator size="large" color={'gray'}/>
                                </View>
                            }
                    </View>
                }
            </>   
        </TouchableHighlight>
    )
}

export default PromotionItem;

