import React, { useState } from 'react'
import { ConnectedProps } from 'react-redux'
import { 
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  FlatList,
  SectionList,
  ActivityIndicator,
  Dimensions,
} from 'react-native'
import { Colors } from '../../styles'
import styles from './Styles'
import { ScrollView } from 'react-native-gesture-handler'
import { WebView } from 'react-native-webview';
const { width, height } = Dimensions.get('window');
import { Rules } from '../../models'
import { images } from '../../constants'
import Lightbox from 'react-native-lightbox-v2';
import ImageZoom, { IOnMove } from 'react-native-image-pan-zoom'
  
const PinchLightBox = ({ imageStyle={ height: null, width: null }, source=null, children=null}) => {
    let imageHeight = imageStyle.height ? imageStyle.height : height*.28
    let imageWidth = imageStyle.width ? imageStyle.width : width
    let closeFunction: () => {}
    const [backgroundColor, setBackgroundColor] = useState('black')
    const [pinch, setPinch] = useState(false)

    const CustomLightboxPinchImage = () => (
        <Lightbox   underlayColor="white"
                    onOpen={() => { setPinch(true) }}
                    willClose={() => { setPinch(false) }}
                    swipeToDismiss={false}
                    backgroundColor={backgroundColor}
                    renderHeader={close => {
                        closeFunction = () => {
                            close()
                            return close
                        }
                        return (
                            <TouchableOpacity onPress={close} style={{ position: 'absolute', height: 100, width: 80, justifyContent: 'center', alignItems: 'center', alignSelf: 'flex-end', }}>
                                <View style={{ backgroundColor: '#00000080',  justifyContent: 'center', height: 50, width: 50, borderRadius: 25, alignItems: 'center', }}>
                                    <View style={{ backgroundColor: '#FFFFFF20',  justifyContent: 'center', height: 50, width: 50, borderRadius: 25, alignItems: 'center', }}>
                                        <Text style={{ fontSize: 30, color: 'white', textAlign: 'center', paddingBottom: 3, }}>
                                            x
                                        </Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        )
                    }}
                    >

            <Image resizeMode="stretch" style={{ top: pinch ? 0 : imageHeight, height: pinch ? 0 : height, width: pinch ? 0 : width,  }} source={source} />
                        
            {pinch &&
                <ImageZoom cropWidth={Dimensions.get('window').width}
                            cropHeight={Dimensions.get('window').height}
                            imageWidth={width}
                            imageHeight={height}
                            enableSwipeDown={true}
                            onMove={(position: IOnMove) => {
                                const yPosition = 1 - (position.positionY / 1000)
                                setBackgroundColor(`rgba(0, 0, 0, ${yPosition})`)
                            }}
                            onSwipeDown={() => { closeFunction() }}>
                    <Image resizeMode="stretch" style={{ top: pinch ? 0 : imageHeight, height: height, width: width,  }} source={source} />
                </ImageZoom>
            }
        </Lightbox>
    );
  
    return (
      <CustomLightboxPinchImage />
    )
}
  
export default PinchLightBox