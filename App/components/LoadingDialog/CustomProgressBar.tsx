import React, { useEffect, useState} from 'react';
import { 
  Modal, 
  View, 
  Text,
  ActivityIndicator, 
  Button,
  Platform,
} from 'react-native';
import styles from './Styles'
  
const LoadingDialog = ({ visible=false, title=null, subtitle=null }) => {

    const isIOS = (Platform.OS === 'ios')
    const modelColor = isIOS ? 'rgba(0, 0, 0, 0.15)' : 'rgba(0, 0, 0, 0.5)'
    const backgroundColor = isIOS ? 'rgba(0, 0, 0, 0.5)' : 'white'
    const contentColor = isIOS ? 'white' : 'rgba(0, 0, 0, 0.5)'

    const CustomProgressBar = ({ visible }) => (
      <Modal onRequestClose={null} visible={visible} animationType="fade" transparent>
          <View style={[{ backgroundColor: modelColor, }, styles.container]}>
              <View style={{ borderRadius: isIOS ? 10 : 2, backgroundColor: backgroundColor, padding: 25,  }}>
                  {!isIOS && title &&
                    <Text style={[{ paddingLeft: isIOS ? 0 : 8, }, styles.titleText]}>
                        {title}
                    </Text>
                  }
                  <View style={{ flexDirection: isIOS ? 'column' : 'row', justifyContent: 'space-between', paddingLeft: isIOS ? 0 : 0, }}>
                    <ActivityIndicator color={contentColor} size="large" />
                    {subtitle &&
                        <Text style={[{ color: contentColor,
                                        paddingHorizontal: isIOS ? 0 : 40,
                                        paddingBottom: isIOS ? 0 : 10,  }, styles.subtitleText]}>
                            {subtitle}
                        </Text>
                    }
                  </View>
              </View>
          </View>
      </Modal>
    );
  
    return visible ? (
      <CustomProgressBar visible={visible} />
    )
    : (<View />)
}
  
export default LoadingDialog