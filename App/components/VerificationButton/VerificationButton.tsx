import React, { useState, useEffect } from 'react';
import { 
    View,
    TextInput,
    TouchableOpacity,
    Image,
    Dimensions
  } from 'react-native'
import { images, lotties } from '../../constants'
import { Text, LottieView } from '../../components'
const { width, height } = Dimensions.get('window');
import styles from './Styles'

type Props = {
    style?: any,
    theme?: string,
    title?: string,
    onPress?: () => void,
}

const VerificationButton = ({
    theme,
    onPress,
    title,
    style,
}: Props) => {
    
    return (
        <View style={styles.container}>
            <LottieView style={{ position: 'absolute', width: width*.6, left: width*.18, bottom: 20, transform: [{ rotate: '185.5deg'}], }}
                                                    source={lotties.catchVerification}
                                                    animate={true}
                                                    speed={.8}
                                                />
            <TouchableOpacity onPress={onPress} style={styles.verifyButton}>
                    <View style={styles.leftContent}>
                        <Image style={styles.image} source={images.verifyStar} />
                    </View>
                    <View style={styles.centerContent}>
                        <Text fontWeight='bold' style={styles.verifyText}>
                            {`VERIFY`}
                        </Text>
                        <Text style={styles.verifySubText}>
                            {`Verify your account to be eligible to win.`}
                        </Text>
                    </View>
                    <View style={styles.rightContent}>
                        <LottieView style={{ opacity: 0.9 }}
                                                    source={lotties.verification}
                                                    animate={true}
                                                    speed={1.5}
                                                />
                    </View>
            </TouchableOpacity>
        </View>
    )
}

export default VerificationButton;

