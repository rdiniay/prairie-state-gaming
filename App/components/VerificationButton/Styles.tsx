import { StyleSheet } from 'react-native'
import { Colors, } from '../../styles'
import { Dimensions, PixelRatio, } from  'react-native'
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 20,
    },
    verifyButton: {
        flex: 1, 
        flexDirection: 'row', 
        paddingTop: 15,
        paddingBottom: 20, 
        borderBottomWidth: .5, 
        borderBottomColor: Colors.lightgray,
        paddingLeft: 10,
    },
    verifyText: {
        padding: 2,
        textAlign: 'center', 
        fontSize: 22 / PixelRatio.getFontScale(),
    },
    verifySubText: {
        textAlign: 'center', 
        fontSize: 18 / PixelRatio.getFontScale(), 
        color: Colors.darkgray, 
    },
    leftContent: {
        flex: .1, 
        justifyContent: 'center',
        right: 10,
    },
    centerContent: {
        flex: .7, 
        justifyContent: 'center',
        left: 12,
    },
    rightContent: {
        flex: .3,
        justifyContent: 'center', 
        alignItems: 'center',
    },
    image: {
        marginLeft: 20, 
        height: 24, 
        width: 24,
    },
    
    
});

export default styles;