import React, { useEffect, useState} from 'react'
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    TouchableHighlight,
    ScrollView,
    Dimensions,
    ActivityIndicator,
} from  'react-native'
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import {styles, commons, commonsAssets} from './Styles'
import { Colors, Commons } from '../../styles'
const { width, height } = Dimensions.get('window');

import { SwipeablePanel, SwipeablePanelProps } from 'rn-swipeable-panel';


interface Props extends SwipeablePanelProps {
    fixLarge?: boolean,
    titleValue?: string,
    header?: any,
    children?: any,
    style?: any,
}

const CustomSwipeablePanel = (props: Props) => {
    const insets = useSafeAreaInsets()
    const [loaded, setLoaded] = useState(false);
    const [isPanelActive, setIsPanelActive] = useState(true);
    const [render, setRender] = useState(true)
    const [showCloseButton, shouldShowCloseButton] = useState(null)

    const [panelProps, setPanelProps] = useState({ 
      fullWidth: true,
      openLarge: false,

      showCloseButton: false,

        ...props,

        onClose: () => { 
          toggleDown() 
          setTimeout(() => {
            setRender(false)
            setTimeout(() => {
              setRender(true)
            }, 1);
          }, 10);
        },
        onPressCloseButton: () => {
          toggleDown()
          setTimeout(() => {
            setRender(false)
            setTimeout(() => {
              setRender(true)
            }, 1);
          }, 10);
        },
        // ...or any prop you want
        allowTouchOutside: props.allowTouchOutside,
        noBackgroundOpacity: true,
        smallPanelHeight: props.openLarge ? props.smallPanelHeight  : (props.smallPanelHeight ? props.smallPanelHeight : (height - insets.top - insets.bottom) / 1.35),
        barStyle: {
            backgroundColor: Colors.gray
        },
        noBar: true,
        scrollViewProps: {
          showsVerticalScrollIndicator: false,
          onScroll: (event) => {
            if (props.fixLarge)
                return

            if (event.nativeEvent.contentOffset.y === 0 &&
                event.nativeEvent.contentSize.height > height)
                toggleDown()
          },
          onScrollEndDrag: (event) => {
              setRender(true)

              if (props.fixLarge)
                return

              if (event.nativeEvent.contentOffset.y < -25) {
                  toggleDown()
              }
              else if (event.nativeEvent.contentOffset.y > 10) {
                  toggleUp()
              }
          }
        }
    });

    useEffect(() => {
      if (props.fixLarge && props.openLarge)
        return

      if (loaded)
        toggleUp()
      else {
        setLoaded(true)
        toggleDown()
      }
      if (showCloseButton === null) {
        
        shouldShowCloseButton(props.showCloseButton)
      }
    }, [props.isActive])
    
      const openPanel = () => {
        setIsPanelActive(true);
      };
    
      const closePanel = () => {
        setIsPanelActive(false);
      };

     const toggleUp = () => {
        setPanelProps({...panelProps, openLarge: true, showCloseButton: props.showCloseButton,})
        closePanel()
        setTimeout(() => {
            openPanel()
        }, 1);
      }

      const toggleDown = () => {
        setPanelProps({...panelProps, openLarge: false, showCloseButton: false,})
        closePanel()
        setTimeout(() => {
            openPanel()
        }, 1);
      }

      const togglePanel = () => {
        if (panelProps.openLarge)
            toggleDown()
        else
            toggleUp()
      }

    return (
        <SwipeablePanel {...panelProps} style={props.style}  isActive={isPanelActive}>
            <ScrollView keyboardShouldPersistTaps={'handled'} keyboardDismissMode="interactive" 
                        style={[props.style, 
                                props.fixLarge ? { } : { paddingTop: 0 },
                                { backgroundColor: props.header ? Colors.red : Colors.transparent }]}>
                {props.titleValue &&
                    <TouchableHighlight underlayColor={Colors.transparent} onPress={togglePanel} style={{ flex: 1, paddingTop: 30, }}>
                        <Text style={{ ...commons.contentTitleStyle, }}>{`${props.titleValue ?? ''}`}</Text>
                    </TouchableHighlight>
                }
                {props.header &&
                  <View style={{ flex: 1, paddingTop: 20 < insets.top ? (insets.top *.7) : insets.top, }}>
                      { props.header}
                  </View>
                }

                {render && props.children &&
                  <TouchableHighlight underlayColor={Colors.transparent} onPress={() => { }} >
                    { props.children }
                  </TouchableHighlight>
                }

                {!props.titleValue && !props.header &&
                  <TouchableOpacity onPress={togglePanel} style={styles.defaultTogglePanelButton} />
                }
            </ScrollView>
        </SwipeablePanel>
    )
}

  
export default CustomSwipeablePanel