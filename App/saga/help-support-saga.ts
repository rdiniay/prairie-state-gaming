import { call, put, select, takeLatest } from 'redux-saga/effects'
import { ApiAction, ApiHelpSupport } from '../services'
import HelpSupportAction, { HelpSupportTypes } from '../redux/help-support-redux'
import api from '../services/shared/help-support-service'

import DeviceInfo from 'react-native-device-info'
import Preference from 'react-native-preference'
import { AuthSelectors } from '../redux/auth-redux'
import { ProfileSelectors} from '../redux/profile-redux'
import { PreparationSelectors} from '../redux/preparation-redux'
import { LocationSelectors } from '../redux/location-redux'
import { MobileSelectors } from '../redux/mobile-redux'
import { DashboardSelectors } from '../redux/dashboard-redux'
import { BackgroundFetchSelectors} from '../redux/background-fetch-redux'
import { BeaconManagerSelectors} from '../redux/beacon-manager-redux'

const getDeviceDetails = () => {
    let deviceBrand = DeviceInfo.getBrand()
    let deviceModel = DeviceInfo.getModel()
    let devicePlatform = DeviceInfo.getSystemName()
    let deviceOSVersion = DeviceInfo.getSystemVersion()

    return {
        brand : deviceBrand,
        model : deviceModel,
        platform : devicePlatform,
        os_version: deviceOSVersion
    }
}

const getAppVersion = () => {
    let appVersion = DeviceInfo.getReadableVersion()  
    return appVersion
}

const getServerEnv = () => {
    const serverEnv = Preference.get("ENV")

    return serverEnv
}

const getAccountInfo = (auth: any, profile: any) => {
    let accountId = auth?.accountId
    let flags = auth?.flags
    let hideTestLocations = auth?.hideTestLocations
    let verified = auth?.verified
    let email = profile?.emailAddress
    let name = profile?.firstname + ' ' + profile?.lastname
    let phoneNumber = profile?.phoneNumber ?? ""
    
    return {
        accountId : accountId,
        name: name,
        email: email,
        phone: phoneNumber,
        verified: verified,
        flags: flags,
        hideTestLocations: hideTestLocations
    }
}

const getAppSetttings = (gps: any, geolocation: any, bluetoothConnection: any,
        nearbyDevicesPermission: any, notificationPermission: any) => {
    return {
        gps : gps,
        geolocation: geolocation,
        bluetooth: bluetoothConnection,
        nearby: nearbyDevicesPermission,
        notification: notificationPermission
    }
}

function getCheckInLocationById(locations, id) {
    return locations.filter(
        function(locations){ return locations.id == id }
    );
}

const getLocation = (coordinates: any, locations: any, locationId: any) => {
    let lat = coordinates?.latitude
    let long = coordinates?.longitude
    let locId = locationId ? locationId : -1
    let locationName = ""
    let locationCompleteAddress = ""
    let isActive = ""
    let clientExcluded = ""

    if (locationId) {
        let location = getCheckInLocationById(locations, locationId)
        console.log ("CheckIn Location: ", location)

        if (location && location.length > 0) {
            locationName = location[0].name ?? ""
            locationCompleteAddress = location[0].completeAddress ?? ""
            isActive = location[0].active === 1 ? "Yes" : "No"
            clientExcluded = location[0].clientExcluded ? "Yes" : "No"
        }
    }

    console.log ("Location lat long: ", lat + ',' + long)
    console.log ("CheckIn Location Id: ", locId)
    console.log ("CheckIn Location Name: ", locationName)
    console.log ("CheckIn Location Address: ", locationCompleteAddress)

    return {
        lat: lat,
        long: long,
        checkIn_location_id: locId,
        checkIn_location_name: locationName,
        checkIn_location_complete_address: locationCompleteAddress,
        active: isActive,
        clientExcluded: clientExcluded
    }
}

const getCheckInDetails = (mobile: any, disconnectingDateTime: any, currentCheckInPeriodTime: any,
    lastCheckInPeriodTime: any, lastCheckOutPeriodTime: any, isAttemptingToCheckin: any, 
    isPendingCheckin: any, isPendingCheckout: any) => {

    return {
        beacon: mobile?.beacon,
        checkinId: mobile?.checkinId,
        locationId: mobile?.locationId,
        disconnectingDateTime: disconnectingDateTime,
        currentCheckInPeriodTime: currentCheckInPeriodTime,
        lastCheckInPeriodTime: lastCheckInPeriodTime,
        lastCheckOutPeriodTime: lastCheckOutPeriodTime,
        isAttemptingToCheckin: isAttemptingToCheckin,
        isPendingCheckin: isPendingCheckin,
        isPendingCheckout: isPendingCheckout
    }
}

function * sendDiagnosticLogs (api: ApiHelpSupport, action: ApiAction) {
    console.log("help-support-sage: sendDiagnosticLogs:", action)
    const auth = yield select(AuthSelectors.auth)
    const profile = yield select(ProfileSelectors.profile)
    const bluetooth = yield select(PreparationSelectors.bluetooth)
    const geoLocation = yield select(PreparationSelectors.geoLocation)
    const gps = yield select(PreparationSelectors.gps)
    const nearbyDevices = yield select(PreparationSelectors.nearbyDevices)
    const notification = yield select(PreparationSelectors.notification)
    const beaconsDetected = yield select(BeaconManagerSelectors.beaconsDetected)
    const tickPercentage = yield select(DashboardSelectors.earningTick)
    const coordinates = yield select(LocationSelectors.coordinate)
    const locations = yield select(LocationSelectors.locations)
    const mobile = yield select(MobileSelectors.mobile)
    const disconnectingDateTime = yield select(MobileSelectors.disconnectingDateTime)
    const currentCheckInPeriodTime = yield select(MobileSelectors.currentCheckInPeriodTime)
    const lastCheckInPeriodTime = yield select(MobileSelectors.lastCheckInPeriodTime)
    const lastCheckOutPeriodTime = yield select(MobileSelectors.lastCheckOutPeriodTime)
    const isAttemptingToCheckin = yield select(MobileSelectors.isAttemptingToCheckin)
    const isPendingCheckin = yield select(MobileSelectors.isPendingCheckin)
    const isPendingCheckout = yield select(MobileSelectors.isPendingCheckout)
    const lastFetch = yield select(BackgroundFetchSelectors.lastFetch)
    const logTrace = yield select(BeaconManagerSelectors.logTrace)

    let deviceDetails = getDeviceDetails()
    let appVersion = getAppVersion()
    let serverEnv = getServerEnv()
    let accounInfo = getAccountInfo(auth, profile)
    let appSettings = getAppSetttings(gps, geoLocation, bluetooth, nearbyDevices, notification)
    let locationInfo = getLocation(coordinates, locations, mobile?.locationId)
    let checkInDetails = getCheckInDetails(mobile, disconnectingDateTime, currentCheckInPeriodTime,
        lastCheckInPeriodTime, lastCheckOutPeriodTime, isAttemptingToCheckin, 
        isPendingCheckin, isPendingCheckout)

    let info = {
        device: deviceDetails,
        app_version: appVersion,
        server_env: serverEnv,
        account_info: accounInfo,
        app_settings: appSettings,
        location: locationInfo,
        beacons_detected: beaconsDetected,
        tick_percentage: tickPercentage,
        check_in_details: checkInDetails,
        last_background_fetch: lastFetch,
        logTrace: logTrace
    }

    const response = yield call(api.sendDiagnosticLogs, info)

    yield put(
        response.matchWith({
            Ok: ({ value }) => HelpSupportAction.sendDiagnosticLogsSuccess(value),
            Error: ({ value }) => HelpSupportAction.sendDiagnosticLogsFailure(value)
        })
    )
}

export default function() {
    return [
        takeLatest(HelpSupportTypes.SEND_DIAGNOSTIC_LOGS,  sendDiagnosticLogs, api),
    ]
}
  