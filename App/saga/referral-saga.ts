import { delay, call, put, putResolve, select, takeLatest } from 'redux-saga/effects'
import { ApiAction, ApiReferral } from '../services'
import { AuthSelectors } from '../redux/auth-redux'
import ReferralActions, { ReferralTypes } from '../redux/referral-redux'
import BankActions, { BankSelectors } from '../redux/bank-redux'
import { Entry, Referral } from '../models'
import api from '../services/shared/referral-service'

function * sendPromoCode (api: ApiReferral, action: ApiAction) {
    const { code } = action
    const auth = yield select(AuthSelectors.auth)
    const response = yield call(api.sendPromoCode, code, auth)

    yield put(
        response.matchWith({
            Ok: ({ value }) => ReferralActions.sendPromoCodeSuccess(value),
            Error: ({ value }) => ReferralActions.sendPromoCodeFailure(value)
        })
    )
}

function * sendPromoCodeSuccess (api: ApiReferral, action: ApiAction) {
    const { payload } = action
    if (payload && payload.referral) {
        const referral: Referral = payload.referral
        const bank = yield select(BankSelectors.bank)
        const entries: Entry[] = bank.entries
        const bankEntries = entries.map((drawingEntry: Entry) => {
            if (!drawingEntry.locationId && drawingEntry.locationId === 0) 
                return ({ ...drawingEntry, 
                            earnedEntryCount: Number(drawingEntry.earnedEntryCount) + referral.award, 
                            entriesBanked: Number(drawingEntry.entriesBanked) + referral.award, })
            else 
                return drawingEntry
        })
        
        yield put(BankActions.addBankEntriesSuccess({ ...bank, entries: bankEntries }))
        yield delay(1000)
        yield putResolve(ReferralActions.sendPromoCodeSuccess(null))
    }
    
}

export default function() {
    return [
        takeLatest(ReferralTypes.SEND_PROMO_CODE,  sendPromoCode, api),
        takeLatest(ReferralTypes.SEND_PROMO_CODE_SUCCESS,  sendPromoCodeSuccess, api),
    ]
}
  