import { all, call, put, select, takeLatest } from 'redux-saga/effects'
import { ApiAction, ApiRegistration } from '../services'
import RegistrationActions, { RegistrationTypes } from '../redux/registration-redux'
import api from '../services/shared/registration-service'
import { MessagingSelectors } from '../redux/messaging-redux'
import { getUniqueId } from 'react-native-device-info'

function * createAccount (api: ApiRegistration, action: ApiAction) {
    let uniqueId = getUniqueId();
    let token  = yield select(MessagingSelectors.token)
    const { user } = action
    user.pushToken = token
    user.deviceToken = uniqueId
    const response = yield call(api.createAccount, user)

    yield put(
        response.matchWith({
            Ok: ({ value }) => RegistrationActions.createAccountSuccess(value),
            Error: ({ value }) => RegistrationActions.createAccountFailure(value)
        })
    )
}

export default function() {
    return [
        takeLatest(RegistrationTypes.CREATE_ACCOUNT,  createAccount, api),
    ]
}
  