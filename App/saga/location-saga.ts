import { all, call, put, putResolve, select, takeLatest, takeEvery, take, delay } from 'redux-saga/effects'
import { eventChannel, END } from 'redux-saga'
import { ApiAction, ApiLocation } from '../services'
import AuthActions, { AuthSelectors, AuthTypes } from '../redux/auth-redux'
import { ProfileSelectors } from '../redux/profile-redux'
import DrawingActions, { DrawingTypes } from '../redux/drawing-redux'
import BankActions, { BankTypes } from '../redux/bank-redux'
import LocationActions, { LocationTypes, LocationSelectors } from '../redux/location-redux'
import BeaconManagerActions, { BeaconManagerSelectors } from '../redux/beacon-manager-redux'
import MessagingActions from '../redux/messaging-redux';
import api from '../services/shared/location-service'
import { Auth, Message, Coordinate, Mobile, Profile, Location, LocationBeacon, LocationCheckin, Beacon, LocationPermissionStatus } from '../models'
import MobileActions, { MobileSelectors } from '../redux/mobile-redux'
import iOSBeaconMonitoringTask from '../background-task/beacon/iOSBeaconMonitoringTask'
import AndroidBeaconMonitoringTask from '../background-task/beacon/AndroidBeaconMonitoringTask'
import {Platform} from "react-native";
import { strings } from '../localization/strings'
import { AppStateSelectors } from '../redux/app-state-redux'
import PreparationActions, { PreparationSelectors } from '../redux/preparation-redux'

const oniOSLocationProvideChanged = () =>
  eventChannel(emitter => {
        iOSBeaconMonitoringTask.onLocationProviderChanged((data) => {
            emitter(data)
        })
    return () => {
    }
})

const onAndroidLocationProvideChanged = () =>
    eventChannel(emitter => {
        // TODO
        // AndroidBeaconMonitoringTask.onLocationProviderChanged((data) => {
        //     emitter(data)
        // })
    return () => {

    }
})

function * subscribeLocationProviderChanged (api: ApiLocation, action: ApiAction) {
    let locationProviderChanged = Platform.OS === 'ios' ? oniOSLocationProvideChanged : onAndroidLocationProvideChanged
    let locationProviderChangedChannel = yield call(locationProviderChanged)
    yield takeLatest(locationProviderChangedChannel, function* (result: any) {
        let coordinate: Coordinate = yield select(LocationSelectors.coordinate)
        yield put(LocationActions.getCurrentLocationSuccess({ ...coordinate, status: result }))
        const nextAppState = yield select(AppStateSelectors.nextAppState)
        const auth: Auth = yield select(AuthSelectors.auth)
        const mobile: Mobile = yield select(MobileSelectors.mobile)
        const mobileLoding: Mobile = yield select(MobileSelectors.isLoading)
        const isCheckedIn = auth && mobile && mobile.checkinId && !mobileLoding
        const disabledPreciseLocation: Boolean = result.accuracyAuthorization
        const notGranted: Boolean = result?.status !== LocationPermissionStatus.Always && result?.status !== LocationPermissionStatus.WhileUsingTheApp
        if (Platform.OS === 'ios') {
            if (isCheckedIn) {
                let shouldForceCheckout = disabledPreciseLocation
                if (result?.status === LocationPermissionStatus.AskNextTime)
                    shouldForceCheckout = true
                else if (result?.status === LocationPermissionStatus.Never)
                    shouldForceCheckout = true
    
                if (shouldForceCheckout) {
                    yield put(MobileActions.checkOut(true, false, true, "location-saga:subscribeLocationProviderChanged", null))
                }
            }
        }
    })

    yield take(LocationTypes.UNSUBSCRIBE_LOCATION_PROVIDER_CHANGED)
    locationProviderChangedChannel.close()
}

function * getCurrentLocation (api: ApiLocation, action: ApiAction) {
    const currentCoordinate: Boolean = yield select(LocationSelectors.coordinate)
    const currentLocationChannel = yield call(api.getCurrentLocationWithParams, currentCoordinate==null? false : true)
    const auth = yield select(AuthSelectors.auth)
    yield takeEvery(currentLocationChannel, function* (result: ApiAction) {
        if (result.latitude) {
            const coordinate: Coordinate = {  ...result }
            yield put(LocationActions.getCurrentLocationSuccess(coordinate))
            if (auth && auth.authData){
                yield put(LocationActions.getLocationBeacons(coordinate))
            }

        }
        else {
            yield put(LocationActions.getCurrentLocationFailure(result))
            //yield delay(1000)
            //yield put(LocationActions.getCurrentLocation())
        }

        yield put(PreparationActions.getPreparations())
        currentLocationChannel.close()
    })
}

function * getCurrentPromotionLocation (api: ApiLocation, action: ApiAction) {
    console.log('location-saga: getCurrentPromotionLocation')
    const currentCoordinate: Boolean = yield select(LocationSelectors.coordinate)
    const currentLocationChannel = yield call(api.getCurrentLocationWithParams, currentCoordinate==null? false : true)
    const auth = yield select(AuthSelectors.auth)
    yield takeEvery(currentLocationChannel, function* (result: ApiAction) {
        if (result.latitude) {
            const coordinate: Coordinate = {  ...result }
            yield put(LocationActions.getCurrentLocationSuccess(coordinate))
            if (auth && auth.authData){
                yield put(LocationActions.getLocationBeacons(coordinate))
            }
        }
        currentLocationChannel.close()
    })
    if (auth && auth.authData){
        yield take(LocationTypes.GET_CURRENT_LOCATION_SUCCESS)
        const coordinate: Coordinate = yield select(LocationSelectors.coordinate)
        yield put(LocationActions.getLocationBeacons(coordinate))
        yield take(LocationTypes.GET_LOCATION_BEACONS_SUCCESS)
        yield put(DrawingActions.getRecentDrawings())
        yield take(DrawingTypes.GET_RECENT_DRAWINGS_SUCCESS)
        yield put(BankActions.getBankEntries())
        yield take(BankTypes.GET_BANK_ENTRIES_SUCCESS)
        yield put(AuthActions.getAuthTestLocations())
        yield take(AuthTypes.GET_AUTH_TEST_LOCATIONS_SUCCESS)

         // Starts the beacon monitoring only when authenticated and app started launching
         const mobile: Mobile = yield select(MobileSelectors.mobile)
         const isCheckedIn = (mobile && mobile.checkinId)
         let isCurrentlyMonitoring = yield select(BeaconManagerSelectors.isRunning)
         if(__DEV__){
             console.log("location-saga.getCurrentPromotionLocation (auth && auth.authData):  isCurrentlyMonitoring : " , isCurrentlyMonitoring)
         }

        console.log('location-saga: starting beacon...')
        if (!isCurrentlyMonitoring)
            yield putResolve(BeaconManagerActions.startBeaconMonitoring())
         else if (!isCheckedIn) {
            yield putResolve(BeaconManagerActions.stopBeaconMonitoring())
            yield delay(1000)
            yield putResolve(BeaconManagerActions.startBeaconMonitoring())
         }

    }
}

function * getCurrentPromotionLocationForLogin (api: ApiLocation, action: ApiAction) {
    const currentCoordinate: Boolean = yield select(LocationSelectors.coordinate)
    const currentLocationChannel = yield call(api.getCurrentLocationWithParams, currentCoordinate==null? false : true)
    const auth = yield select(AuthSelectors.auth)
    yield takeEvery(currentLocationChannel, function* (result: ApiAction) {
        if (result.latitude) {
            const coordinate: Coordinate = {  ...result }
            yield put(LocationActions.getCurrentLocationSuccess(coordinate))
            if (auth && auth.authData){
                yield put(LocationActions.getLocationBeacons(coordinate))
            }

        }
        else {
            yield put(LocationActions.getCurrentLocationFailure(result))
            yield delay(1000)
            yield put(LocationActions.getCurrentPromotionLocationForLogin())
        }

        currentLocationChannel.close()
    })
    if (auth && auth.authData){
        yield take(LocationTypes.GET_CURRENT_LOCATION_SUCCESS)
        yield put(DrawingActions.getRecentDrawings())
        yield take(DrawingTypes.GET_RECENT_DRAWINGS_SUCCESS)
        yield put(BankActions.getBankEntries())
        yield take(BankTypes.GET_BANK_ENTRIES_SUCCESS)
    }
}

function * getLocationBeacons (api: ApiLocation, action: ApiAction) {
    const { coordinate } = action
    const auth = yield select(AuthSelectors.auth)
    if (coordinate && auth) {
        const response = yield call(api.getLocationBeaconsWithCoordinate, coordinate, auth)

        yield put(
            response.matchWith({
                Ok: ({ value }) => LocationActions.getLocationBeaconsSuccess(value),
                Error: ({ value }) => LocationActions.getLocationBeaconsFailure(value)
            })
        )
    }
}

function * saveLocationCheckin (api: ApiLocation, action: ApiAction) {
    const actionType = action.action
    const beacon: Beacon = action.beacon
    const locations: Location[] = yield select(LocationSelectors.locations)
    if (!locations) return

    const locationBeaconList: LocationBeacon[] = locations.map((location: Location)=> {
        if (location.beacons && location.active){
            return location.beacons.map((beacon)=> {
                const LocationBeacon: LocationBeacon = { id: Number(location.id), beacon: beacon}
                return LocationBeacon
            });
        }
        return [];
    }).flat();

    const locationBeaconItem = locationBeaconList.find((locationBeaconItem) => locationBeaconItem.beacon.major === beacon.major && locationBeaconItem.beacon.minor === beacon.minor);
    if (locationBeaconItem) {

        if (actionType === 'checkin') {
            const locationCheckin: LocationCheckin = {
                id: locationBeaconItem.id,
                lastCheckInPeriodTime: Date.now()
            }
            yield put(LocationActions.saveLocationCheckinSuccess(locationCheckin))
        }
        else {
            const locationCheckins: LocationCheckin[] = yield select(LocationSelectors.locationCheckins)
            const lastLocationCheckin: LocationCheckin = locationCheckins.find((locationCheckin) => locationCheckin.id === locationBeaconItem.id);
            if (lastLocationCheckin) {
                const locationCheckin: LocationCheckin = { ...lastLocationCheckin, lastCheckOutPeriodTime: Date.now() }
                yield put(LocationActions.saveLocationCheckinSuccess(locationCheckin))
            }
        }
    }
}

export default function() {
    return [
        takeLatest(LocationTypes.SUBSCRIBE_LOCATION_PROVIDER_CHANGED,  subscribeLocationProviderChanged, api),
        takeLatest(LocationTypes.SAVE_LOCATION_CHECKIN,  saveLocationCheckin, api),
        takeLatest(LocationTypes.GET_CURRENT_PROMOTION_LOCATION,  getCurrentPromotionLocation, api),
        takeLatest(LocationTypes.GET_CURRENT_PROMOTION_LOCATION_FOR_LOGIN,  getCurrentPromotionLocationForLogin, api),
        takeLatest(LocationTypes.GET_CURRENT_LOCATION,  getCurrentLocation, api),
        takeLatest(LocationTypes.GET_LOCATION_BEACONS,  getLocationBeacons, api),
    ]
}
