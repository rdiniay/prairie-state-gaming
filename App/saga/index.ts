import { all, put, takeLatest, fork } from 'redux-saga/effects'

/* ------------- REDUX ------------- */

import { AppLaunchTypes } from '../redux/app-launch-redux'

/* ------------- SAGA ------------- */


/* ------------- API SERVICES ------------- */

import firestoreAuthModule from '../services/firestore-auth-module'
import firestoreModule from '../services/firestore-module'

import { networkSaga } from 'react-native-offline';

export default function* root() {
  yield all([
    
      // NETWORK SAGA
    fork(networkSaga as any, { pingInterval: 20000 }),

    // USER SAGA
    //takeLatest(UserTypes.REQUEST_FETCH_USERS,  fetchUsers, firestoreModule),

    // APP LAUNCH SAGA
    ...require(`./app-launch-saga`).default(),

    // AUTH SAGA
    ...require(`./auth-saga`).default(),

    // APP STATE SAGA
    ...require(`./app-state-saga`).default(),

    // MENU SAGA
    ...require(`./menu-saga`).default(),

    // REGISTRATION SAGA
    ...require(`./registration-saga`).default(),

    // LOGIN SAGA
    ...require(`./login-saga`).default(),

    // PROFILE SAGA
    ...require(`./profile-saga`).default(),

    // DRAWING SAGA
    ...require(`./drawing-saga`).default(),

    // TOS SAGA
    ...require(`./tos-saga`).default(),

    // ABOUT SAGA
    ...require(`./about-saga`).default(),

    // REFERRAL SAGA
    ...require(`./referral-saga`).default(),

    // LOCATION SAGA
    ...require(`./location-saga`).default(),

    // HOME SAGA
    // ...require(`./home-saga`).default(),

    // MOBILE SAGA
    ...require(`./mobile-saga`).default(),

    // FORGOT PIN SAGA
    ...require(`./forgot-pin-saga`).default(),

    // BANK SAGA
    ...require(`./bank-saga`).default(),

    // BEACON MANAGER SAGA
    ...require(`./beacon-manager-saga`).default(),

    // MESSAGING SAGA
    ...require(`./messaging-saga`).default(),

    // DASHBOARD SAGA
    ...require(`./dashboard-saga`).default(),

    // BACKGROUND FETCH SAGA
    ...require(`./background-fetch-saga`).default(),

    // PREPARATION SAGA
    ...require(`./preparation-saga`).default(),

     // HELP SUPPORT SAGA
     ...require(`./help-support-saga`).default(),

  ])
}
