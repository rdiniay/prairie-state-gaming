import { all, call, put, select, take, takeLatest, takeEvery, delay } from 'redux-saga/effects'
import { eventChannel, END } from 'redux-saga'
import { ApiAction, ApiForgotPin } from '../services'
import AuthActions, { AuthSelectors } from '../redux/auth-redux'
import ForgotPinActions, { ForgotPinTypes, ForgotPinSelectors } from '../redux/forgot-pin-redux'
import LoginActions from '../redux/login-redux'
import api from '../services/shared/forgot-pin-service'

function * loginPin (api: ApiForgotPin, action: ApiAction) {
    const { pin } = action
    const emailAddress = yield select(ForgotPinSelectors.emailAddress)
    yield put(ForgotPinActions.updatePinSuccess(true))
    yield put(LoginActions.signIn(emailAddress, pin))
}
function * updatePin (api: ApiForgotPin, action: ApiAction) {
    const emailAddress = yield select(ForgotPinSelectors.emailAddress)
    const code = yield select(ForgotPinSelectors.code)
    
    if (emailAddress && code) {
        const { newPin } = action
        const response = yield call(api.verifyPin, newPin, emailAddress, code)

        yield put(
            response.matchWith({
                Ok: ({ value }) => ForgotPinActions.updatePinSuccess(value),
                Error: ({ value }) => ForgotPinActions.updatePinFailure(value)
            })
        )
    }
}

function * sendForgotPinVerification (api: ApiForgotPin, action: ApiAction) {
    const { email } = action
    const auth = yield select(AuthSelectors.auth)
    const response = yield call(api.sendForgotPinVerification, email, auth)
    yield put(
        response.matchWith({
            Ok: ({ value }) => ForgotPinActions.sendForgotPinVerificationSuccess(value),
            Error: ({ value }) => ForgotPinActions.sendForgotPinVerificationFailure(value)
        })
    )
}

export default function() {
    return [
        takeLatest(ForgotPinTypes.LOGIN_PIN, loginPin, api),
        takeLatest(ForgotPinTypes.UPDATE_PIN, updatePin, api),
        takeLatest(ForgotPinTypes.SEND_FORGOT_PIN_VERIFICATION, sendForgotPinVerification, api),
    ]
}
  