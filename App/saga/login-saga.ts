import { all, call, delay, put, putResolve, select, takeLatest, take } from 'redux-saga/effects'
import { ApiAction, ApiLogin } from '../services'
import AuthActions from '../redux/auth-redux'
import LoginActions, { LoginTypes } from '../redux/login-redux'
import { MessagingSelectors } from '../redux/messaging-redux'
import { getUniqueId } from 'react-native-device-info'
import LocationActions, { LocationSelectors, LocationTypes } from '../redux/location-redux'
import { Auth } from '../models'
import api from '../services/shared/login-service'
import BeaconManagerActions, { BeaconManagerSelectors } from '../redux/beacon-manager-redux'
import BankActions, { BankTypes } from '../redux/bank-redux'
import DrawingActions, { DrawingTypes } from '../redux/drawing-redux'
import crashlytics from '@react-native-firebase/crashlytics';
import {Platform} from "react-native";

function * signIn (api: ApiLogin, action: ApiAction) {
    let uniqueId = getUniqueId();
    let token  = yield select(MessagingSelectors.token)
    let coords = yield select(LocationSelectors.coordinate);
    const { userName, pin } = action
    const response = yield call(api.signIn, userName, pin, token, uniqueId, coords)

    yield put(
        response.matchWith({
            Ok: ({ value }) => LoginActions.signInSuccess(value),
            Error: ({ value }) => LoginActions.signInFailure(value)
        })
    )
}

function * signInSuccess (api: ApiLogin, action: ApiAction) {
    const { auth } = action.payload
    const coordinate = yield select(LocationSelectors.coordinate);
    const authSuccess: Auth = {
        accountId: auth.accountId,
        authData: auth.authData,
        verified: auth.verified,
        contact: auth.contact,
        flags: auth.flags,
        hideTestLocations: auth.hideTestLocations,
    }
    console.log("login-saga: authSuccess " , authSuccess)
    yield put(AuthActions.signInAuthSuccess(authSuccess))

    const isCurrentlyMonitoring = yield select(BeaconManagerSelectors.isRunning)
    if (__DEV__) {
        console.log("login-saga: isCurrentlyMonitoring: ", isCurrentlyMonitoring)
    }
    if(authSuccess && authSuccess.authData){

        crashlytics().setUserId(`${authSuccess.accountId}`).then(r => console.log('crashlytics setup account id'))
        crashlytics().setAttribute('authData', authSuccess.authData).then(r => console.log('crashlytics setup attribute data'))

        yield put(DrawingActions.getRecentDrawings())
        yield take(DrawingTypes.GET_RECENT_DRAWINGS_SUCCESS)
        yield put(BankActions.getBankEntries())
        yield take(BankTypes.GET_BANK_ENTRIES_SUCCESS)

        console.log('login-saga: starting beacon...')
        if (!isCurrentlyMonitoring)
            yield putResolve(BeaconManagerActions.startBeaconMonitoring())
        else {
            yield putResolve(BeaconManagerActions.stopBeaconMonitoring())
            yield delay(1000)
            yield putResolve(BeaconManagerActions.startBeaconMonitoring())
        }
    }

    yield put(LocationActions.getLocationBeaconsSuccess(auth.locations))
    yield put(LocationActions.getLocationBeacons(coordinate))
}

export default function() {
    return [
        takeLatest(LoginTypes.SIGN_IN_SUCCESS,  signInSuccess, api),
        takeLatest(LoginTypes.SIGN_IN,  signIn, api),
    ]
}
