import { all, call, put, select, takeLatest } from 'redux-saga/effects'
import { ApiAction, ApiTOS } from '../services'
import { AuthSelectors } from '../redux/auth-redux'
import TOSActions, { TOSTypes } from '../redux/tos-redux'
import api from '../services/shared/tos-service'

function * getTOSContent (api: ApiTOS, action: ApiAction) {
    const response = yield call(api.getTOSContent)
    yield put(
        response.matchWith({
            Ok: ({ value }) => TOSActions.getTOSContentSuccess(value),
            Error: ({ value }) => TOSActions.getTOSContentFailure(value)
        })
    )
}

export default function() {
    return [
        takeLatest(TOSTypes.GET_TOS_CONTENT,  getTOSContent, api),
    ]
}
  