import {all, call, put, select, take, takeLatest, takeEvery, delay, putResolve} from 'redux-saga/effects'
import {Platform} from "react-native";
import { eventChannel, END } from 'redux-saga'
import { ApiAction, ApiDashboard } from '../services'
import AuthActions, { AuthSelectors } from '../redux/auth-redux'
import ProfileActions, { ProfileSelectors, ProfileTypes } from '../redux/profile-redux'
import DashboardActions, { DashboardTypes } from '../redux/dashboard-redux'
import { MobileSelectors } from '../redux/mobile-redux'
import LocationActions from '../redux/location-redux'
import BankActions, { BankSelectors } from '../redux/bank-redux'
import BeaconManagerActions, {BeaconManagerSelectors} from '../redux/beacon-manager-redux'
import api from '../services/shared/dashboard-service'
import { Mobile } from '../models'
import BackgroundTimer from 'react-native-background-timer'

const onTimerTickChannel = (duration: number, lastPaused: number = null) =>
  eventChannel(emitter => {
      let startTime = lastPaused
      const timerId = setInterval(async () => {
        const currentTime = Number(Date.now())
        const elapse = (currentTime - startTime) / 1000
        let tick = duration - elapse
        let tickPercentage = (elapse/duration) * 100

        emitter(100 <= tickPercentage ? 100 : tickPercentage)
        if (tick > 0) {}
        else {
            // emitter(END)
            startTime = Number(Date.now())
        }
      }, 120)

      return () => {
        clearInterval(timerId)
      }
  })

function * startEarningTimer (api: ApiDashboard, action: ApiAction) {
    console.log('dashboard-saga: Started Earning for display...')
    yield put(BankActions.updateBankEntries())
    yield put(DashboardActions.stopEarningTimer())
    const mobile = yield select(MobileSelectors.mobile)
    if (mobile) {
        let shouldStart = false
        let duration = 120
        let startTime = yield select(MobileSelectors.currentCheckInPeriodTime)
        let earningTimerTick = yield call(onTimerTickChannel, duration, startTime)
        yield takeEvery(earningTimerTick, function* (tick: number) {
              if (shouldStart) {
                  const mobile: Mobile = yield select(MobileSelectors.mobile)
                  const isLoading = yield select(BankSelectors.isLoading)
                  if (!isLoading && mobile) {
                      if (tick >= 100)
                          yield put(BankActions.updateBankEntries())
                  }

                  if (mobile && mobile.beacon)
                      yield put(DashboardActions.setTimerTick(tick))
                  else{
                      console.log('dashboard-saga: startEarningTimer.earningTimerTick.close()')
                      earningTimerTick.close()
                  }
              }
              shouldStart = true
        })

        yield take(DashboardTypes.STOP_EARNING_TIMER)
        earningTimerTick.close()
        console.log('dashboard-saga: Stopped Earning for display...')
    }
}

function * verifyEmailUpdate (api: ApiDashboard, action: ApiAction) {
    const { profile } = action
    const auth = yield select(AuthSelectors.auth)
    yield put(DashboardActions.verifyEmailSuccess(profile.verified))
    yield put(AuthActions.signInAuthSuccess({ ...auth, verified: profile.verified}))

    yield delay(5000)
    yield put(ProfileActions.updateProfile(profile))
}

function * verifyEmail (api: ApiDashboard, action: ApiAction) {
    const { code } = action
    const profile = yield select(ProfileSelectors.profile)
    const response = yield call(api.verifyEmail, profile.emailAddress, code)

    yield put(
        response.matchWith({
            Ok: ({ value }) => DashboardActions.verifyEmailUpdate({ ...profile, verified: value }),
            Error: ({ value }) => DashboardActions.verifyEmailFailure(value)
        })
    )
}

function * sendEmailVerification (api: ApiDashboard, action: ApiAction) {
    const auth = yield select(AuthSelectors.auth)
    const profile = yield select(ProfileSelectors.profile)
    const response = yield call(api.sendEmailVerification, profile.emailAddress, auth)

    yield put(
        response.matchWith({
            Ok: ({ value }) => DashboardActions.sendEmailVerificationSuccess(value),
            Error: ({ value }) => DashboardActions.sendEmailVerificationFailure(value)
        })
    )
}


function * getDashboardDetails (api: ApiDashboard, action: ApiAction) {
    const auth = yield select(AuthSelectors.auth)
    const profile = yield select(ProfileSelectors.profile)
    const response = yield call(api.getDashboardDetails, profile.emailAddress, auth)

    yield put(
        response.matchWith({
            Ok: ({ value }) => DashboardActions.getDashboardDetailsSuccess(value),
            Error: ({ value }) => DashboardActions.getDashboardDetailsFailure(value)
        })
    )
}

export default function() {
    return [
        takeLatest(DashboardTypes.START_EARNING_TIMER, startEarningTimer, api),
        takeLatest(DashboardTypes.VERIFY_EMAIL_UPDATE, verifyEmailUpdate, api),
        takeLatest(DashboardTypes.VERIFY_EMAIL, verifyEmail, api),
        takeLatest(DashboardTypes.SEND_EMAIL_VERIFICATION, sendEmailVerification, api),
        takeLatest(DashboardTypes.GET_DASHBOARD_DETAILS, getDashboardDetails, api),
    ]
}
