import { all, call, delay, put, select, take, takeLatest, takeEvery } from 'redux-saga/effects'
import { eventChannel, END } from 'redux-saga'
import { ApiAction, ApiMobile } from '../services'
import AuthActions, { AuthTypes, AuthSelectors } from '../redux/auth-redux'
import { ProfileSelectors } from '../redux/profile-redux'
import LocationActions, { LocationSelectors, LocationTypes } from '../redux/location-redux'
import HomeActions from '../redux/home-redux'
import DashboardActions from '../redux/dashboard-redux'
import BankActions from '../redux/bank-redux'
import MessagingActions, { MessagingSelectors } from '../redux/messaging-redux'
import MobileActions, { MobileSelectors, MobileTypes } from '../redux/mobile-redux'
import { Auth, Status, Profile, Mobile, Drawing, Beacon, Coordinate, Message, LocationBeacon, LocationPermissionStatus, } from '../models'
import api from '../services/shared/mobile-service'
import { strings } from '../localization/strings'
import { Platform } from 'react-native'
import Preference from 'react-native-preference'
import Utils from '../services/shared/utils/Utils'
import { globals } from '../constants'
import { Location } from '../models'
import DrawingActions, { DrawingTypes, DrawingSelectors } from '../redux/drawing-redux'
import {Checkin} from "../models/checkin";
import {Checkout} from "../models/checkout";
import BackgroundGeolocation from 'react-native-background-geolocation'
import { AppStateSelectors } from '../redux/app-state-redux'

const onLocationProviderChanged = () =>
  eventChannel(emitter => {
    BackgroundGeolocation.getProviderState((data) => {
        emitter(data)
    })

    return () => {
    }
  })

function * checkIn (api: ApiMobile, action: ApiAction) {
    const { locationBeacon }:{ locationBeacon: LocationBeacon } = action
    const auth: Auth = yield select(AuthSelectors.auth)
    const currentLocationCoordinate: Coordinate = yield select(LocationSelectors.coordinate)

    if (auth && currentLocationCoordinate && locationBeacon) {
        const onCurrentPositionChangedChannel  = yield call(onLocationProviderChanged)
        yield takeLatest(onCurrentPositionChangedChannel, function* (provider: Status) {
            onCurrentPositionChangedChannel.close()

            const disabledPreciseLocation = Boolean(provider?.accuracyAuthorization)
            let isCheckinInvalid = disabledPreciseLocation
            if (provider?.status === LocationPermissionStatus.AskNextTime)
                isCheckinInvalid = true
            else if (provider?.status === LocationPermissionStatus.Never)
                isCheckinInvalid = true

            if (isCheckinInvalid)
                return


            yield put(MobileActions.attemptCheckIn(true))
            const beacon: Beacon = locationBeacon.beacon
            const coordinate: Coordinate = {
                ...currentLocationCoordinate,
            }
            const mobile: Mobile = {
                accountId: `${auth.accountId}`,
                keychain: auth.authData,
                beacon: beacon,
                coordinate: coordinate,
                existingCheckinId: -1,
            }
            const response = yield call(api.checkIn, mobile, auth)


            // We get env if there is a saved record and use it as prefix in the topic
            const env = Preference.get("ENV")

            const checkoutTopic = env ? `${env}-checkout`: 'checkout'
            yield put(MessagingActions.unsubscribeMessagingOnTopicMessage(checkoutTopic))

            const topic = env ? `${env}-checkin` : 'checkin'
            yield put(MessagingActions.subscribeMessagingOnTopicMessage(topic))

            // check if within statewide drawing session
            const isStateWideDrawingExpired = yield select(DrawingSelectors.isStateWideDrawingExpired)
            console.log('mobile-saga: Checking if we are still within statewide drawing session ' , !isStateWideDrawingExpired)
            if (!isStateWideDrawingExpired){
                yield put(DashboardActions.clearData())
                yield put(
                    response.matchWith({
                        Ok: ({ value }) => {

                            return MobileActions.checkInStartEarning(value, null)
                        },
                        Error: ({ value }) => {
                            const pendingCheckin: Checkin = {
                                locationBeacon: locationBeacon,
                                dateTime: Date.now(),
                            }
                            console.log("mobile-saga: Saving pendingCheckin data due to no session: " , pendingCheckin)
                            return MobileActions.checkInFailure(value, pendingCheckin)
                        }
                    })
                )
            }else{
                const pendingCheckin: Checkin = {
                    locationBeacon: locationBeacon,
                    dateTime: Date.now(),
                }
                console.log("mobile-saga: Saving pendingCheckin data due to no session: " , pendingCheckin)
                yield put(MobileActions.pendingCheckin(pendingCheckin))
            }
        })
    }
}

function * checkOut (api: ApiMobile, action: ApiAction) {
    const { isBluetoothEnabled, isSilent, force, event, beacon } = action
    const auth: Auth = yield select(AuthSelectors.auth)
    const currentMobile: Mobile = yield select(MobileSelectors.mobile)
    const currentLocationCoordinate = yield select(LocationSelectors.coordinate)

    if (auth && currentLocationCoordinate) {
        if(!isSilent) {
            var bluetoothMessage = ``

            if(!isBluetoothEnabled){
                bluetoothMessage = strings.bluetooth_enable_notification_message
                if (Platform.OS === 'android' ){
                    bluetoothMessage = strings.bluetooth_enable_notification_android_message
                }
            }
            const message: Message = {
                notification: {
                    title: strings.checkin_notification_title,
                    body: `${strings.checkout_notification_message} ${bluetoothMessage}`
                }
            }
            yield put(MessagingActions.sendMessagingOnMessage(message))

            const env = Preference.get("ENV")

            const checkInTopic = env ? `${env}-checkin`: 'checkin'
            yield put(MessagingActions.unsubscribeMessagingOnTopicMessage(checkInTopic))

            const topic = env ? `${env}-checkout`: 'checkout'
            yield put(MessagingActions.subscribeMessagingOnTopicMessage(topic))
        }
        if (currentMobile && currentMobile.beacon ){
            yield put(LocationActions.saveLocationCheckin(currentMobile.beacon, "checkout"))
        }

        const coordinate: Coordinate = {
            ...currentLocationCoordinate,
        }
        const mobile: Mobile = {
            accountId: `${auth.accountId}`,
            keychain: auth.authData,
            coordinate: coordinate,
            existingCheckinId: -1,
            extra: {
                event: event,
                isBluetoothEnabled: isBluetoothEnabled,
                isSilent: isSilent,
                isForce: force,
                beacon: beacon,
            }
        }
        yield put(MobileActions.checkOutSuccess(null, null))
        const response = yield call(api.checkOut, mobile, auth)

        yield put(
            response.matchWith({
                Ok: ({ value }) => {
                    // remove everything that is pending of checkout event
                    return MobileActions.checkOutSuccess(value, null)
                },
                Error: ({ value }) => {

                    // no need to store pending checkout since it says 404, no records found
                    if(value?.statusCode === 404){
                        return MobileActions.checkOutFailure(value, null)
                    }
                    // checkout data is confirmed true so we set as 'queue'
                    const pendingCheckout: Checkout = {
                        value: true,
                        data: {
                            event: "queue",
                            isBluetoothEnabled: isBluetoothEnabled,
                            isSilent: true,
                            isForce: force,
                            beacon: beacon
                        }
                    }
                    return MobileActions.checkOutFailure(value, pendingCheckout)

                }
            })
        )

    }
    yield delay(200)
    yield put(MobileActions.checkInReset())

}

function * checkInStartEarning (api: ApiMobile, action: ApiAction) {
    const { mobile, pendingCheckin } = action

    console.log('mobile-saga: * checkInStartEarning ', mobile, pendingCheckin )
    const coordinate: Coordinate = yield select(LocationSelectors.coordinate)
    const recentDrawings: Drawing[] = yield select(DrawingSelectors.recentDrawings)
    const stateWideDrawing: Drawing = recentDrawings?.find((drawing) => !drawing.locationId)
    const updateLocations: Location[] = mobile.locations
    const updateMobile: Mobile = {
        accountId: mobile.accountId,
        keychain: mobile.keychain,
        action: mobile.action,
        beacon: mobile.beacon,
        coordinate: mobile.coordinate,
        existingCheckinId: mobile.existingCheckinId,
        email: mobile.email,
        pin: mobile.pin,
        checkinId: mobile.checkinId,
        locationId: mobile.locationId,
        drawing: stateWideDrawing,
    }
    const message: Message = {
        notification: {
            title: strings.checkin_notification_title,
            body: strings.checkin_notification_message
        },
    }

    yield put(LocationActions.saveLocationCheckin(updateMobile.beacon, "checkin"))
    yield put(MessagingActions.sendMessagingOnMessage(message))
    yield put(MobileActions.lastCheckInPeriodTime(Date.now()))
    yield put(MobileActions.checkInSuccess(updateMobile, pendingCheckin))
    yield delay(100)
    yield put(DashboardActions.startEarningTimer())
    yield put(LocationActions.getLocationBeacons(coordinate))
    yield take(LocationTypes.GET_LOCATION_BEACONS_SUCCESS)
    yield put(DrawingActions.getRecentDrawings())
    yield take(DrawingTypes.GET_RECENT_DRAWINGS_SUCCESS)
    yield put(AuthActions.getAuthTestLocations())
}

function * checkInContinueEarning (api: ApiMobile, action: ApiAction) {
    const { mobile, pendingCheckin } = action
    yield put(MobileActions.checkInSuccess(mobile, pendingCheckin))
    yield delay(500)
    yield put(BankActions.updateBankEntries())
}

export default function() {
    return [
        takeLatest(MobileTypes.CHECK_IN_START_EARNING,  checkInStartEarning, api),
        takeLatest(MobileTypes.CHECK_IN_CONTINUE_EARNING,  checkInContinueEarning, api),
        takeLatest(MobileTypes.CHECK_IN,  checkIn, api),
        takeLatest(MobileTypes.CHECK_OUT,  checkOut, api),
    ]
}
