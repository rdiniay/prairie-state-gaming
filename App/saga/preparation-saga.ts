import { call, put, takeLatest, take, select, } from 'redux-saga/effects'
import { eventChannel } from 'redux-saga'
import PreparationActions, { PreparationSelectors, PreparationTypes } from '../redux/preparation-redux'
import LocationActions, { LocationSelectors } from '../redux/location-redux'
import { Coordinate, LocationPermissionStatus } from '../models'
import BackgroundGeolocation, {ProviderChangeEvent, Config } from 'react-native-background-geolocation'
import BluetoothStateManager from 'react-native-bluetooth-state-manager';
import NetInfo from "@react-native-community/netinfo";
import api from '../services/shared/messaging-service'
import { getSystemVersion  } from 'react-native-device-info'
import {check, PERMISSIONS, RESULTS} from 'react-native-permissions'
import { Alert, Platform } from 'react-native'
import AndroidBackgroundBeacons from '../background-task/beacon/AndroidBeaconMonitoringTask'

const onGetPreparations = () =>
  eventChannel(emitter => {
    BackgroundGeolocation.getProviderState(data => {
        console.log("getProviderState", data)
        let geoLocEnabled = data?.enabled
        let permissionStatus = data?.status
        let isPreciseLocation: Boolean = !data?.accuracyAuthorization
        let isGranted: Boolean = ((permissionStatus === LocationPermissionStatus.Always || 
                                    permissionStatus === LocationPermissionStatus.WhileUsingTheApp) &&
                                    isPreciseLocation)
                                    
        BluetoothStateManager.getState().then( (bluetoothState) => {
            let isBluetoothPoweredOn: Boolean = false
            let isNearbyDevicesEnabled: Boolean = false
            switch (bluetoothState) {
                case 'PoweredOn': {
                    isBluetoothPoweredOn = true
                    break;
                }
                case 'PoweredOff':
                    isBluetoothPoweredOn = false
                    break;
            }

            if (Platform.OS === 'android'){
                if (parseInt(getSystemVersion()) >= 12 ) {
                    // nearby devices check permission
                    let bluetoothConnect: Boolean = false
                    let bluetoothScan: Boolean = false
                    check(PERMISSIONS.ANDROID.BLUETOOTH_CONNECT)
                        .then((result) => {
                            switch (result) {
                            case RESULTS.GRANTED:
                                bluetoothConnect = true
                                break;
                            default:
                                bluetoothConnect = false
                                break;
                            }
                    }).then( () => {
                        check(PERMISSIONS.ANDROID.BLUETOOTH_SCAN)
                            .then((result) => {
                                switch (result) {
                                case RESULTS.GRANTED:
                                    bluetoothScan = true
                                    break;
                                default:
                                    bluetoothScan = false
                                    break;
                                }

                                isNearbyDevicesEnabled = bluetoothConnect && bluetoothScan

                                api.hasMessagingPermission().then(enable => {
                                    NetInfo.fetch().then(networkState => {
                                        emitter({ 
                                            gps: isGranted, 
                                            network: networkState.isInternetReachable ?? networkState.isConnected,
                                            geoLocation: geoLocEnabled,
                                            bluetooth: isBluetoothPoweredOn,
                                            notification: enable,
                                            nearbyDevices: isNearbyDevicesEnabled,
                                        })
                                    })
                                })
                            })
                    })
                }
                else {
                    api.hasMessagingPermission().then(enable => {
                        NetInfo.fetch().then(networkState => {
                            emitter({ 
                                gps: isGranted, 
                                network: networkState.isInternetReachable ?? networkState.isConnected,
                                geoLocation: geoLocEnabled,
                                bluetooth: isBluetoothPoweredOn,
                                notification: enable,
                            })
                        })
                    })
                }
            }
            else {
                api.hasMessagingPermission().then(enable => {
                    NetInfo.fetch().then(networkState => {
                        emitter({ 
                            gps: isGranted, 
                            network: networkState.isInternetReachable ?? networkState.isConnected,
                            geoLocation: geoLocEnabled,
                            bluetooth: isBluetoothPoweredOn,
                            notification: enable,
                        })
                    })
                })
            }
        })
            
    })

    return () => {
    }
})

const onSubscribeNetworkState = () => 
    eventChannel(emitter => {

        const unsubscribe = NetInfo.addEventListener((networkState) => {
            emitter("")
        })        

        return () => unsubscribe
    })


const onSubscribeBluetoothState = () =>
  eventChannel(emitter => {
    const unsubscribe = BluetoothStateManager.onStateChange((bluetoothState) => {
        // do something...
        emitter(bluetoothState)
    }, true /*=emitCurrentState*/)

    return () => unsubscribe
  })

const onSubscribeGeolocationState = () =>
  eventChannel(emitter => {

    /**
     * Return ProviderChangeEvent object
     * 
     * Location: Off = ProviderChangeEvent object 
     *    {
     *      accuracyAuthorization: 0
     *      enabled: false
     *      gps: false
     *      network: false
     *      status: 4
     *    }
     * 
     * Location: ON = ProviderChangeEvent 
     *    {
     *      accuracyAuthorization: 0
     *      enabled: true
     *      gps: false
     *      network: true
     *      status: 4
     *    }
     *     
     */
    const providerChange: any = BackgroundGeolocation.onProviderChange(async (event: ProviderChangeEvent) => {
        console.log("Monitoring: onProviderChange: ", event)
        emitter(event.enabled)
    });

    /// 2. ready the plugin.
    BackgroundGeolocation.ready(AndroidBackgroundBeacons.Config).then((state) => {
        //setEnabled(state.enabled)
        console.log("- BackgroundGeolocation is configured and ready: ", state.enabled);
    });

    return () =>  providerChange?.remove()
  })

function * subscribeBluetoothState () {
    const onSubscribeNetworkStateChannel  = yield call(onSubscribeNetworkState)
    const onSubscribeBluetoothStateChannel  = yield call(onSubscribeBluetoothState)

    yield takeLatest(onSubscribeBluetoothStateChannel, function* (preparationResult: any) {
        yield put(PreparationActions.getPreparations())
    })
    yield takeLatest(onSubscribeNetworkStateChannel, function* (isInternetReachable: any) {
        yield put(PreparationActions.getPreparations())
    })
    
    yield take(PreparationTypes.UNSUBSCRIBE_BLUETOOTH_STATE)
    onSubscribeBluetoothStateChannel.close()
    onSubscribeNetworkStateChannel.close()
}

function * subscribeGeolocationState () {
    const onSubscribeGeolocationStateChannel  = yield call(onSubscribeGeolocationState)

    yield takeLatest(onSubscribeGeolocationStateChannel, function* (isEnabled: any) {
        yield put(PreparationActions.getPreparations())
    })
    
    yield take(PreparationTypes.UNSUBSCRIBE_GEOLOCATION_STATE)
    onSubscribeGeolocationStateChannel.close()
}

function * getPreparations () {
    const onGetPreparationsChannel  = yield call(onGetPreparations)

    yield takeLatest(onGetPreparationsChannel, function* (preparationResult: any) {
        console.log("preparationResult", preparationResult)
        //let coordinate: Coordinate = yield select(LocationSelectors.coordinate)
        let preparations: any = { 
            ...preparationResult//, 
            //geolocation: coordinate?.latitude ? true : false 
        }
        yield put(PreparationActions.getPreparationsSuccess(preparations))
        onGetPreparationsChannel.close()
    })
    
}

export default function() {
    return [
        takeLatest(PreparationTypes.SUBSCRIBE_GEOLOCATION_STATE, subscribeGeolocationState),
        takeLatest(PreparationTypes.SUBSCRIBE_BLUETOOTH_STATE, subscribeBluetoothState),
        takeLatest(PreparationTypes.GET_PREPARATIONS, getPreparations),
    ]
}