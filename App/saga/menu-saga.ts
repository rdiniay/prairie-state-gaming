import { all, call, delay, put, select, takeLatest } from 'redux-saga/effects'
import { ApiAction, ApiMenu } from '../services'
import AuthActions, { AuthSelectors } from '../redux/auth-redux'
import RegistrationActions from '../redux/registration-redux'
import HomeActions from '../redux/home-redux'
import DashboardActions from '../redux/dashboard-redux'
import MenuActions, { MenuTypes } from '../redux/menu-redux'
import BankActions from '../redux/bank-redux'
import MobileActions from '../redux/mobile-redux'
import DrawingActions from '../redux/drawing-redux'
import api from '../services/shared/menu-service'
import LocationActions, { LocationSelectors } from '../redux/location-redux'
import MessagingActions from '../redux/messaging-redux'
import BeaconManagerActions, { BeaconManagerSelectors } from '../redux/beacon-manager-redux'
import Preference from 'react-native-preference'
import PushNotification  from 'react-native-push-notification'
import BackgroundActions from '../redux/background-fetch-redux'
import {BackgroundFetchTypes} from "../redux/background-fetch-redux"
import BackgroundFetch from "react-native-background-fetch"
import { Platform } from 'react-native'

function * getAbout (api: ApiMenu, action: ApiAction) {
    const auth = yield select(AuthSelectors.auth)
    const response = yield call(api.getAbout, auth)

    yield put(
        response.matchWith({
            Ok: ({ value }) => MenuActions.getAboutSuccess(value),
            Error: ({ value }) => MenuActions.getAboutFailure(value)
        })
    )
}

function * signOut (api: ApiMenu, action: ApiAction) {
    PushNotification.cancelAllLocalNotifications()

    const env = Preference.get("ENV")

    const adhocTopic = env ? `${env}-adhoc`: 'adhoc'
    const checkInTopic = env ? `${env}-checkin`: 'checkin'
    const checkOutTopic = env ? `${env}-checkout`: 'checkout'
    yield put(MessagingActions.unsubscribeMessagingOnTopicMessage(adhocTopic))
    yield put(MessagingActions.unsubscribeMessagingOnTopicMessage(checkInTopic))
    yield put(MessagingActions.unsubscribeMessagingOnTopicMessage(checkOutTopic))
    yield put(MessagingActions.unsubscribeMessagingOnBackgroundMessage())

    Preference.clear("ENV").then(r => {
        console.log("Preference clear ENV ", r)
    })

    yield put(DashboardActions.stopEarningTimer())
    yield put(BeaconManagerActions.stopBGTask())
    yield delay(200)
    yield put(BeaconManagerActions.stopBeaconMonitoring())
    yield delay(300)
    yield put(DashboardActions.clearData())
    yield put(AuthActions.signInAuthSuccess(null))
    yield put(RegistrationActions.createAccountSuccess(null))
    yield put(BankActions.resetBankEntries())
    yield put(BankActions.getBankEntriesSuccess(null))
    yield put(DrawingActions.getRecentDrawingsSuccess(null))
    yield put(LocationActions.clear())

    yield put(BackgroundActions.stopBackgroundFetchMonitoring())
    yield put(MobileActions.checkInReset())
    // buggy if not reset to null
    yield put(MobileActions.checkOutSuccess(null, null))



    const taskId = Platform.OS === 'ios' ? `com.rcrewards.penn` : `com.redcarpetrewards.penn`
    BackgroundFetch.stop(taskId)
        .then(result => { console.log(`Stopping background fetch: ${result}`) })

    yield put(DashboardActions.stopEarningTimer())
}

export default function() {
    return [
        takeLatest(MenuTypes.GET_ABOUT,  getAbout, api),
        takeLatest(MenuTypes.SIGN_OUT,  signOut, api),
    ]
}
