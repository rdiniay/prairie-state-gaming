import {all, call, delay, put, putResolve, select, take, takeEvery, takeLatest} from 'redux-saga/effects'
import { DeviceEventEmitter, Platform } from 'react-native'
import { eventChannel, END } from 'redux-saga'
import { ApiAction, ApiBank } from '../services'
import { AuthSelectors } from '../redux/auth-redux'
import BeaconManagerActions, { BeaconManagerSelectors, BeaconManagerTypes } from '../redux/beacon-manager-redux'
import { MobileSelectors } from '../redux/mobile-redux'
import crashlytics from '@react-native-firebase/crashlytics';
import HomeActions from '../redux/home-redux'
import DashboardActions from '../redux/dashboard-redux'
import BankActions from '../redux/bank-redux'
import MobileActions from '../redux/mobile-redux'
import LocationActions, { LocationSelectors, LocationTypes } from '../redux/location-redux'
import BackgroundFetchActions, {BackgroundFetchSelectors} from '../redux/background-fetch-redux'
import api from '../services/shared/location-service'
import { Auth, Mobile, Drawing, Beacon, Coordinate, Location, LocationBeacon } from '../models'
import MessagingActions from '../redux/messaging-redux'
import iOSBackgroundBeacons from '../background-task/beacon/iOSBeaconMonitoringTask'
import AndroidBackgroundBeacons from '../background-task/beacon/AndroidBeaconMonitoringTask'
import Utils from '../services/shared/utils/Utils'
import * as GLOBAL from '../constants'
import BluetoothStateManager from 'react-native-bluetooth-state-manager';
import Preference from "react-native-preference";
import {DrawingSelectors} from "../redux/drawing-redux";
import {Checkout} from "../models/checkout";
import {Checkin} from "../models/checkin";
import {Status} from "../models/status";

const backgroundiOSTask = (auth: Auth) =>
  eventChannel(emitter => {
        iOSBackgroundBeacons.BeginMonitoringTask((data) => {
            emitter(data)
        }, auth)
      return () => {
        iOSBackgroundBeacons.StopMonitoringTask()
      }
})

const backgroundAndroidTask = (auth: Auth) =>
    eventChannel(emitter => {
        console.log("AndroidBackgroundBeacons BeginMonitoringTask....")
        AndroidBackgroundBeacons.BeginMonitoringTask((data) => {
            emitter(data)
        }, auth)
    return () => {
        AndroidBackgroundBeacons.StopMonitoringTask()
    }
})

function * startBeaconMonitoring (action: ApiAction) {
    if(__DEV__){
        console.log("STARTING MONITORING...")
    }
    yield delay(1000)
    yield put(LocationActions.subscribeLocationProviderChanged())
    yield put(DashboardActions.startEarningTimer())
    yield put(BeaconManagerActions.isCurrentlyMonitoring(true))
    if (__DEV__) {
        const isCurrentlyMonitoring = yield select(BeaconManagerSelectors.isRunning)
        console.log("beacon-manager-saga: isCurrentlyMonitoring: ", isCurrentlyMonitoring)
    }
    const auth:Auth = yield select(AuthSelectors.auth)


    let backgroundTask = Platform.OS === 'ios' ? backgroundiOSTask : backgroundAndroidTask
    let backgroundTaskListener = yield call(backgroundTask, auth)
    yield takeEvery(backgroundTaskListener, function* (data: any) {
        yield put(BeaconManagerActions.checkInAttemptDone(false))
        const event = data.event
        let logTrace: string = ".1"

        const isBluetoothEnabled: Boolean = data.isBluetoothEnabled
        let beacons: Beacon[] = data.beacons
        yield put(BeaconManagerActions.beaconsDetected(beacons))

        if(data.beacons){
            console.log("Beacons retrieved before sorting and filtering: ", beacons)
            if(data.beacons?.length > 1){
                // sort by smallest distance as first
                beacons = data.beacons?.sort( (b1, b2) => b1?.distance - b2?.distance)
            }
            // remove beacon that has beacon distance of -1
            beacons = beacons?.filter(beacon => beacon?.distance !== -1)
            console.log("Beacons retrieved after sorting and filtering: ", beacons)
            logTrace += ".2"
        }

        const isLoading: Boolean = yield select(MobileSelectors.isLoading)
        const mobile: Mobile = yield select(MobileSelectors.mobile)
        const auth:Auth = yield select(AuthSelectors.auth)
        const beaconProximity = data?.beaconProximity

        let coordinate: Coordinate = yield select(LocationSelectors.coordinate)
        // new location has been received from background geolocation #onLocationChanged
        // high prio data
        if(data?.newLocation){
            const {latitude, longitude, accuracy, altitude} = data.newLocation.coords
            const status: Status = {
                accuracyAuthorization: data?.newLocation?.provider?.accuracyAuthorization,
                enabled: data?.newLocation?.provider?.enabled,
                gps: data?.newLocation?.provider?.gps,
                network: data?.newLocation?.provider?.network,
                status: data?.newLocation?.provider?.status
            }
            coordinate = {  latitude, longitude, accuracy, altitude, status }
            yield put(LocationActions.getCurrentLocationSuccess(coordinate))
            logTrace += ".3"
        }

        // fetch locations that was stored in the database
        const locations: Location[] = yield select(LocationSelectors.locations)
        if (!locations)  {
            logTrace += ".4"
            yield put(BeaconManagerActions.logTrace(logTrace))
            yield put(BeaconManagerActions.checkInAttemptDone(true))
            return
        }
        

        // find all applicable beaconList records that are coming from locations data
        // the purpose of this is to find acceptable beacon that matches from the actual beacon device that sends and transmits its information for us to use against the
        // location beacons data stored from the database
        const beaconList: Beacon[] = locations.map((location: Location) => location.beacons? location.beacons : []).flat();


        // find all location + beacon combo data
        const locationBeaconList: LocationBeacon[] = locations.map((location: Location)=> {
            if (location.beacons && location.active){
                return location.beacons.map((beacon)=> {
                    const LocationBeacon: LocationBeacon = {
                        id: Number(location.id),
                        points: location.points,
                        beacon: beacon,
                        clientExcluded: location.clientExcluded,
                    }
                    return LocationBeacon
                });
            }
            return [];
        }).flat();

        // here we determine to process checkin or checkout
        // if we have existing data from mobile checkin then determine if we are receiving checkin data
        // checkin data are events that includes `beaconsDidRange`, `regionDidExit`, `regionStateOutside`  and beacons is not empty
        // beaconsDidRange is indication that the user is close to the beacon
        // regionDidExit is an indication that the beacon pushed an event that the user is far away from the beacon proximity
        // regionStateOutside is an indication that there's user intervention that the earning will stop (eg., turn off bluetooth and etc.)

        if(__DEV__ && beacons && beacons.length > 0){
            console.log("Beacons received: " , beacons)
        }

        let shouldCheckIn = true
        let forceCheckout = false
        // we have determined here that app is currently checked in so we process further below if the event is referring to checking out.
        if (mobile && mobile.checkinId){
            logTrace += ".5"
            // exception to the rule that if we still mobile.beacon and we received a transmission of `beaconsDidRange` then we must checkout and checkin again
            // this is only possible when the location is so close that it quickly detected another beacon in sight
            // but this should not always happen
            // if(event.includes("beaconsDidRange") && beacons){
            // //     // if we are currently attempting to check in while we are currently checkin - this should not always happen
            // // else if (mobile.beacon && (acceptableBeacon.major !== mobile.beacon.major && acceptableBeacon.minor !== mobile.beacon.minor)) {
            // //         yield put(MobileActions.checkIn(acceptableLocationBeacon))
            // //     }
            //
            //     return
            // }
            shouldCheckIn = false
            let shouldCheckout = false
            // only used in pending checkout
            let isSilent = false

            // We want to make sure if it is necessary to checkout when there is still a mobile beacon data and beacon region states says outside or undefined meaning no beacon within users range
            // But we are possibly inside the fence otherwise check the user out
            if (event.includes("regionStateOutside") ){
                logTrace += ".6"
                shouldCheckout = true

                // data?.force is used when user selects location permission
                // 1. Ask once
                // 2. Never
                if(!isBluetoothEnabled || data?.force){
                    forceCheckout = true
                    logTrace += ".7"
                }
            }
            else if(event.includes("locationChanged") ){
                logTrace += ".8"
                shouldCheckout = true
                isSilent = true

                // isolate to iOS for now because somehow in Android, it executes location changed and beacon proximity outside even though its inside
                if(Platform.OS === 'ios' && beaconProximity && beaconProximity === GLOBAL.globals.Proximity.OUTSIDE){
                    forceCheckout = true
                    logTrace += ".9"
                }
            }
            // the monitoring service transmitted a region did exit event then we have to flag it as possible checkout
            else if (event.includes('regionDidExit')){
                shouldCheckout = true
                logTrace += ".10"
            }

            // Will override all conditions from the above
            // Check if we are still receiving the same beacon from mobile.checkin data
            if((event.includes('beaconsDidRange') && beacons && beacons.length > 0 ) || (beacons && beacons.length > 0)) {
                const acceptableBeaconStillMatchesCheckin = beacons.find((beaconItem) => mobile.beacon.major === beaconItem.major && mobile.beacon.minor === beaconItem.minor)
                if(!acceptableBeaconStillMatchesCheckin){
                    shouldCheckout = true
                    isSilent = true
                    logTrace += ".11"
                }
                else {
                    // return here since we are still in proximity to the beacon that matches checkin session
                    // remove pending checkout because its irrelevant
                    yield putResolve(MobileActions.pendingCheckout(null))
                    shouldCheckout = false
                    logTrace += ".12"
                }
            }

            const hasPendingCheckout = yield select(MobileSelectors.isPendingCheckout)

            // Last condition to meet is to determine if the user is still fence or not
            // if it returns true then all conditions that must be met in order for us to check the user out
            if (shouldCheckout || (hasPendingCheckout && hasPendingCheckout.value)) {
                logTrace += ".13"
                const locationBeaconItem = locationBeaconList.find((locationBeaconItem) => locationBeaconItem.beacon.major === mobile.beacon.major && mobile.beacon.minor === locationBeaconItem.beacon.minor)
                const geoFencingChannel = yield call(api.getGeofencingWithParams, coordinate, locationBeaconItem.points, auth)
                // TODO: Confirm how many numbers of api calls for this to be called?
                yield takeEvery(geoFencingChannel, function* (geofence: ApiAction) {
                    logTrace += ".14"
                    console.log("isWithin ShouldCheckout " , geofence , data)
                    console.log("Data for Checkout:", geofence, coordinate, locationBeaconItem)
                    if(forceCheckout || geofence?.stillInside === false){
                        if (hasPendingCheckout && hasPendingCheckout.value){
                            console.log("Pending Data checkout ", hasPendingCheckout)
                            yield put(MobileActions.checkOut(hasPendingCheckout.data.isBluetoothEnabled , hasPendingCheckout.data.isSilent, hasPendingCheckout.data.isForce, hasPendingCheckout.data.event, hasPendingCheckout.data.beacon))
                            logTrace += ".15"
                        }else{
                            const beacon : Beacon = (beacons && beacons.length > 0) ? beacons[0] : null
                            yield put(MobileActions.checkOut(isBluetoothEnabled, false, forceCheckout || data?.force, "regular:1-" + event, beacon))
                            logTrace += ".16"
                        }
                        // FORCE CHECKING OUT THE USER IMMEDIATELY
                        if(forceCheckout){
                            yield put(MobileActions.checkOutSuccess(null, null))
                            logTrace += ".17"
                        }
                        yield put(BeaconManagerActions.logTrace(logTrace))
                    }
                    else if(geofence?.stillInside === true && geofence?.location_id !== mobile?.locationId){
                        logTrace += ".18"
                        if (hasPendingCheckout && hasPendingCheckout.value){
                            console.log("Pending Data checkout ", hasPendingCheckout)
                            yield put(MobileActions.checkOut(hasPendingCheckout.data.isBluetoothEnabled , hasPendingCheckout.data.isSilent, hasPendingCheckout.data.isForce, hasPendingCheckout.data.event, hasPendingCheckout.data.beacon))
                            logTrace += ".19"
                        }else{
                            const beacon : Beacon = (beacons && beacons.length > 0) ? beacons[0] : null

                            yield put(MobileActions.checkOut(isBluetoothEnabled, true, forceCheckout || data?.force, "regular:2-" + event, beacon))
                            logTrace += ".20"
                        }
                        // FORCE CHECKING OUT THE USER IMMEDIATELY
                        yield put(MobileActions.checkOutSuccess(null, null))

                        // This is a scenario where user is offline in a couple of location travels
                        shouldCheckIn = true
                        logTrace += ".21"
                        yield put(BeaconManagerActions.logTrace(logTrace))
                    }
                    else if(geofence?.stillInside === true && geofence?.location_id === mobile?.locationId){
                        // This probably happens when there's an event from the beacon which is either:
                        // 1. stateRegionExit
                        // 2. triggerRegionState that would return a value 'outside'
                        // we prevent it from checking out because we are still inside fence of the location and its session is active
                        geoFencingChannel.close()
                        logTrace += ".22"
                        yield put(BeaconManagerActions.logTrace(logTrace))
                        return
                    }
                    // other options is that since the pending checkout data is queue meaning it has met all conditions to checkout but issue with api only
                    else if (hasPendingCheckout && hasPendingCheckout.value && hasPendingCheckout.data.event.includes("queue")){
                        yield put(MobileActions.checkOut(hasPendingCheckout.data.isBluetoothEnabled , hasPendingCheckout.data.isSilent, hasPendingCheckout.data.isForce, hasPendingCheckout.data.event, hasPendingCheckout.data.beacon))
                        // FORCE CHECKING OUT THE USER IMMEDIATELY
                        yield put(MobileActions.checkOutSuccess(null, null))
                        logTrace += ".23"
                        yield put(BeaconManagerActions.logTrace(logTrace))
                    }
                    // almost all parameters have been met except for the geofencing so we kept the record as pending checkout
                    else {
                        // reset pending checkout since we have a new data to store
                        if(hasPendingCheckout){
                            yield put(MobileActions.pendingCheckout(null))
                        }
                        const beacon : Beacon = (beacons && beacons.length > 0) ? beacons[0] : null

                        const pendingCheckout: Checkout = {
                            value: true,
                            data: {
                                event: event,
                                isBluetoothEnabled: isBluetoothEnabled,
                                isSilent: isSilent,
                                isForce: forceCheckout || data?.force,
                                beacon: beacon
                            }
                        }
                        yield put(MobileActions.pendingCheckout(pendingCheckout))
                        logTrace += ".24"
                        yield put(BeaconManagerActions.logTrace(logTrace))
                    }
                    geoFencingChannel.close()
                })
            }
        }
        // we have determined here automatically(else) that the app should check in provided all conditions must be met
        // refactor to use boolean if we ever need to checkin
        // default is true
        // false is when it enters checkout process
        // true whenever it entered checkout process but somehow due to delay of processes(some OS however does this perhaps) it transferred to a new location

        if(shouldCheckIn) {
            logTrace += ".25"
            // since we have pending checkin record here
            // and we still don't have any record that the user was checked in
            // then we first process pending checkin before further moving to the below process

            const hasPendingCheckin = yield select(MobileSelectors.isPendingCheckin)
            // make sure we checked carefully that pending checkin has all the dat and beaconProximity is inside
            if((hasPendingCheckin && hasPendingCheckin.locationBeacon && hasPendingCheckin.dateTime > 0) && (beaconProximity && beaconProximity === GLOBAL.globals.Proximity.INSIDE)){
                logTrace += ".26"
                console.log("Pending Checkin: ", hasPendingCheckin , beaconProximity)
                // calculate elapse time to match against threshold in minutes
                const dateTimeStored = Number(hasPendingCheckin.dateTime ?? 0)
                const thresholdInMinutes = ((Date.now() - dateTimeStored) / 1000) / 60

                // we set 5 threshold in minutes here
                // if the pending checkin beacon was stored 5 minutes ago or less then we proceed with checkin otherwise do nothing for now
                // and if there's no checkin attempt and no other data is loading then proceed checking in
                // isLoading indicates that ranging is still active
                if(thresholdInMinutes <= 5){
                    logTrace += ".27"
                    let checkInAttemptInProgress = yield select(MobileSelectors.isAttemptingToCheckin)
                    console.log("beacon-manager-saga: Attempting to checkin with data =  ", hasPendingCheckin, beacons, isLoading , checkInAttemptInProgress)
                    if(!isLoading && !checkInAttemptInProgress){
                        logTrace += ".28"
                        if((event.includes('beaconsDidRange') || event.includes('locationChanged')) && beacons && beacons.length > 0){
                            const acceptablePendingCheckinThatHasSimilarBeacon = beacons.find((beaconItem) => hasPendingCheckin.major === beaconItem.major && hasPendingCheckin.minor === beaconItem.minor)
                            if(acceptablePendingCheckinThatHasSimilarBeacon){
                                logTrace += ".29"
                                let pendingCheckinLocationBeacon: LocationBeacon = {
                                    ...hasPendingCheckin.locationBeacon,
                                    beacon: {
                                        ...hasPendingCheckin.locationBeacon.beacon,
                                        ...acceptablePendingCheckinThatHasSimilarBeacon,
                                    }
                                }
                                yield put(MobileActions.checkIn(pendingCheckinLocationBeacon))
                                // we return immediately , no point in processing below for another checkin since we are already checked in
                                // unless there's an error with checkin api
                                crashlytics().log(`beacon-manager-saga: User ${auth?.accountId} executed pending checkin with pending checkin data = beacon data: ${hasPendingCheckin.locationBeacon.beacon.major}, ${hasPendingCheckin.locationBeacon.beacon.minor} | locId: ${hasPendingCheckin.locationBeacon.id}  @ ${Date.now()}`)
                                yield put(BeaconManagerActions.logTrace(logTrace))
                                yield put(BeaconManagerActions.checkInAttemptDone(true))
                                return
                            }else{
                                // drop the record since it doesn't match with the beacon received.
                                yield put(MobileActions.pendingCheckin(null))
                                logTrace += ".30"
                            }
                        }
                    }

                }else{
                    // drop the record since it elapses with the threshold
                    yield put(MobileActions.pendingCheckin(null))
                    logTrace += ".31"
                }

            }else{
                // drop the record since its erroneous
                yield put(MobileActions.pendingCheckin(null))
                logTrace += ".32"
            }

            // beaconsDidRange have two(2) purpose
            // 1. beacon transmitted an event which is regionDidEnter
            // 2. beacon transmitted an event that is rangingna
            // 3. beacon transmitted an event that is coming from locationChanged
            if (event.includes('beaconsDidRange') && beacons || (event.includes("locationChanged") && beacons)){
                logTrace += ".33"
                console.log("beacon-manager-saga: DB beacon List: ", beaconList)
                console.log("beacon-manager-saga: DB location beacon List: ", locationBeaconList)
                // FIND BEACON IN beaconList if the beacon we detected by proximity is searchable from beaconList we have from the backend service
                // THEN RETURN it as an acceptable beacon
                const acceptableBeacon = beaconList.find((beaconItem)=> beacons.find(beacon => beacon.major === beaconItem.major && beacon.minor === beaconItem.minor));

                console.log("beacon-manager-saga: acceptableBeacon: ", acceptableBeacon)

                // FIND the actual beacon record from BE using the acceptableBeacon that we found the above acceptableBeacon
                const acceptableLocationBeacon = locationBeaconList.find((locationBeaconItem) => {

                    // Acceptable beacon has been cross matched and if its available then we return it as is
                    if (acceptableBeacon){
                        return acceptableBeacon.major === locationBeaconItem.beacon.major &&
                            acceptableBeacon.minor === locationBeaconItem.beacon.minor
                    }
                        // Acceptable doesn't match with the beacons that is stored in our backend database then we forfeit or disqualify
                    // the beacon we found hence returning null.
                    else{
                        return null
                    }
                })

                if (!acceptableLocationBeacon){
                    logTrace += ".34"
                    console.log("beacon-manager-saga: There's no acceptable beacon available");
                    // we are probably doing nothing not checked in nor checked out so we cancel local push notification to remove the notification for checkin checkout
                    // whichid = 1 is the notification id for checkin and checkout
                    yield put(MessagingActions.cancelMessage(1))
                    crashlytics().log(`beacon-manager-saga: User ${auth?.accountId} executed cancel notification message due no acceptable location beacon detected... @ ${Date.now()}`)
                    yield put(BeaconManagerActions.logTrace(logTrace))
                    return
                }
                console.log("beacon-manager-saga: Auth / Location exclusion : " , auth, acceptableLocationBeacon.clientExcluded)

                // WHILE MOST OF THE PARAMETERS AND CONDITIONS TO CHECKINS ARE MET WE STILL NEED TO VERIFY FURTHER BELOW
                // IF WE CAN PROCEED TO CHECKIN
                // IF ENVIRONMENT IS PROD THEN ONLY ALLOW PROCESSING CHECKIN WITH APPROPRIATE FLAGS
                // const env: string = Preference.get('ENV')
                // if (!env){
                // WE OPT TO USE THE SAME VARIABLE HIDE_TEST_LOCATIONS APP CONFIG IF ITS VALUE IS 1 THEN WE WILL USE LOGIC IF WE CAN PROCEED WITH CHECKIN BELOW
                // THIS ONLY APPLIES IF WE ARE ABOUT TO CHECKIN
                // FOR CHECKOUT WILL JUST SKIP THIS
                if (auth.hideTestLocations){
                    logTrace += ".35"
                    // SET DEFAULT TO TRUE TO ALWAYS ALLOW PROCEEDING TO CHECKIN
                    let proceedWithCheckin = true;
                    switch(auth?.flags){
                        case 0 : // ELLIGIBLE

                            // DONT ALLOW PROCEEDING TO CHECK IN IF LOCATION IS JUST A TEST LOCATION
                            if(acceptableLocationBeacon.clientExcluded){
                                proceedWithCheckin = false
                            }
                            break;
                        case 1 : // INELLIGIBLE
                            // ALWAYS ALLOW REGARDLESS IF LOCATION IS A TEST LOCATION OR NOT
                            // BACKEND SERVICE WILL TAKE CARE OF FILTERING WINNERS
                            break;
                        case 2 : // ELLIGIBILITY VERIFIED
                            // DONT ALLOW PROCEEDING TO CHECK IN IF LOCATION IS JUST A TEST LOCATION
                            if(acceptableLocationBeacon.clientExcluded){
                                proceedWithCheckin = false
                            }
                            break;
                    }

                    if(!proceedWithCheckin){
                        logTrace += ".36"
                        console.log("beacon-manager-saga: Not proceeding to checkin due to : " , auth, acceptableLocationBeacon)
                        yield put(BeaconManagerActions.logTrace(logTrace))
                        yield put(BeaconManagerActions.checkInAttemptDone(true))
                        return
                    }
                }



                // WE STILL NEED TO VERIFY IF THERE ARE OTHER CHECKIN ATTEMPS THAT ARE CURRENTLY IN PROGRESS BEFORE WE PROCEED
                let checkInAttemptInProgress = yield select(MobileSelectors.isAttemptingToCheckin)
                console.log("beacon-manager-saga: Trying to checkin with data =  ", isBluetoothEnabled, mobile, acceptableBeacon, beacons, isLoading , checkInAttemptInProgress)
                if (acceptableBeacon && !checkInAttemptInProgress) {
                    logTrace += ".37"
                    // isLoading indicates that ranging is still active
                    if (!isLoading) {
                        logTrace += ".38"
                        const acceptableCheckinThatHasSimilarBeacon = beacons.find((beaconItem) => acceptableBeacon.major === beaconItem.major && acceptableBeacon.minor === beaconItem.minor)
                        let checkinLocationBeacon: LocationBeacon = {
                            ...acceptableLocationBeacon,
                            beacon: {
                                ...acceptableLocationBeacon.beacon,
                                ...acceptableCheckinThatHasSimilarBeacon,
                            }
                        }
                        yield put(MobileActions.checkIn(checkinLocationBeacon))
                        crashlytics().log(`beacon-manager-saga: User ${auth?.accountId} executed checkin with acceptable beacon and location data = locId : ${acceptableLocationBeacon.id} | beacon data ${acceptableLocationBeacon.beacon.major}, ${acceptableLocationBeacon.beacon.minor}  @ ${Date.now()}`)
                    }

                }
                else {
                    logTrace += ".39"
                    // reset pending checkin since we have a new data to store
                    if(hasPendingCheckin){
                        yield put(MobileActions.pendingCheckin(null))
                    }
                    const pendingCheckin: Checkin = {
                        locationBeacon: acceptableLocationBeacon,
                        dateTime: Date.now(),
                    }
                    console.log("beacon-manager-saga: Saving pendingCheckin data : " , pendingCheckin)
                    yield put(MobileActions.pendingCheckin(pendingCheckin))
                    crashlytics().log(`beacon-manager-saga: User ${auth?.accountId} executed storing of pending checkin beacon and location data = locId : ${pendingCheckin.locationBeacon.id} | beacon data ${pendingCheckin.locationBeacon.beacon.major}, ${pendingCheckin.locationBeacon.beacon.minor}  @ ${pendingCheckin.dateTime}`)

                }

                yield put(MobileActions.startDisconnecting(null))
            }
            
            yield put(BeaconManagerActions.checkInAttemptDone(true))
        }

        logTrace += ".40"
        yield put(BeaconManagerActions.logTrace(logTrace))
    })
    yield delay(1000)
    yield put(BackgroundFetchActions.startBackgroundFetchMonitoring())
    yield take(BeaconManagerTypes.STOP_BG_TASK)
    console.log('STOP BG TASK called... closing backgroundTaskListener...')
    backgroundTaskListener.close()
}

function * stopBeaconMonitoring (action: ApiAction) {

    const mobile: Mobile = yield select(MobileSelectors.mobile)
    if (mobile && mobile.checkinId){
        const isSilent = true
        const isBluetoothEnabled = true
        yield put(MobileActions.checkOut(isBluetoothEnabled, isSilent, true, "beacon-manager-saga:stopBeaconMonitoring", null))
    }


    if (Platform.OS === 'ios') {
        yield put(BeaconManagerActions.isCurrentlyMonitoring(false))

        if(__DEV__){
            console.log("STOPPING MONITORING...")
        }
        iOSBackgroundBeacons.StopRangingTask()
        iOSBackgroundBeacons.StopMonitoringTask()
    }
    else if (Platform.OS === 'android') {
        if(__DEV__){
            console.log("STOPPING MONITORING...")
        }
        yield put(BeaconManagerActions.isCurrentlyMonitoring(false))
        // Replacing foreground services
        // Android 12 restricts launching foreground services from the background. For most cases, you should use setForeground() from WorkManager rather than handle foreground services yourself. This allows WorkManager to manage the lifecycle of the foregound service, ensuring efficiency.

        // How to reproduce the crash
        // Step 1. Update your targetSdkVersion and compileSdkVersion to SDK 31.
        //
        // Step 2. Try to run any Foreground service when your app is in background. In my case, it was the widget's onUpdate method being called after updatePeriodMillis time, which will start a Foreground service, which updates the data by fetching appropriate information from internet.
        //
        // Remember: The background execution limits added in Android 8.0 have nothing to do with this problem. This limitation/exception was added in Android 12/SDK 31 - Source.
        //
        //     What is this exception, and why was it added?
        //     Apps that target Android 12 (API level 31) or higher can't start foreground services while running in the background, except for a few special cases. If an app tries to start a foreground service while the app is running in the background, and the foreground service doesn't satisfy one of the exceptional cases, the system throws a ForegroundServiceStartNotAllowedException.
        //
        //     These special cases are:
        //
        //     Your app transitions from a user-visible state, such as an activity.
        //
        //     Your app can start an activity from the background, except for the case where the app has an activity in the back stack of an existing task.
        //
        //     Your app receives a high-priority message using Firebase Cloud Messaging.
        //
        //     The user performs an action on a UI element related to your app. For example, they might interact with a bubble, notification, widget, or activity.
        //
        //     Your app invokes an exact alarm to complete an action that the user requests.
        //
        //     Your app is the device's current input method.
        //
        // Your app receives an event that's related to geofencing or activity recognition transition.
        //
        // After the device reboots and receives the ACTION_BOOT_COMPLETED, ACTION_LOCKED_BOOT_COMPLETED, or ACTION_MY_PACKAGE_REPLACED intent action in a broadcast receiver.
        //
        //     Your app receives the ACTION_TIMEZONE_CHANGED, ACTION_TIME_CHANGED, or ACTION_LOCALE_CHANGED intent action in a broadcast receiver.
        //
        //     Your app receives a Bluetooth broadcast that requires the BLUETOOTH_CONNECT or BLUETOOTH_SCAN permissions.
        //
        //     Apps with certain system roles or permission, such as device owners and profile owners.
        //
        //     Your app uses the Companion Device Manager and declares the REQUEST_COMPANION_START_FOREGROUND_SERVICES_FROM_BACKGROUND permission or the REQUEST_COMPANION_RUN_IN_BACKGROUND permission. Whenever possible, use REQUEST_COMPANION_START_FOREGROUND_SERVICES_FROM_BACKGROUND.
        //
        //     The user turns off battery optimizations for your app. You can help users find this option by sending them to your app's App info page in system settings. To do so, invoke an intent that contains the ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS intent action.
        // Since we have added BLUETOOTH_SCAN, BLUETOOTH_CONNECT This should be considered as special case for us
        // More from this issue https://stackoverflow.com/a/69604952
        AndroidBackgroundBeacons.StopForegroundService()
    }
    // problem : there is a phantom instance running if we don't stop this only specific to Android, but in iOS there seems no relevant issue that exist
    // solution: once we stopped the monitoring, we also need to notify the bg task that receives beacon data to stop listening
    yield put(BeaconManagerActions.stopBGTask())
    yield put(LocationActions.unsubscribeLocationProviderChanged())

}

export default function() {
    return [
        takeLatest(BeaconManagerTypes.START_BEACON_MONITORING,  startBeaconMonitoring),
        takeLatest(BeaconManagerTypes.STOP_BEACON_MONITORING,  stopBeaconMonitoring),
    ]
}
