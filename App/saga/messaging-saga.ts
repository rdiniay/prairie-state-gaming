import {all, call, put, select, take, takeLatest, takeEvery, delay, cancel, putResolve} from 'redux-saga/effects'
import { eventChannel, END } from 'redux-saga'
import { ApiMessaging, ApiAction } from '../services'
import api from '../services/shared/messaging-service'
import MessagingActions, { MessagingSelectors, MessagingTypes } from '../redux/messaging-redux'
import BankActions  from '../redux/bank-redux'
import { AuthSelectors } from '../redux/auth-redux'
import { Auth, Message, Notification } from '../models'
import PushNotification from 'react-native-push-notification';
import { strings } from '../localization/strings'
const subscriptionChannel: Object[] = []

const onRefreshMessagingToken = (api: ApiMessaging) =>
  eventChannel(emitter => {
    const unsubscribe = api.subscribeRefreshMessagingToken((fcmToken: String) => {
      emitter(fcmToken)
    });

    return () => unsubscribe
  })

const onMessage = (api: ApiMessaging) =>
  eventChannel(emitter => {
    const unsubscribe = api.subscribeMessagingOnMessage((remoteMessage: any) => {
        emitter(remoteMessage)
    });

    return () => unsubscribe
  })

const onBackgroundMessage = (api: ApiMessaging) =>
  eventChannel(emitter => {
    const unsubscribe = api.subscribeMessagingOnBackgroundMessage((remoteMessage: any) => {
        emitter(remoteMessage)
    });

    return () => unsubscribe
  })

const onTopicMessage = (topic: String, api: ApiMessaging) =>
  eventChannel(emitter => {
    const unsubscribe = api.subscribeMessagingOnTopicMessage(topic, (remoteMessage: any) => {
        emitter(remoteMessage)
    });

    return () => unsubscribe
  })

function * generateMessagingToken (api: ApiMessaging) {
    const currentFcmToken = yield select(MessagingSelectors.token)
    const response = yield call(api.generateMessagingToken)
    const fcmToken = response.getOrElse(null)
    console.log(`fcmToken: ${fcmToken}`)
    if (fcmToken && fcmToken !== currentFcmToken) {
        yield put(
            response.matchWith({
                Ok: ({ value }) => MessagingActions.generateMessagingTokenSuccess(value),
                Error: ({ value }) => MessagingActions.generateMessagingTokenFailure(value)
            })
        )
    }
    yield put(MessagingActions.subscribeRefreshMessagingToken())
}

function * subscribeMessagingOnTopicMessage (api: ApiMessaging, action: ApiAction) {
    const { topic } = action
    subscriptionChannel[`${topic}`]  = yield call(onTopicMessage, topic, api)

    yield takeEvery(subscriptionChannel[`${topic}`], function* (topicMessage: any) {
        console.log(`${topicMessage}: `, topicMessage)
        yield put(MessagingActions.subscribeMessagingOnTopicMessageSuccess(topicMessage))
    })

    yield take(MessagingTypes.UNSUBSCRIBE_MESSAGING_ON_TOPIC_MESSAGE)
    subscriptionChannel[`${topic}`].close()
}

function * subscribeMessagingOnBackgroundMessage (api: ApiMessaging) {
    const onBackgroundMessageChannel  = yield call(onBackgroundMessage, api)

    yield takeEvery(onBackgroundMessageChannel, function* (remoteMessage: any) {
        console.log(`${remoteMessage}: `, remoteMessage)
        if(__DEV__){
            console.log("messaging-saga: Remote Background Message: ", remoteMessage , remoteMessage.includes("content-available"))
        }
        if (remoteMessage.includes("content-available")){
            console.log("messaging-saga: Attempting to bank entries...")
            yield putResolve(BankActions.updateBankEntries())
            console.log("messaging-saga: Done bank entries...")
        }
        yield put(MessagingActions.subscribeMessagingOnBackgroundMessageSuccess(remoteMessage))
    })

    yield take(MessagingTypes.UNSUBSCRIBE_MESSAGING_ON_BACKGROUND_MESSAGE)
    onBackgroundMessageChannel.close()
}

function * subscribeMessagingOnMessage (api: ApiMessaging) {
    const onMessageChannel  = yield call(onMessage, api)

    yield takeEvery(onMessageChannel, function* (remoteMessage: any) {
        console.log(`${remoteMessage}: `, remoteMessage)
        console.log("Remote Background Message: ", remoteMessage)
        if(__DEV__){
            console.log("Remote Message: ", remoteMessage)
        }
        yield put(MessagingActions.subscribeMessagingOnMessageSuccess(remoteMessage))
    })

    yield take(MessagingTypes.UNSUBSCRIBE_MESSAGING_ON_MESSAGE)
    onMessageChannel.close()
}


function * subscribeRefreshMessagingToken (api: ApiMessaging) {
    const tokenRefreshChannel  = yield call(onRefreshMessagingToken, api)

    yield takeEvery(tokenRefreshChannel, function* (fcmToken: String) {
        const currentFcmToken = yield select(MessagingSelectors.token)
        if(fcmToken && fcmToken!==currentFcmToken)
            yield put(MessagingActions.subscribeRefreshMessagingTokenSuccess(fcmToken))
    })

    yield take(MessagingTypes.UNSUBSCRIBE_REFRESH_MESSAGING_TOKEN)
    tokenRefreshChannel.close()
}

function * sendMessagingOnMessage (api: ApiMessaging, action: ApiAction) {
    if(__DEV__){
        console.log("Sending Push Notification Message...", api, action)
    }
    const auth: Auth = yield select(AuthSelectors.auth)
    if (auth && auth.authData) {
        const { message } = action
        PushNotification.localNotification({
            /* Android Only Properties */
            channelId: strings.android_channel_id, // (required) channelId, if the channel doesn't exist, notification will not trigger.
            ticker: "My Notification Ticker", // (optional)
            showWhen: true, // (optional) default: true
            autoCancel: true, // (optional) default: true
            /* iOS and Android properties */
            id: 1, // (optional) Valid unique 32 bit integer specified as string. default: Autogenerated Unique ID
            title: message.notification.title, // (optional)
            message: message.notification.body, // (required)

            foreground: true, // BOOLEAN: If the notification was received in foreground or not
            userInteraction: true, // BOOLEAN: If the notification was opened by the user from the notification area or not
          });
    }
}

function * sendScheduleMessagingOnMessage (api: ApiMessaging, action: ApiAction) {
    if(__DEV__){
        console.log("Sending Push Schedule Notification Message...", api, action)
    }
    const auth: Auth = yield select(AuthSelectors.auth)
    if (auth && auth.authData) {
        const { message, scheduledSeconds} = action
        PushNotification.localNotificationSchedule({
            /* Android Only Properties */
            channelId: strings.android_channel_id, // (required) channelId, if the channel doesn't exist, notification will not trigger.
            ticker: "My Notification Ticker", // (optional)
            showWhen: true, // (optional) default: true
            autoCancel: true, // (optional) default: true

            id: 1,
            date:new Date(new Date().getTime()+ (scheduledSeconds * 1000)),
            title: message.notification.title,
            message: message.notification.body,
            allowWhileIdle:false,
        });
    }
}

function * cancelLocalPushNotification(api: ApiMessaging, action: ApiAction){
    if(__DEV__){
        console.log("Cancel local push notification...", api, action)
    }
    const { whichId } = action
    PushNotification.cancelLocalNotifications({id: whichId})
}

function * requestMessagingPermission (api: ApiMessaging) {
    const response = yield call(api.requestMessagingPermission)
    yield put(
        response.matchWith({
            Ok: ({ value }) => MessagingActions.generateMessagingToken(),
            Error: ({ value }) => MessagingActions.requestMessagingPermissionFailure(value)
        })
    )
}

export default function() {
    return [
        takeLatest(MessagingTypes.SEND_SCHEDULE_MESSAGING_ON_MESSAGE,  sendScheduleMessagingOnMessage, api),
        takeLatest(MessagingTypes.SEND_MESSAGING_ON_MESSAGE,  sendMessagingOnMessage, api),
        takeLatest(MessagingTypes.CANCEL_MESSAGE, cancelLocalPushNotification, api),
        takeLatest(MessagingTypes.SUBSCRIBE_MESSAGING_ON_TOPIC_MESSAGE,  subscribeMessagingOnTopicMessage, api),
        takeLatest(MessagingTypes.SUBSCRIBE_MESSAGING_ON_BACKGROUND_MESSAGE,  subscribeMessagingOnBackgroundMessage, api),
        takeLatest(MessagingTypes.SUBSCRIBE_MESSAGING_ON_MESSAGE,  subscribeMessagingOnMessage, api),
        takeLatest(MessagingTypes.SUBSCRIBE_REFRESH_MESSAGING_TOKEN,  subscribeRefreshMessagingToken, api),
        takeLatest(MessagingTypes.GENERATE_MESSAGING_TOKEN,  generateMessagingToken, api),
        takeLatest(MessagingTypes.REQUEST_MESSAGING_PERMISSION,  requestMessagingPermission, api),
    ]
}
