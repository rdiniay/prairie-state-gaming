import {all, call, put, select, take, takeLatest, takeEvery, delay} from 'redux-saga/effects'
import { eventChannel, END } from 'redux-saga'
import { ApiAction, ApiDrawing } from '../services'
import { AuthSelectors } from '../redux/auth-redux'
import DrawingActions, { DrawingSelectors, DrawingTypes } from '../redux/drawing-redux'
import api from '../services/shared/drawing-service'
import { Coordinate, Drawing } from '../models'
import { LocationSelectors } from '../redux/location-redux'
import DashboardActions from '../redux/dashboard-redux'
import MobileActions from '../redux/mobile-redux'
import BackgroundTimer from 'react-native-background-timer'

import DateString from 'moment'
import BankActions, { BankTypes } from '../redux/bank-redux'

const onTimerTickChannel = (timeStamp: number = null) =>
  eventChannel(emitter => {
      let endTime = timeStamp
      const timerId = setInterval(async () => {
          const currentTime = Number(Date.now())
          const elapse = (endTime - currentTime) / 1000

          if (0 < elapse) {}
          else {
            emitter(0)
            emitter(END)
          }
      }, 1000)

      return () => {
        clearInterval(timerId)
      }
  })

function * startCountDownStateWideDrawing (api: ApiDrawing, action: ApiAction) {
    yield put(DrawingActions.getRecentDrawings())
    yield take(DrawingTypes.GET_RECENT_DRAWINGS_SUCCESS)
    yield put(BankActions.getBankEntries())
    
    const recentDrawings = yield select(DrawingSelectors.recentDrawings)
    const stateWideDrawing: Drawing = recentDrawings ? recentDrawings.find((drawing) => !drawing.locationId) : null
    let endDateTime = Number(new Date(`${stateWideDrawing?.endDate}`)) // 2022-05-05T13:00:00.000Z
    let countDownTick = yield call(onTimerTickChannel, endDateTime)
    yield takeEvery(countDownTick, function* (tick: number) {
        // BACKEND/AUTO-COMPLETE WILL CHECKOUT THE USER
        // INSTEAD CLEAR THE LOCAL CHECKIN DATA WHEN STATEWIDE IS OVER
        console.log('drawing-saga: startCountDownStateWideDrawing')
        yield put(DashboardActions.stopEarningTimer())
        yield put(DrawingActions.stopCountDownStateWideDrawing())
        yield put(DrawingActions.getRecentDrawings())
        yield put(MobileActions.checkOutSuccess(null, null))
    })
    yield take(DrawingTypes.STOP_COUNT_DOWN_STATE_WIDE_DRAWING)
    countDownTick.close()
}

function * getDrawingRules (api: ApiDrawing, action: ApiAction) {
    const auth = yield select(AuthSelectors.auth)
    const response = yield call(api.getDrawingRules, auth)

    yield put(
        response.matchWith({
            Ok: ({ value }) => DrawingActions.getDrawingRulesSuccess(value),
            Error: ({ value }) => DrawingActions.getDrawingRulesFailure(value)
        })
    )
}

function * getRecentDrawings (api: ApiDrawing, action: ApiAction) {
    const auth = yield select(AuthSelectors.auth)
    const coordinate: Coordinate = yield select(LocationSelectors.coordinate)
    if (auth && coordinate) {
        const response = yield call(api.getRecentDrawings, coordinate, auth)

        yield put(
            response.matchWith({
                Ok: ({ value }) => DrawingActions.getRecentDrawingsSuccess(value),
                Error: ({ value }) => DrawingActions.getRecentDrawingsFailure(value)
            })
        )
    }
}

function * getDrawingDetails (api: ApiDrawing, action: ApiAction) {
    const { drawing } = action
    yield put(DrawingActions.getDrawingDetailsSuccess(drawing))
}

export default function() {
    return [
        takeLatest(DrawingTypes.START_COUNT_DOWN_STATE_WIDE_DRAWING, startCountDownStateWideDrawing, api),
        takeLatest(DrawingTypes.GET_DRAWING_RULES,  getDrawingRules, api),
        takeLatest(DrawingTypes.GET_RECENT_DRAWINGS,  getRecentDrawings, api),
        takeLatest(DrawingTypes.GET_DRAWING_DETAILS,  getDrawingDetails, api),
    ]
}
