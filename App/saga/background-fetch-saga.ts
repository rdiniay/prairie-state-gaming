import {all, call, delay, put, putResolve, select, take, takeEvery, takeLatest} from 'redux-saga/effects'
import { DeviceEventEmitter, Platform } from 'react-native'
import { eventChannel, END } from 'redux-saga'
import { ApiAction, ApiBank } from '../services'
import LocationActions, { LocationSelectors } from '../redux/location-redux'
import BankActions from '../redux/bank-redux'
import { DrawingSelectors } from '../redux/drawing-redux'
import { AuthSelectors } from '../redux/auth-redux';
import MobileActions, {MobileSelectors} from '../redux/mobile-redux'
import BackgroundFetchActions, { BackgroundFetchSelectors, BackgroundFetchTypes } from '../redux/background-fetch-redux'
import { LocationPermissionStatus, Coordinate, Mobile, Auth  } from '../models';
import iOSBackgroundBeacons from '../background-task/beacon/iOSBeaconMonitoringTask'
import AndroidBackgroundBeacons from '../background-task/beacon/AndroidBeaconMonitoringTask'
import BackgroundGeolocation from 'react-native-background-geolocation';

const backgroundiOSFetch = () =>
  eventChannel(emitter => {
        iOSBackgroundBeacons.onBackgroundFetch((data) => {
            BackgroundGeolocation.getProviderState((data) => {
                let locationPermissionStatus = data?.status
                emitter({data, locationPermissionStatus})
            })
        })
    return () => {
    }
})

const backgroundAndroidFetch = () =>
    eventChannel(emitter => {
        AndroidBackgroundBeacons.onBackgroundFetch((data) => {
            BackgroundGeolocation.getProviderState((data) => {
                let locationPermissionStatus = data?.status
                emitter({data, locationPermissionStatus})
            })
        })
        return () => {
        }
    })





function * startBackgroundFetchMonitoring (action: ApiAction) {
    let backgroundFetch = Platform.OS === 'ios' ? backgroundiOSFetch : backgroundAndroidFetch
    let backgroundFetchListener = yield call(backgroundFetch)
    yield takeEvery(backgroundFetchListener, function* (result: any) {
        console.log('background-fetch-saga: takeEvery ' , result)
        const lastFetch = Number(yield select(BackgroundFetchSelectors.lastFetch) ?? 0)
        const seconds = ((Date.now() - lastFetch) / 1000)
        const hours = (seconds / 60) / 60
        const minimumHours = 0 < lastFetch ? hours : 24 //<-- Default for the first time trigger
        if (24 <= minimumHours) {
            const coordinate: Coordinate = yield select(LocationSelectors.coordinate)
            yield put(LocationActions.getLocationBeacons(coordinate))
            yield put(BackgroundFetchActions.saveLastFetch(Date.now()))
        }
        else {
            const isStateWideDrawingExpired = yield select(DrawingSelectors.isStateWideDrawingExpired)
            if (isStateWideDrawingExpired){
                console.log('background-fetch-saga: startBackgroundFetchMonitoring.checkOutSuccess ', minimumHours, lastFetch, hours)
                yield put(MobileActions.checkOutSuccess(null, null))
            }
            else {
                // Attempting to bank entries
                console.log("background-fetch-saga: Attempting to bank entries...")
                yield putResolve(BankActions.updateBankEntries())
                console.log("background-fetch-saga: Done attempting to bank entries...")
            }
        }
        // its possible API failed and all conditions when checking out passed, so we forced the checkout when we can bank entries
        const pendingCheckout = yield select(MobileSelectors.isPendingCheckout)
        if (pendingCheckout && pendingCheckout?.value && pendingCheckout?.data?.event?.includes("queue")){
            yield put(MobileActions.checkOut(pendingCheckout.data?.isBluetoothEnabled , pendingCheckout.data?.isSilent, false, "background-fetch-saga:pendingCheckout", null))
            // FORCE CHECKING OUT THE USER IMMEDIATELY
            yield put(MobileActions.checkOutSuccess(null, null))
        }
    })

    // Will be called from menu-saga#46 when signing out
    yield take(BackgroundFetchTypes.STOP_BACKGROUND_FETCH_MONITORING)
    console.log("background-fetch-saga: Stopping background fetch monitoring...")
    backgroundFetchListener.close()
}


export default function() {
    return [
        takeLatest(BackgroundFetchTypes.START_BACKGROUND_FETCH_MONITORING,  startBackgroundFetchMonitoring),
    ]
}
