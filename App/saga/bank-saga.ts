import { delay, all, call, put, select, take, takeLatest } from 'redux-saga/effects'
import { eventChannel, END } from 'redux-saga'
import { ApiAction, ApiBank, ApiMobile } from '../services'
import { AuthSelectors } from '../redux/auth-redux'
import MobileActions, { MobileSelectors } from '../redux/mobile-redux'
import LocationActions, { LocationSelectors } from '../redux/location-redux'
import { DrawingSelectors } from '../redux/drawing-redux'
import BankActions, { BankSelectors, BankTypes } from '../redux/bank-redux'
import HomeActions from '../redux/home-redux'
import api from '../services/shared/bank-service'
import apiMobile from '../services/shared/mobile-service'
import BackgroundGeolocation from 'react-native-background-geolocation'
import { Auth, Drawing, Bank, Entry, Period, Location, LocationBeacon, Coordinate, Mobile, LocationPermissionStatus } from '../models'
import Geolocation from 'react-native-geolocation-service'
import DateString from 'moment'

const onCurrentPositionChanged = () =>
  eventChannel(emitter => {
    BackgroundGeolocation.getProviderState((data) => {
    let preciseLocationDisabled: boolean = Boolean(data?.accuracyAuthorization)

    const params: any = {
        accuracy: {
            android: 'high',
            ios: 'best',
        },
        enableHighAccuracy: true,
        timeout: 3000,
        forceLocation: true
    }
    Geolocation.getCurrentPosition(
        (location) => {
            const coordinate: Coordinate = {
                latitude: location.coords.latitude,
                longitude: location.coords.longitude,
                accuracy: location.coords.accuracy,
                altitude: location.coords.altitudeAccuracy,
                status: data,
            }
            emitter(coordinate)
        },
        (error) => { },
        params
    );

    })
    return () => {
    }
  })

function * updateBankEntries (api: ApiBank, action: ApiAction) {
    const mobile = yield select(MobileSelectors.mobile)
    let shouldStopLoading = true

    if (mobile && mobile.beacon) {
        let lastCheckOutPeriodTime = yield select(MobileSelectors.lastCheckOutPeriodTime)
        let lastCheckInPeriodTime = yield select(MobileSelectors.lastCheckInPeriodTime)
        let startPeriodTime = lastCheckInPeriodTime ? lastCheckInPeriodTime : Number(Date.now())
        const duration = 120
        const elapse = ((Date.now() - startPeriodTime) / 1000)
        const countedEntries = `${(elapse/duration)}`.includes('.99') ? Math.round(elapse/duration) : Math.floor(elapse/duration)
        const onGoingSeconds = ( (elapse/duration) - (countedEntries) ) * 1000
        let startTime = Date.now() - (onGoingSeconds*duration)
        yield put(MobileActions.lastCheckInPeriodTime(startTime))

        const locations: Location[] = yield select(LocationSelectors.locations)
        if (!locations) return

        const locationBeaconList: LocationBeacon[] = locations.map((location: Location)=> {
            if (location.beacons && location.active){
                return location.beacons.map((beacon)=> {
                    const LocationBeacon: LocationBeacon = { id: Number(location.id), beacon: beacon}
                    return LocationBeacon
                });
            }
            return [];
        }).flat();

        const locationBeaconItem = locationBeaconList.find((locationBeaconItem) => locationBeaconItem.beacon.major === mobile.beacon.major && locationBeaconItem.beacon.minor === mobile.beacon.minor);
        if (!locationBeaconItem){
            console.log("NO UPDATING BANK ENTRIES DUE TO BEACON UNAVAILABILITY");
            return;
        }

        let periods: Period[] = []
        for (let count = 0; count < countedEntries; count++) {
            let startPeriodTime = startTime - ((countedEntries - count) * (duration * 1000))
            let endPeriodTime = startPeriodTime + (duration * 1000)

            if (lastCheckOutPeriodTime && (0 < Math.floor((endPeriodTime - lastCheckOutPeriodTime) / 1000))) {
                startTime = Date.now()
                yield put(MobileActions.lastCheckInPeriodTime(startTime))
                break;
            }

            let period: Period = {
                locationId: locationBeaconItem.id,
                checkinId: mobile.checkinId,
                start: DateString(startPeriodTime).format(),
                end: DateString(endPeriodTime).format(),
            }
            periods.push(period)
            console.log(`start: ${period.start} ==== end ${period.end}`)
        }

        if (0 < periods.length) {
            yield put(BankActions.addBankEntries(periods))

            yield take(BankTypes.ADD_BANK_ENTRIES_SUCCESS)
            yield put(MobileActions.lastCheckOutPeriodTime(null))
        }

        shouldStopLoading = countedEntries < 1
    }

    if (shouldStopLoading)
        yield put(BankActions.stopBankLoading())
}

function * addBankEntries (api: ApiBank, action: ApiAction) {
    const { periods } = action
    const auth = yield select(AuthSelectors.auth)
    if (auth) {
        const recentDrawings: Drawing[] = yield select(DrawingSelectors.recentDrawings)
        const currentBank: Bank = yield select(BankSelectors.bank)
        const { checkinId } = yield select(MobileSelectors.mobile)
        const bankPeriods: Period[] = periods
        if (!currentBank)
            return

        let bankEntries: Entry[] = currentBank && currentBank.entries ? currentBank.entries : []
        if (bankEntries)
            bankEntries = bankEntries.map((entry: Entry) => {
                let stateWideDrawing: Drawing = recentDrawings.find((recent: Drawing) => recent.locationId === null && entry.locationId === recent.locationId)
                if (stateWideDrawing)
                    return ({ ...entry, multiplier: stateWideDrawing.multiplier })
                else
                    return entry
            })
        else {
            let stateWideDrawing: Drawing = recentDrawings.find((recent: Drawing) => recent.locationId === null)
            if (stateWideDrawing)
                bankEntries = [({ locationId: null, earnedEntryCount: 0, multiplier: stateWideDrawing.multiplier })]
        }

        const onCurrentPositionChangedChannel  = yield call(onCurrentPositionChanged)
        yield takeLatest(onCurrentPositionChangedChannel, function* (coordinate: Coordinate) {
            yield put(LocationActions.getCurrentLocationSuccess(coordinate))
            onCurrentPositionChangedChannel.close()
            let forceCheckout = Boolean(coordinate?.status?.accuracyAuthorization)
            if (coordinate?.status?.status === LocationPermissionStatus.AskNextTime)
                forceCheckout = true
            if (coordinate?.status?.status === LocationPermissionStatus.Never)
                forceCheckout = true
                
            if (forceCheckout) {
                yield put(MobileActions.checkOut(true, false, true, "bank-saga:addBankEntries", null))
            }
            else {
                const bank: Bank = {
                    periods: bankPeriods,
                    coordinate: coordinate,
                    checkinId: checkinId,
                    continueSession: true,
                }
                const response = yield call(api.addBankEntries, bank, bankEntries, auth)

                yield put(
                    response.matchWith({
                        Ok: ({ value }) => BankActions.addBankEntriesValidation(value),
                        Error: ({ value }) => BankActions.addBankEntriesFailure(value)
                    })
                )
            }
        })
    }
}

function * addBankEntriesValidation (api: ApiMobile, action: ApiAction) {
    const { payload } = action
    const mobile: Mobile = yield select(MobileSelectors.mobile)
    const auth = yield select(AuthSelectors.auth)
    if (mobile && auth) {
        const bank: Bank = payload.bank
        const entries: Entry[] = bank.entries
        yield put(BankActions.addBankEntriesSuccess(bank))

        const stateWideDrawingEntry = entries.find((drawingEntry: Entry) => !drawingEntry.locationId && drawingEntry.locationId === 0)
        if (stateWideDrawingEntry && !stateWideDrawingEntry.clientCheckinIsActiveOne) {
            // IF CHECKED OUT MANUALLY IN THE PORTAL
            // THEN CLEAR CHECKIN DATA IN THE MOBILE
            yield put(MobileActions.checkOut(true, false, true, "bank-saga:addBankEntriesValidation", null))
        }
    }
}

function * getBankEntries (api: ApiBank, action: ApiAction) {
    const auth = yield select(AuthSelectors.auth)
    const coordinate = yield select(LocationSelectors.coordinate)
    if (auth && coordinate) {
        const recentDrawings: Drawing[] = yield select(DrawingSelectors.recentDrawings)
        let bank: Bank = yield select(BankSelectors.bank)
        let bankCoordinate = bank && bank.coordinate ? { ...bank.coordinate, ...coordinate } : coordinate
        let bankEntries: Entry[] = bank && bank.entries ? bank.entries : null
        if (bankEntries)
            bankEntries = bankEntries.map((entry: Entry) => {
                let stateWideDrawing: Drawing = recentDrawings.find((recent: Drawing) => recent.locationId === null && entry.locationId === recent.locationId)
                if (stateWideDrawing)
                    return ({ ...entry, multiplier: stateWideDrawing.multiplier })
                else
                    return entry
            })
        else {
            bankEntries = [({ locationId: 0, earnedEntryCount: 0, multiplier: null })]
        }
        bank = { ...bank, coordinate: bankCoordinate, entries: bankEntries }
        console.log('bankEntriesbankEntries: ', bankEntries)
        const response = yield call(api.getBankEntries, bank, auth)

        yield put(
            response.matchWith({
                Ok: ({ value }) => BankActions.getBankEntriesSuccess(value),
                Error: ({ value }) => BankActions.getBankEntriesFailure(value)
            })
        )
    }
}

export default function() {
    return [
        takeLatest(BankTypes.UPDATE_BANK_ENTRIES,  updateBankEntries, api),
        takeLatest(BankTypes.ADD_BANK_ENTRIES_VALIDATION,  addBankEntriesValidation, apiMobile),
        takeLatest(BankTypes.ADD_BANK_ENTRIES,  addBankEntries, api),
        takeLatest(BankTypes.GET_BANK_ENTRIES,  getBankEntries, api),
    ]
}
