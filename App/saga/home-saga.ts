import { all, call, put, select, take, takeLatest, takeEvery, delay } from 'redux-saga/effects'
import { eventChannel, END } from 'redux-saga'
import { ApiAction, ApiHome } from '../services'
import AuthActions, { AuthSelectors } from '../redux/auth-redux'
import ProfileActions, { ProfileSelectors, ProfileTypes } from '../redux/profile-redux'
import HomeActions, { HomeTypes } from '../redux/home-redux'
import { MobileSelectors } from '../redux/mobile-redux'
import BankActions, { BankSelectors } from '../redux/bank-redux'
import BeaconManagerActions from '../redux/beacon-manager-redux'
import api from '../services/shared/home-service'
import { Mobile } from '../models'

const onTimerTickChannel = (duration: number, lastPaused: number = null) =>
  eventChannel(emitter => {
      let startTime = lastPaused
      const timerId = setInterval(() => {
          const currentTime = Number(Date.now()) 
          const elapse = (currentTime - startTime) / 1000
          let tick = duration - elapse
          let tickPercentage = (elapse/duration) * 100

          emitter(100 <= tickPercentage ? 100 : tickPercentage)
          if (tick > 0) {}
          else {
            // emitter(END)
            startTime = Number(Date.now())
          }
      }, 120)

      return () => {
        clearInterval(timerId)
      }
  })
  
function * startEarningTimer (api: ApiHome, action: ApiAction) {
  yield put(BankActions.updateBankEntries())
  yield put(HomeActions.stopEarningTimer())
  const mobile = yield select(MobileSelectors.mobile)
  if (mobile) {
      let shouldStart = false
      let duration = 120
      let startTime = yield select(MobileSelectors.currentCheckInPeriodTime)
      let earningTimerTick = yield call(onTimerTickChannel, duration, startTime)
      yield takeEvery(earningTimerTick, function* (tick: number) {
            if (shouldStart) {
                const mobile: Mobile = yield select(MobileSelectors.mobile)
                const isLoading = yield select(BankSelectors.isLoading)
                if (!isLoading && mobile) {
                    if (tick >= 100)
                        yield put(BankActions.updateBankEntries())
                }
                
                if (mobile && mobile.beacon)
                    yield put(HomeActions.setTimerTick(tick))
                else
                    earningTimerTick.close()
            }
            shouldStart = true
      })

      yield take(HomeTypes.STOP_EARNING_TIMER)
      earningTimerTick.close()
  }
}

function * verifyEmailUpdate (api: ApiHome, action: ApiAction) {
    const { profile } = action
    const auth = yield select(AuthSelectors.auth)
    yield put(HomeActions.verifyEmailSuccess(profile.verified))
    yield put(AuthActions.signInAuthSuccess({ ...auth, verified: profile.verified}))
    yield put(BeaconManagerActions.stopBeaconMonitoring())
    yield delay(500)
    yield put(BeaconManagerActions.startBeaconMonitoring())
    yield delay(5000)
    yield put(ProfileActions.updateProfile(profile))
}

function * verifyEmail (api: ApiHome, action: ApiAction) {
    const { code } = action
    const profile = yield select(ProfileSelectors.profile)
    const response = yield call(api.verifyEmail, profile.emailAddress, code)

    yield put(
        response.matchWith({
            Ok: ({ value }) => HomeActions.verifyEmailUpdate({ ...profile, verified: value }),
            Error: ({ value }) => HomeActions.verifyEmailFailure(value)
        })
    )
}

function * sendEmailVerification (api: ApiHome, action: ApiAction) {
    const auth = yield select(AuthSelectors.auth)
    const profile = yield select(ProfileSelectors.profile)
    const response = yield call(api.sendEmailVerification, profile.emailAddress, auth)

    yield put(
        response.matchWith({
            Ok: ({ value }) => HomeActions.sendEmailVerificationSuccess(value),
            Error: ({ value }) => HomeActions.sendEmailVerificationFailure(value)
        })
    )
}

export default function() {
    return [
        // takeLatest(HomeTypes.START_EARNING_TIMER, startEarningTimer, api),
        // takeLatest(HomeTypes.VERIFY_EMAIL_UPDATE, verifyEmailUpdate, api),
        // takeLatest(HomeTypes.VERIFY_EMAIL, verifyEmail, api),
        // takeLatest(HomeTypes.SEND_EMAIL_VERIFICATION, sendEmailVerification, api),
    ]
}
  