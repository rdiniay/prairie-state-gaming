import { all, call, put, select, take, takeLatest, takeEvery, delay, cancel, putResolve } from 'redux-saga/effects'
import { eventChannel, END } from 'redux-saga'
import { AppState, Platform, StyleSheet, Text, View } from "react-native";
import AppStateActions, { AppStateSelectors, AppStateTypes } from '../redux/app-state-redux'
import DashboardActions  from '../redux/dashboard-redux'
import { LocationPermissionStatus, Message, Mobile, Auth } from '../models';
import { AuthSelectors } from '../redux/auth-redux';
import MobileActions, { MobileSelectors } from '../redux/mobile-redux';
import MessagingActions from '../redux/messaging-redux';
import { strings } from '../localization/strings'
import iOSBeaconMonitoringTask from '../background-task/beacon/iOSBeaconMonitoringTask';
import AndroidBeaconMonitoringTask from '../background-task/beacon/AndroidBeaconMonitoringTask';
import BackgroundGeolocation from 'react-native-background-geolocation';
import BeaconManagerActions, { BeaconManagerSelectors } from '../redux/beacon-manager-redux';
import PreparationActions from '../redux/preparation-redux';
let currentState = AppState.currentState;

const onAppStateChange = () =>
  eventChannel(emitter => {
    const onHandleAppStateChange = (nextAppState: String) => {
        BackgroundGeolocation.getProviderState(data => {
            let locationPermissionStatus = data?.status
            emitter({nextAppState, locationPermissionStatus})
        })
    };
    AppState.addEventListener("change", onHandleAppStateChange);

    return () => {
        AppState.removeEventListener("change", onHandleAppStateChange);
    }
  })



function * subscribeAppStateChange () {
    yield put(PreparationActions.subscribeBluetoothState())
    if (Platform.OS === 'android')
        yield put(PreparationActions.subscribeGeolocationState())

    const onAppStateChangeChannel  = yield call(onAppStateChange)
    yield takeLatest(onAppStateChangeChannel, function* (appState: any) {
        if (appState) {
            const { nextAppState, locationPermissionStatus } = appState
            yield put(AppStateActions.subscribeAppStateChangeSuccess(nextAppState))
            if (nextAppState === 'active') {
                yield put(MobileActions.checkInReset())
                yield put(DashboardActions.startEarningTimer())
            }
        }
    })

    yield put(AppStateActions.subscribeAppStateChangeSuccess('active'))
    yield take(AppStateTypes.UNSUBSCRIBE_APP_STATE_CHANGE)
    yield put(PreparationActions.unsubscribeBluetoothState())
    if (Platform.OS === 'android')
        yield put(PreparationActions.unsubscribeGeolocationState())
    onAppStateChangeChannel.close()
}

export default function() {
    return [
        takeLatest(AppStateTypes.SUBSCRIBE_APP_STATE_CHANGE,  subscribeAppStateChange),
    ]
}
