import { all, call, put, select, take, takeLatest } from 'redux-saga/effects'
import { Auth, Profile } from '../models'
import AuthActions, { AuthSelectors } from '../redux/auth-redux'
import LoginActions from '../redux/login-redux'
import ProfileActions, { ProfileSelectors, ProfileTypes } from '../redux/profile-redux'
import { ApiProfile, ApiAction } from '../services'
import api from '../services/shared/profile-service'

function * getProfile (api: ApiProfile) {
    const auth = yield select(AuthSelectors.auth)
    const response = yield call(api.getProfile, auth)

    yield put(
        response.matchWith({
            Ok: ({ value }) => ProfileActions.getProfileSuccess(value),
            Error: ({ value }) => ProfileActions.getProfileFailure(value)
        })
    )
}

function * updateProfile (api: ApiProfile, action: ApiAction) {
    const { profile } = action
    const auth = yield select(AuthSelectors.auth)
    const response = yield call(api.updateProfile, profile, auth)

    yield put(
        response.matchWith({
            Ok: ({ value }) => ProfileActions.updateProfileSuccess(value),
            Error: ({ value }) => ProfileActions.updateProfileFailure(value)
        })
    )
}

function * updateEmail (api: ApiProfile, action: ApiAction) {
    const { newEmail } = action
    const auth = yield select(AuthSelectors.auth)
    const profile = yield select(ProfileSelectors.profile)

    const response = yield call(api.updateEmail, newEmail, profile, auth)

    yield put(
        response.matchWith({
            Ok: ({ value }) => ProfileActions.updateEmailSuccess(value),
            Error: ({ value }) => ProfileActions.updateEmailFailure(value)
        })
    )
}

function * changePin (api: ApiProfile, action: ApiAction) {
    const { params } = action
    const { currentPin, newPin } = params
    const profile: Profile = yield select(ProfileSelectors.profile)
    const auth = yield select(AuthSelectors.auth)
    const response = yield call(api.changePin, currentPin, newPin, auth)

    yield put(
        response.matchWith({
            Ok: ({ value }) => ProfileActions.changePinUpdate(value),
            Error: ({ value }) => ProfileActions.changePinFailure(value)
        })
    )
}

function * changePinUpdate (api: ApiProfile, action: ApiAction) {
    const { newAuthData } = action
    const auth = yield select(AuthSelectors.auth)
    const authSuccess = { ...auth, authData: newAuthData }
    yield put(ProfileActions.changePinSuccess(true))
    yield put(AuthActions.signInAuthSuccess(authSuccess))
}

function * deactivateAccount (api: ApiProfile, action: ApiAction) {
    const auth: Auth = yield select(AuthSelectors.auth)
    const profile: Profile = yield select(ProfileSelectors.profile)
    if (auth && profile) {
        const response = yield call(api.deactivateAccount, profile, auth)
    
        yield put(
            response.matchWith({
                Ok: ({ value }) => ProfileActions.deactivateAccountSuccess(value),
                Error: ({ value }) => ProfileActions.deactivateAccountFailure(value)
            })
        )
    }
}

export default function() {
    return [
        takeLatest(ProfileTypes.GET_PROFILE,  getProfile, api),
        takeLatest(ProfileTypes.UPDATE_PROFILE,  updateProfile, api),
        takeLatest(ProfileTypes.UPDATE_EMAIL,  updateEmail, api),
        takeLatest(ProfileTypes.CHANGE_PIN,  changePin, api),
        takeLatest(ProfileTypes.CHANGE_PIN_UPDATE,  changePinUpdate, api),
        takeLatest(ProfileTypes.DEACTIVATE_ACCOUNT,  deactivateAccount, api),
    ]
}
  