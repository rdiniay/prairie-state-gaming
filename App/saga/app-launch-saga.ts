import { call, put, select, takeLatest } from 'redux-saga/effects'
import { eventChannel, END } from 'redux-saga'
import AppLaunchActions, { AppLaunchTypes } from '../redux/app-launch-redux'
import LocationActions, { LocationTypes, LocationSelectors } from '../redux/location-redux'
import MobileActions, { MobileSelectors } from '../redux/mobile-redux'
import MessagingActions from '../redux/messaging-redux'
import { AuthSelectors } from '../redux/auth-redux'
import { Auth, Message, Coordinate, Mobile, LocationPermissionStatus } from '../models'
import Utils from '../services/shared/utils/Utils'
import crashlytics from '@react-native-firebase/crashlytics';
import PushNotification, {Importance} from 'react-native-push-notification'
import { Platform, Alert  } from 'react-native'
import { strings } from '../localization/strings'
import Preference from 'react-native-preference'
import Config from '../config/debugConfig'
import BackgroundGeolocation from 'react-native-background-geolocation'

const onAndroidLocationProvideChanged = () =>
    eventChannel(emitter => {
        BackgroundGeolocation.getProviderState((data) => {
            emitter(data)
        })
    return () => {
    }
})

function * restartLocationProviderChanged () {
    if (Platform.OS === 'android') {
        let locationProviderChanged = onAndroidLocationProvideChanged
        let locationProviderChangedChannel = yield call(locationProviderChanged)
        yield takeLatest(locationProviderChangedChannel, function* (result: any) {
            locationProviderChangedChannel.close()
            
            let coordinate: Coordinate = yield select(LocationSelectors.coordinate)
            yield put(LocationActions.getCurrentLocationSuccess({ ...coordinate, status: result }))
            const auth: Auth = yield select(AuthSelectors.auth)
            const mobile: Mobile = yield select(MobileSelectors.mobile)
            const mobileLoding: Mobile = yield select(MobileSelectors.isLoading)
            const isCheckedIn = auth && mobile && mobile.checkinId && !mobileLoding
            const disabledPreciseLocation: Boolean = result.accuracyAuthorization
            if (isCheckedIn) {
                let shouldForceCheckout = disabledPreciseLocation
                if (result?.status === LocationPermissionStatus.AskNextTime)
                    shouldForceCheckout = true
                else if (result?.status === LocationPermissionStatus.Never)
                    shouldForceCheckout = true
    
                if (shouldForceCheckout)
                    yield put(MobileActions.checkOut(true, false, true, "app-launch-saga:restartLocationProviderChanged", null))
                
            }
        })
    }
}

function * appLaunch () {
    if (Config.useReactotron) {
        console.tron = console
    }
    const auth: Auth = yield select(AuthSelectors.auth)
    yield put(MobileActions.checkInReset())
    yield put(AppLaunchActions.restartLocationProviderChanged())
    yield put(MessagingActions.requestMessagingPermission())
    yield put(MessagingActions.subscribeMessagingOnMessage())
    console.log('app-launch-saga')
    Utils.setGlobalConnectionDialogIsShown(false)
    // Fixed for the issue reported https://github.com/shimohq/react-native-preference/issues/23
    Preference.setWhiteList([])

    if (__DEV__){
        crashlytics()
        // TODO : Add Condition checker that if user opts-out to crashlytics then collection set below will be disabled
        .setCrashlyticsCollectionEnabled(true)
        .then( () => {
            console.log("CRASHLYTICS ENABLED AUTOMATICALLY FOR DEV ENV")
        })
        .catch( (e) => {
            crashlytics().recordError(e)
        })
    }
    else{
        // TODO : Add proper crashlytics configuration for collecting data
    }

    // TODO : Add Condition checker that if user opts-out to crashlytics then the below will be discarded.
    if(auth && auth?.accountId){
        crashlytics().setUserId(`${auth.accountId}`)
        crashlytics().setAttribute('authData', auth.authData)
    }

    // For Android we register channel id if not exists
    if (Platform.OS === 'android'){
        PushNotification.channelExists("RCR-Channel", function (exists) {
            if(__DEV__){
                console.log("RCR-Channel exists?", exists)
            }
            if (!exists){
             PushNotification.createChannel(
                 {
                   channelId: strings.android_channel_id, // (required)
                   channelName: strings.checkin_notification_name, // (required)
                 //   channelDescription: "A channel to categorise your notifications", // (optional) default: undefined.
                 //   playSound: false, // (optional) default: true
                 //   soundName: "default", // (optional) See `soundName` parameter of `localNotification` function
                   importance: Importance.HIGH, // (optional) default: Importance.HIGH. Int value of the Android notification importance
                 //   vibrate: true, // (optional) default: true. Creates the default vibration pattern if true.
                 },
                 (created) => __DEV__ ? console.log(`createChannel returned '${created}'`): '' // (optional) callback returns whether the channel was created, false means it already existed.
               );
            }
         });
    }


}

export default function() {
    return [
        takeLatest(AppLaunchTypes.APP_LAUNCH, appLaunch),
        takeLatest(AppLaunchTypes.RESTART_LOCATION_PROVIDER_CHANGED,  restartLocationProviderChanged),
    ]
}