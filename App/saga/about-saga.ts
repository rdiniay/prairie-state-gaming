import { all, call, put, select, takeLatest } from 'redux-saga/effects'
import { ApiAction, ApiAbout } from '../services'
import { AuthSelectors } from '../redux/auth-redux'
import AboutActions, { AboutTypes } from '../redux/about-redux'
import api from '../services/shared/about-service'

function * getAboutContent (api: ApiAbout, action: ApiAction) {
    const response = yield call(api.getAboutContent)
    yield put(
        response.matchWith({
            Ok: ({ value }) => AboutActions.getAboutContentSuccess(value),
            Error: ({ value }) => AboutActions.getAboutContentFailure(value)
        })
    )
}

function * getAboutFooter (api: ApiAbout, action: ApiAction) {
    const response = yield call(api.getAboutFooter)
    yield put(
        response.matchWith({
            Ok: ({ value }) => AboutActions.getAboutFooterSuccess(value),
            Error: ({ value }) => AboutActions.getAboutFooterFailure(value)
        })
    )
}

export default function() {
    return [
        takeLatest(AboutTypes.GET_ABOUT_CONTENT,  getAboutContent, api),
        takeLatest(AboutTypes.GET_ABOUT_FOOTER,  getAboutFooter, api),
    ]
}
  