import { takeLatest, call, put, select } from 'redux-saga/effects'
import { ApiAuth } from '../services'
import AuthActions, { AuthTypes, AuthSelectors } from '../redux/auth-redux'
import api from '../services/shared/auth-service'

function * getAuthTestLocations (api: ApiAuth) {
    const response = yield call(api.getAuthTestLocations)

    yield put(
        response.matchWith({
            Ok: ({ value }) => AuthActions.getAuthTestLocationsSuccess(value),
            Error: ({ value }) => AuthActions.getAuthTestLocationsFailure(value)
        })
    )
}

export default function() {
    return [
        takeLatest(AuthTypes.GET_AUTH_TEST_LOCATIONS,  getAuthTestLocations, api),
    ]
}
  