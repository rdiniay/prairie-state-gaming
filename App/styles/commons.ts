import { Dimensions, PixelRatio, Platform } from 'react-native'
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import ScaledSheet from 'react-native-scaled-sheet'
import DeviceInfo from 'react-native-device-info'
import { Colors } from '.';
const { width, height } = Dimensions.get('window');

const HeaderTouchableOpacityStyle = () => { 
    return { 
        flexDirection: "row", alignItems: 'center', paddingLeft: 15, paddingVertical: 6, 
    }
}

const HeaderBackButtonTextStyle = () => {

    // If there are difference between platforms then this is where we add it.
    return {
        color : Colors.white,
        textAlign: 'center',
        fontSize: 20 / PixelRatio.getFontScale(),
        fontFamily: 'Raleway-Regular',
        fontWeight: 'normal', 
        fontStyle: 'normal',
    }
}

const HeaderContentLeftContainerStyle = () => { 
    return{
        flex: Platform.OS === 'ios' ? PixelRatio.getPixelSizeForLayoutSize(.25) : PixelRatio.getPixelSizeForLayoutSize(.35),
    }
}

// Usually used on Firstname + Lastname display at the top of the logo
const ContentHeaderTextViewStyle = () => { 
    return { 
        padding: 15, 
        paddingBottom: 0, 
        paddingTop: 0, 
        color: 'white', 
        fontSize: 23 / PixelRatio.getFontScale(), 
        fontWeight: 'bold', 
        fontFamily: "Raleway-Bold", }
}

const ContentHeaderSubTextViewStyle = () => { 
    return { 
        padding: 15, 
        paddingVertical: 2, 
        color: 'white', 
        fontSize: 17 / PixelRatio.getFontScale(), }
}

const ContentLogoViewStyle = () => {
    return { 
        marginLeft: 31 / PixelRatio.get(),
        height: PixelRatio.getPixelSizeForLayoutSize(80),
        marginTop: (height*.140) + useSafeAreaInsets().top,
    }
}

const imageSizeW = 188
const imageSizeH = 46
const ContentImageSource = () => { 
    return require('../assets/new_logo_text.png')
}

const ContentLogoStyle = () => {
    return { 
        width: PixelRatio.getPixelSizeForLayoutSize(imageSizeW / PixelRatio.get()),
        height: PixelRatio.getPixelSizeForLayoutSize(imageSizeH / PixelRatio.get()),
    }
}

const ContentSwipePanelStyle = () => { 
    return { 
        paddingTop: DeviceInfo.hasNotch() ? 20 : 0,
        backgroundColor: '#E6DDCE'
    }
}

const ContentTitleStyle = () => { 
    return { 
        paddingHorizontal: 20, 
        paddingBottom: 7, 
        paddingTop: 8, 
        color: 'black', 
        fontSize: 30 / PixelRatio.getFontScale(), 
        fontWeight: "bold" , 
        fontFamily: "Raleway-Bold", 
        lineHeight: 35
    }
}

const InnerViewStyle = () => { 
    return { 
        borderTopLeftRadius: 20, 
        borderTopRightRadius: 20, 
        backgroundColor: '#F7F5EB',
        paddingBottom: height *.27,
    }
}

const InnerContentViewStyle = () => { 
    return { 
        backgroundColor: Colors.white, 
        flex: 1, 
        marginTop: 30,
        marginBottom: 16,
        marginHorizontal: 20, 
        marginVertical: 5, 
        padding: 20, 
        paddingTop: 10, 
        borderRadius: 15, 
        borderWidth: 0.5, 
        borderColor: Colors.lightgray 
    }
}

const InlineContentViewInnerHeaderViewStyle = () => { 
    return {
        paddingTop: 10, 
        paddingVertical: 5, 
        marginHorizontal: 8, 
    }
}

const InlineContentViewInnerHeaderTextViewStyle = () => { 
    return { 
        color: Colors.black, 
        paddingBottom: 5, 
        fontSize: 12 / PixelRatio.getFontScale(),
        fontWeight: 'bold',
        fontStyle: 'normal',
        fontFamily: "Raleway-Bold",
        lineHeight: 14.09 / PixelRatio.getFontScale(),
        textTransform: 'uppercase',
    }
}


const InlineContentViewInnerContentTextViewStyle = () => { 
    return { 
        color: Colors.black, 
        paddingBottom: 5, 
        fontSize: 12 / PixelRatio.getFontScale(),
        fontFamily: "Raleway-Regular",
        fontStyle: 'normal',
        fontWeight: 'normal',
    }
}

const InlineContentViewInnerContentErrorTextViewStyle = () => { 
    return { 
        color: Colors.red, 
        paddingBottom: 5, 
        fontSize: 12 / PixelRatio.getFontScale(),
        fontFamily: "Raleway-Regular",
        fontStyle: 'normal',
        fontWeight: 'normal',
        lineHeight: 14.09 / PixelRatio.getFontScale(),
    }
}


export default {
    HeaderTouchableOpacityStyle,
    HeaderBackButtonTextStyle, 
    HeaderContentLeftContainerStyle,
    ContentHeaderTextViewStyle,
    ContentHeaderSubTextViewStyle,
    ContentLogoStyle,
    ContentImageSource,
    ContentLogoViewStyle,
    ContentSwipePanelStyle,
    ContentTitleStyle,
    InnerViewStyle,
    InnerContentViewStyle,
    InlineContentViewInnerHeaderViewStyle,
    InlineContentViewInnerHeaderTextViewStyle,
    InlineContentViewInnerContentTextViewStyle,
    InlineContentViewInnerContentErrorTextViewStyle,
}