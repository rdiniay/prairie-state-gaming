import { Action as ReduxAction } from 'redux'
import { useSelector, TypedUseSelectorHook } from 'react-redux'
import { RootState } from '../../redux'
import { Auth, User, Error } from '../../models'

export const RegistrationSelectors = {
    uid : (state: RootState) => state.auth.auth.authData,
    auth : (state: RootState) => state.registration.auth,
    error : (state: RootState) => state.registration.error,
    isLoading : (state: RootState) => state.registration.loading,
}

export enum RegistrationTypes {
  CREATE_ACCOUNT = 'CREATE_ACCOUNT',
  CREATE_ACCOUNT_SUCCESS = 'CREATE_ACCOUNT_SUCCESS',
  CREATE_ACCOUNT_FAILURE = 'CREATE_ACCOUNT_FAILURE'
}

export type RegistrationAction =
  | CreateAccount
  | CreateAccountSuccess
  | CreateAccountFailure

interface CreateAccount
  extends ReduxAction<RegistrationTypes.CREATE_ACCOUNT> {
  user: User
}

interface CreateAccountSuccess
  extends ReduxAction<RegistrationTypes.CREATE_ACCOUNT_SUCCESS> {
  payload: {
    auth: Auth
  }
}

interface CreateAccountFailure
  extends ReduxAction<RegistrationTypes.CREATE_ACCOUNT_FAILURE> {
  payload: {
    error: Error
  }
}

const createAccount = (user: User): CreateAccount => ({
  type: RegistrationTypes.CREATE_ACCOUNT,
  user: user,
})

const createAccountSuccess = (auth: Auth): CreateAccountSuccess => ({
  type: RegistrationTypes.CREATE_ACCOUNT_SUCCESS,
  payload: { auth }
})

const createAccountFailure = (error: Error): CreateAccountFailure => ({
  type: RegistrationTypes.CREATE_ACCOUNT_FAILURE,
  payload: { error }
})

export default {
  createAccount,
  createAccountSuccess,
  createAccountFailure,
}
