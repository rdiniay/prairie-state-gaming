import { Reducer } from 'redux'
import { RegistrationTypes, RegistrationAction } from '../registration-redux'
import { Auth, Error } from '../../models'

type RegistrationState = {
  loading: boolean
  auth: Auth
  error: Error
}

const INITIAL_STATE: RegistrationState = {
  loading: false,
  auth: null, 
  error: null
}

const createAccount = (state: RegistrationState) => ({
    ...state, loading: true,
})

const createAccountSuccess = (state: RegistrationState, auth: Auth) => ({
  ...state, loading: false, error: null, auth: auth
})

const createAccountFailure = (state: RegistrationState, error: Error) => ({
  ...state, loading: false, error: error
})

const reducer: Reducer<RegistrationState, RegistrationAction> = (
  state = INITIAL_STATE,
  action
) => {
  switch (action.type) {
    case RegistrationTypes.CREATE_ACCOUNT:
      return createAccount(state)
    case RegistrationTypes.CREATE_ACCOUNT_SUCCESS:
      return createAccountSuccess(state, action.payload.auth)
    case RegistrationTypes.CREATE_ACCOUNT_FAILURE:
      return createAccountFailure(state, action.payload.error)
    default:
      return state
  }
}

export default reducer
