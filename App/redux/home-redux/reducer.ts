import { Reducer } from 'redux'
import { HomeTypes, HomeAction } from '../home-redux'
import { Drawing, Error } from '../../models'

type HomeState = {
  loading: boolean
  earningTick: number 
  isEmailVerified: boolean
  isEmailSent: boolean
  error: Error
}

const INITIAL_STATE: HomeState = {
  loading: false,
  earningTick: 0, 
  isEmailVerified: false, 
  isEmailSent: false, 
  error: null
}

const clearData = (state: HomeState) => ({
  ...state, ...INITIAL_STATE
})

const setTimerTick = (state: HomeState, tick: number) => ({
  ...state, earningTick: tick
})

const setLoading = (state: HomeState) => ({
  ...state, loading: true, error: null, isEmailVerified: false,
})

const verifyEmailSuccess = (state: HomeState, success: boolean) => ({
  ...state, loading: false, error: null, isEmailSent: false, isEmailVerified: true
})

const sendEmailVerificationSuccess = (state: HomeState, success: boolean) => ({
  ...state, loading: false, error: null, isEmailSent: true
})

const failure = (state: HomeState, error: Error) => ({
  ...state, loading: false, error: error,
})

const reducer: Reducer<HomeState, HomeAction> = (
  state = INITIAL_STATE,
  action
) => {
  switch (action.type) {
    case HomeTypes.CLEAR_DATA:
      return clearData(state)
    // case HomeTypes.START_EARNING_TIMER:
      // return setTimerTick(state, 0)
    case HomeTypes.SET_TIMER_TICK:
      return setTimerTick(state, action.tick)

    case HomeTypes.VERIFY_EMAIL:
      return setLoading(state)
    case HomeTypes.VERIFY_EMAIL_SUCCESS:
      return verifyEmailSuccess(state, action.payload.success)
    case HomeTypes.VERIFY_EMAIL_FAILURE:
      return failure(state, action.payload.error)

    case HomeTypes.SEND_EMAIL_VERIFICATION:
      return setLoading(state)
    case HomeTypes.SEND_EMAIL_VERIFICATION_SUCCESS:
      return sendEmailVerificationSuccess(state, action.payload.success)
    case HomeTypes.SEND_EMAIL_VERIFICATION_FAILURE:
      return failure(state, action.payload.error)
    default:
      return state
  }
}

export default reducer
