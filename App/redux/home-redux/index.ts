import { Action as ReduxAction } from 'redux'
import { useSelector, TypedUseSelectorHook } from 'react-redux'
import { RootState } from '../../redux'
import { Profile, Error } from '../../models'

export const HomeSelectors = {
    error : (state: RootState) => null, //state.home.error,
    isEmailSent : (state: RootState) => null, //state.home.isEmailSent,
    earningTick : (state: RootState) => null, //state.home.earningTick,
    isLoading : (state: RootState) => null, //state.home.loading
}

export enum HomeTypes {
  CLEAR_DATA = 'CLEAR_DATA',
  STOP_EARNING_TIMER = 'STOP_EARNING_TIMER',
  START_EARNING_TIMER = 'START_EARNING_TIMER',
  SET_TIMER_TICK = 'SET_TIMER_TICK',
  VERIFY_EMAIL_UPDATE = 'VERIFY_EMAIL_UPDATE',

  VERIFY_EMAIL = 'VERIFY_EMAIL',
  VERIFY_EMAIL_SUCCESS = 'VERIFY_EMAIL_SUCCESS',
  VERIFY_EMAIL_FAILURE = 'VERIFY_EMAIL_FAILURE',

  SEND_EMAIL_VERIFICATION = 'SEND_EMAIL_VERIFICATION',
  SEND_EMAIL_VERIFICATION_SUCCESS = 'SEND_EMAIL_VERIFICATION_SUCCESS',
  SEND_EMAIL_VERIFICATION_FAILURE = 'SEND_EMAIL_VERIFICATION_FAILURE',
}

export type HomeAction =
  | ClearData
  | StopEarningTimer
  | StartEarningTimer
  | SetTimerTick
  | VerifyEmailUpdate
  | VerifyEmail
  | VerifyEmailSuccess
  | VerifyEmailFailure
  | SendEmailVerification
  | SendEmailVerificationSuccess
  | SendEmailVerificationFailure

type ClearData = ReduxAction<HomeTypes.CLEAR_DATA>
type StopEarningTimer = ReduxAction<HomeTypes.STOP_EARNING_TIMER>
type StartEarningTimer = ReduxAction<HomeTypes.START_EARNING_TIMER>

interface SetTimerTick
  extends ReduxAction<HomeTypes.SET_TIMER_TICK> {
  tick: number
}

interface VerifyEmailUpdate
  extends ReduxAction<HomeTypes.VERIFY_EMAIL_UPDATE> {
  profile: Profile
}

interface VerifyEmail
  extends ReduxAction<HomeTypes.VERIFY_EMAIL> {
  code: string
}

interface VerifyEmailSuccess
  extends ReduxAction<HomeTypes.VERIFY_EMAIL_SUCCESS> {
  payload: {
    success: boolean
  }
}

interface VerifyEmailFailure
  extends ReduxAction<HomeTypes.VERIFY_EMAIL_FAILURE> {
  payload: {
    error: Error
  }
}

interface SendEmailVerification
  extends ReduxAction<HomeTypes.SEND_EMAIL_VERIFICATION> {
}

interface SendEmailVerificationSuccess
  extends ReduxAction<HomeTypes.SEND_EMAIL_VERIFICATION_SUCCESS> {
  payload: {
    success: boolean
  }
}

interface SendEmailVerificationFailure
  extends ReduxAction<HomeTypes.SEND_EMAIL_VERIFICATION_FAILURE> {
  payload: {
    error: Error
  }
}

const clearData = (): ClearData => ({
  type: HomeTypes.CLEAR_DATA,
})

const stopEarningTimer = (): StopEarningTimer => ({
  type: HomeTypes.STOP_EARNING_TIMER,
})

const startEarningTimer = (): StartEarningTimer => ({
  type: HomeTypes.START_EARNING_TIMER,
})

const setTimerTick = (tick: number): SetTimerTick => ({
  type: HomeTypes.SET_TIMER_TICK,
  tick: tick,
})

const verifyEmailUpdate = (profile: Profile): VerifyEmailUpdate => ({
  type: HomeTypes.VERIFY_EMAIL_UPDATE,
  profile: profile,
})

const verifyEmail = (code: string): VerifyEmail => ({
  type: HomeTypes.VERIFY_EMAIL,
  code: code,
})

const verifyEmailSuccess = (success: boolean): VerifyEmailSuccess => ({
  type: HomeTypes.VERIFY_EMAIL_SUCCESS,
  payload: { success }
})

const verifyEmailFailure = (error: Error): VerifyEmailFailure => ({
  type: HomeTypes.VERIFY_EMAIL_FAILURE,
  payload: { error }
})

const sendEmailVerification = (): SendEmailVerification => ({
  type: HomeTypes.SEND_EMAIL_VERIFICATION,
})

const sendEmailVerificationSuccess = (success: boolean): SendEmailVerificationSuccess => ({
  type: HomeTypes.SEND_EMAIL_VERIFICATION_SUCCESS,
  payload: { success }
})

const sendEmailVerificationFailure = (error: Error): SendEmailVerificationFailure => ({
  type: HomeTypes.SEND_EMAIL_VERIFICATION_FAILURE,
  payload: { error }
})

export default {
  clearData,
  stopEarningTimer,
  startEarningTimer,
  setTimerTick,
  verifyEmailUpdate,

  verifyEmail,
  verifyEmailSuccess,
  verifyEmailFailure,

  sendEmailVerification,
  sendEmailVerificationSuccess,
  sendEmailVerificationFailure,
}
