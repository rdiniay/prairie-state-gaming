import { Reducer } from 'redux'
import { HelpSupportTypes, HelpSupportAction } from '../help-support-redux'
import { Error } from '../../models'

type HelpSupportState = {
  loading: boolean
  success: any
  error: Error
}

const INITIAL_STATE: HelpSupportState = {
  loading: false,
  success: null, 
  error: null,
}

const sendDiagnosticLogs = (state: HelpSupportState) => ({
    ...state, loading: true
})

const sendDiagnosticLogsSuccess = (state: HelpSupportState, success: any) => ({
  ...state, loading: false, error: null, success: success
})

const sendDiagnosticLogsFailure = (state: HelpSupportState, error: Error) => ({
  ...state, loading: false, error: error
})

const reducer: Reducer<HelpSupportState, HelpSupportAction> = (
  state = INITIAL_STATE,
  action
) => {
  switch (action.type) {
    case HelpSupportTypes.SEND_DIAGNOSTIC_LOGS:
      console.log("support reducer", action)
      return sendDiagnosticLogs(state)
    case HelpSupportTypes.SEND_DIAGNOSTIC_LOGS_SUCCESS:
      return sendDiagnosticLogsSuccess(state, action.payload.success)
    case HelpSupportTypes.SEND_DIAGNOSTIC_LOGS_FAILURE:
      return sendDiagnosticLogsFailure(state, action.payload.error)
    default:
      return state
  }
}

export default reducer
