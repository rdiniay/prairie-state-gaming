import { Action as ReduxAction } from 'redux'
import { RootState } from '../../redux'
import { Error } from '../../models'

export const HelpSupportSelectors = {
    error : (state: RootState) => state.helpSupport.error,
    success : (state: RootState) => state.helpSupport.success,
    isLoading : (state: RootState) => state.helpSupport.loading,
}

export enum HelpSupportTypes {
  SEND_DIAGNOSTIC_LOGS = 'SEND_DIAGNOSTIC_LOGS',
  SEND_DIAGNOSTIC_LOGS_SUCCESS = 'SEND_DIAGNOSTIC_LOGS_SUCCESS',
  SEND_DIAGNOSTIC_LOGS_FAILURE = 'SEND_DIAGNOSTIC_LOGS_FAILURE'
}

export type HelpSupportAction =
  | SendDiagnosticLogs
  | SendDiagnosticLogsSuccess
  | SendDiagnosticLogsFailure

interface SendDiagnosticLogs
  extends ReduxAction<HelpSupportTypes.SEND_DIAGNOSTIC_LOGS> {
}

interface SendDiagnosticLogsSuccess
  extends ReduxAction<HelpSupportTypes.SEND_DIAGNOSTIC_LOGS_SUCCESS> {
  payload: {
    success: any
  }
}

interface SendDiagnosticLogsFailure
  extends ReduxAction<HelpSupportTypes.SEND_DIAGNOSTIC_LOGS_FAILURE> {
  payload: {
    error: Error
  }
}

const sendDiagnosticLogs = (): SendDiagnosticLogs => ({
  type: HelpSupportTypes.SEND_DIAGNOSTIC_LOGS
})

const sendDiagnosticLogsSuccess = (success: any): SendDiagnosticLogsSuccess => ({
  type: HelpSupportTypes.SEND_DIAGNOSTIC_LOGS_SUCCESS,
  payload: { success }
})

const sendDiagnosticLogsFailure = (error: Error): SendDiagnosticLogsFailure => ({
  type: HelpSupportTypes.SEND_DIAGNOSTIC_LOGS_FAILURE,
  payload: { error }
})

export default {
  sendDiagnosticLogs,
  sendDiagnosticLogsSuccess,
  sendDiagnosticLogsFailure,
}
