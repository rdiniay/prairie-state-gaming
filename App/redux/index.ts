import { applyMiddleware, createStore, Reducer, compose } from 'redux'
import { combineReducers } from 'redux'
import { persistReducer } from 'redux-persist'
import createSagaMiddleware, { Saga } from 'redux-saga'
import reduxPersist from '../config/reduxPersist'
import rehydration from '../services/rehydration'
import Config from '../config/debugConfig'

import appLaunchReducer from './app-launch-redux/reducer'
import authReducer from './auth-redux/reducer'
import loginReducers from './login-redux/reducer'
import profileReducer from './profile-redux/reducer'
import registrationReducer from './registration-redux/reducer'
import drawingReducer from './drawing-redux/reducer'
import tosReducer from './tos-redux/reducer'
import aboutReducer from './about-redux/reducer'
import menuReducer from './menu-redux/reducer'
import referralReducer from './referral-redux/reducer'
import locationReducer from './location-redux/reducer'
import homeReducer from './home-redux/reducer'
import mobileReducer from './mobile-redux/reducer'
import forgotPinReducer from './forgot-pin-redux/reducer'
import helpSupportReducer from './help-support-redux/reducer'
import bankReducer from './bank-redux/reducer'
import beaconManagerReducer from './beacon-manager-redux/reducer'
import messagingReducer from './messaging-redux/reducer'
import appStateReducer from './app-state-redux/reducer'
import dashboardReducer from './dashboard-redux/reducer'
import preparationReducer from './preparation-redux/reducer'
import backgroundFetchReducer from './background-fetch-redux/reducer'
import {reducer as network } from 'react-native-offline'

import rootSaga from '../saga'

export const rootReducer = combineReducers({
  appLaunch: appLaunchReducer,
  auth: authReducer,
  login: loginReducers,
  registration: registrationReducer,
  profile: profileReducer,
  drawing: drawingReducer,
  tos: tosReducer,
  about: aboutReducer,
  menu: menuReducer,
  referral: referralReducer,
  location: locationReducer,
  // home: homeReducer,
  mobile: mobileReducer,
  forgotPin: forgotPinReducer,
  helpSupport: helpSupportReducer,
  bank: bankReducer,
  beaconManager: beaconManagerReducer,
  messaging: messagingReducer,
  appState: appStateReducer,
  dashboard: dashboardReducer,
  preparation: preparationReducer,
  backgroundFetch: backgroundFetchReducer,
  network,
})

const configureStore = (reducer: Reducer, saga: Saga) => {
  let finalReducers = reducer
  // If rehydration is on use persistReducer otherwise default combineReducers
  if (reduxPersist.active)
    finalReducers = persistReducer(reduxPersist.storeConfig, reducer)

  const sagaMonitor = Config.useReactotron ? console.tron?.createSagaMonitor() : null
  const sagaMiddleware = createSagaMiddleware({ sagaMonitor })
  const middlewares = [sagaMiddleware]

  const store = Config.useReactotron ? createStore(finalReducers, {}, compose(applyMiddleware(...middlewares), console.tron.createEnhancer())) :
                                        createStore(finalReducers, applyMiddleware(...middlewares))


  if (reduxPersist.active)
    rehydration.updateReducers(store)

  sagaMiddleware.run(saga)

  if (module.hot) {
    module.hot.accept(() => {
      const nextRootReducer = require('../redux').rootReducer
      store.replaceReducer(nextRootReducer)

      // const newYieldedSagas = require('../saga')
      // sagasManager.cancel()
      // sagasManager.done.then(() => {
      //   sagasManager = sagaMiddleware(newYieldedSagas)
      // })
    })
  }
  return store
}

const store = configureStore(rootReducer, rootSaga)
export type RootDispatch = typeof store.dispatch
export type RootState = ReturnType<typeof rootReducer>

export default store
