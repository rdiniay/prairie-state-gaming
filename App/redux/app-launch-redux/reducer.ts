import { Reducer } from 'redux'
import AppLaunchActions, { AppLaunchTypes } from '../app-launch-redux'

type AppLaunchState = {
  didAppLaunch: boolean,
  onboardingSuccess: boolean,
}

const INITIAL_STATE: AppLaunchState = {
  didAppLaunch: false,
  onboardingSuccess: false,
}

const appLaunch = (state: AppLaunchState) => ({
    ...state, didAppLaunch: true,
})

const onboardingLocationSuccess = (state: AppLaunchState) => ({
  ...state, onboardingSuccess: true,
})


const reducer: Reducer<AppLaunchState> = (
  state = INITIAL_STATE,
  action
) => {
  switch (action.type) {
    case AppLaunchTypes.APP_LAUNCH:
      return appLaunch(state)

    case AppLaunchTypes.ONBOARDING_LOCATION_SUCCESS:
      return onboardingLocationSuccess(state)
    default:
      return state
  }
}

export default reducer
