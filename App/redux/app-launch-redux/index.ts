import { Action as ReduxAction } from 'redux'
import { useSelector, TypedUseSelectorHook } from 'react-redux'
import { RootState } from '..'

export const AppLaunchSelectors = {
  didAppLaunch: (state: RootState) => state.appLaunch.didAppLaunch,
  onboardingSuccess: (state: RootState) => state.appLaunch.onboardingSuccess,
}

export enum AppLaunchTypes {
  APP_LAUNCH = 'APP_LAUNCH',
  ONBOARDING_LOCATION_SUCCESS = 'ONBOARDING_LOCATION_SUCCESS',
  RESTART_LOCATION_PROVIDER_CHANGED = 'RESTART_LOCATION_PROVIDER_CHANGED',
}

export type AppLaunchAction = 
    | AppLaunch
    | OnboardingLocationSuccess
    | RestartLocationProviderChanged

type AppLaunch = ReduxAction<AppLaunchTypes.APP_LAUNCH>

type RestartLocationProviderChanged = ReduxAction<AppLaunchTypes.RESTART_LOCATION_PROVIDER_CHANGED>

interface OnboardingLocationSuccess
  extends ReduxAction<AppLaunchTypes.ONBOARDING_LOCATION_SUCCESS> {
}

const appLaunch = (): AppLaunch => ({
  type: AppLaunchTypes.APP_LAUNCH
})

const onboardingLocationSuccess = (): OnboardingLocationSuccess => ({
  type: AppLaunchTypes.ONBOARDING_LOCATION_SUCCESS,
})

const restartLocationProviderChanged = (): RestartLocationProviderChanged => ({
  type: AppLaunchTypes.RESTART_LOCATION_PROVIDER_CHANGED,
})

export default {
  appLaunch,
  onboardingLocationSuccess,
  restartLocationProviderChanged,
}
