import { Action as ReduxAction } from 'redux'
import { useSelector, TypedUseSelectorHook } from 'react-redux'
import { RootState } from '..'

export const PreparationSelectors = {
  gps: (state: RootState) => state.preparation.gps,
  geoLocation: (state: RootState) => state.preparation.geoLocation,
  network: (state: RootState) => state.preparation.network,
  bluetooth: (state: RootState) => state.preparation.bluetooth,
  notification: (state: RootState) => state.preparation.notification,
  nearbyDevices: (state: RootState) => state.preparation.nearbyDevices,
  timestamp: (state: RootState) => state.preparation.timestamp,
}

export enum PreparationTypes {
  SUBSCRIBE_GEOLOCATION_STATE = 'SUBSCRIBE_GEOLOCATION_STATE',
  UNSUBSCRIBE_GEOLOCATION_STATE = 'UNSUBSCRIBE_GEOLOCATION_STATE',
  SUBSCRIBE_BLUETOOTH_STATE = 'SUBSCRIBE_BLUETOOTH_STATE',
  UNSUBSCRIBE_BLUETOOTH_STATE = 'UNSUBSCRIBE_BLUETOOTH_STATE',
  GET_PREPARATIONS = 'GET_PREPARATIONS',
  GET_PREPARATIONS_SUCCESS = 'GET_PREPARATIONS_SUCCESS',
}

export type PreparationAction =
    | SubscribeGeolocationState
    | UnsubscribeGeolocationState
    | SubscribeBluetoothState
    | UnsubscribeBluetoothState
    | GetPreparations
    | GetPreparationsSuccess

type SubscribeGeolocationState = ReduxAction<PreparationTypes.SUBSCRIBE_GEOLOCATION_STATE>
type UnsubscribeGeolocationState = ReduxAction<PreparationTypes.UNSUBSCRIBE_GEOLOCATION_STATE>

type SubscribeBluetoothState = ReduxAction<PreparationTypes.SUBSCRIBE_BLUETOOTH_STATE>
type UnsubscribeBluetoothState = ReduxAction<PreparationTypes.UNSUBSCRIBE_BLUETOOTH_STATE>

type GetPreparations = ReduxAction<PreparationTypes.GET_PREPARATIONS>

interface GetPreparationsSuccess
  extends ReduxAction<PreparationTypes.GET_PREPARATIONS_SUCCESS> {
  payload: {
    preparations: any
  }
}

const subscribeGeolocationState = (): SubscribeGeolocationState => ({
  type: PreparationTypes.SUBSCRIBE_GEOLOCATION_STATE
})

const unsubscribeGeolocationState = (): UnsubscribeGeolocationState => ({
  type: PreparationTypes.UNSUBSCRIBE_GEOLOCATION_STATE
})

const subscribeBluetoothState = (): SubscribeBluetoothState => ({
  type: PreparationTypes.SUBSCRIBE_BLUETOOTH_STATE
})

const unsubscribeBluetoothState = (): UnsubscribeBluetoothState => ({
  type: PreparationTypes.UNSUBSCRIBE_BLUETOOTH_STATE
})

const getPreparations = (): GetPreparations => ({
  type: PreparationTypes.GET_PREPARATIONS
})

const getPreparationsSuccess = (preparations: any): GetPreparationsSuccess => ({
  type: PreparationTypes.GET_PREPARATIONS_SUCCESS,
  payload: {
    preparations,
  },
})

export default {
  subscribeGeolocationState,
  unsubscribeGeolocationState,
  subscribeBluetoothState,
  unsubscribeBluetoothState,
  getPreparations,
  getPreparationsSuccess,
}
