import { Reducer } from 'redux'
import AppLaunchActions, { PreparationTypes } from '../preparation-redux'

type PreparationState = {
  gps: boolean,
  geoLocation: boolean,
  network: boolean,
  bluetooth: boolean,
  notification: boolean,
  nearbyDevices: boolean,
  timestamp: Number,
}

const INITIAL_STATE: PreparationState = {
  gps: false,
  geoLocation: false,
  network: false,
  bluetooth: false,
  notification: false,
  nearbyDevices: false,
  timestamp: null,
}

const getPreparationsSuccess = (state: PreparationState, preparations: any) => ({
  ...state, 
  gps: preparations?.gps, 
  geoLocation: preparations?.geoLocation,
  network: preparations?.network,
  bluetooth: preparations?.bluetooth, 
  notification: preparations?.notification === 1,
  nearbyDevices: preparations?.nearbyDevices,
  timestamp: Date.now(),
})


const reducer: Reducer<PreparationState> = (
  state = INITIAL_STATE,
  action
) => {
  switch (action.type) {

    case PreparationTypes.GET_PREPARATIONS_SUCCESS:
      return getPreparationsSuccess(state, action.payload.preparations)
    default:
      return state
  }
}

export default reducer
