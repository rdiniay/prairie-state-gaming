import { Action as ReduxAction } from 'redux'
import { useSelector, TypedUseSelectorHook } from 'react-redux'
import { RootState } from '..'
import { Bank, Period, Error } from '../../models'

export const BackgroundFetchSelectors = {
    lastFetch: (state: RootState) => state.backgroundFetch.lastFetch,
    isLoading : (state: RootState) => state.backgroundFetch.loading,
    error : (state: RootState) => state.backgroundFetch.error,
}

export enum BackgroundFetchTypes {
  START_BACKGROUND_FETCH_MONITORING = 'START_BACKGROUND_FETCH_MONITORING',
  STOP_BACKGROUND_FETCH_MONITORING = 'STOP_BACKGROUND_FETCH_MONITORING',
  SAVE_LAST_FETCH = 'SAVE_LAST_FETCH',
}

export type BackgroundFetchAction =
  | StartBackgroundFetchMonitoring
  | SaveLastFetch

type StartBackgroundFetchMonitoring = ReduxAction<BackgroundFetchTypes.START_BACKGROUND_FETCH_MONITORING>
type StopBackgroundFetchMonitoring = ReduxAction<BackgroundFetchTypes.STOP_BACKGROUND_FETCH_MONITORING>


interface SaveLastFetch
  extends ReduxAction<BackgroundFetchTypes.SAVE_LAST_FETCH> {
    lastFetch: Number
  }

const startBackgroundFetchMonitoring = (): StartBackgroundFetchMonitoring => ({
  type: BackgroundFetchTypes.START_BACKGROUND_FETCH_MONITORING,
})

const stopBackgroundFetchMonitoring = (): StopBackgroundFetchMonitoring => ({
    type: BackgroundFetchTypes.STOP_BACKGROUND_FETCH_MONITORING,
})


const saveLastFetch = (lastFetch: Number): SaveLastFetch => ({
  type: BackgroundFetchTypes.SAVE_LAST_FETCH,
  lastFetch: lastFetch
})

export default {
  startBackgroundFetchMonitoring,
  stopBackgroundFetchMonitoring,
  saveLastFetch,
}
