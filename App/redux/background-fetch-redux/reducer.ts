import { Reducer } from 'redux'
import { BackgroundFetchTypes, BackgroundFetchAction } from '../background-fetch-redux'
import { Bank, Entry, Error } from '../../models'

type BackgroundFetchState = {
  lastFetch: Number
  loading: boolean
  error: Error
}

const INITIAL_STATE: BackgroundFetchState = {
  lastFetch: null,
  loading: false,
  error: null
}

const saveLastFetch = (state: BackgroundFetchState, lastFetch: Number) => ({
  ...state, lastFetch: lastFetch
})

const setLoading = (state: BackgroundFetchState) => ({
    ...state, loading: true,
})

const failure = (state: BackgroundFetchState, error: Error) => ({
  ...state, loading: false, error: error
})

const reducer: Reducer<BackgroundFetchState, BackgroundFetchAction> = (
  state = INITIAL_STATE,
  action
) => {
  switch (action.type) {
    case BackgroundFetchTypes.START_BACKGROUND_FETCH_MONITORING:
      return setLoading(state)
    case BackgroundFetchTypes.SAVE_LAST_FETCH:
      return saveLastFetch(state, action.lastFetch)
    default:
      return state
  }
}

export default reducer
