import { Reducer } from 'redux'
import { LocationTypes, LocationAction } from '../location-redux'
import { LocationCheckin, Coordinate, Location, Error } from '../../models'
import Immutable from 'seamless-immutable'

type LocationState = {
  loading: boolean
  coordinate: Coordinate
  locations: Location[]
  locationCheckins: LocationCheckin[]
  error: Error
}

const INITIAL_STATE: LocationState = {
  loading: false,
  coordinate: null,
  locations: [],
  locationCheckins: [],
  error: null
}

const setLoading = (state: LocationState) => ({
  ...state, loading: true,
})

const clear = (state: LocationState) => ({
  ...state, ...INITIAL_STATE
})

const saveLocationCheckinSuccess = (state: LocationState, locationCheckin: LocationCheckin) => {
  let locationCheckins: LocationCheckin[] = Immutable.asMutable(state.locationCheckins ? state.locationCheckins : [])
  if (!locationCheckins.find((locationCheckin: LocationCheckin) => locationCheckin.id === locationCheckin.id))
    locationCheckins.push(locationCheckin)
  else {
    locationCheckins = locationCheckins.map((currentLocationCheckin: LocationCheckin) => {
      if (currentLocationCheckin.id === locationCheckin.id)
        return locationCheckin

      return currentLocationCheckin
    })
  }
  return ({
    ...state, locationCheckins: locationCheckins
  })
}

const getCurrentLocationSuccess = (state: LocationState, coordinate: Coordinate) => ({
  ...state, loading: false, error: null, coordinate: coordinate
})

const getLocationBeaconsSuccess = (state: LocationState, locations: Location[]) => {
  let locationArray: Location[] = Immutable.asMutable(state.locations ? state.locations : locations)
  if (state.locations && locations) {
    locations.map((newLocation: Location) => {
      // OVERWRITE ALL LOCATIONS INCLUDING ITS DETAILS
      if (!locationArray.find((location: Location) => location.id === newLocation.id))
        locationArray.push(newLocation)
      else {
        locationArray = locationArray.map((currentLocation: Location) => {
          if (currentLocation.id === newLocation.id)
            return ({ ...currentLocation, ...newLocation })
          // HARD DELETE
          else if(!(locations.find((location: Location) => location.id === currentLocation.id)) && currentLocation.isNear)
            return []

          return currentLocation
        }).flat()
      }
    })
  }
  return ({
    ...state, loading: false, error: null, locations: locations ? locationArray : []
  })
}

const failure = (state: LocationState, error: Error) => ({
  ...state, loading: false, error: error, coordinate: null
})

const reducer: Reducer<LocationState, LocationAction> = (
    state = INITIAL_STATE,
    action
) => {
  switch (action.type) {
    case LocationTypes.CLEAR:
      return clear(state)

    case LocationTypes.SAVE_LOCATION_CHECKIN_SUCCESS:
      return saveLocationCheckinSuccess(state, action.locationCheckin)

    case LocationTypes.GET_CURRENT_LOCATION:
      return setLoading(state)
    case LocationTypes.GET_CURRENT_LOCATION_SUCCESS:
      return getCurrentLocationSuccess(state, action.payload.coordinate)
    case LocationTypes.GET_CURRENT_LOCATION_FAILURE:
      return failure(state, action.payload.error)

    case LocationTypes.GET_LOCATION_BEACONS:
      return setLoading(state)
    case LocationTypes.GET_LOCATION_BEACONS_SUCCESS:
      return getLocationBeaconsSuccess(state, action.payload.locations)
    case LocationTypes.GET_LOCATION_BEACONS_FAILURE:
      return failure(state, action.payload.error)
    default:
      return state
  }
}

export default reducer
