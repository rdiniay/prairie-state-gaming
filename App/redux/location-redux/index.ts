import { Action as ReduxAction } from 'redux'
import { useSelector, TypedUseSelectorHook } from 'react-redux'
import { RootState } from '..'
import { Coordinate, Error, Location, Beacon, LocationCheckin } from '../../models'

export const LocationSelectors = {
  coordinate : (state: RootState) => state.location.coordinate,
  locations : (state: RootState) => state.location.locations,
  locationCheckins : (state: RootState) => state.location.locationCheckins,
  isLoading : (state: RootState) => state.drawing.loading,
  error: (state: RootState) => state.drawing.error,
}

export enum LocationTypes {
  CLEAR = 'CLEAR_LOCATION',
  SAVE_LOCATION_CHECKIN = 'SAVE_LOCATION_CHECKIN',
  SAVE_LOCATION_CHECKIN_SUCCESS = 'SAVE_LOCATION_CHECKIN_SUCCESS',

  GET_CURRENT_PROMOTION_LOCATION = 'GET_CURRENT_PROMOTION_LOCATION',
  GET_CURRENT_PROMOTION_LOCATION_FOR_LOGIN = 'GET_CURRENT_PROMOTION_LOCATION_FOR_LOGIN',
  GET_CURRENT_LOCATION = 'GET_CURRENT_LOCATION',
  GET_CURRENT_LOCATION_SUCCESS = 'GET_CURRENT_LOCATION_SUCCESS',
  GET_CURRENT_LOCATION_FAILURE = 'GET_CURRENT_LOCATION_FAILURE',

  GET_LOCATION_BEACONS = 'GET_LOCATION_BEACONS',
  GET_LOCATION_BEACONS_SUCCESS = 'GET_LOCATION_BEACONS_SUCCESS',
  GET_LOCATION_BEACONS_FAILURE = 'GET_LOCATION_BEACONS_FAILURE',

  SUBSCRIBE_LOCATION_PROVIDER_CHANGED = 'SUBSCRIBE_LOCATION_PROVIDER_CHANGED',
  UNSUBSCRIBE_LOCATION_PROVIDER_CHANGED = 'UNSUBSCRIBE_LOCATION_PROVIDER_CHANGED',
}

export type LocationAction =
    | Clear
    | SaveLocationCheckin
    | SaveLocationCheckinSuccess
    | GetCurrentPromotionLocation
    | GetCurrentPromotionLocationForLogin
    | GetCurrentLocation
    | GetCurrentLocationSuccess
    | GetCurrentLocationFailure
    | GetLocationBeacons
    | GetLocationBeaconsSuccess
    | GetLocationBeaconsFailure
    | SubscribeLocationProviderChanged
    | UnsubscribeLocationProviderChanged


type Clear = ReduxAction<LocationTypes.CLEAR>

interface SaveLocationCheckin
    extends ReduxAction<LocationTypes.SAVE_LOCATION_CHECKIN> {
  beacon: Beacon,
  action: String,
}

interface SaveLocationCheckinSuccess
    extends ReduxAction<LocationTypes.SAVE_LOCATION_CHECKIN_SUCCESS> {
  locationCheckin: LocationCheckin
}

type GetCurrentPromotionLocation = ReduxAction<LocationTypes.GET_CURRENT_PROMOTION_LOCATION>
type GetCurrentPromotionLocationForLogin = ReduxAction<LocationTypes.GET_CURRENT_PROMOTION_LOCATION_FOR_LOGIN>

type GetCurrentLocation = ReduxAction<LocationTypes.GET_CURRENT_LOCATION>

interface GetCurrentLocationSuccess
    extends ReduxAction<LocationTypes.GET_CURRENT_LOCATION_SUCCESS> {
  payload: {
    coordinate: Coordinate
  }
}

interface GetCurrentLocationFailure
    extends ReduxAction<LocationTypes.GET_CURRENT_LOCATION_FAILURE> {
  payload: {
    error: Error
  }
}

interface GetLocationBeacons
    extends ReduxAction<LocationTypes.GET_LOCATION_BEACONS> {
  coordinate: Coordinate
}

interface GetLocationBeaconsSuccess
    extends ReduxAction<LocationTypes.GET_LOCATION_BEACONS_SUCCESS> {
  payload: {
    locations: Location[]
  }
}

interface GetLocationBeaconsFailure
    extends ReduxAction<LocationTypes.GET_LOCATION_BEACONS_FAILURE> {
  payload: {
    error: Error
  }
}

type SubscribeLocationProviderChanged = ReduxAction<LocationTypes.SUBSCRIBE_LOCATION_PROVIDER_CHANGED>
type UnsubscribeLocationProviderChanged = ReduxAction<LocationTypes.UNSUBSCRIBE_LOCATION_PROVIDER_CHANGED>

const clear = (): Clear => ({
  type: LocationTypes.CLEAR,
})

const saveLocationCheckin = (beacon: Beacon, action: String): SaveLocationCheckin => ({
  type: LocationTypes.SAVE_LOCATION_CHECKIN,
  beacon: beacon,
  action: action
})

const saveLocationCheckinSuccess = (locationCheckin: LocationCheckin): SaveLocationCheckinSuccess => ({
  type: LocationTypes.SAVE_LOCATION_CHECKIN_SUCCESS,
  locationCheckin: locationCheckin
})

const getCurrentPromotionLocation = (): GetCurrentPromotionLocation => ({
  type: LocationTypes.GET_CURRENT_PROMOTION_LOCATION,
})

const getCurrentPromotionLocationForLogin = (): GetCurrentPromotionLocationForLogin => ({
  type: LocationTypes.GET_CURRENT_PROMOTION_LOCATION_FOR_LOGIN,
})


const getCurrentLocation = (): GetCurrentLocation => ({
  type: LocationTypes.GET_CURRENT_LOCATION,
})

const getCurrentLocationSuccess = (coordinate: Coordinate): GetCurrentLocationSuccess => ({
  type: LocationTypes.GET_CURRENT_LOCATION_SUCCESS,
  payload: { coordinate }
})

const getCurrentLocationFailure = (error: Error): GetCurrentLocationFailure => ({
  type: LocationTypes.GET_CURRENT_LOCATION_FAILURE,
  payload: { error }
})

const getLocationBeacons = (coordinate: Coordinate): GetLocationBeacons => ({
  type: LocationTypes.GET_LOCATION_BEACONS,
  coordinate
})

const getLocationBeaconsSuccess = (locations: Location[]): GetLocationBeaconsSuccess => ({
  type: LocationTypes.GET_LOCATION_BEACONS_SUCCESS,
  payload: { locations }
})

const getLocationBeaconsFailure = (error: Error): GetLocationBeaconsFailure => ({
  type: LocationTypes.GET_LOCATION_BEACONS_FAILURE,
  payload: { error }
})

const subscribeLocationProviderChanged = (): SubscribeLocationProviderChanged => ({
  type: LocationTypes.SUBSCRIBE_LOCATION_PROVIDER_CHANGED,
})

const unsubscribeLocationProviderChanged = (): UnsubscribeLocationProviderChanged => ({
  type: LocationTypes.UNSUBSCRIBE_LOCATION_PROVIDER_CHANGED,
})

export default {
  clear,
  saveLocationCheckin,
  saveLocationCheckinSuccess,

  getCurrentPromotionLocation,
  getCurrentPromotionLocationForLogin,
  getCurrentLocation,
  getCurrentLocationSuccess,
  getCurrentLocationFailure,

  getLocationBeacons,
  getLocationBeaconsSuccess,
  getLocationBeaconsFailure,

  subscribeLocationProviderChanged,
  unsubscribeLocationProviderChanged,
}
