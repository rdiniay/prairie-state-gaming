import { Reducer } from 'redux'
import { AboutTypes, AboutAction } from '../about-redux'
import { Rules, Error } from '../../models'

type AboutState = {
  loading: boolean
  aboutContent: Rules
  aboutFooter: Rules
  error: Error
}

const INITIAL_STATE: AboutState = {
  loading: false,
  aboutContent: null,
  aboutFooter: null,
  error: null
}

const setLoading = (state: AboutState) => ({
    ...state, loading: true,
})

const getAboutContentSuccesss = (state: AboutState, aboutContent: Rules) => ({
  ...state, loading: false, error: null, aboutContent: aboutContent
})

const getAboutFooterSuccesss = (state: AboutState, aboutFooter: Rules) => ({
  ...state, loading: false, error: null, aboutFooter: aboutFooter
})

const failure = (state: AboutState, error: Error) => ({
  ...state, loading: false, error: error
})

const reducer: Reducer<AboutState, AboutAction> = (
  state = INITIAL_STATE,
  action
) => {
  switch (action.type) {
    case AboutTypes.GET_ABOUT_CONTENT:
      return setLoading(state)
    case AboutTypes.GET_ABOUT_CONTENT_SUCCESS:
      return getAboutContentSuccesss(state, action.payload.aboutContent)
    case AboutTypes.GET_ABOUT_CONTENT_FAILURE:
      return failure(state, action.payload.error)

    case AboutTypes.GET_ABOUT_FOOTER_SUCCESS:
      return getAboutFooterSuccesss(state, action.payload.aboutFooter)
    case AboutTypes.GET_ABOUT_FOOTER_FAILURE:
      return failure(state, action.payload.error)
    default:
      return state
  }
}

export default reducer
