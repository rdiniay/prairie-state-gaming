import { Action as ReduxAction } from 'redux'
import { RootState } from '..'
import { Rules, Error } from '../../models'

export const AboutSelectors = {
    aboutContent : (state: RootState) => state.about.aboutContent,
    aboutFooter : (state: RootState) => state.about.aboutFooter,
    isLoading : (state: RootState) => state.about.loading
}

export enum AboutTypes {
  GET_ABOUT_CONTENT = 'GET_ABOUT_CONTENT',
  GET_ABOUT_CONTENT_SUCCESS = 'GET_ABOUT_CONTENT_SUCCESS',
  GET_ABOUT_CONTENT_FAILURE = 'GET_ABOUT_CONTENT_FAILURE',
  GET_ABOUT_FOOTER = 'GET_ABOUT_FOOTER',
  GET_ABOUT_FOOTER_SUCCESS = 'GET_ABOUT_FOOTER_SUCCESS',
  GET_ABOUT_FOOTER_FAILURE = 'GET_ABOUT_FOOTER_FAILURE',
}

export type AboutAction =
  | GetAboutContent
  | GetAboutContentSuccess
  | GetAboutContentFailure
  | GetAboutFooter
  | GetAboutFooterSuccess
  | GetAboutFooterFailure

type GetAboutContent = ReduxAction<AboutTypes.GET_ABOUT_CONTENT> 

interface GetAboutContentSuccess
  extends ReduxAction<AboutTypes.GET_ABOUT_CONTENT_SUCCESS> {
  payload: {
    aboutContent: Rules
  }
}

interface GetAboutContentFailure
  extends ReduxAction<AboutTypes.GET_ABOUT_CONTENT_FAILURE> {
  payload: {
    error: Error
  }
}

type GetAboutFooter = ReduxAction<AboutTypes.GET_ABOUT_FOOTER> 

interface GetAboutFooterSuccess
  extends ReduxAction<AboutTypes.GET_ABOUT_FOOTER_SUCCESS> {
  payload: {
    aboutFooter: Rules
  }
}

interface GetAboutFooterFailure
  extends ReduxAction<AboutTypes.GET_ABOUT_FOOTER_FAILURE> {
  payload: {
    error: Error
  }
}

const getAboutContent = (): GetAboutContent => ({
  type: AboutTypes.GET_ABOUT_CONTENT,
})

const getAboutContentSuccess = (aboutContent: Rules): GetAboutContentSuccess => ({
  type: AboutTypes.GET_ABOUT_CONTENT_SUCCESS,
  payload: { aboutContent }
})

const getAboutContentFailure = (error: Error): GetAboutContentFailure => ({
  type: AboutTypes.GET_ABOUT_CONTENT_FAILURE,
  payload: { error }
})

const getAboutFooter = (): GetAboutFooter => ({
  type: AboutTypes.GET_ABOUT_FOOTER,
})

const getAboutFooterSuccess = (aboutFooter: Rules): GetAboutFooterSuccess => ({
  type: AboutTypes.GET_ABOUT_FOOTER_SUCCESS,
  payload: { aboutFooter }
})

const getAboutFooterFailure = (error: Error): GetAboutFooterFailure => ({
  type: AboutTypes.GET_ABOUT_FOOTER_FAILURE,
  payload: { error }
})

export default {
  getAboutContent,
  getAboutContentSuccess,
  getAboutContentFailure,
  getAboutFooter,
  getAboutFooterSuccess,
  getAboutFooterFailure,
}
