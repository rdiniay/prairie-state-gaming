import { Reducer } from 'redux'
import { ProfileTypes, ProfileAction } from '../profile-redux'
import { Profile, Error } from '../../models'

type ProfileState = {
  error: Error,
  loading: boolean
  profile: Profile,
  isChangedPin: Boolean
  isChangedEmail: Boolean
}

const INITIAL_STATE: ProfileState = {
  error: null,
  loading: false,
  profile: null, 
  isChangedPin: false,
  isChangedEmail: false
}

const resetData = (state: ProfileState) => ({
  ...state, loading: false, error: null, isChangedPin: false, isChangedEmail: false,
})

const setLoading = (state: ProfileState) => ({
  ...state, loading: true,
})

const profileSuccess = (state: ProfileState, profile: Profile) => ({
  ...state, loading: false, error: null, isChangedEmail: false, profile: profile
})

const changePinSuccess = (state: ProfileState, success: Boolean) => ({
  ...state, loading: false, error: null, isChangedPin: success
})

const changeEmailSuccess = (state: ProfileState, newEmail: string) => ({
  ...state, loading: false, error: null, isChangedEmail: true, profile: { ...state.profile, emailAddress: newEmail }
})

const deactivateAccountSuccess = (state: ProfileState, profile: Profile) => ({
  ...state, loading: false, error: null, profile: profile
})

const failure = (state: ProfileState, error: Error) => ({
  ...state, loading: false, error: error,
})

const reducer: Reducer<ProfileState, ProfileAction> = (
  state = INITIAL_STATE,
  action
) => {
  switch (action.type) {
    case ProfileTypes.RESET_DATA:
      return resetData(state)

    case ProfileTypes.GET_PROFILE:
      return setLoading(state)
    case ProfileTypes.GET_PROFILE_SUCCESS:
      return profileSuccess(state, action.payload.profile)
    case ProfileTypes.GET_PROFILE_FAILURE:
      return failure(state, action.payload.error)

    case ProfileTypes.UPDATE_PROFILE:
      return setLoading(state)
    case ProfileTypes.UPDATE_PROFILE_SUCCESS:
      return profileSuccess(state, action.payload.profile)
    case ProfileTypes.UPDATE_PROFILE_FAILURE:
      return failure(state, action.payload.error)

    case ProfileTypes.UPDATE_EMAIL:
      return setLoading(state)
    case ProfileTypes.UPDATE_EMAIL_SUCCESS:
      return changeEmailSuccess(state, action.payload.newEmail)
    case ProfileTypes.UPDATE_EMAIL_FAILURE:
      return failure(state, action.payload.error)

    case ProfileTypes.CHANGE_PIN:
      return setLoading(state)
    case ProfileTypes.CHANGE_PIN_SUCCESS:
      return changePinSuccess(state, action.payload.success)
    case ProfileTypes.CHANGE_PIN_FAILURE:
      return failure(state, action.payload.error)

    case ProfileTypes.DEACTIVATE_ACCOUNT:
      return setLoading(state)
    case ProfileTypes.DEACTIVATE_ACCOUNT_SUCCESS:
      return deactivateAccountSuccess(state, action.payload.profile)
    case ProfileTypes.DEACTIVATE_ACCOUNT_FAILURE:
      return failure(state, action.payload.error)

    default:
      return state
  }
}

export default reducer
