import { Action as ReduxAction } from 'redux'
import { useSelector, TypedUseSelectorHook } from 'react-redux'
import { RootState } from '..'
import { Profile, Error } from '../../models'

export const ProfileSelectors = {
    error: (state: RootState) => state.profile.error,
    isLoading: (state: RootState) => state.profile.loading,
    profile : (state: RootState) => state.profile.profile,
    isChangedPin : (state: RootState) => state.profile.isChangedPin,
    isChangedEmail: (state: RootState) => state.profile.isChangedEmail,
}

export enum ProfileTypes {
  RESET_DATA = 'RESET_DATA',

  GET_PROFILE = 'GET_PROFILE',
  GET_PROFILE_SUCCESS = 'GET_PROFILE_SUCCESS',
  GET_PROFILE_FAILURE = 'GET_PROFILE_FAILURE',

  UPDATE_PROFILE = 'UPDATE_PROFILE',
  UPDATE_PROFILE_SUCCESS = 'UPDATE_PROFILE_SUCCESS',
  UPDATE_PROFILE_FAILURE = 'UPDATE_PROFILE_FAILURE',

  UPDATE_EMAIL = 'UPDATE_EMAIL',
  UPDATE_EMAIL_SUCCESS = 'UPDATE_EMAIL_SUCCESS',
  UPDATE_EMAIL_FAILURE = 'UPDATE_EMAIL_FAILURE',

  CHANGE_PIN = 'CHANGE_PIN',
  CHANGE_PIN_UPDATE = 'CHANGE_PIN_UPDATE',
  CHANGE_PIN_SUCCESS = 'CHANGE_PIN_SUCCESS',
  CHANGE_PIN_FAILURE = 'CHANGE_PIN_FAILURE',

  DEACTIVATE_ACCOUNT = 'DEACTIVATE_ACCOUNT',
  DEACTIVATE_ACCOUNT_SUCCESS = 'DEACTIVATE_ACCOUNT_SUCCESS',
  DEACTIVATE_ACCOUNT_FAILURE = 'DEACTIVATE_ACCOUNT_FAILURE',
}

export type ProfileAction =
  | ResetData
  | GetProfile
  | GetProfileSuccess
  | GetProfileFailure
  | UpdateProfile
  | UpdateProfileSuccess
  | UpdateProfileFailure
  | UpdateEmail
  | UpdateEmailSuccess
  | UpdateEmailFailure
  | ChangePin
  | ChangePinUpdate
  | ChangePinSuccess
  | ChangePinFailure
  | DeactivateAccount
  | DeactivateAccountSuccess
  | DeactivateAccountFailure

type ResetData = ReduxAction<ProfileTypes.RESET_DATA>

type GetProfile = ReduxAction<ProfileTypes.GET_PROFILE>

interface GetProfileSuccess
  extends ReduxAction<ProfileTypes.GET_PROFILE_SUCCESS> {
  payload: {
    profile: Profile
  }
}

interface GetProfileFailure
  extends ReduxAction<ProfileTypes.GET_PROFILE_FAILURE> {
  payload: {
    error: Error
  }
}

interface UpdateProfile
  extends ReduxAction<ProfileTypes.UPDATE_PROFILE> {
  profile: Profile
}

interface UpdateProfileSuccess
  extends ReduxAction<ProfileTypes.UPDATE_PROFILE_SUCCESS> {
  payload: {
    profile: Profile
  }
}

interface UpdateProfileFailure
  extends ReduxAction<ProfileTypes.UPDATE_PROFILE_FAILURE> {
  payload: {
    error: Error
  }
}

interface UpdateEmail
  extends ReduxAction<ProfileTypes.UPDATE_EMAIL> {
  newEmail: string
}

interface UpdateEmailSuccess
  extends ReduxAction<ProfileTypes.UPDATE_EMAIL_SUCCESS> {
  payload: {
    newEmail: string
  }
}

interface UpdateEmailFailure
  extends ReduxAction<ProfileTypes.UPDATE_EMAIL_FAILURE> {
  payload: {
    error: Error
  }
}

interface ChangePin
  extends ReduxAction<ProfileTypes.CHANGE_PIN> {
  params: any
}

interface ChangePinUpdate
  extends ReduxAction<ProfileTypes.CHANGE_PIN_UPDATE> {
  newAuthData: String
}

interface ChangePinSuccess
  extends ReduxAction<ProfileTypes.CHANGE_PIN_SUCCESS> {
  payload: {
    success: Boolean
  }
}

interface ChangePinFailure
extends ReduxAction<ProfileTypes.CHANGE_PIN_FAILURE> {
  payload: {
    error: Error
  }
}

type DeactivateAccount = ReduxAction<ProfileTypes.DEACTIVATE_ACCOUNT>

interface DeactivateAccountSuccess
  extends ReduxAction<ProfileTypes.DEACTIVATE_ACCOUNT_SUCCESS> {
  payload: {
    profile: Profile
  }
}

interface DeactivateAccountFailure
  extends ReduxAction<ProfileTypes.DEACTIVATE_ACCOUNT_FAILURE> {
  payload: {
    error: Error
  }
}

const resetData = (): ResetData => ({
  type: ProfileTypes.RESET_DATA
})

const getProfile = (): GetProfile => ({
  type: ProfileTypes.GET_PROFILE
})
const getProfileSuccess = (profile: Profile): GetProfileSuccess => ({
  type: ProfileTypes.GET_PROFILE_SUCCESS,
  payload: { profile }
})
const getProfileFailure = (error: Error): GetProfileFailure => ({
  type: ProfileTypes.GET_PROFILE_FAILURE,
  payload: { error }
})

const updateProfile = (profile: Profile): UpdateProfile => ({
  type: ProfileTypes.UPDATE_PROFILE,
  profile: profile,
})
const updateProfileSuccess = (profile: Profile): UpdateProfileSuccess => ({
  type: ProfileTypes.UPDATE_PROFILE_SUCCESS,
  payload: { profile }
})
const updateProfileFailure = (error: Error): UpdateProfileFailure => ({
  type: ProfileTypes.UPDATE_PROFILE_FAILURE,
  payload: { error }
})

const updateEmail = (newEmail: string): UpdateEmail => ({
  type: ProfileTypes.UPDATE_EMAIL,
  newEmail: newEmail,
})
const updateEmailSuccess = (newEmail: string): UpdateEmailSuccess => ({
  type: ProfileTypes.UPDATE_EMAIL_SUCCESS,
  payload: { newEmail: newEmail }
})
const updateEmailFailure = (error: Error): UpdateEmailFailure => ({
  type: ProfileTypes.UPDATE_EMAIL_FAILURE,
  payload: { error }
})

const changePin = (params: any): ChangePin => ({
  type: ProfileTypes.CHANGE_PIN,
  params: params
})
const changePinUpdate = (newAuthData: String): ChangePinUpdate => ({
  type: ProfileTypes.CHANGE_PIN_UPDATE,
  newAuthData: newAuthData
})
const changePinSuccess = (isSuccess: Boolean): ChangePinSuccess => ({
  type: ProfileTypes.CHANGE_PIN_SUCCESS,
  payload: { success: isSuccess }
})
const changePinFailure = (error: Error): ChangePinFailure => ({
  type: ProfileTypes.CHANGE_PIN_FAILURE,
  payload: { error }
})

const deactivateAccount = (): DeactivateAccount => ({
  type: ProfileTypes.DEACTIVATE_ACCOUNT,
})
const deactivateAccountSuccess = (profile: Profile): DeactivateAccountSuccess => ({
  type: ProfileTypes.DEACTIVATE_ACCOUNT_SUCCESS,
  payload: { profile }
})
const deactivateAccountFailure = (error: Error): DeactivateAccountFailure => ({
  type: ProfileTypes.DEACTIVATE_ACCOUNT_FAILURE,
  payload: { error }
})

export default {
  resetData,

  getProfile,
  getProfileSuccess,
  getProfileFailure,

  updateProfile,
  updateProfileSuccess,
  updateProfileFailure,

  updateEmail,
  updateEmailSuccess,
  updateEmailFailure,

  changePin,
  changePinUpdate,
  changePinSuccess,
  changePinFailure,

  deactivateAccount,
  deactivateAccountSuccess,
  deactivateAccountFailure,
}
