import { Reducer } from 'redux'
import { BankTypes, BankAction } from '../bank-redux'
import { Bank, Entry, Error } from '../../models'

type BankState = {
  loading: boolean
  bank: Bank
  entry: Entry
  error: Error
}

const INITIAL_STATE: BankState = {
  loading: true,
  bank: null, 
  entry: null,
  error: null
}

const resetBankEntries = (state: BankState) => ({
  ...state, ...INITIAL_STATE, 
})

const stopLoading = (state: BankState) => ({
  ...state, loading: false, 
})

const setLoading = (state: BankState) => ({
    ...state, loading: true,
})

const updateBankEntries = (state: BankState) => ({
  ...state, loading: false, bank: state.bank ? { ...state.bank, refresh: false } : null
})

const addBankEntriesSuccess = (state: BankState, bank: Bank) => {
  const bankEntry = bank && bank.entries ? bank.entries.find((entry: Entry) => !entry.locationId && entry.locationId === 0) : null
  return ({
      ...state, loading: false, error: null, bank: bank, entry: bankEntry,
  })
}

const getBankEntriesSuccess = (state: BankState, bank: Bank) => {
  const bankEntry = bank && bank.entries ? bank.entries.find((entry: Entry) => !entry.locationId && entry.locationId === 0) : null
  return ({
      ...state, loading: false, error: null, bank: bank, entry: bankEntry,
  })
}

const failure = (state: BankState, error: Error) => ({
  ...state, loading: false, error: error
})

const reducer: Reducer<BankState, BankAction> = (
  state = INITIAL_STATE,
  action
) => {
  switch (action.type) {
    case BankTypes.RESET_BANK_ENTRIES:
      return resetBankEntries(state)
    case BankTypes.STOP_BANK_LOADING:
      return stopLoading(state)
    case BankTypes.REQUEST_BANK_LOADING:
      return setLoading(state)
    // case BankTypes.UPDATE_BANK_ENTRIES:
    //   return updateBankEntries(state)
    // case BankTypes.ADD_BANK_ENTRIES:
    case BankTypes.ADD_BANK_ENTRIES_SUCCESS:
      return addBankEntriesSuccess(state, action.payload.bank)
    case BankTypes.ADD_BANK_ENTRIES_FAILURE:
      return failure(state, action.payload.error)

    case BankTypes.GET_BANK_ENTRIES:
      return setLoading(state)
    case BankTypes.GET_BANK_ENTRIES_SUCCESS:
      return getBankEntriesSuccess(state, action.payload.bank)
    case BankTypes.GET_BANK_ENTRIES_FAILURE:
      return failure(state, action.payload.error)
    default:
      return state
  }
}

export default reducer
