import { Action as ReduxAction } from 'redux'
import { useSelector, TypedUseSelectorHook } from 'react-redux'
import { RootState } from '../../redux'
import { Bank, Period, Error } from '../../models'

export const BankSelectors = {
    isLoading : (state: RootState) => state.bank.loading,
    error : (state: RootState) => state.bank.error,
    bank : (state: RootState) => state.bank.bank,
    entry: (state: RootState) => state.bank.entry,
}

export enum BankTypes {
  RESET_BANK_ENTRIES = 'RESET_BANK_ENTRIES',
  STOP_BANK_LOADING = 'STOP_BANK_LOADING',
  REQUEST_BANK_LOADING = 'REQUEST_BANK_LOADING',
  UPDATE_BANK_ENTRIES = 'UPDATE_BANK_ENTRIES',

  ADD_BANK_ENTRIES = 'ADD_BANK_ENTRIES',
  ADD_BANK_ENTRIES_VALIDATION = 'ADD_BANK_ENTRIES_VALIDATION',
  ADD_BANK_ENTRIES_SUCCESS = 'ADD_BANK_ENTRIES_SUCCESS',
  ADD_BANK_ENTRIES_FAILURE = 'ADD_BANK_ENTRIES_FAILURE',

  GET_BANK_ENTRIES = 'GET_BANK_ENTRIES',
  GET_BANK_ENTRIES_SUCCESS = 'GET_BANK_ENTRIES_SUCCESS',
  GET_BANK_ENTRIES_FAILURE = 'GET_BANK_ENTRIES_FAILURE'
}

export type BankAction =
  | ResetBankEntries
  | StopBankLoading
  | RequestBankLoading
  | UpdateBankEntries
  | AddBankEntries
  | AddBankEntriesValidation
  | AddBankEntriesSuccess
  | AddBankEntriesFailure
  | GetBankEntries
  | GetBankEntriesSuccess
  | GetBankEntriesFailure

interface ResetBankEntries
  extends ReduxAction<BankTypes.RESET_BANK_ENTRIES> {
}

interface StopBankLoading
  extends ReduxAction<BankTypes.STOP_BANK_LOADING> {
}

interface RequestBankLoading
  extends ReduxAction<BankTypes.REQUEST_BANK_LOADING> {
}

interface UpdateBankEntries
  extends ReduxAction<BankTypes.UPDATE_BANK_ENTRIES> {
}

interface AddBankEntries
  extends ReduxAction<BankTypes.ADD_BANK_ENTRIES> {
  periods: Period[]
}

interface AddBankEntriesValidation
  extends ReduxAction<BankTypes.ADD_BANK_ENTRIES_VALIDATION> {
  payload: {
    bank: Bank
  }
}

interface AddBankEntriesSuccess
  extends ReduxAction<BankTypes.ADD_BANK_ENTRIES_SUCCESS> {
  payload: {
    bank: Bank
  }
}

interface AddBankEntriesFailure
  extends ReduxAction<BankTypes.ADD_BANK_ENTRIES_FAILURE> {
  payload: {
    error: Error
  }
}

interface GetBankEntries
  extends ReduxAction<BankTypes.GET_BANK_ENTRIES> {
}

interface GetBankEntriesSuccess
  extends ReduxAction<BankTypes.GET_BANK_ENTRIES_SUCCESS> {
  payload: {
    bank: Bank
  }
}

interface GetBankEntriesFailure
  extends ReduxAction<BankTypes.GET_BANK_ENTRIES_FAILURE> {
  payload: {
    error: Error
  }
}

const resetBankEntries = (): ResetBankEntries => ({
  type: BankTypes.RESET_BANK_ENTRIES,
})

const stopBankLoading = (): StopBankLoading => ({
  type: BankTypes.STOP_BANK_LOADING,
})

const requestBankLoading = (): RequestBankLoading => ({
  type: BankTypes.REQUEST_BANK_LOADING,
})

const updateBankEntries = (): UpdateBankEntries => ({
  type: BankTypes.UPDATE_BANK_ENTRIES,
})

const addBankEntries = (periods: Period[]): AddBankEntries => ({
  type: BankTypes.ADD_BANK_ENTRIES,
  periods: periods,
})

const addBankEntriesValidation = (bank: Bank): AddBankEntriesValidation=> ({
  type: BankTypes.ADD_BANK_ENTRIES_VALIDATION,
  payload: { bank }
})

const addBankEntriesSuccess = (bank: Bank): AddBankEntriesSuccess => ({
  type: BankTypes.ADD_BANK_ENTRIES_SUCCESS,
  payload: { bank }
})

const addBankEntriesFailure = (error: Error): AddBankEntriesFailure => ({
  type: BankTypes.ADD_BANK_ENTRIES_FAILURE,
  payload: { error }
})

const getBankEntries = (): GetBankEntries => ({
  type: BankTypes.GET_BANK_ENTRIES,
})

const getBankEntriesSuccess = (bank: Bank): GetBankEntriesSuccess => ({
  type: BankTypes.GET_BANK_ENTRIES_SUCCESS,
  payload: { bank }
})

const getBankEntriesFailure = (error: Error): GetBankEntriesFailure => ({
  type: BankTypes.GET_BANK_ENTRIES_FAILURE,
  payload: { error }
})

export default {
  resetBankEntries,
  stopBankLoading,
  requestBankLoading,
  updateBankEntries,
  
  addBankEntries,
  addBankEntriesValidation,
  addBankEntriesSuccess,
  addBankEntriesFailure,

  getBankEntries,
  getBankEntriesSuccess,
  getBankEntriesFailure,
}
