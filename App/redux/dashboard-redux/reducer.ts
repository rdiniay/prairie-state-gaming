import { Reducer } from 'redux'
import { DashboardTypes, DashboardAction } from '../dashboard-redux'
import { Drawing, Error } from '../../models'

type DashboardState = {
  loading: boolean
  earningTick: number 
  isEmailVerified: boolean
  isEmailSent: boolean
  error: Error
}

const INITIAL_STATE: DashboardState = {
  loading: false,
  earningTick: 0, 
  isEmailVerified: false, 
  isEmailSent: false, 
  error: null
}

const clearData = (state: DashboardState) => ({
  ...state, ...INITIAL_STATE
})

const setTimerTick = (state: DashboardState, tick: number) => ({
  ...state, earningTick: tick
})

const setLoading = (state: DashboardState) => ({
  ...state, loading: true, error: null, isEmailVerified: false,
})

const verifyEmailSuccess = (state: DashboardState, success: boolean) => ({
  ...state, loading: false, error: null, isEmailSent: false, isEmailVerified: true
})

const sendEmailVerificationSuccess = (state: DashboardState, success: boolean) => ({
  ...state, loading: false, error: null, isEmailSent: true
})
const getDashboardDetailsSuccess = (state: DashboardState, success: boolean) => ({
  ...state, loading: false, error: null, 
})

const failure = (state: DashboardState, error: Error) => ({
  ...state, loading: false, error: error,
})

const reducer: Reducer<DashboardState, DashboardAction> = (
  state = INITIAL_STATE,
  action
) => {
  switch (action.type) {
    case DashboardTypes.CLEAR_DATA:
      return clearData(state)
    // case HomeTypes.START_EARNING_TIMER:
      // return setTimerTick(state, 0)
    case DashboardTypes.SET_TIMER_TICK:
      return setTimerTick(state, action.tick)

    case DashboardTypes.VERIFY_EMAIL:
      return setLoading(state)
    case DashboardTypes.VERIFY_EMAIL_SUCCESS:
      return verifyEmailSuccess(state, action.payload.success)
    case DashboardTypes.VERIFY_EMAIL_FAILURE:
      return failure(state, action.payload.error)

    case DashboardTypes.SEND_EMAIL_VERIFICATION:
      return setLoading(state)
    case DashboardTypes.SEND_EMAIL_VERIFICATION_SUCCESS:
      return sendEmailVerificationSuccess(state, action.payload.success)
    case DashboardTypes.SEND_EMAIL_VERIFICATION_FAILURE:
      return failure(state, action.payload.error)

    case DashboardTypes.GET_DASHBOARD_DETAILS:
      return setLoading(state)
    case DashboardTypes.GET_DASHBOARD_DETAILS_SUCCESS:
      return getDashboardDetailsSuccess(state, action.payload.success)
    case DashboardTypes.GET_DASHBOARD_DETAILS_FAILURE:
      return failure(state, action.payload.error)
    default:
      return state
  }
}

export default reducer
