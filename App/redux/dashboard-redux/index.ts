import { Action as ReduxAction } from 'redux'
import { useSelector, TypedUseSelectorHook } from 'react-redux'
import { RootState } from '..'
import { Profile, Error } from '../../models'

export const DashboardSelectors = {
  error : (state: RootState) => state.dashboard.error,
  isEmailSent : (state: RootState) => state.dashboard.isEmailSent,
  earningTick : (state: RootState) => state.dashboard.earningTick,
  isLoading : (state: RootState) => state.dashboard.loading
}

export enum DashboardTypes {
  CLEAR_DATA = 'CLEAR_DATA',
  STOP_EARNING_TIMER = 'STOP_EARNING_TIMER',
  START_EARNING_TIMER = 'START_EARNING_TIMER',
  SET_TIMER_TICK = 'SET_TIMER_TICK',
  VERIFY_EMAIL_UPDATE = 'VERIFY_EMAIL_UPDATE',

  VERIFY_EMAIL = 'VERIFY_EMAIL',
  VERIFY_EMAIL_SUCCESS = 'VERIFY_EMAIL_SUCCESS',
  VERIFY_EMAIL_FAILURE = 'VERIFY_EMAIL_FAILURE',

  SEND_EMAIL_VERIFICATION = 'SEND_EMAIL_VERIFICATION',
  SEND_EMAIL_VERIFICATION_SUCCESS = 'SEND_EMAIL_VERIFICATION_SUCCESS',
  SEND_EMAIL_VERIFICATION_FAILURE = 'SEND_EMAIL_VERIFICATION_FAILURE',

  GET_DASHBOARD_DETAILS = 'GET_DASHBOARD_DETAILS',
  GET_DASHBOARD_DETAILS_SUCCESS = 'GET_DASHBOARD_DETAILS_SUCCESS',
  GET_DASHBOARD_DETAILS_FAILURE = 'GET_DASHBOARD_DETAILS_FAILURE',
}

export type DashboardAction =
  | ClearData
  | StopEarningTimer
  | StartEarningTimer
  | SetTimerTick
  | VerifyEmailUpdate
  | VerifyEmail
  | VerifyEmailSuccess
  | VerifyEmailFailure
  | SendEmailVerification
  | SendEmailVerificationSuccess
  | SendEmailVerificationFailure
  | GetDashboardDetails
  | GetDashboardDetailsSuccess
  | GetDashboardDetailsFailure

type ClearData = ReduxAction<DashboardTypes.CLEAR_DATA>
type StopEarningTimer = ReduxAction<DashboardTypes.STOP_EARNING_TIMER>
type StartEarningTimer = ReduxAction<DashboardTypes.START_EARNING_TIMER>

interface SetTimerTick
  extends ReduxAction<DashboardTypes.SET_TIMER_TICK> {
  tick: number
}

interface VerifyEmailUpdate
  extends ReduxAction<DashboardTypes.VERIFY_EMAIL_UPDATE> {
  profile: Profile
}

interface VerifyEmail
  extends ReduxAction<DashboardTypes.VERIFY_EMAIL> {
  code: string
}

interface VerifyEmailSuccess
  extends ReduxAction<DashboardTypes.VERIFY_EMAIL_SUCCESS> {
  payload: {
    success: boolean
  }
}

interface VerifyEmailFailure
  extends ReduxAction<DashboardTypes.VERIFY_EMAIL_FAILURE> {
  payload: {
    error: Error
  }
}

interface SendEmailVerification
  extends ReduxAction<DashboardTypes.SEND_EMAIL_VERIFICATION> {
}

interface SendEmailVerificationSuccess
  extends ReduxAction<DashboardTypes.SEND_EMAIL_VERIFICATION_SUCCESS> {
  payload: {
    success: boolean
  }
}

interface SendEmailVerificationFailure
  extends ReduxAction<DashboardTypes.SEND_EMAIL_VERIFICATION_FAILURE> {
  payload: {
    error: Error
  }
}

interface GetDashboardDetails
  extends ReduxAction<DashboardTypes.GET_DASHBOARD_DETAILS> {
}

interface GetDashboardDetailsSuccess
  extends ReduxAction<DashboardTypes.GET_DASHBOARD_DETAILS_SUCCESS> {
  payload: {
    success: boolean
  }
}

interface GetDashboardDetailsFailure
  extends ReduxAction<DashboardTypes.GET_DASHBOARD_DETAILS_FAILURE> {
  payload: {
    error: Error
  }
}

const clearData = (): ClearData => ({
  type: DashboardTypes.CLEAR_DATA,
})

const stopEarningTimer = (): StopEarningTimer => ({
  type: DashboardTypes.STOP_EARNING_TIMER,
})

const startEarningTimer = (): StartEarningTimer => ({
  type: DashboardTypes.START_EARNING_TIMER,
})

const setTimerTick = (tick: number): SetTimerTick => ({
  type: DashboardTypes.SET_TIMER_TICK,
  tick: tick,
})

const verifyEmailUpdate = (profile: Profile): VerifyEmailUpdate => ({
  type: DashboardTypes.VERIFY_EMAIL_UPDATE,
  profile: profile,
})

const verifyEmail = (code: string): VerifyEmail => ({
  type: DashboardTypes.VERIFY_EMAIL,
  code: code,
})

const verifyEmailSuccess = (success: boolean): VerifyEmailSuccess => ({
  type: DashboardTypes.VERIFY_EMAIL_SUCCESS,
  payload: { success }
})

const verifyEmailFailure = (error: Error): VerifyEmailFailure => ({
  type: DashboardTypes.VERIFY_EMAIL_FAILURE,
  payload: { error }
})

const sendEmailVerification = (): SendEmailVerification => ({
  type: DashboardTypes.SEND_EMAIL_VERIFICATION,
})

const sendEmailVerificationSuccess = (success: boolean): SendEmailVerificationSuccess => ({
  type: DashboardTypes.SEND_EMAIL_VERIFICATION_SUCCESS,
  payload: { success }
})

const sendEmailVerificationFailure = (error: Error): SendEmailVerificationFailure => ({
  type: DashboardTypes.SEND_EMAIL_VERIFICATION_FAILURE,
  payload: { error }
})

const getDashboardDetails = (): GetDashboardDetails => ({
  type: DashboardTypes.GET_DASHBOARD_DETAILS,
})

const getDashboardDetailsSuccess = (success: boolean): GetDashboardDetailsSuccess => ({
  type: DashboardTypes.GET_DASHBOARD_DETAILS_SUCCESS,
  payload: { success }
})

const getDashboardDetailsFailure = (error: Error): GetDashboardDetailsFailure => ({
  type: DashboardTypes.GET_DASHBOARD_DETAILS_FAILURE,
  payload: { error }
})

export default {
  clearData,
  stopEarningTimer,
  startEarningTimer,
  setTimerTick,
  verifyEmailUpdate,

  verifyEmail,
  verifyEmailSuccess,
  verifyEmailFailure,

  sendEmailVerification,
  sendEmailVerificationSuccess,
  sendEmailVerificationFailure,
  
  getDashboardDetails,
  getDashboardDetailsSuccess,
  getDashboardDetailsFailure,
}
