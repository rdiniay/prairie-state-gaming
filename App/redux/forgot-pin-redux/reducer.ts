import { Reducer } from 'redux'
import { ForgotPinTypes, ForgotPinAction } from '../forgot-pin-redux'
import { Drawing, Error } from '../../models'

type ForgotPinState = {
  loading: boolean
  code: String
  isEmailVerified: boolean
  isEmailSent: boolean
  emailAddress: String
  error: Error
}

const INITIAL_STATE: ForgotPinState = {
  loading: false,
  code: null,
  isEmailVerified: false, 
  isEmailSent: false, 
  emailAddress: null,
  error: null
}

const clearForgotPin = (state: ForgotPinState) => ({
  ...state, ...INITIAL_STATE
})

const setPinCode = (state: ForgotPinState, code: String) => ({
  ...state, loading: false, error: null, code: code,
})

const updatePin = (state: ForgotPinState) => ({
  ...state, loading: true, error: null, isEmailVerified: false,
})

const updatePinSuccess = (state: ForgotPinState, success: boolean) => ({
  ...state, loading: false, error: null, isEmailSent: false, isEmailVerified: true
})

const sendForgotPinVerification = (state: ForgotPinState, emailAddress: String) => ({
  ...state, loading: true, error: null, emailAddress: emailAddress, isEmailVerified: false,
})

const sendForgotPinVerificationSuccess = (state: ForgotPinState, success: boolean) => ({
  ...state, loading: false, error: null, isEmailSent: true
})

const failure = (state: ForgotPinState, error: Error) => ({
  ...state, loading: false, error: error,
})

const reducer: Reducer<ForgotPinState, ForgotPinAction> = (
  state = INITIAL_STATE,
  action
) => {
  switch (action.type) {
    case ForgotPinTypes.CLEAR_FORGOT_PIN:
      return clearForgotPin(state)
    case ForgotPinTypes.SET_PIN_CODE:
      return setPinCode(state, action.code)

    case ForgotPinTypes.UPDATE_PIN:
      return updatePin(state)
    case ForgotPinTypes.UPDATE_PIN_SUCCESS:
      return updatePinSuccess(state, action.payload.success)
    case ForgotPinTypes.UPDATE_PIN_FAILURE:
      return failure(state, action.payload.error)

    case ForgotPinTypes.SEND_FORGOT_PIN_VERIFICATION:
      return sendForgotPinVerification(state, action.email)
    case ForgotPinTypes.SEND_FORGOT_PIN_VERIFICATION_SUCCESS:
      return sendForgotPinVerificationSuccess(state, action.payload.success)
    case ForgotPinTypes.SEND_FORGOT_PIN_VERIFICATION_FAILURE:
      return failure(state, action.payload.error)
    default:
      return state
  }
}

export default reducer
