import { Action as ReduxAction } from 'redux'
import { useSelector, TypedUseSelectorHook } from 'react-redux'
import { RootState } from '../../redux'
import { Profile, Error } from '../../models'

export const ForgotPinSelectors = {
    error : (state: RootState) => state.forgotPin.error,
    isEmailSent : (state: RootState) => state.forgotPin.isEmailSent,
    isEmailVerified: (state: RootState) => state.forgotPin.isEmailVerified,
    isLoading : (state: RootState) => state.forgotPin.loading,
    emailAddress : (state: RootState) => state.forgotPin.emailAddress,
    code : (state: RootState) => state.forgotPin.code,
}

export enum ForgotPinTypes {
  CLEAR_FORGOT_PIN = 'CLEAR_FORGOT_PIN',
  SET_PIN_CODE = 'SET_PIN_CODE',
  LOGIN_PIN = 'LOGIN_PIN',

  UPDATE_PIN = 'UPDATE_PIN',
  UPDATE_PIN_SUCCESS = 'UPDATE_PIN_SUCCESS',
  UPDATE_PIN_FAILURE = 'UPDATE_PIN_FAILURE',

  SEND_FORGOT_PIN_VERIFICATION = 'SEND_FORGOT_PIN_VERIFICATION',
  SEND_FORGOT_PIN_VERIFICATION_SUCCESS = 'SEND_FORGOT_PIN_VERIFICATION_SUCCESS',
  SEND_FORGOT_PIN_VERIFICATION_FAILURE = 'SEND_FORGOT_PIN_VERIFICATION_FAILURE',
}

export type ForgotPinAction =
  | ClearForgotPin
  | SetPinCode
  | LoginPin
  | UpdatePin
  | UpdatePinSuccess
  | UpdatePinFailure
  | SendForgotPinVerification
  | SendForgotPinVerificationSuccess
  | SendForgotPinVerificationFailure

type ClearForgotPin = ReduxAction<ForgotPinTypes.CLEAR_FORGOT_PIN>

interface SetPinCode
  extends ReduxAction<ForgotPinTypes.SET_PIN_CODE> {
  code: String
}

interface LoginPin
  extends ReduxAction<ForgotPinTypes.LOGIN_PIN> {
  pin: String
}

interface UpdatePin
  extends ReduxAction<ForgotPinTypes.UPDATE_PIN> {
  newPin: String
}

interface UpdatePinSuccess
  extends ReduxAction<ForgotPinTypes.UPDATE_PIN_SUCCESS> {
  payload: {
    success: boolean
  }
}

interface UpdatePinFailure
  extends ReduxAction<ForgotPinTypes.UPDATE_PIN_FAILURE> {
  payload: {
    error: Error
  }
}

interface SendForgotPinVerification
  extends ReduxAction<ForgotPinTypes.SEND_FORGOT_PIN_VERIFICATION> {
  email: String
}

interface SendForgotPinVerificationSuccess
  extends ReduxAction<ForgotPinTypes.SEND_FORGOT_PIN_VERIFICATION_SUCCESS> {
  payload: {
    success: boolean
  }
}

interface SendForgotPinVerificationFailure
  extends ReduxAction<ForgotPinTypes.SEND_FORGOT_PIN_VERIFICATION_FAILURE> {
  payload: {
    error: Error
  }
}

const clearForgotPin = (): ClearForgotPin => ({
  type: ForgotPinTypes.CLEAR_FORGOT_PIN,
})

const setPinCode = (code: String): SetPinCode => ({
  type: ForgotPinTypes.SET_PIN_CODE,
  code: code,
})

const loginPin = (pin: String): LoginPin => ({
  type: ForgotPinTypes.LOGIN_PIN,
  pin: pin,
})

const updatePin = (newPin: String): UpdatePin => ({
  type: ForgotPinTypes.UPDATE_PIN,
  newPin: newPin,
})

const updatePinSuccess = (success: boolean): UpdatePinSuccess => ({
  type: ForgotPinTypes.UPDATE_PIN_SUCCESS,
  payload: { success }
})

const updatePinFailure = (error: Error): UpdatePinFailure => ({
  type: ForgotPinTypes.UPDATE_PIN_FAILURE,
  payload: { error }
})

const sendForgotPinVerification = (email: String): SendForgotPinVerification => ({
  type: ForgotPinTypes.SEND_FORGOT_PIN_VERIFICATION,
  email: email,
})

const sendForgotPinVerificationSuccess = (success: boolean): SendForgotPinVerificationSuccess => ({
  type: ForgotPinTypes.SEND_FORGOT_PIN_VERIFICATION_SUCCESS,
  payload: { success }
})

const sendForgotPinVerificationFailure = (error: Error): SendForgotPinVerificationFailure => ({
  type: ForgotPinTypes.SEND_FORGOT_PIN_VERIFICATION_FAILURE,
  payload: { error }
})

export default {
   clearForgotPin,
   setPinCode,
   loginPin,

   updatePin,
   updatePinSuccess,
   updatePinFailure,

   sendForgotPinVerification,
   sendForgotPinVerificationSuccess,
   sendForgotPinVerificationFailure,
}
