import { Action as ReduxAction } from 'redux'
import { useSelector, TypedUseSelectorHook } from 'react-redux'
import { RootState } from '../../redux'
import { Beacon, Mobile, Error, LocationBeacon } from '../../models'
import {BeaconManagerTypes} from "../beacon-manager-redux";
import {Checkin} from "../../models/checkin";
import {Checkout} from "../../models/checkout";

export const MobileSelectors = {
    disconnectingDateTime : (state: RootState) => state.mobile.disconnectingDateTime,
    isLoading : (state: RootState) => state.mobile.loading,
    error : (state: RootState) => state.mobile.error,
    mobile: (state: RootState) => state.mobile.mobile,
    currentCheckInPeriodTime: (state: RootState) => state.mobile.currentCheckInPeriodTime,
    lastCheckInPeriodTime: (state: RootState) => state.mobile.lastCheckInPeriodTime,
    lastCheckOutPeriodTime: (state: RootState) => state.mobile.lastCheckOutPeriodTime,
    isAttemptingToCheckin: (state: RootState) => state.mobile.isAttemptingToCheckin,
    isPendingCheckin: (state: RootState) => state.mobile.pendingCheckin,
    isPendingCheckout: (state: RootState) => state.mobile.pendingCheckout,
}

export enum MobileTypes {
  LAST_CHECK_OUT_PERIOD_TIME = 'LAST_CHECK_OUT_PERIOD_TIME',
  LAST_CHECK_IN_PERIOD_TIME = 'LAST_CHECK_IN_PERIOD_TIME',
  START_DISCONNECTING = 'START_DISCONNECTING',
  CHECK_IN_RESET = 'CHECK_IN_RESET',
  CHECK_IN_START_EARNING = 'CHECK_IN_START_EARNING',
  CHECK_IN_CONTINUE_EARNING = 'CHECK_IN_CONTINUE_EARNING',
  CHECK_IN = 'CHECK_IN',
  CHECK_IN_SUCCESS = 'CHECK_IN_SUCCESS',
  CHECK_IN_FAILURE = 'CHECK_IN_FAILURE',
  ATTEMPT_CHECKIN = 'ATTEMPT_CHECKIN',
  CHECK_OUT = 'CHECK_OUT',
  CHECK_OUT_SUCCESS = 'CHECK_OUT_SUCCESS',
  CHECK_OUT_FAILURE = 'CHECK_OUT_FAILURE',
  PENDING_CHECKIN = "PENDING_CHECKIN",
  PENDING_CHECKOUT = "PENDING_CHECKOUT",
}

export type MobileAction =
  | LastCheckOutPeriodTime
  | LastCheckInPeriodTime
  | StartDisconnecting
  | CheckInReset
  | CheckInStartEarning
  | CheckInContinueEarning
  | CheckIn
  | CheckInSuccess
  | CheckInFailure
  | CheckOut
  | CheckOutSuccess
  | CheckOutFailure
  | PendingCheckin
  | PendingCheckout

interface LastCheckOutPeriodTime
  extends ReduxAction<MobileTypes.LAST_CHECK_OUT_PERIOD_TIME> {
  lastCheckOutPeriodTime: Number
}

interface LastCheckInPeriodTime
  extends ReduxAction<MobileTypes.LAST_CHECK_IN_PERIOD_TIME> {
  lastCheckInPeriodTime: Number
}

interface StartDisconnecting
  extends ReduxAction<MobileTypes.START_DISCONNECTING> {
  disconnectingDateTime: Number
}

type CheckInReset = ReduxAction<MobileTypes.CHECK_IN_RESET>

interface CheckInStartEarning
  extends ReduxAction<MobileTypes.CHECK_IN_START_EARNING> {
  mobile: Mobile,
  isAttemptingToCheckin: boolean,
  pendingCheckin: Checkin
}

interface CheckInContinueEarning
  extends ReduxAction<MobileTypes.CHECK_IN_CONTINUE_EARNING> {
  mobile: Mobile
}

interface CheckIn
  extends ReduxAction<MobileTypes.CHECK_IN> {
  locationBeacon: LocationBeacon
}

interface AttemptCheckin
    extends ReduxAction<MobileTypes.ATTEMPT_CHECKIN> {
  isAttemptingToCheckIn: boolean
}

interface CheckInSuccess
  extends ReduxAction<MobileTypes.CHECK_IN_SUCCESS> {
  payload: {
    mobile: Mobile
  },
  pendingCheckin: Checkin,
}

interface CheckInFailure
  extends ReduxAction<MobileTypes.CHECK_IN_FAILURE> {
  payload: {
    error: Error
  },
  pendingCheckin: Checkin
}

interface CheckOut
  extends ReduxAction<MobileTypes.CHECK_OUT> {
  isBluetoothEnabled: Boolean,
  isSilent: Boolean,
  force: Boolean,
  event: String,
  beacon: Beacon
}

interface CheckOutSuccess
  extends ReduxAction<MobileTypes.CHECK_OUT_SUCCESS> {
  payload: {
    mobile: Mobile
  },
  pendingCheckout: Checkout
}

interface CheckOutFailure
  extends ReduxAction<MobileTypes.CHECK_OUT_FAILURE> {
  payload: {
    error: Error
  },
  pendingCheckout: Checkout
}

interface PendingCheckin
    extends ReduxAction<MobileTypes.PENDING_CHECKIN> {
  pendingCheckin: Checkin
}

interface PendingCheckout
    extends ReduxAction<MobileTypes.PENDING_CHECKOUT> {
  pendingCheckout: Checkout
}

const lastCheckOutPeriodTime = (dateTime: Number): LastCheckOutPeriodTime => ({
  type: MobileTypes.LAST_CHECK_OUT_PERIOD_TIME,
  lastCheckOutPeriodTime: dateTime,
})

const lastCheckInPeriodTime = (dateTime: Number): LastCheckInPeriodTime => ({
  type: MobileTypes.LAST_CHECK_IN_PERIOD_TIME,
  lastCheckInPeriodTime: dateTime,
})

const startDisconnecting = (dateTime: Number): StartDisconnecting => ({
  type: MobileTypes.START_DISCONNECTING,
  disconnectingDateTime: dateTime,
})

const checkInReset = (): CheckInReset => ({
  type: MobileTypes.CHECK_IN_RESET,
})

const checkInStartEarning = (mobile: Mobile, pendingCheckin: Checkin): CheckInStartEarning => ({
  type: MobileTypes.CHECK_IN_START_EARNING,
  mobile: mobile,
  isAttemptingToCheckin: true,
  pendingCheckin: pendingCheckin,
})

const checkInContinueEarning = (mobile: Mobile): CheckInContinueEarning => ({
  type: MobileTypes.CHECK_IN_CONTINUE_EARNING,
  mobile: mobile,
})

const checkIn = (locationBeacon: LocationBeacon): CheckIn => ({
  type: MobileTypes.CHECK_IN,
  locationBeacon: locationBeacon
})

const pendingCheckin = (pendingCheckin: Checkin): PendingCheckin => ({
  type: MobileTypes.PENDING_CHECKIN,
  pendingCheckin: pendingCheckin
})

const checkInSuccess = (mobile: Mobile, pendingCheckin: Checkin): CheckInSuccess => ({
  type: MobileTypes.CHECK_IN_SUCCESS,
  payload: { mobile },
  pendingCheckin: pendingCheckin,
})

const attemptCheckIn = (isAttemptingToCheckIn: boolean): AttemptCheckin => ({
  type: MobileTypes.ATTEMPT_CHECKIN,
  isAttemptingToCheckIn,
})

const checkInFailure = (error: Error, pendingCheckin: Checkin): CheckInFailure => ({
  type: MobileTypes.CHECK_IN_FAILURE,
  payload: { error },
  pendingCheckin: pendingCheckin
})

const checkOut = (isBluetoothEnabled: Boolean, isSilent: Boolean = false, force: Boolean = false, event: String = "regular", beacon: Beacon = null): CheckOut => ({
  type: MobileTypes.CHECK_OUT,
  isBluetoothEnabled: isBluetoothEnabled,
  isSilent: isSilent,
  force: force,
  event: event,
  beacon: beacon
})

const pendingCheckout = (pendingCheckout: Checkout): PendingCheckout => ({
  type: MobileTypes.PENDING_CHECKOUT,
  pendingCheckout: pendingCheckout,
})

const checkOutSuccess = (mobile: Mobile, pendingCheckout: Checkout): CheckOutSuccess => ({
  type: MobileTypes.CHECK_OUT_SUCCESS,
  payload: { mobile },
  pendingCheckout: pendingCheckout,
})

const checkOutFailure = (error: Error, pendingCheckout: Checkout): CheckOutFailure => ({
  type: MobileTypes.CHECK_OUT_FAILURE,
  payload: { error },
  pendingCheckout: pendingCheckout,
})

export default {
  lastCheckOutPeriodTime,
  lastCheckInPeriodTime,

  startDisconnecting,

  checkInReset,
  checkInStartEarning,
  checkInContinueEarning,

  checkIn,
  pendingCheckin,
  attemptCheckIn,
  checkInSuccess,
  checkInFailure,

  checkOut,
  pendingCheckout,
  checkOutSuccess,
  checkOutFailure,
}
