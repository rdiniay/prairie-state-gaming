import {Reducer} from 'redux'
import {MobileAction, MobileTypes} from '../mobile-redux'
import {Error, LocationBeacon, Mobile} from '../../models'
import {Checkin} from "../../models/checkin";
import {Checkout} from "../../models/checkout";

type MobileState = {
  disconnectingDateTime: Number
  loading: boolean
  mobile: Mobile
  error: Error
  currentCheckInPeriodTime: Number
  lastCheckInPeriodTime: Number
  lastCheckOutPeriodTime: Number
  isAttemptingToCheckin: boolean
  pendingCheckin: Checkin
  pendingCheckout: Checkout
}

const INITIAL_STATE: MobileState = {
  disconnectingDateTime: null,
  loading: false,
  mobile: null,
  error: null,
  currentCheckInPeriodTime: null,
  lastCheckInPeriodTime: null,
  lastCheckOutPeriodTime: null,
  isAttemptingToCheckin: false,
  pendingCheckin: null,
  pendingCheckout: null
}

const reset = (state: MobileState) => ({
  ...state, loading: false, error: null, isAttemptingToCheckin: false, pendingCheckin: null, pendingCheckout: null,
})

const lastCheckOutPeriodTime = (state: MobileState, dateTime: Number) => ({
  ...state, lastCheckOutPeriodTime: state.lastCheckOutPeriodTime && dateTime ? state.lastCheckOutPeriodTime : dateTime ? dateTime : null,
})

const lastCheckInPeriodTime = (state: MobileState, dateTime: Number) => ({
  ...state, currentCheckInPeriodTime: dateTime, lastCheckInPeriodTime: dateTime
})

const startDisconnectingDateTime = (state: MobileState, dateTime: Number) => ({
  ...state, disconnectingDateTime: dateTime
})

const setLoading = (state: MobileState) => ({
    ...state, loading: true,
})

const setIsAttemptingToCheckin = (state: MobileState, isAttemptingToCheckin: boolean) => ({
    ...state, isAttemptingToCheckin: isAttemptingToCheckin
})

const setPendingCheckin = (state: MobileState, pendingCheckin: any) => ({
  ...state, pendingCheckin: pendingCheckin
})


const setPendingCheckout = (state: MobileState, pendingCheckout: any) => ({
  ...state, pendingCheckout: pendingCheckout
})

const checkInSuccess = (state: MobileState, mobile: Mobile, pendingCheckin: Checkin) => ({
  ...state, loading: false, error: null, mobile: mobile, isAttemptingToCheckin: false, pendingCheckin: pendingCheckin,
})

const checkOutSuccess = (state: MobileState, mobile: Mobile) => ({
  ...state, loading: false, error: null, mobile: null, pendingCheckout: null,
  disconnectingDateTime: null,
  currentCheckInPeriodTime: null,
  lastCheckInPeriodTime: null,
  lastCheckOutPeriodTime: null,
})

const failure = (state: MobileState, error: Error, pendingCheckin: Checkin) => ({
  ...state, loading: false, error: error, isAttemptingToCheckin: false, pendingCheckin: pendingCheckin,
})

const reducer: Reducer<MobileState, MobileAction> = (
  state = INITIAL_STATE,
  action
) => {
  switch (action.type) {
    case MobileTypes.LAST_CHECK_OUT_PERIOD_TIME:
      return lastCheckOutPeriodTime(state, action.lastCheckOutPeriodTime)
    case MobileTypes.LAST_CHECK_IN_PERIOD_TIME:
      return lastCheckInPeriodTime(state, action.lastCheckInPeriodTime)

    case MobileTypes.START_DISCONNECTING:
      return startDisconnectingDateTime(state, action.disconnectingDateTime)

    case MobileTypes.CHECK_IN_RESET:
      return reset(state)
    case MobileTypes.CHECK_IN:
      return setLoading(state)
    case MobileTypes.CHECK_IN_SUCCESS:
      return checkInSuccess(state, action.payload.mobile, action.pendingCheckin)
    case MobileTypes.CHECK_IN_FAILURE:
      return failure(state, action.payload.error, action.pendingCheckin)

    case MobileTypes.CHECK_OUT:
      return setLoading(state)
    case MobileTypes.PENDING_CHECKOUT:
      // clear out possible pending checkin
      setPendingCheckin(state, null)
      console.log("mobile-redux: reducer - Pending Checkout ... " , action.pendingCheckout)
      return setPendingCheckout(state, action.pendingCheckout)
    case MobileTypes.PENDING_CHECKIN:
      // clear out possible pending checkout
      console.log("mobile-redux: reducer - Pending Checkin ... " , action.pendingCheckin)
      setPendingCheckout(state, null)
      return setPendingCheckin(state, action.pendingCheckin)
    case MobileTypes.CHECK_OUT_SUCCESS:
      return checkOutSuccess(state, action.payload.mobile)
    default:
      return state
  }
}

export default reducer
