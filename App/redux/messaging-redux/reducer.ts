import { Reducer } from 'redux'
import { MessagingTypes, MessagingAction } from '../messaging-redux'
import { Bank, Entry, Error } from '../../models'

type MessagingState = {
  loading: boolean
  error: Error
  token: String
  granted: Boolean
  remoteMessage: any
  topicMessage: any
}

const INITIAL_STATE: MessagingState = {
  loading: false,
  error: null,
  token: null,
  granted: false,
  remoteMessage: null,
  topicMessage: null
}

const setLoading = (state: MessagingState) => ({
    ...state, loading: true,
})

const generateMessagingTokenSuccess = (state: MessagingState, token: String) => {
  console.log(`new-fcmToken: ${token}`)
  return ({ ...state, loading: false, token: token })
}

const requestMessagingPermissionSuccesss = (state: MessagingState, granted: Boolean) => ({
  ...state, loading: false, granted: granted
})

const subscribeMessagingOnMessageSuccess = (state: MessagingState, remoteMessage: any) => ({
  ...state, loading: false, remoteMessage: remoteMessage
})

const subscribeMessagingOnTopicMessageSuccess = (state: MessagingState, topicMessage: any) => ({
  ...state, loading: false, topicMessage: topicMessage
})

const failure = (state: MessagingState, error: Error) => ({
  ...state, loading: false, error: error
})

const reducer: Reducer<MessagingState, MessagingAction> = (
  state = INITIAL_STATE,
  action
) => {
  switch (action.type) {
    case MessagingTypes.SUBSCRIBE_REFRESH_MESSAGING_TOKEN_SUCCESS:
      return generateMessagingTokenSuccess(state, action.payload.token)

    case MessagingTypes.SUBSCRIBE_MESSAGING_ON_TOPIC_MESSAGE_SUCCESS:
      return subscribeMessagingOnTopicMessageSuccess(state, action.payload.topicMessage)

    case MessagingTypes.SUBSCRIBE_MESSAGING_ON_BACKGROUND_MESSAGE_SUCCESS:
      return subscribeMessagingOnMessageSuccess(state, action.payload.remoteMessage)

    case MessagingTypes.SUBSCRIBE_MESSAGING_ON_MESSAGE_SUCCESS:
      return subscribeMessagingOnMessageSuccess(state, action.payload.remoteMessage)

    case MessagingTypes.GENERATE_MESSAGING_TOKEN:
      return setLoading(state)
    case MessagingTypes.GENERATE_MESSAGING_TOKEN_SUCCESS:
      return generateMessagingTokenSuccess(state, action.payload.token)
    case MessagingTypes.GENERATE_MESSAGING_TOKEN_FAILURE:
      return failure(state, action.payload.error)

    case MessagingTypes.REQUEST_MESSAGING_PERMISSION:
      return setLoading(state)
    case MessagingTypes.REQUEST_MESSAGING_PERMISSION_SUCCESS:
      return requestMessagingPermissionSuccesss(state, action.payload.granted)
    case MessagingTypes.REQUEST_MESSAGING_PERMISSION_FAILURE:
      return failure(state, action.payload.error)
    default:
      return state
  }
}

export default reducer
