import { Action as ReduxAction } from 'redux'
import { useSelector, TypedUseSelectorHook } from 'react-redux'
import { RootState } from '../../redux'
import { Bank, Period, Message, Error } from '../../models'

export const MessagingSelectors = {
    isLoading : (state: RootState) => state.messaging.loading,
    error : (state: RootState) => state.messaging.error,
    token : (state: RootState) => state.messaging.token,
    isGranted : (state: RootState) => state.messaging.granted,
}

export enum MessagingTypes {
  SEND_SCHEDULE_MESSAGING_ON_MESSAGE = 'SEND_SCHEDULE_MESSAGING_ON_MESSAGE',
  SEND_MESSAGING_ON_MESSAGE = 'SEND_MESSAGING_ON_MESSAGE',
  CANCEL_MESSAGE = 'CANCEL_MESSAGE',
  SEND_MESSAGING_ON_MESSAGE_SUCCESS = 'SEND_MESSAGING_ON_MESSAGE_SUCCESS',
  SEND_MESSAGING_ON_MESSAGE_FAILURE = 'SEND_MESSAGING_ON_MESSAGE_FAILURE',

  UNSUBSCRIBE_MESSAGING_ON_TOPIC_MESSAGE = 'UNSUBSCRIBE_MESSAGING_ON_TOPIC_MESSAGE',
  SUBSCRIBE_MESSAGING_ON_TOPIC_MESSAGE = 'SUBSCRIBE_MESSAGING_ON_TOPIC_MESSAGE',
  SUBSCRIBE_MESSAGING_ON_TOPIC_MESSAGE_SUCCESS = 'SUBSCRIBE_MESSAGING_ON_TOPIC_MESSAGE_SUCCESS',

  UNSUBSCRIBE_MESSAGING_ON_BACKGROUND_MESSAGE = 'UNSUBSCRIBE_MESSAGING_ON_BACKGROUND_MESSAGE',
  SUBSCRIBE_MESSAGING_ON_BACKGROUND_MESSAGE = 'SUBSCRIBE_MESSAGING_ON_BACKGROUND_MESSAGE',
  SUBSCRIBE_MESSAGING_ON_BACKGROUND_MESSAGE_SUCCESS = 'SUBSCRIBE_MESSAGING_ON_BACKGROUND_MESSAGE_SUCCESS',

  UNSUBSCRIBE_MESSAGING_ON_MESSAGE = 'UNSUBSCRIBE_MESSAGING_ON_MESSAGE',
  SUBSCRIBE_MESSAGING_ON_MESSAGE = 'SUBSCRIBE_MESSAGING_ON_MESSAGE',
  SUBSCRIBE_MESSAGING_ON_MESSAGE_SUCCESS = 'SUBSCRIBE_MESSAGING_ON_MESSAGE_SUCCESS',

  UNSUBSCRIBE_REFRESH_MESSAGING_TOKEN = 'UNSUBSCRIBE_REFRESH_MESSAGING_TOKEN',
  SUBSCRIBE_REFRESH_MESSAGING_TOKEN = 'SUBSCRIBE_REFRESH_MESSAGING_TOKEN',
  SUBSCRIBE_REFRESH_MESSAGING_TOKEN_SUCCESS = 'SUBSCRIBE_REFRESH_MESSAGING_TOKEN_SUCCESS',

  GENERATE_MESSAGING_TOKEN = 'GENERATE_MESSAGING_TOKEN',
  GENERATE_MESSAGING_TOKEN_SUCCESS = 'GENERATE_MESSAGING_TOKEN_SUCCESS',
  GENERATE_MESSAGING_TOKEN_FAILURE = 'GENERATE_MESSAGING_TOKEN_FAILURE',

  REQUEST_MESSAGING_PERMISSION = 'REQUEST_MESSAGING_PERMISSION',
  REQUEST_MESSAGING_PERMISSION_SUCCESS = 'REQUEST_MESSAGING_PERMISSION_SUCCESS',
  REQUEST_MESSAGING_PERMISSION_FAILURE = 'REQUEST_MESSAGING_PERMISSION_FAILURE',
}

export type MessagingAction =
  | SendScheduleMessagingOnMessage
  | SendMessagingOnMessage
  | CancelMessage
  | SendMessagingOnMessageSuccess
  | SendMessagingOnMessageFailure
  | UnsubscribeMessagingOnTopicMessage
  | SubscribeMessagingOnTopicMessage
  | SubscribeMessagingOnTopicMessageSuccess
  | UnsubscribeMessagingOnBackgroundMessage
  | SubscribeMessagingOnBackgroundMessage
  | SubscribeMessagingOnBackgroundMessageSuccess
  | UnsubscribeMessagingOnMessage
  | SubscribeMessagingOnMessage
  | SubscribeMessagingOnMessageSuccess
  | UnsubscribeRefreshMessagingToken
  | SubscribeRefreshMessagingToken
  | SubscribeRefreshMessagingTokenSuccess
  | GenerateMessagingToken
  | GenerateMessagingTokenSuccess
  | GenerateMessagingTokenFailure
  | RequestMessagingPermission
  | RequestMessagingPermissionSuccesss
  | RequestMessagingPermissionFailure

interface SendScheduleMessagingOnMessage
  extends ReduxAction<MessagingTypes.SEND_SCHEDULE_MESSAGING_ON_MESSAGE> {
  message: Message,
  scheduledSeconds: Number,
}

interface SendMessagingOnMessage
  extends ReduxAction<MessagingTypes.SEND_MESSAGING_ON_MESSAGE> {
  message: Message
}

interface CancelMessage
    extends ReduxAction<MessagingTypes.CANCEL_MESSAGE> {
    whichId: number
}

interface SendMessagingOnMessageSuccess
  extends ReduxAction<MessagingTypes.SEND_MESSAGING_ON_MESSAGE_SUCCESS> {
  payload: {
    success: any
  }
}

interface SendMessagingOnMessageFailure
  extends ReduxAction<MessagingTypes.SEND_MESSAGING_ON_MESSAGE_FAILURE> {
  payload: {
    error: Error
  }
}

interface UnsubscribeMessagingOnTopicMessage
  extends ReduxAction<MessagingTypes.UNSUBSCRIBE_MESSAGING_ON_TOPIC_MESSAGE> {
  topic: String
}

interface SubscribeMessagingOnTopicMessage
  extends ReduxAction<MessagingTypes.SUBSCRIBE_MESSAGING_ON_TOPIC_MESSAGE> {
  topic: String
}

interface SubscribeMessagingOnTopicMessageSuccess
  extends ReduxAction<MessagingTypes.SUBSCRIBE_MESSAGING_ON_TOPIC_MESSAGE_SUCCESS> {
  payload: {
    topicMessage: any
  }
}

type UnsubscribeMessagingOnBackgroundMessage = ReduxAction<MessagingTypes.UNSUBSCRIBE_MESSAGING_ON_BACKGROUND_MESSAGE>

type SubscribeMessagingOnBackgroundMessage = ReduxAction<MessagingTypes.SUBSCRIBE_MESSAGING_ON_BACKGROUND_MESSAGE>

interface SubscribeMessagingOnBackgroundMessageSuccess
  extends ReduxAction<MessagingTypes.SUBSCRIBE_MESSAGING_ON_BACKGROUND_MESSAGE_SUCCESS> {
  payload: {
    remoteMessage: any
  }
}

type UnsubscribeMessagingOnMessage = ReduxAction<MessagingTypes.UNSUBSCRIBE_MESSAGING_ON_MESSAGE>

type SubscribeMessagingOnMessage = ReduxAction<MessagingTypes.SUBSCRIBE_MESSAGING_ON_MESSAGE>

interface SubscribeMessagingOnMessageSuccess
  extends ReduxAction<MessagingTypes.SUBSCRIBE_MESSAGING_ON_MESSAGE_SUCCESS> {
  payload: {
    remoteMessage: any
  }
}

type UnsubscribeRefreshMessagingToken = ReduxAction<MessagingTypes.UNSUBSCRIBE_REFRESH_MESSAGING_TOKEN>

type SubscribeRefreshMessagingToken = ReduxAction<MessagingTypes.SUBSCRIBE_REFRESH_MESSAGING_TOKEN>

interface SubscribeRefreshMessagingTokenSuccess
  extends ReduxAction<MessagingTypes.SUBSCRIBE_REFRESH_MESSAGING_TOKEN_SUCCESS> {
  payload: {
    token: String
  }
}

interface GenerateMessagingTokenFailure
  extends ReduxAction<MessagingTypes.GENERATE_MESSAGING_TOKEN_FAILURE> {
  payload: {
    error: Error
  }
}

type GenerateMessagingToken = ReduxAction<MessagingTypes.GENERATE_MESSAGING_TOKEN>

interface GenerateMessagingTokenSuccess
  extends ReduxAction<MessagingTypes.GENERATE_MESSAGING_TOKEN_SUCCESS> {
  payload: {
    token: String
  }
}

interface GenerateMessagingTokenFailure
  extends ReduxAction<MessagingTypes.GENERATE_MESSAGING_TOKEN_FAILURE> {
  payload: {
    error: Error
  }
}

type RequestMessagingPermission = ReduxAction<MessagingTypes.REQUEST_MESSAGING_PERMISSION>

interface RequestMessagingPermissionSuccesss
  extends ReduxAction<MessagingTypes.REQUEST_MESSAGING_PERMISSION_SUCCESS> {
  payload: {
    granted: Boolean
  }
}

interface RequestMessagingPermissionFailure
  extends ReduxAction<MessagingTypes.REQUEST_MESSAGING_PERMISSION_FAILURE> {
  payload: {
    error: Error
  }
}

const sendScheduleMessagingOnMessage = (nessage: Message, scheduledSeconds: Number): SendScheduleMessagingOnMessage => ({
  type: MessagingTypes.SEND_SCHEDULE_MESSAGING_ON_MESSAGE,
  message: nessage,
  scheduledSeconds: scheduledSeconds,
})

const sendMessagingOnMessage = (nessage: Message): SendMessagingOnMessage => ({
  type: MessagingTypes.SEND_MESSAGING_ON_MESSAGE,
  message: nessage,
})

const cancelMessage = (whichId: number): CancelMessage => ({
    type: MessagingTypes.CANCEL_MESSAGE,
    whichId: whichId
})

const sendMessagingOnMessageSuccess = (success: any): SendMessagingOnMessageSuccess => ({
  type: MessagingTypes.SEND_MESSAGING_ON_MESSAGE_SUCCESS,
  payload: { success }
})

const sendMessagingOnMessageFailure = (error: Error): SendMessagingOnMessageFailure => ({
  type: MessagingTypes.SEND_MESSAGING_ON_MESSAGE_FAILURE,
  payload: { error }
})

const unsubscribeMessagingOnTopicMessage = (topic: String): UnsubscribeMessagingOnTopicMessage => ({
  type: MessagingTypes.UNSUBSCRIBE_MESSAGING_ON_TOPIC_MESSAGE,
  topic: topic,
})

const subscribeMessagingOnTopicMessage = (topic: String): SubscribeMessagingOnTopicMessage => ({
  type: MessagingTypes.SUBSCRIBE_MESSAGING_ON_TOPIC_MESSAGE,
  topic: topic,
})

const subscribeMessagingOnTopicMessageSuccess = (topicMessage: any): SubscribeMessagingOnTopicMessageSuccess => ({
  type: MessagingTypes.SUBSCRIBE_MESSAGING_ON_TOPIC_MESSAGE_SUCCESS,
  payload: { topicMessage }
})

const unsubscribeMessagingOnBackgroundMessage = (): UnsubscribeMessagingOnBackgroundMessage => ({
  type: MessagingTypes.UNSUBSCRIBE_MESSAGING_ON_BACKGROUND_MESSAGE,
})

const subscribeMessagingOnBackgroundMessage = (): SubscribeMessagingOnBackgroundMessage => ({
  type: MessagingTypes.SUBSCRIBE_MESSAGING_ON_BACKGROUND_MESSAGE,
})

const subscribeMessagingOnBackgroundMessageSuccess = (remoteMessage: any): SubscribeMessagingOnBackgroundMessageSuccess => ({
  type: MessagingTypes.SUBSCRIBE_MESSAGING_ON_BACKGROUND_MESSAGE_SUCCESS,
  payload: { remoteMessage }
})

const unsubscribeMessagingOnMessage = (): UnsubscribeMessagingOnMessage => ({
  type: MessagingTypes.UNSUBSCRIBE_MESSAGING_ON_MESSAGE,
})

const subscribeMessagingOnMessage = (): SubscribeMessagingOnMessage => ({
  type: MessagingTypes.SUBSCRIBE_MESSAGING_ON_MESSAGE,
})

const subscribeMessagingOnMessageSuccess = (remoteMessage: any): SubscribeMessagingOnMessageSuccess => ({
  type: MessagingTypes.SUBSCRIBE_MESSAGING_ON_MESSAGE_SUCCESS,
  payload: { remoteMessage }
})

const unsubscribeRefreshMessagingToken = (): UnsubscribeRefreshMessagingToken => ({
  type: MessagingTypes.UNSUBSCRIBE_REFRESH_MESSAGING_TOKEN,
})

const subscribeRefreshMessagingToken = (): SubscribeRefreshMessagingToken => ({
  type: MessagingTypes.SUBSCRIBE_REFRESH_MESSAGING_TOKEN,
})

const subscribeRefreshMessagingTokenSuccess = (token: String): SubscribeRefreshMessagingTokenSuccess => ({
  type: MessagingTypes.SUBSCRIBE_REFRESH_MESSAGING_TOKEN_SUCCESS,
  payload: { token }
})

const generateMessagingToken = (): GenerateMessagingToken => ({
  type: MessagingTypes.GENERATE_MESSAGING_TOKEN,
})

const generateMessagingTokenSuccess = (token: String): GenerateMessagingTokenSuccess => ({
  type: MessagingTypes.GENERATE_MESSAGING_TOKEN_SUCCESS,
  payload: { token }
})

const generateMessagingTokenFailure = (error: Error): GenerateMessagingTokenFailure => ({
  type: MessagingTypes.GENERATE_MESSAGING_TOKEN_FAILURE,
  payload: { error }
})
const requestMessagingPermission = (): RequestMessagingPermission => ({
  type: MessagingTypes.REQUEST_MESSAGING_PERMISSION,
})

const requestMessagingPermissionSuccess = (granted: Boolean): RequestMessagingPermissionSuccesss => ({
  type: MessagingTypes.REQUEST_MESSAGING_PERMISSION_SUCCESS,
  payload: { granted }
})

const requestMessagingPermissionFailure = (error: Error): RequestMessagingPermissionFailure => ({
  type: MessagingTypes.REQUEST_MESSAGING_PERMISSION_FAILURE,
  payload: { error }
})

export default {
    sendScheduleMessagingOnMessage,
    sendMessagingOnMessage,
    cancelMessage,
    sendMessagingOnMessageSuccess,
    sendMessagingOnMessageFailure,

    unsubscribeMessagingOnTopicMessage,
    subscribeMessagingOnTopicMessage,
    subscribeMessagingOnTopicMessageSuccess,

    unsubscribeMessagingOnBackgroundMessage,
    subscribeMessagingOnBackgroundMessage,
    subscribeMessagingOnBackgroundMessageSuccess,

    unsubscribeMessagingOnMessage,
    subscribeMessagingOnMessage,
    subscribeMessagingOnMessageSuccess,

    unsubscribeRefreshMessagingToken,
    subscribeRefreshMessagingToken,
    subscribeRefreshMessagingTokenSuccess,

    generateMessagingToken,
    generateMessagingTokenSuccess,
    generateMessagingTokenFailure,

    requestMessagingPermission,
    requestMessagingPermissionSuccess,
    requestMessagingPermissionFailure,
}
