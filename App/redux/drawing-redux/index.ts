import { Action as ReduxAction } from 'redux'
import { useSelector, TypedUseSelectorHook } from 'react-redux'
import { RootState } from '..'
import { Drawing, Error } from '../../models'

export const DrawingSelectors = {
    error : (state: RootState) => state.drawing.error,
    drawingDetails : (state: RootState) => state.drawing.drawingDetails,
    recentDrawings : (state: RootState) => state.drawing.recentDrawings,
    isStateWideDrawingExpired: (state: RootState) => isStateWideDrawingExpired(state),
    drawingRules : (state: RootState) => state.drawing.drawingRules,
    isLoading : (state: RootState) => state.drawing.loading,
}

const isStateWideDrawingExpired = (state: RootState) => {
  const recentDrawings: Drawing[] = state.drawing.recentDrawings
  const stateWideDrawing: Drawing = recentDrawings ? recentDrawings.find((drawing) => !drawing.locationId) : null
  if(stateWideDrawing){
    let endDateTime = Number(new Date(`${stateWideDrawing?.endDate}`)) // 2022-05-05T13:00:00.000Z
    const currentTime = Number(Date.now())
    console.log('drawing-redux: isStateWideDrawingExpired : ' , endDateTime, currentTime, stateWideDrawing)
    return currentTime > endDateTime
  }
  console.log('drawing-redux: isStateWideDrawingExpired : ', stateWideDrawing)
  return true

}

export enum DrawingTypes {
  CLEAR_DRAWING_RULES = 'CLEAR_DRAWING_RULES',

  START_COUNT_DOWN_STATE_WIDE_DRAWING = 'START_COUNT_DOWN_STATE_WIDE_DRAWING',
  STOP_COUNT_DOWN_STATE_WIDE_DRAWING = 'STOP_COUNT_DOWN_STATE_WIDE_DRAWING',

  GET_DRAWING_RULES = 'GET_DRAWING_RULES',
  GET_DRAWING_RULES_SUCCESS = 'GET_DRAWING_RULES_SUCCESS',
  GET_DRAWING_RULES_FAILURE = 'GET_DRAWING_RULES_FAILURE',

  GET_RECENT_DRAWINGS = 'GET_RECENT_DRAWINGS',
  GET_RECENT_DRAWINGS_SUCCESS = 'GET_RECENT_DRAWINGS_SUCCESS',
  GET_RECENT_DRAWINGS_FAILURE = 'GET_RECENT_DRAWINGS_FAILURE',

  GET_DRAWING_DETAILS = 'GET_DRAWING_DETAILS',
  GET_DRAWING_DETAILS_SUCCESS = 'GET_DRAWING_DETAILS_SUCCESS',
  GET_DRAWING_DETAILS_FAILURE = 'GET_DRAWING_DETAILS_FAILURE',
}

export type DrawingAction =
  | ClearDrawingRules
  | StartCountDownStateWideDrawing
  | StopCountDownStateWideDrawing
  | GetDrawingRules
  | GetDrawingRulesSuccess
  | GetDrawingRulesFailure
  | GetRecentDrawings
  | GetRecentDrawingsSuccess
  | GetRecentDrawingsFailure
  | GetDrawingDetails
  | GetDrawingDetailsSuccess
  | GetDrawingDetailsFailure

type ClearDrawingRules = ReduxAction<DrawingTypes.CLEAR_DRAWING_RULES>

type StartCountDownStateWideDrawing = ReduxAction<DrawingTypes.START_COUNT_DOWN_STATE_WIDE_DRAWING>
type StopCountDownStateWideDrawing = ReduxAction<DrawingTypes.STOP_COUNT_DOWN_STATE_WIDE_DRAWING>

type GetDrawingRules = ReduxAction<DrawingTypes.GET_DRAWING_RULES>

interface GetDrawingRulesSuccess
  extends ReduxAction<DrawingTypes.GET_DRAWING_RULES_SUCCESS> {
  payload: {
    drawingRules: Drawing
  }
}

interface GetDrawingRulesFailure
  extends ReduxAction<DrawingTypes.GET_DRAWING_RULES_FAILURE> {
  payload: {
    error: Error
  }
}

type GetRecentDrawings = ReduxAction<DrawingTypes.GET_RECENT_DRAWINGS>

interface GetRecentDrawingsSuccess
  extends ReduxAction<DrawingTypes.GET_RECENT_DRAWINGS_SUCCESS> {
  payload: {
    recentDrawings: Drawing[]
  }
}

interface GetRecentDrawingsFailure
  extends ReduxAction<DrawingTypes.GET_RECENT_DRAWINGS_FAILURE> {
  payload: {
    error: Error
  }
}

interface GetDrawingDetails
  extends ReduxAction<DrawingTypes.GET_DRAWING_DETAILS> {
  drawing: Drawing
}


interface GetDrawingDetailsSuccess
  extends ReduxAction<DrawingTypes.GET_DRAWING_DETAILS_SUCCESS> {
  payload: {
    drawing: Drawing
  }
}

interface GetDrawingDetailsFailure
  extends ReduxAction<DrawingTypes.GET_DRAWING_DETAILS_FAILURE> {
  payload: {
    error: Error
  }
}

const clearDrawingRules = (): ClearDrawingRules => ({
  type: DrawingTypes.CLEAR_DRAWING_RULES,
})

const startCountDownStateWideDrawing = (): StartCountDownStateWideDrawing => ({
  type: DrawingTypes.START_COUNT_DOWN_STATE_WIDE_DRAWING,
})

const stopCountDownStateWideDrawing = (): StopCountDownStateWideDrawing => ({
  type: DrawingTypes.STOP_COUNT_DOWN_STATE_WIDE_DRAWING,
})

const getDrawingRules = (): GetDrawingRules => ({
  type: DrawingTypes.GET_DRAWING_RULES,
})

const getDrawingRulesSuccess = (drawingRules: Drawing): GetDrawingRulesSuccess => ({
  type: DrawingTypes.GET_DRAWING_RULES_SUCCESS,
  payload: { drawingRules }
})

const getDrawingRulesFailure = (error: Error): GetDrawingRulesFailure => ({
  type: DrawingTypes.GET_DRAWING_RULES_FAILURE,
  payload: { error }
})

const getRecentDrawings = (): GetRecentDrawings => ({
  type: DrawingTypes.GET_RECENT_DRAWINGS,
})

const getRecentDrawingsSuccess = (recentDrawings: Drawing[]): GetRecentDrawingsSuccess => ({
  type: DrawingTypes.GET_RECENT_DRAWINGS_SUCCESS,
  payload: { recentDrawings }
})

const getRecentDrawingsFailure = (error: Error): GetRecentDrawingsFailure => ({
  type: DrawingTypes.GET_RECENT_DRAWINGS_FAILURE,
  payload: { error }
})

const getDrawingDetails = (drawing: Drawing): GetDrawingDetails => ({
  type: DrawingTypes.GET_DRAWING_DETAILS,
  drawing: drawing,
})

const getDrawingDetailsSuccess = (drawing: Drawing): GetDrawingDetailsSuccess => ({
  type: DrawingTypes.GET_DRAWING_DETAILS_SUCCESS,
  payload: { drawing }
})

const getDrawingDetailsFailure = (error: Error): GetDrawingDetailsFailure => ({
  type: DrawingTypes.GET_DRAWING_DETAILS_FAILURE,
  payload: { error }
})

export default {
  clearDrawingRules,

  startCountDownStateWideDrawing,
  stopCountDownStateWideDrawing,

  getDrawingRules,
  getDrawingRulesSuccess,
  getDrawingRulesFailure,

  getRecentDrawings,
  getRecentDrawingsSuccess,
  getRecentDrawingsFailure,

  getDrawingDetails,
  getDrawingDetailsSuccess,
  getDrawingDetailsFailure,
}
