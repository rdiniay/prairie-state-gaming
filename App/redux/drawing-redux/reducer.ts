import { Reducer } from 'redux'
import { DrawingTypes, DrawingAction } from '../drawing-redux'
import { Drawing, Error } from '../../models'

type DrawingState = {
  loading: boolean
  drawingRules: Drawing
  recentDrawings: Drawing[]
  drawingDetails: Drawing
  isStateWideDrawingExpired: boolean
  error: Error
}

const INITIAL_STATE: DrawingState = {
  loading: false,
  drawingRules: null, 
  recentDrawings: null,
  drawingDetails: null,
  isStateWideDrawingExpired: false,
  error: null
}

const clearDrawingRules = (state: DrawingState) => ({
  ...state, loading: false, error: null, drawingRules: null,
})

const setLoading = (state: DrawingState) => ({
    ...state, loading: true, 
})

const getDrawingRulesSuccesss = (state: DrawingState, drawingRules: Drawing) => ({
  ...state, loading: false, error: null, drawingRules: drawingRules
})

const getRecentDrawingSuccess = (state: DrawingState, recentDrawings: Drawing[]) => ({
  ...state, loading: false, error: null, recentDrawings: recentDrawings
})

const getDrawingDetailsSuccess = (state: DrawingState, drawingDetails: Drawing) => ({
  ...state, loading: false, error: null, drawingDetails: drawingDetails
})

const failure = (state: DrawingState, error: Error) => ({
  ...state, loading: false, error: error
})

const reducer: Reducer<DrawingState, DrawingAction> = (
  state = INITIAL_STATE,
  action
) => {
  switch (action.type) {
    case DrawingTypes.CLEAR_DRAWING_RULES:
      return clearDrawingRules(state)

    case DrawingTypes.GET_DRAWING_RULES:
      return setLoading(state)
    case DrawingTypes.GET_DRAWING_RULES_SUCCESS:
      return getDrawingRulesSuccesss(state, action.payload.drawingRules)
    case DrawingTypes.GET_DRAWING_RULES_FAILURE:
      return failure(state, action.payload.error)

    case DrawingTypes.GET_RECENT_DRAWINGS:
      return setLoading(state)
    case DrawingTypes.GET_RECENT_DRAWINGS_SUCCESS:
      return getRecentDrawingSuccess(state, action.payload.recentDrawings)
    case DrawingTypes.GET_RECENT_DRAWINGS_FAILURE:
      return failure(state, action.payload.error)

      case DrawingTypes.GET_DRAWING_DETAILS:
        return setLoading(state)
      case DrawingTypes.GET_DRAWING_DETAILS_SUCCESS:
        return getDrawingDetailsSuccess(state, action.payload.drawing)
      case DrawingTypes.GET_DRAWING_DETAILS_FAILURE:
        return failure(state, action.payload.error)
    default:
      return state
  }
}

export default reducer
