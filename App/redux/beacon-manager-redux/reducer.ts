import { Reducer } from 'redux'
import { BeaconManagerTypes, BeaconManagerAction } from '../beacon-manager-redux'
import { Bank, Beacon, Entry, Error } from '../../models'

type BeaconManagerState = {
  running: boolean
  loading: boolean
  checkInAttemptDone: boolean
  error: Error
  beaconsDetected: Beacon[]
  logTrace: string
}

const INITIAL_STATE: BeaconManagerState = {
  running: false,
  loading: false,
  checkInAttemptDone: false,
  error: null,
  beaconsDetected: [],
  logTrace: ''
}


const setRunning = (state: BeaconManagerState, isRunning: boolean) => ({
  ...state, running: isRunning
})

const setLoading = (state: BeaconManagerState) => ({
    ...state, loading: true,
})

const failure = (state: BeaconManagerState, error: Error) => ({
  ...state, loading: false, error: error
})

const setBeaconsDetected = (state: BeaconManagerState, beacons: Beacon[]) => ({
  ...state, beaconsDetected: beacons
})

const setLogTrace = (state: BeaconManagerState, logTrace: string) => ({
  ...state, logTrace: logTrace
})

const setCheckInAttemptDone = (state: BeaconManagerState, checkInAttemptDone: boolean) => ({
  ...state, checkInAttemptDone: checkInAttemptDone
})

const reducer: Reducer<BeaconManagerState, BeaconManagerAction> = (
  state = INITIAL_STATE,
  action
) => {
  switch (action.type) {
    case BeaconManagerTypes.START_BEACON_MONITORING:
      return setLoading(state)
    case BeaconManagerTypes.CURRENTLY_MONITORING:
      return setRunning(state, action.isRunning)
    case BeaconManagerTypes.BEACONS_DETECTED:
      return setBeaconsDetected(state, action.beaconsDetected)
    case BeaconManagerTypes.LOG_TRACE:
      return setLogTrace(state, action.logTrace)
    case BeaconManagerTypes.CHECK_IN_ATTEMPT_DONE:
      return setCheckInAttemptDone(state, action.checkInAttemptDone)
    default:
      return state
  }
}

export default reducer
