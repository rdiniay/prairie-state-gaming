import { Action as ReduxAction } from 'redux'
import { useSelector, TypedUseSelectorHook } from 'react-redux'
import { RootState } from '..'
import { Bank, Period, Error, Beacon } from '../../models'

export const BeaconManagerSelectors = {
    isRunning: (state: RootState) => state.beaconManager.running,
    isLoading : (state: RootState) => state.beaconManager.loading,
    checkInAttemptDone: (state: RootState) => state.beaconManager.checkInAttemptDone,
    error : (state: RootState) => state.beaconManager.error,
    beaconsDetected : (state: RootState) => state.beaconManager.beaconsDetected,
    logTrace : (state: RootState) => state.beaconManager.logTrace,
}

export enum BeaconManagerTypes {
  START_BEACON_MONITORING = 'START_BEACON_MONITORING',
  CURRENTLY_MONITORING = 'CURRENTLY_MONITORING',
  STOP_BEACON_MONITORING = 'STOP_BEACON_MONITORING',
  STOP_BG_TASK = 'STOP_BG_TASK',
  BEACONS_DETECTED = 'BEACONS_DETECTED',
  LOG_TRACE = 'LOG_TRACE',
  CHECK_IN_ATTEMPT_DONE = 'CHECK_IN_ATTEMPT_DONE',
}

export type BeaconManagerAction =
  | CurrentlyMonitoring
  | StartBeaconMonitoring
  | StopBeaconMonitoring
  | BeaconsDetected
  | LogTrace
  | CheckInAttemptDone

type StartBeaconMonitoring = ReduxAction<BeaconManagerTypes.START_BEACON_MONITORING>
interface StopBeaconMonitoring
    extends ReduxAction<BeaconManagerTypes.STOP_BEACON_MONITORING>{
    includeForegroundService: boolean
}

type StopBGTask = ReduxAction<BeaconManagerTypes.STOP_BG_TASK>

interface CurrentlyMonitoring
  extends ReduxAction<BeaconManagerTypes.CURRENTLY_MONITORING> {
    isRunning: boolean
  }

interface BeaconsDetected
  extends ReduxAction<BeaconManagerTypes.BEACONS_DETECTED> {
    beaconsDetected: Beacon[]
  }

interface LogTrace
  extends ReduxAction<BeaconManagerTypes.LOG_TRACE> {
    logTrace: string
  }

interface CheckInAttemptDone
  extends ReduxAction<BeaconManagerTypes.CHECK_IN_ATTEMPT_DONE> {
    checkInAttemptDone: boolean
  }

const startBeaconMonitoring = (): StartBeaconMonitoring => ({
  type: BeaconManagerTypes.START_BEACON_MONITORING,
})

const isCurrentlyMonitoring = (isRunning: boolean): CurrentlyMonitoring => ({
  type: BeaconManagerTypes.CURRENTLY_MONITORING,
  isRunning
})

const stopBeaconMonitoring = (includeForegroundService: boolean  = false): StopBeaconMonitoring => ({
  type: BeaconManagerTypes.STOP_BEACON_MONITORING,
  includeForegroundService
})

const stopBGTask = (): StopBGTask => ({
  type: BeaconManagerTypes.STOP_BG_TASK,
})

const beaconsDetected = (beaconsDetected: Beacon[]): BeaconsDetected => ({
  type: BeaconManagerTypes.BEACONS_DETECTED,
  beaconsDetected
})

const logTrace = (logTrace: string): LogTrace => ({
  type: BeaconManagerTypes.LOG_TRACE,
  logTrace
})

const checkInAttemptDone = (checkInAttemptDone: boolean): CheckInAttemptDone => ({
  type: BeaconManagerTypes.CHECK_IN_ATTEMPT_DONE,
  checkInAttemptDone
})

export default {
    startBeaconMonitoring,
    isCurrentlyMonitoring,
    stopBeaconMonitoring,
    stopBGTask,
    beaconsDetected,
    logTrace,
    checkInAttemptDone,
}
