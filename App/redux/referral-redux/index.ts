import { Action as ReduxAction } from 'redux'
import { useSelector, TypedUseSelectorHook } from 'react-redux'
import { RootState } from '..'
import { Referral, Error } from '../../models'

export const ReferralSelectors = {
    referral : (state: RootState) => state.referral.referral,
    isLoading : (state: RootState) => state.referral.loading,
    error: (state: RootState) => state.registration.error,
}

export enum ReferralTypes {
  SEND_PROMO_CODE = 'SEND_PROMO_CODE',
  SEND_PROMO_CODE_SUCCESS = 'SEND_PROMO_CODE_SUCCESS',
  SEND_PROMO_CODE_FAILURE = 'SEND_PROMO_CODE_FAILURE'
}

export type ReferralAction =
  | SendPromoCode
  | SendPromoCodeSuccess
  | SendPromoCodeFailure

interface SendPromoCode
  extends ReduxAction<ReferralTypes.SEND_PROMO_CODE> {
  code: string,
}

interface SendPromoCodeSuccess
  extends ReduxAction<ReferralTypes.SEND_PROMO_CODE_SUCCESS> {
  payload: {
    referral: Referral
  }
}

interface SendPromoCodeFailure
  extends ReduxAction<ReferralTypes.SEND_PROMO_CODE_FAILURE> {
  payload: {
    error: Error
  }
}

const sendPromoCode = (code: string): SendPromoCode => ({
  type: ReferralTypes.SEND_PROMO_CODE,
  code: code,
})

const sendPromoCodeSuccess = (referral: Referral): SendPromoCodeSuccess => ({
  type: ReferralTypes.SEND_PROMO_CODE_SUCCESS,
  payload: { referral }
})

const sendPromoCodeFailure = (error: Error): SendPromoCodeFailure => ({
  type: ReferralTypes.SEND_PROMO_CODE_FAILURE,
  payload: { error }
})

export default {
  sendPromoCode,
  sendPromoCodeSuccess,
  sendPromoCodeFailure,
}
