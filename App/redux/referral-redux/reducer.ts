import { Reducer } from 'redux'
import { ReferralTypes, ReferralAction } from '../referral-redux'
import { Referral, Error } from '../../models'

type ReferralState = {
  loading: boolean
  referral: Referral
  error: Error
}

const INITIAL_STATE: ReferralState = {
  loading: false,
  referral: null, 
  error: null
}

const setLoading = (state: ReferralState) => ({
    ...state, loading: true,
})

const sendPromoCodeSuccess = (state: ReferralState, referral: Referral) => ({
  ...state, loading: false, error: null, referral: referral
})

const failure = (state: ReferralState, error: Error) => ({
  ...state, loading: false, error: error
})

const reducer: Reducer<ReferralState, ReferralAction> = (
  state = INITIAL_STATE,
  action
) => {
  switch (action.type) {
    case ReferralTypes.SEND_PROMO_CODE:
      return setLoading(state)
    case ReferralTypes.SEND_PROMO_CODE_SUCCESS:
      return sendPromoCodeSuccess(state, action.payload.referral)
    case ReferralTypes.SEND_PROMO_CODE_FAILURE:
      return failure(state, action.payload.error)
    default:
      return state
  }
}

export default reducer
