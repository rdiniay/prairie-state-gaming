import { Reducer } from 'redux'
import { MenuTypes, MenuAction } from '../menu-redux'
import { Menu, Error } from '../../models'

type MenuState = {
  loading: boolean
  menu: Menu
  error: Error
}

const INITIAL_STATE: MenuState = {
  loading: false,
  menu: null, 
  error: null
}

const setLoading = (state: MenuState) => ({
    ...state, loading: true,
})

const getAboutSuccess = (state: MenuState, menu: Menu) => ({
    ...state, loading: false, error: null, menu: state.menu ? { ...state.menu, ...menu } : menu
})

const failure = (state: MenuState, error: Error) => ({
    ...state, loading: false, error: error
})

const reducer: Reducer<MenuState, MenuAction> = (
  state = INITIAL_STATE,
  action
) => {
  switch (action.type) {
    case MenuTypes.GET_ABOUT:
      return setLoading(state)
    case MenuTypes.GET_ABOUT_SUCCESS:
      return getAboutSuccess(state, action.payload.menu)
    case MenuTypes.GET_ABOUT_FAILURE:
      return failure(state, action.payload.error)
    default:
      return state
  }
}

export default reducer
