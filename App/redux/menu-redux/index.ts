import { Action as ReduxAction } from 'redux'
import { useSelector, TypedUseSelectorHook } from 'react-redux'
import { RootState } from '..'
import { Menu, Error } from '../../models'

export const MenuSelectors = {
    about : (state: RootState) => state.menu.menu ? state.menu.menu.about : '',
}

export enum MenuTypes {
  SIGN_OUT = 'SIGN_OUT',
  GET_ABOUT = 'GET_ABOUT',
  GET_ABOUT_SUCCESS = 'GET_ABOUT_SUCCESS',
  GET_ABOUT_FAILURE = 'GET_ABOUT_FAILURE'
}

export type MenuAction =
  | SignOut
  | GetAbout
  | GetAboutSuccess
  | GetAboutFailure

type SignOut = ReduxAction<MenuTypes.SIGN_OUT> 

type GetAbout = ReduxAction<MenuTypes.GET_ABOUT> 

interface GetAboutSuccess
  extends ReduxAction<MenuTypes.GET_ABOUT_SUCCESS> {
  payload: {
    menu: Menu
  }
}

interface GetAboutFailure
  extends ReduxAction<MenuTypes.GET_ABOUT_FAILURE> {
  payload: {
    error: Error
  }
}

const signOut = (): SignOut => ({
  type: MenuTypes.SIGN_OUT,
})

const getAbout = (): GetAbout => ({
  type: MenuTypes.GET_ABOUT,
})

const getAboutSuccess = (menu: Menu): GetAboutSuccess => ({
  type: MenuTypes.GET_ABOUT_SUCCESS,
  payload: { menu }
})

const getAboutFailure = (error: Error): GetAboutFailure => ({
  type: MenuTypes.GET_ABOUT_FAILURE,
  payload: { error }
})

export default {
  signOut,
  getAbout,
  getAboutSuccess,
  getAboutFailure,
}
