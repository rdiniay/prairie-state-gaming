import { Reducer } from 'redux'
import { AuthTypes, AuthAction } from '../auth-redux'
import { Auth } from '../../models'

type AuthState = {
  loading: boolean
  auth: Auth
}

const INITIAL_STATE: AuthState = {
  loading: false,
  auth: null,
}

const signInSuccess = (state: AuthState, auth: Auth) => ({
  ...state, loading: false, auth: auth
})

const authTestLocations = (state: AuthState, hideTestLocations: boolean) => ({
  ...state, loading: false, auth: state?.auth ? { ...state?.auth, hideTestLocations } : state?.auth
})

const reducer: Reducer<AuthState, AuthAction> = (
  state = INITIAL_STATE,
  action
) => {
  switch (action.type) {
    case AuthTypes.SIGN_IN_AUTH_SUCCESS:
      return signInSuccess(state, action.payload.auth)
    case AuthTypes.GET_AUTH_TEST_LOCATIONS_SUCCESS:
      return authTestLocations(state, action.payload.hideTestLocations)
    default:
      return state
  }
}

export default reducer
