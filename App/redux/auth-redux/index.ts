import { Action as ReduxAction } from 'redux'
import { useSelector, TypedUseSelectorHook } from 'react-redux'
import { RootState } from '../../redux'
import { Auth, Error } from '../../models'

export const AuthSelectors = {
    auth : (state: RootState) => state.auth.auth ? state.auth.auth : state.registration.auth,
}

export enum AuthTypes {
  SIGN_IN_AUTH_SUCCESS = 'SIGN_IN_AUTH_SUCCESS',
  GET_AUTH_TEST_LOCATIONS = 'GET_AUTH_TEST_LOCATIONS',
  GET_AUTH_TEST_LOCATIONS_SUCCESS = 'GET_AUTH_TEST_LOCATIONS_SUCCESS',
  GET_AUTH_TEST_LOCATIONS_FAILURE = 'GET_AUTH_TEST_LOCATIONS_FAILURE'
}

export type AuthAction =
  | SignInAuthSuccess
  | GetAuthTestLocations
  | GetAuthTestLocationsSuccess
  | GetAuthTestLocationsFailure

interface SignInAuthSuccess
  extends ReduxAction<AuthTypes.SIGN_IN_AUTH_SUCCESS> {
  payload: {
    auth: Auth
  }
}

type GetAuthTestLocations = ReduxAction<AuthTypes.GET_AUTH_TEST_LOCATIONS>

interface GetAuthTestLocationsSuccess
  extends ReduxAction<AuthTypes.GET_AUTH_TEST_LOCATIONS_SUCCESS> {
  payload: {
    hideTestLocations: boolean
  }
}

interface GetAuthTestLocationsFailure
  extends ReduxAction<AuthTypes.GET_AUTH_TEST_LOCATIONS_FAILURE> {
  payload: {
    error: Error
  }
}

const signInAuthSuccess = (auth: Auth): SignInAuthSuccess => ({
  type: AuthTypes.SIGN_IN_AUTH_SUCCESS,
  payload: { auth }
})

const getAuthTestLocations = (): GetAuthTestLocations => ({
  type: AuthTypes.GET_AUTH_TEST_LOCATIONS
})

const getAuthTestLocationsSuccess = (hideTestLocations: boolean): GetAuthTestLocationsSuccess => ({
  type: AuthTypes.GET_AUTH_TEST_LOCATIONS_SUCCESS,
  payload: { hideTestLocations }
})

const getAuthTestLocationsFailure = (error: Error): GetAuthTestLocationsFailure => ({
  type: AuthTypes.GET_AUTH_TEST_LOCATIONS_FAILURE,
  payload: { error }
})

export default {
  signInAuthSuccess,
  getAuthTestLocations,
  getAuthTestLocationsSuccess,
  getAuthTestLocationsFailure,
}
