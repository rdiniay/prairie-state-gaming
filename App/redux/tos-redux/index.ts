import { Action as ReduxAction } from 'redux'
import { RootState } from '..'
import { Rules, Error } from '../../models'

export const TOSSelectors = {
    tosContent : (state: RootState) => state.tos.tosContent,
    isLoading : (state: RootState) => state.tos.loading
}

export enum TOSTypes {
  GET_TOS_CONTENT = 'GET_TOS_CONTENT',
  GET_TOS_CONTENT_SUCCESS = 'GET_TOS_CONTENT_SUCCESS',
  GET_TOS_CONTENT_FAILURE = 'GET_TOS_CONTENT_FAILURE'
}

export type TOSAction =
  | GetTOSContent
  | GetTOSContentSuccess
  | GetTOSContentFailure

type GetTOSContent = ReduxAction<TOSTypes.GET_TOS_CONTENT> 

interface GetTOSContentSuccess
  extends ReduxAction<TOSTypes.GET_TOS_CONTENT_SUCCESS> {
  payload: {
    tosContent: Rules
  }
}

interface GetTOSContentFailure
  extends ReduxAction<TOSTypes.GET_TOS_CONTENT_FAILURE> {
  payload: {
    error: Error
  }
}

const getTOSContent = (): GetTOSContent => ({
  type: TOSTypes.GET_TOS_CONTENT,
})

const getTOSContentSuccess = (tosContent: Rules): GetTOSContentSuccess => ({
  type: TOSTypes.GET_TOS_CONTENT_SUCCESS,
  payload: { tosContent }
})

const getTOSContentFailure = (error: Error): GetTOSContentFailure => ({
  type: TOSTypes.GET_TOS_CONTENT_FAILURE,
  payload: { error }
})

export default {
  getTOSContent,
  getTOSContentSuccess,
  getTOSContentFailure,
}
