import { Reducer } from 'redux'
import { TOSTypes, TOSAction } from '.'
import { Rules, Error } from '../../models'

type TOSState = {
  loading: boolean
  tosContent: Rules
  error: Error
}

const INITIAL_STATE: TOSState = {
  loading: false,
  tosContent: null,
  error: null
}

const setLoading = (state: TOSState) => ({
    ...state, loading: true,
})

const getTOSSuccesss = (state: TOSState, tosContent: Rules) => ({
  ...state, loading: false, error: null, tosContent: tosContent
})

const failure = (state: TOSState, error: Error) => ({
  ...state, loading: false, error: error
})

const reducer: Reducer<TOSState, TOSAction> = (
  state = INITIAL_STATE,
  action
) => {
  switch (action.type) {
    case TOSTypes.GET_TOS_CONTENT:
      return setLoading(state)
    case TOSTypes.GET_TOS_CONTENT_SUCCESS:
      return getTOSSuccesss(state, action.payload.tosContent)
    case TOSTypes.GET_TOS_CONTENT_FAILURE:
      return failure(state, action.payload.error)
    default:
      return state
  }
}

export default reducer
