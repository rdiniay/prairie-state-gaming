import Result from 'folktale/result'
import firebase from '@react-native-firebase/app';
import '@react-native-firebase/auth'
import { Auth } from '../models'
import crashlytics from '@react-native-firebase/crashlytics';
const auth = firebase.auth()


const signInWithToken = (token: string): Result<Auth>  => 
  auth.signInAnonymously()
  // auth.signInWithCustomToken(token)
    .then(({ user }) => Result.Ok({ ...user, uid: auth.currentUser.uid }))
    .catch(e => {
      crashlytics().recordError(e)
      return Result.Error(e)
    
    })

export default {
  signInWithToken,
}