import Result from 'folktale/result'
import { Auth, Profile, Settings } from '../../models'
import { get, put, post } from '../../api'
import { User, Error, Coordinate, Location, Beacon } from '../../models'
import Utils from '../../services/shared/utils/Utils'

const toLocation = (data: any): Location => {
  const beacons: Beacon[] =  data.beacons.map((beaconData: any) => (
    { uuid: beaconData.beacon_id, major: beaconData.major, minor: beaconData.minor, }
  ))
  const coordinate: Coordinate = {
      latitude: Number(data.latitude),
      longitude: Number(data.longitude),
  }
  const points: Coordinate[] = data.points.coordinates
                                  .map(data => data).flat()
                                  .map(item => ({ latitude: item[0], longitude: item[1] }))
  let location: Location = {
      id: data.id,
      name: data.name,
      active: data.active,
      address1: data.address_1,
      address2: data.address_2,
      city: data.city,
      clientExcluded: data.client_excluded,
      companyId: data.company_id,
      drawingEnabled: data.drawing_enabled,
      phoneNumber: data.phone_number,
      points: points,
      reportingId: data.reporting_id,
      state: data.state,
      zipCode: data.zip_code,
      coordinate: coordinate,
      beacons: beacons,
      deleted: data.deleted,
      distance: data.distance,
  }
  const completeAddress = (location: Location) => {
      let address = location.address1 ? `${location.address1} ` : ''
      address += location.address2 ? `${location.address2} ` : ''
      address += location.city ? `${location.city} ` : ''
      address += location.state ? `${location.state} ` : ''
      address += location.zipCode ? `${location.zipCode} ` : ''
      return address
  }
  location = { ...location, completeAddress: completeAddress(location) }
  return location
}

const signIn = (userName: string, pin: string, push_token: string, device_id?: string, coordinate?: Coordinate) => {
  const isSigningInByPhone = Utils.validatePhoneNumber(userName)
  const accountAuth = isSigningInByPhone ? "/byphone" : ""
  const defaultUserName = isSigningInByPhone ? { phone_no: userName } : { email: userName }
  const signInPararms = {
    ...defaultUserName,
    pin,
    push_token,
    device_id
  }

  const data = {
    body: JSON.stringify(signInPararms),
  }
  return post(`account/auth${accountAuth}`, data,
          (success => {
            const data = success.data
            const locations: Location[] = data.locations.map((location: any) => toLocation(location))
            const auth: Auth = {
                ...data,
                locations: locations
            }
            return Result.Ok(auth)
          }),
          (failure => {
            let status = parseInt(failure.statusCode);
            switch(status){
              case 403: 
                let data = failure.data;
                if (data.message && data.message.includes("Please update app to latest version.") && data.url){
                  const error403FAU: Error = { 
                    forceAppUpdate:  { 
                      status: data.status,
                      message: data.message,
                      url: data.url
                    },
                    statusCode: status
                    
                  }
                  return Result.Error(error403FAU)
                }else{
                  const errorDefault: Error = {
                    genericError: 'Something went wrong please try again',
                    statusCode: status
                  }
                  return Result.Error(errorDefault)
                }
              case 404:
                data = failure.data
                const error404: Error = {
                   backendError: data.message ?  data.message : data.errorMessage,
                   statusCode: status
                }
                return Result.Error(error404)
             

                  
              default:
                const errorDefault: Error = {
                    genericError: 'Something went wrong please try again',
                    statusCode: status
                }
                return Result.Error(errorDefault)
            }
            
          }))
}

export default {
    signIn,
}