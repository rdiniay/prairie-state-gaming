import AsyncStorage from '@react-native-community/async-storage'
import * as GLOBAL from '../../../constants' 
import crashlytics from '@react-native-firebase/crashlytics'
import Preference from 'react-native-preference'
import { env } from '../../../config'


let THROTTLE_COUNT = 0;
const FIRST_TIME_LAUNCHED = 'FIRST_TIME_LAUNCHED'
const GLOBAL_APP_STATE = "GLOBAL_APP_STATE"
const GLOBAL_CONNECTION_DIALOG_IS_SHOWN = 'GLOBAL_CONNECTION_DIALOG_IS_SHOWN'
const EMAIL_ADDRESS_REG_EXP = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/
const BEACON_PROXIMITY = 'BEACON_PROXIMITY' // INSIDE OR OUTSIDE
const LOCATION_PROXIMITY = 'LOCATION_PROXIMITY' // INSIDE OR OUTSIDE
const AUTOSTART_PERMISSION = "AUTOSTART_PERMISSION"



function setFirstTimeLaunched() {
    AsyncStorage.setItem(FIRST_TIME_LAUNCHED, 'true');
}

function validateEmail(email){
    return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)
}

function validateFirstname(firstname) {
    return /^[a-z- A-Z.]+$/.test(firstname)
}

function validateLastname(lastname) {
    return /^[a-z- A-Z.]+$/.test(lastname)
}

function validateUniversalPhoneNumber(phoneNum) {
    const isWithoutSpace = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/.test(phoneNum)
    return isWithoutSpace ? isWithoutSpace : /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/.test(phoneNum)
}

function validatePhoneNumber(phoneNum){

    var cleaned = ('' + phoneNum).replace(/\D/g, '')

    return cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/)

}

const checkIfFirstLaunch = async () => {
  return await AsyncStorage.getItem(FIRST_TIME_LAUNCHED)
    .then( (result) => {
        if(result === null){
            setFirstTimeLaunched()
            return true
        }else{
            return false
        }
    })
    .catch((error) => {
        crashlytics().recordError(error)
        return false;
    })
}

const setGlobalConnectionDialogIsShown = (booleanVal) => {
    AsyncStorage.setItem(GLOBAL_CONNECTION_DIALOG_IS_SHOWN, `${booleanVal}`)
}

const getGlobalConnectionDialogIsShown = async () => { 
      return await AsyncStorage.getItem(GLOBAL_CONNECTION_DIALOG_IS_SHOWN)
            .then( (result) => {
                // convert to boolean data type
                console.log("getGlobalConnectionDialogIsShown: ", result)
                return JSON.parse(result)
            })
            .catch( (error) => {
                crashlytics().recordError(error)
                setGlobalConnectionDialogIsShown(false)
                return false
            })
      ;
    
}

const setBeaconProximity = (beaconProximity) => {
    AsyncStorage.setItem(BEACON_PROXIMITY, beaconProximity)
}

const getBeaconProximity = async ()  => { 
    return await AsyncStorage.getItem(BEACON_PROXIMITY)
        .then( (result) => {
            return result
        } )
        .catch ( (error) => {
            crashlytics().recordError(error)
            return GLOBAL.globals.Proximity.UNKNOWN
        })
}

const setLocationProximity = (locationProximity) => {
    AsyncStorage.setItem(LOCATION_PROXIMITY, locationProximity)
}

const getLocationProximity = async () => {
    return await new Promise((resolve, reject) => { 
        AsyncStorage.getItem(LOCATION_PROXIMITY)
        .then( (result) => {
            return resolve(result)
        } )
        .catch ( (error) => {
            crashlytics().recordError(error)
            return resolve(GLOBAL.globals.Proximity.UNKNOWN)
        })
    })
}

const setGlobalAppState = (stateVal) => { 
    AsyncStorage.setItem(GLOBAL_APP_STATE , stateVal)
}

const getGlobalAppState = async () => { 
    return await new Promise((resolve, reject) => {
        AsyncStorage.getItem(GLOBAL_APP_STATE)
            .then(result => { 
                return resolve(result)
            })
            .catch( error => { 
                crashlytics().recordError(error)
                return resolve(GLOBAL.globals.AppState.NA)
            })
    })
    
}

const getBaseUrlData = () => {
    var baseUrl = env.PSG_BASE_URL
    var xApiKey = env.X_API_KEY
    const newBaseUrl = Preference.get('ENV')

    switch(newBaseUrl){
      case 'dev':
        baseUrl = env.DEV_PSG_BASE_URL
        xApiKey = env.DEV_X_API_KEY
      break;
      case 'stage':
        baseUrl = env.STAGE_PSG_BASE_URL
        xApiKey = env.STAGE_X_API_KEY
      break;
      default:
        baseUrl = env.PSG_BASE_URL
        xApiKey = env.X_API_KEY
      break;
    }

    return {
        baseUrl: baseUrl,
        xApiKey: xApiKey
    }
}

// check if user dismissed before
const setAutoStartPermission = (cancelled: boolean) => {
    AsyncStorage.setItem(AUTOSTART_PERMISSION , `${cancelled}`)
}

const getAutoStartPermission = async () => {
    return await new Promise((resolve, reject) => {
        AsyncStorage.getItem(AUTOSTART_PERMISSION)
        .then( result => {
            if(result == null){
                return resolve(true)
            }
            return resolve(JSON.parse(result))
        })
        .catch( (error) => {
            crashlytics().recordError(error)
            return resolve(true)
        })
    })
}

const multipleTapHandler = (func: () => void, wait = 500) => {
    let handler: any;

    return function() {
        if (THROTTLE_COUNT === 0) {
            THROTTLE_COUNT++;
            func();
        }
        // Clear the previous timeout and set a new one.
        clearTimeout(handler);
        handler = setTimeout(() => (THROTTLE_COUNT = 0), wait);
    };
};

export default  {
    multipleTapHandler,
    validateEmail,
    validateFirstname,
    validateLastname,
    validateUniversalPhoneNumber,
    validatePhoneNumber,
    checkIfFirstLaunch,
    setGlobalConnectionDialogIsShown,
    getGlobalConnectionDialogIsShown,
    setBeaconProximity,
    getBeaconProximity,
    setLocationProximity,
    getLocationProximity,
    setGlobalAppState,
    getGlobalAppState,
    getBaseUrlData,
    setAutoStartPermission,
    getAutoStartPermission,
}