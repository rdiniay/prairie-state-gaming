import Result from 'folktale/result'
import { Auth, Profile, Settings } from '../../models'
import { env } from '../../config'
import { get, put, post } from '../../api'
import { Referral, Rules, Error } from '../../models'
import { strings } from '../../localization/strings'

const sendPromoCode = (code: string, auth: Auth) => {
  const options = {
    headers: {
      'Authorization': `Basic ${auth.authData}`,
    },
    body: JSON.stringify({
        code: code,
        accountId: auth.accountId,
    })
  }
  return post(`referral/redemption`, options,
          (success => {
            const data = success.data
            const referral: Referral = {
                award: data.award
            }
            return Result.Ok(referral)
          }),
          (failure => {
            let data = failure.data;
            if(data.message && data.message.includes("Network request failed")){
              const errorConnection: Error = {
                connectionError: strings.no_internet_connection_when_requesting_alert_dialog_title,
                connectionErrorMessage: strings.no_internet_connection_when_requesting_alert_dialog_message,
                statusCode: -1
              }
              return Result.Error(errorConnection)
            }else{
              let status = parseInt(failure.statusCode);
              console.log('error-create: ', failure)
              switch(status){
                case 403: 
                  if (data.message && data.message.includes("App is outdated. Please download latest app.")){
                    const error403SignOut: Error = { 
                      signoutUser: true,
                      statusCode: status
                      
                    }
                    return Result.Error(error403SignOut)
                  }else{
                    const errorDefault: Error = {
                      genericError: 'Something went wrong please try again',
                      statusCode: status
                    }
                    return Result.Error(errorDefault)
                  }
                case 404:
                  data = failure.data
                  const error404: Error = {
                      backendError: data.message ?  data.message : data.errorMessage,
                      statusCode: status
                  }
                  return Result.Error(error404)
                    
                default:
                  const errorDefault: Error = {
                      genericError: 'Something went wrong please try again',
                      statusCode: status
                  }
                  return Result.Error(errorDefault)
              }
            }
            
            
          }))
}

export default {
    sendPromoCode,
}