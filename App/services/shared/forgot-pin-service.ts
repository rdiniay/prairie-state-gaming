import Result from 'folktale/result'
import { Auth, Profile, Settings } from '../../models'
import { get, put, post } from '../../api'
import { Rules, Error } from '../../models'
import { strings } from '../../localization/strings'

const verifyPin = (newPin: String, emailAddress: String, code: String) => {
  const options = {
    body: JSON.stringify({
      email_address: emailAddress,
      code: code,
      pin_number: newPin,
    })
  }
  return post(`pinResetRequest/verification`, options,
          (success => {
            console.log(success)
            return Result.Ok(true)
          }),
          (failure => {
            let data = failure.data;
            if(data.message && data.message.includes("Network request failed")){
              const errorConnection: Error = {
                connectionError: strings.no_internet_connection_when_requesting_alert_dialog_title,
                connectionErrorMessage: strings.no_internet_connection_when_requesting_alert_dialog_message,
                statusCode: -1
              }
              return Result.Error(errorConnection)
            }else{
              let status = parseInt(failure.statusCode);
              console.log('error-create: ', failure)
              switch(status){
                case 403: 
                  if (data.message && data.message.includes("App is outdated. Please download latest app.")){
                    const error403SignOut: Error = { 
                      signoutUser: true,
                      statusCode: status
                      
                    }
                    return Result.Error(error403SignOut)
                  }else{
                    const errorDefault: Error = {
                      genericError: 'Something went wrong please try again',
                      statusCode: status
                    }
                    return Result.Error(errorDefault)
                  }
                case 404:
                  data = failure.data
                  const error404: Error = {
                      api: 'pinResetRequest/verification',
                      backendError: data.message ?  data.message : data.errorMessage,
                      statusCode: status
                  }
                  return Result.Error(error404)
                case 500:
                  let data500 = failure.data
                  const error500: Error = {
                      api: 'pinResetRequest/verification',
                      backendError: data500.message ?  data500.message : data500.errorMessage,
                      statusCode: status
                  }
                  return Result.Error(error500)
                    
                default:
                  const errorDefault: Error = {
                      genericError: 'Something went wrong please try again',
                      statusCode: status
                  }
                  return Result.Error(errorDefault)
              }
            }
            
            
          }))
}

const sendForgotPinVerification = (emailAddress: string, auth: Auth) => {
  const options = {
    body: JSON.stringify({
        email_address: emailAddress,
    })
  }
  return post(`pinResetRequest`, options,
          (success => {
            console.log(success)
            
            return Result.Ok(true)
          }),
          (failure => {
            let data = failure.data;
            if(data.message && data.message.includes("Network request failed")){
              const errorConnection: Error = {
                connectionError: strings.no_internet_connection_when_requesting_alert_dialog_title,
                connectionErrorMessage: strings.no_internet_connection_when_requesting_alert_dialog_message,
                statusCode: -1
              }
              return Result.Error(errorConnection)
            }else{
              let status = parseInt(failure.statusCode);
              console.log('error-create: ', failure)
              switch(status){
                case 403: 
                  if (data.message && data.message.includes("App is outdated. Please download latest app.")){
                    const error403SignOut: Error = { 
                      signoutUser: true,
                      statusCode: status
                      
                    }
                    return Result.Error(error403SignOut)
                  }else{
                    const errorDefault: Error = {
                      genericError: 'Something went wrong please try again',
                      statusCode: status
                    }
                    return Result.Error(errorDefault)
                  }
                case 404:
                  data = failure.data
                  const error404: Error = {
                      backendError: data.message ?  data.message : data.errorMessage,
                      statusCode: status
                  }
                  return Result.Error(error404)
                    
                default:
                  const errorDefault: Error = {
                      genericError: 'Something went wrong please try again',
                      statusCode: status
                  }
                  return Result.Error(errorDefault)
              }
            }
            
            
          }))
}

export default {
    verifyPin,
    sendForgotPinVerification,
}