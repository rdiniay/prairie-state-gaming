import Result from 'folktale/result'
import { Auth, Profile, Settings } from '../../models'
import { get, put, post } from '../../api'
import { Mobile, Rules, Error } from '../../models'
import { strings } from '../../localization/strings'

const toObjectMobile = (mobile: Mobile): any => {
    const object = ({ accountId: mobile.accountId,
        keychain: mobile.keychain,
        action: mobile.action,
        coords: {
            lat: mobile.coordinate.latitude,
            lng: mobile.coordinate.longitude,
            accuracy: mobile.coordinate.accuracy,
            altitude: mobile.coordinate.altitude,
            status: mobile.coordinate?.status,
        },
        existingCheckinId: mobile.existingCheckinId,
        email: mobile.email,
        pin: mobile.pin,
        extras: mobile?.extra,
    })
    return mobile.beacon ? ({ ...object, beacon: { ...mobile.beacon }}) : object
}

const checkIn = (mobile: Mobile, auth: Auth) => {
  const options = {
    headers: {
      'Authorization': `Basic ${auth.authData}`,
    },
    body: JSON.stringify(toObjectMobile({ ...mobile, action: "checkin" }))
  }
  return post(`mobile`, options,
          (success => {
            let data = success.data
            let mobileCheckIn: Mobile = { ...mobile, ...data}
            return Result.Ok(mobileCheckIn)
          }),
          (failure => {
            let data = failure.data;
            if(data.message && data.message.includes("Network request failed")){
              const errorConnection: Error = {
                connectionError: strings.no_internet_connection_when_requesting_alert_dialog_title,
                connectionErrorMessage: strings.no_internet_connection_when_requesting_alert_dialog_message,
                statusCode: -1
              }
              return Result.Error(errorConnection)
            }else{
              let status = parseInt(failure.statusCode);
              console.log('error-create: ', failure)
              switch(status){
                case 403:
                  if (data.message && data.message.includes("App is outdated. Please download latest app.")){
                    const error403SignOut: Error = {
                      signoutUser: true,
                      statusCode: status

                    }
                    return Result.Error(error403SignOut)
                  }else{
                    const errorDefault: Error = {
                      genericError: 'Something went wrong please try again',
                      statusCode: status
                    }
                    return Result.Error(errorDefault)
                  }
                case 404:
                  data = failure.data
                  const error404: Error = {
                      backendError: data.message ?  data.message : data.errorMessage,
                      statusCode: status
                  }
                  return Result.Error(error404)

                default:
                  const errorDefault: Error = {
                      genericError: 'Something went wrong please try again',
                      statusCode: status
                  }
                  return Result.Error(errorDefault)
              }
            }


          }))
}

const checkOut = (mobile: Mobile, auth: Auth) => {
  const options = {
    headers: {
      'Authorization': `Basic ${auth.authData}`,
    },
    body: JSON.stringify(toObjectMobile({ ...mobile, action: "checkout" }))
  }
  return post(`mobile`, options,
          (success => {
            let data = success.data
            let mobileCheckOut: Mobile = { ...mobile, ...data}
            console.log('mobileCheckOut: ', mobileCheckOut)
            return Result.Ok(mobileCheckOut)
          }),
          (failure => {
            let data = failure.data;
            if(data.message && data.message.includes("Network request failed")){
              const errorConnection: Error = {
                connectionError: strings.no_internet_connection_when_requesting_alert_dialog_title,
                connectionErrorMessage: strings.no_internet_connection_when_requesting_alert_dialog_message,
                statusCode: -1
              }
              return Result.Error(errorConnection)
            }else{
              let status = parseInt(failure.statusCode);
              console.log('error-create: ', failure)
              switch(status){
                case 403:
                  if (data.message && data.message.includes("App is outdated. Please download latest app.")){
                    const error403SignOut: Error = {
                      signoutUser: true,
                      statusCode: status

                    }
                    return Result.Error(error403SignOut)
                  }else{
                    const errorDefault: Error = {
                      genericError: 'Something went wrong please try again',
                      statusCode: status
                    }
                    return Result.Error(errorDefault)
                  }
                case 404:
                  data = failure.data
                  const error404: Error = {
                      backendError: data.message ?  data.message : data.errorMessage,
                      statusCode: status
                  }
                  return Result.Error(error404)

                default:
                  const errorDefault: Error = {
                      genericError: 'Something went wrong please try again',
                      statusCode: status
                  }
                  return Result.Error(errorDefault)
              }
            }


          }))
}

export default {
    checkIn,
    checkOut,
}
