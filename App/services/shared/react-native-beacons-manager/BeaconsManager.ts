import Beacons from 'react-native-beacons-manager'

type iBeacons = typeof Beacons 
type BeaconsEventEmitter = {
    addListener: Function
}
interface BeaconManager extends iBeacons {
    BeaconsEventEmitter: BeaconsEventEmitter
}

const iBeacons: BeaconManager = Beacons as any

export default iBeacons