import Result from 'folktale/result'
import { eventChannel, END } from 'redux-saga'
import { Platform } from 'react-native'
import { Auth, Profile, Settings } from '../../models'
import { get, put, post } from '../../api'
import { Coordinate, Location, Beacon, Rules, Error } from '../../models'
import Geolocation from 'react-native-geolocation-service'
import BackgroundGeolocation, {Geofence, ProviderChangeEvent} from "react-native-background-geolocation";
import { strings } from '../../localization/strings'
import Utils from '../../services/shared/utils/Utils'
import * as GLOBAL from '../../constants'
import {PERMISSIONS, request, RESULTS} from "react-native-permissions";
import {Status} from "../../models/status";

const toLocation = (data: any): Location => {
    const beacons: Beacon[] =  data.beacons.map((beaconData: any) => (
        { uuid: beaconData.beacon_id, major: beaconData.major, minor: beaconData.minor, }
    ))
    const coordinate: Coordinate = {
        latitude: Number(data.latitude),
        longitude: Number(data.longitude),
    }
    const points: Coordinate[] = data.points.coordinates
                                    .map(data => data).flat()
                                    .map(item => ({ latitude: item[0], longitude: item[1] }))
    let location: Location = {
        id: data.id,
        name: data.name,
        active: data.active,
        address1: data.address_1,
        address2: data.address_2,
        city: data.city,
        clientExcluded: data.client_excluded, //data.id === 693 ? 1 :
        companyId: data.company_id,
        drawingEnabled: data.drawing_enabled,
        phoneNumber: data.phone_number,
        points: points,
        reportingId: data.reporting_id,
        state: data.state,
        zipCode: data.zip_code,
        coordinate: coordinate,
        beacons: beacons,
        deleted: data.deleted,
        distance: data.distance,
        isNear: true,
    }
    const completeAddress = (location: Location) => {
        let address = location.address1 ? `${location.address1} ` : ''
        address += location.address2 ? `${location.address2} ` : ''
        address += location.city ? `${location.city} ` : ''
        address += location.state ? `${location.state} ` : ''
        address += location.zipCode ? `${location.zipCode} ` : ''
        return address
    }
    location = { ...location, completeAddress: completeAddress(location) }
    return location
}

const getGeofencingWithParams = (coordinate: Coordinate, points: Coordinate[], auth: Auth) =>
    eventChannel(emitter => {
        const options = {
            headers: {
                'Authorization': `Basic ${auth.authData}`,
            }
         }
         get(`location/within?lat=${coordinate.latitude}&lon=${coordinate.longitude}`, options,
                (success => {
                    // {
                    //     "status": "200",
                    //     "stillInside": true,
                    //     "location_id": 26
                    // }
                    emitter(success.data)
                }),
                (failure => {

                    emitter("")

                }))
        return () => { }
    })

const getCurrentLocationWithParams = (isGranted: Boolean) =>
  eventChannel(emitter => {
    // console.log("getCurrentLocationWithParams : " , isGranted, emitter)
    const geoLocation = () => {
        const params: any = {
            accuracy: {
                android: 'high',
                ios: 'best',
            },
            enableHighAccuracy: true,
            timeout: 3000,
            forceLocation: true
        }
        Geolocation.getCurrentPosition(
            async (location) => {


                const stateEvent = await BackgroundGeolocation.getProviderState()
                const status: Status =  {
                    accuracyAuthorization: stateEvent?.accuracyAuthorization,
                    enabled: stateEvent?.enabled,
                    gps: stateEvent?.gps,
                    network: stateEvent?.network,
                    status: stateEvent?.status

                }
                const coordinate: Coordinate = {
                    latitude: location.coords.latitude,
                    longitude: location.coords.longitude,
                    accuracy: location.coords.accuracy,
                    altitude: location.coords.altitudeAccuracy,
                    status: status
                }
                console.log('location-service getCurrentPosition : ', coordinate)
                emitter(coordinate)
            },
            (error) => { emitter(error) },
            params
        );
    }
    if(isGranted){
        geoLocation()
    }
    else if (Platform.OS === 'ios') {
        Geolocation.requestAuthorization('always')
        .then(status => {
            geoLocation()
        })
    }
    else if (Platform.OS === 'android' && Platform.Version < 23)
        geoLocation()
    else {
        request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
        .then(status => {
            if (status === RESULTS.GRANTED)
                geoLocation()
        })
    }
    return () => { }
})

const getLocationBeaconsWithCoordinate = (coordinate: Coordinate, auth: Auth) => {
    // console.log("getLocationBeaconsWithCoordinate: " , coordinate, auth)
    const options = {
        headers: {
            'Authorization': `Basic ${auth.authData}`,
        }
     }

    return get(`location/nearby?lat=${coordinate.latitude}&lon=${coordinate.longitude}`, options,
            (success => {
                const data = success.data
                const locations: Location[] = data.map((item: any) => toLocation(item))
                //const locations: Location[] = data.map((item: any) => toLocation(item))
                return Result.Ok(locations)
            }),
            (failure => {
                let data = failure.data;
                if(data.message && data.message.includes("Network request failed")){
                    const errorConnection: Error = {
                      connectionError: strings.no_internet_connection_when_requesting_alert_dialog_title,
                      connectionErrorMessage: strings.no_internet_connection_when_requesting_alert_dialog_message,
                      statusCode: -1
                    }
                    return Result.Error(errorConnection)
                  }else{
                    let status = parseInt(failure.statusCode);
                    console.log('error-get: ', failure)
                    switch(status){
                        case 403:
                            let data = failure.data;
                            if (data.message && data.message.includes("App is outdated. Please download latest app.")){
                            const error403SignOut: Error = {
                                signoutUser: true,
                                statusCode: status

                            }
                            return Result.Error(error403SignOut)
                            }else{
                            const errorDefault: Error = {
                                genericError: 'Something went wrong please try again',
                                statusCode: status
                            }
                            return Result.Error(errorDefault)
                            }
                        case 404:
                        data = failure.data
                        const error404: Error = {
                            backendError: data.message ?  data.message : data.errorMessage,
                            statusCode: status
                        }
                        return Result.Error(error404)

                        default:
                        const errorDefault: Error = {
                            genericError: 'Something went wrong please try again',
                            statusCode: status
                        }
                        return Result.Error(errorDefault)
                    }

                  }

            }))
  }


export default {
    getGeofencingWithParams,
    getCurrentLocationWithParams,
    getLocationBeaconsWithCoordinate,
}
