import Result from 'folktale/result'
import { Auth, Profile, Settings } from '../../models'
import { get, put, post } from '../../api'
import { Menu, Rules, Error } from '../../models'

const getAbout = (auth: Auth) => {
  const menu: Menu = {
    about: "Penn National Gaming owns, operates or has ownership interests in gaming and racing facilities and video gaming terminal operations with a focus on slot machine entertainment. Ass of October 15, 2018, the company operates 40 facilities in 18 jurisdictions, including Colorrado, Florida, Illinois, Indiana, Lowa, Kansas, Louisiana, Maine, Massachusetts, Mississippi, Missouri, Nevada, New Jersey, New Mexico, Ohio, Pennsylvania, Texas, and West Virginia. In aggregate, Penn National Gaming operates approximately 49,400 gaming machines, 1,200 table games and 8,800 hotel rooms. The company also offers social online gaming through its Penn Interactive Ventures division."
    + "\n" +
    "Copyright @ Penn National Gaming Inc, 1.0" +
    "\n" +
    "Prod. 1.0"
  }
  return Result.Ok(menu)
}

export default {
    getAbout,
}