import Result from 'folktale/result'
import { Auth, Profile, Settings } from '../../models'
import { get } from '../../api'
import { TOS, Rules, Error } from '../../models'

const getTOSContent = () => {
  const options = {
    // NO AUTH HEADERS NEEDED
  }
  return get(`appconfig/by?code=TOS`, options,
          (success => {
            console.log(success)
            const tos: TOS = {
                rules: {
                  id: success.data.config_id,
                  value: success.data.value,
                  code: success.data.code,
                }
            }
            console.log("TOS RETURNED SUCCESS", tos);
            return Result.Ok(tos)
          }),
          (failure => {
            let status = parseInt(failure.statusCode);
            console.log('error-create: ', failure)
            switch(status){
              case 403: 
                let data = failure.data;
                if (data.message && data.message.includes("App is outdated. Please download latest app.")){
                  const error403SignOut: Error = { 
                    signoutUser: true,
                    statusCode: status
                    
                  }
                  return Result.Error(error403SignOut)
                }else{
                  const errorDefault: Error = {
                    genericError: 'Something went wrong please try again',
                    statusCode: status
                  }
                  return Result.Error(errorDefault)
                }
              case 404:
                data = failure.data
                const error404: Error = {
                    backendError: data.message ?  data.message : data.errorMessage,
                    statusCode: status
                }
                return Result.Error(error404)
                  
              default:
                const errorDefault: Error = {
                    genericError: 'Something went wrong please try again',
                    statusCode: status
                }
                return Result.Error(errorDefault)
            }
            
          }))
}

export default {
    getTOSContent,
}