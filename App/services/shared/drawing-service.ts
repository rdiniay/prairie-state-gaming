import Result from 'folktale/result'
import DateString from 'moment'
import { get, put, post } from '../../api'
import { Drawing, Multiplier, Rules, Coordinate, Error, Auth, Beacon, DrawingDate } from '../../models'
import { strings } from '../../localization/strings'

const getDrawingRules = (auth: Auth) => {
  const options = {
    headers: {
      'Authorization': `Basic ${auth.authData}`,
    }
  }
  return get(`appconfig/1`, options,
          (success => {
            console.log(success)
            const drawing: Drawing = {
                rules: {
                  id: success.data.config_id,
                  value: success.data.value,
                  code: success.data.code,
                }
            }
            return Result.Ok(drawing)
          }),
          (failure => {
            let data = failure.data;
            if(data.message && data.message.includes("Network request failed")){
              const errorConnection: Error = {
                connectionError: strings.no_internet_connection_when_requesting_alert_dialog_title,
                connectionErrorMessage: strings.no_internet_connection_when_requesting_alert_dialog_message,
                statusCode: -1
              }
              return Result.Error(errorConnection)
            }else{
              let status = parseInt(failure.statusCode);
              console.log('error-create: ', failure)
              switch(status){
                case 403: 
                  if (data.message && data.message.includes("App is outdated. Please download latest app.")){
                    const error403SignOut: Error = { 
                      signoutUser: true,
                      statusCode: status
                      
                    }
                    return Result.Error(error403SignOut)
                  }else{
                    const errorDefault: Error = {
                      genericError: 'Something went wrong please try again',
                      statusCode: status
                    }
                    return Result.Error(errorDefault)
                  }
                case 404:
                  data = failure.data
                  const error404: Error = {
                      backendError: data.message ?  data.message : data.errorMessage,
                      statusCode: status
                  }
                  return Result.Error(error404)
                    
                default:
                  const errorDefault: Error = {
                      genericError: 'Something went wrong please try again',
                      statusCode: status
                  }
                  return Result.Error(errorDefault)
              }
            }
            
            
          }))
}

const getRecentDrawings = (coordinate: Coordinate, auth: Auth) => {
  const options = {
    headers: {
      'Authorization': `Basic ${auth.authData}`,
    }
  }
  return get(`drawing/recentAll?lat=${coordinate.latitude}&lng=${coordinate.longitude}`, options,
          (success => {
            console.log('drawing/recent: ', success)
            const data = success.data
            const drawings: Drawing[] = data.map((item: any) => toParseDrawing(item.drawing))

            console.log('drawing/recent-DATA: ', drawings)
            return Result.Ok(drawings)
          }),
          (failure => {
            let data = failure.data;
            if(data.message && data.message.includes("Network request failed")){
              const errorConnection: Error = {
                connectionError: strings.no_internet_connection_when_requesting_alert_dialog_title,
                connectionErrorMessage: strings.no_internet_connection_when_requesting_alert_dialog_message,
                statusCode: -1
              }
              return Result.Error(errorConnection)
            }else{
              let status = parseInt(failure.statusCode);
              console.log('error-create: ', failure)
              switch(status){
                case 403: 
                  if (data.message && data.message.includes("App is outdated. Please download latest app.")){
                    const error403SignOut: Error = { 
                      signoutUser: true,
                      statusCode: status
                      
                    }
                    return Result.Error(error403SignOut)
                  }else{
                    const errorDefault: Error = {
                      genericError: 'Something went wrong please try again',
                      statusCode: status
                    }
                    return Result.Error(errorDefault)
                  }
                case 404:
                  data = failure.data
                  const error404: Error = {
                      backendError: data.message ?  data.message : data.errorMessage,
                      statusCode: status
                  }
                  return Result.Error(error404)
                    
                default:
                  const errorDefault: Error = {
                      genericError: 'Something went wrong please try again',
                      statusCode: status
                  }
                  return Result.Error(errorDefault)
              }
            }

            
            
          }))
}

const toParseDrawing = (data: any): Drawing => {
  let multiplierValue = 1 < data.multiplier ? `${data.multiplier}x `: ``
  let drawingStartDate = new Date(`${data.start_date}`)
  let drawingEndDate = new Date(`${data.end_date}`)
  let entriesStartDate = DateString(drawingStartDate).format("MMMM D")
  let entriesEndDate = drawingStartDate.getMonth() === drawingEndDate.getMonth() ?
                                          DateString(drawingEndDate).format("D") : 
                                          DateString(drawingEndDate).format("MMMM D")

  const beacons: Beacon[] = data.beacons ? data.beacons.map((beacon: any) => (
       {  uuid: beacon.beacon_id,  major: beacon.major, minor: beacon.minor }
  )) : []

  const drawingDates: DrawingDate[] = data.dates ? data.dates.map((date: any) => ({
    id: date.id, 
    drawingId: date.drawing_id, 
    runDate: date.run_date,
    numWinners: date.num_winners,
    type: date.type,
    complete: date.complete,
  })) : []

  let multiplierDescription = ""
  switch (data.multiplier) {
    case 2:  multiplierDescription = "2x"
      break
    case 3:  multiplierDescription = "3x"
      break
    case 4:  multiplierDescription = "4x"
      break
    case 5:  multiplierDescription = "5x"
      break
    default: 
  }

  const multiplier: Multiplier = { 
    id: data.multiplierId,
    amount: data.multiplier,
    description: multiplierDescription,
    startDate: data.multiplierStart,
    endDate: data.multiplierEnd,
  }
  const rules: Rules = {
    value: data.rules,
  }
  const drawing: Drawing = {
    id: data.id,
    locationId: data.location_id,
    title: data.title,
    description: `${entriesStartDate} - ${entriesEndDate}`,
    weeklyPrize: data.weekly_prize,
    startDate: data.start_date,
    endDate: data.end_date,
    grandPrize: data.grand_prize,
    grandPrizeUrl: data.grand_prize_url,
    status: data.status,
    multiplier: multiplier,
    rules: rules,
    beacons: beacons,
    drawingDates: drawingDates,
  }


  return drawing
}

export default {
    getDrawingRules,
    getRecentDrawings,
}