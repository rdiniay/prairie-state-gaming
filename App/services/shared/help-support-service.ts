import Result from 'folktale/result'
import { Auth, } from '../../models'
import { post } from '../../api'
import { Error } from '../../models'
import { strings } from '../../localization/strings'

const sendDiagnosticLogs = (info: any) => {
  const options = {
    body: JSON.stringify(info)
  }
  return post(`support/sendDiagnosticLogs`, options,
          (success => {
            console.log(success)
            return Result.Ok(success.data)
          }),
          (failure => {
            console.log(failure)
            let data = failure.data;
            if(data.message && data.message.includes("Network request failed")){
              const errorConnection: Error = {
                connectionError: strings.no_internet_connection_when_requesting_alert_dialog_title,
                connectionErrorMessage: strings.no_internet_connection_when_requesting_alert_dialog_message,
                statusCode: -1
              }
              return Result.Error(errorConnection)
            }
            else{
              let status = parseInt(failure.statusCode);
              console.log('error-create: ', failure)
              switch(status){
                case 403:
                  if (data.message && data.message.includes("App is outdated. Please download latest app.")){
                    const error403SignOut: Error = {
                      signoutUser: true,
                      statusCode: status

                    }
                    return Result.Error(error403SignOut)
                  }else{
                    const errorDefault: Error = {
                      genericError: 'Something went wrong please try again',
                      statusCode: status
                    }
                    return Result.Error(errorDefault)
                  }
                case 404:
                  data = failure.data
                  const error404: Error = {
                      backendError: data.message ?  data.message : data.errorMessage,
                      statusCode: status
                  }
                  return Result.Error(error404)

                default:
                  const errorDefault: Error = {
                      genericError: 'Something went wrong please try again',
                      statusCode: status
                  }
                  return Result.Error(errorDefault)
              }
            }


          }))
}

export default {
  sendDiagnosticLogs,
}
