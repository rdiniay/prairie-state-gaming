import Result from 'folktale/result'
import { Auth, Profile, Settings } from '../../models'
import { get, put, _delete } from '../../api'
import { Error } from '../../models'
import { env } from '../../config'
import { strings } from '../../localization/strings'

const toParseProfile = (data: any): Profile => {
    const settings: Settings = { ...data.settings }
    const profile: Profile = {
        id: data.id,
        firstname: data.first_name,
        lastname: data.last_name,
        emailAddress: data.email_address,
        phoneNumber: data.phone_number,
        flags: data.flags,
        verified: data.verified,
        settings: settings,
    }
    return profile
}

const toObjectProfile = (profile: Profile): any => {
  const object: any = {
      id: profile.id,
      first_name: profile.firstname,
      last_name: profile.lastname,
      phone_number: profile.phoneNumber,
      flags: Number(profile.flags),
      verified: Number(profile.verified),
      settings: {
        emailPromos: Number(profile.settings.emailPromos),
        smsReminders: Number(profile.settings.smsReminders),
      },
  }
  return object
}

const getProfile = (auth: Auth) => {
    const options = {
      headers: {
        'Authorization': `Basic ${auth.authData}`,
      }
    }
    return get(`account/${auth.accountId}`, options,
            (success => {
                console.log('getProfile == ', success)
                const data = { ...success.data, }
                const profile = toParseProfile(data)
                return Result.Ok(profile)
            }),
            (failure => {
              let status = parseInt(failure.statusCode);
              switch(status) {
                case 403:
                  if (failure.message && failure.message.includes("App is outdated. Please download latest app.")){
                    const error403SignOut: Error = {
                      signoutUser: true,
                      statusCode: status

                    }
                    return Result.Error(error403SignOut)
                  }else{
                    const errorDefault: Error = {
                      genericError: 'Something went wrong please try again',
                      statusCode: status
                    }
                    return Result.Error(errorDefault)
                  }
                default:
                const errorDefault: Error = {
                  genericError: 'Something went wrong please try again',
                  statusCode: status
                }
                return Result.Error(errorDefault)
              }
            }
            ))
}

const updateProfile = (profile: Profile, auth: Auth) => {
  const options = {
    headers: {
      'Authorization': `Basic ${auth.authData}`,
    },
    body: JSON.stringify(toObjectProfile(profile))
  }
  return put(`account`, options,
          (success =>
            Result.Ok(profile)
          ),
          (failure => {
            let data = failure.data;
            if(data.message && data.message.includes("Network request failed")){
              const errorConnection: Error = {
                connectionError: strings.no_internet_connection_when_requesting_alert_dialog_title,
                connectionErrorMessage: strings.no_internet_connection_when_requesting_alert_dialog_message,
                statusCode: -1
              }
              return Result.Error(errorConnection)
            }else{
              let status = parseInt(failure.statusCode);
              console.log('failureUpdate', failure)
              switch(status) {
                case 403:
                  if (data.message && data.message.includes("App is outdated. Please download latest app.")){
                    const error403SignOut: Error = {
                      signoutUser: true,
                      statusCode: status

                    }
                    return Result.Error(error403SignOut)
                  }else{
                    const errorDefault: Error = {
                      genericError: 'Something went wrong please try again',
                      statusCode: status
                    }
                    return Result.Error(errorDefault)
                  }
                case 404:
                  data = failure.data
                  const error404: Error = {
                      backendError: data.message ?  data.message : data.errorMessage,
                      statusCode: status
                  }
                  return Result.Error(error404)

                case 409:
                  let data409 = failure.data
                  const error409: Error = {
                      backendError: data409.message ?  data409.message : data409.errorMessage,
                      statusCode: status
                  }
                  return Result.Error(error409)

                default:
                  const errorDefault: Error = {
                      genericError: 'Something went wrong please try again',
                      statusCode: status
                  }
                  return Result.Error(errorDefault)
              }
            }


          }))
}

const changePin = (currentPin: Number, newPin: Number, auth: Auth) => {
  const options = {
    headers: {
      'Authorization': `Basic ${auth.authData}`,
    },
    body: JSON.stringify({
        oldPin: currentPin,
        newPin: newPin
    })
  }
  return put(`account/${auth.accountId}/pin`, options,
          (success =>
            Result.Ok(success.data.newAuthData)
          ),
          (failure =>
            {
              let data = failure.data;
              if(data.message && data.message.includes("Network request failed")){
                const errorConnection: Error = {
                  connectionError: strings.no_internet_connection_when_requesting_alert_dialog_title,
                  connectionErrorMessage: strings.no_internet_connection_when_requesting_alert_dialog_message,
                  statusCode: -1
                }
                return Result.Error(errorConnection)
              }else{
                let status = parseInt(failure.statusCode);
                console.log('failureUpdate', failure)
                switch(status) {
                  case 403:
                    if (data.message && data.message.includes("App is outdated. Please download latest app.")){
                      const error403SignOut: Error = {
                        signoutUser: true,
                        statusCode: status

                      }
                      return Result.Error(error403SignOut)
                    }else{
                      const errorDefault: Error = {
                        genericError: 'Something went wrong please try again',
                        statusCode: status
                      }
                      return Result.Error(errorDefault)
                    }
                  case 404:
                    data = failure.data
                    const error404: Error = {
                        backendError: data.message ?  data.message : data.errorMessage,
                        statusCode: status
                    }
                    return Result.Error(error404)

                  case 409:
                    let data409 = failure.data
                    const error409: Error = {
                        backendError: data409.message ?  data409.message : data409.errorMessage,
                        statusCode: status
                    }
                    return Result.Error(error409)

                  default:
                    const errorDefault: Error = {
                        genericError: 'Something went wrong please try again',
                        statusCode: status
                    }
                    return Result.Error(errorDefault)
                }
              }
            }
          ))
}

const updateEmail = (newEmail: string, profile: Profile, auth: Auth) => {
  const options = {
    headers: {
      'Authorization': `Basic ${auth.authData}`,
    },
    body: JSON.stringify({
        email_address: newEmail,
        verified: profile.verified,
        id: profile.id,
    })
  }
  return put(`account`, options,
          (success =>
            Result.Ok(newEmail)
          ),
          (failure => {
            let data = failure.data;
            if(data.message && data.message.includes("Network request failed")){
              const errorConnection: Error = {
                connectionError: strings.no_internet_connection_when_requesting_alert_dialog_title,
                connectionErrorMessage: strings.no_internet_connection_when_requesting_alert_dialog_message,
                statusCode: -1
              }
              return Result.Error(errorConnection)
            }else{
              let status = parseInt(failure.statusCode);
              console.log('failureUpdate', failure)
              switch(status) {
                case 403:
                  if (data.message && data.message.includes("App is outdated. Please download latest app.")){
                    const error403SignOut: Error = {
                      signoutUser: true,
                      statusCode: status

                    }
                    return Result.Error(error403SignOut)
                  }else{
                    const errorDefault: Error = {
                      genericError: 'Something went wrong please try again',
                      statusCode: status
                    }
                    return Result.Error(errorDefault)
                  }
                case 404:
                  data = failure.data
                  const error404: Error = {
                      backendError: data.message ?  data.message : data.errorMessage,
                      statusCode: status
                  }
                  return Result.Error(error404)

                case 409:
                  let data409 = failure.data
                  const error409: Error = {
                      backendError: data409.message ?  data409.message : data409.errorMessage,
                      statusCode: status
                  }
                  return Result.Error(error409)

                default:
                  const errorDefault: Error = {
                      genericError: 'Something went wrong please try again',
                      statusCode: status
                  }
                  return Result.Error(errorDefault)
              }
            }

          }

          ))
}

const deactivateAccount = (profile: Profile, auth: Auth) => {
  const options = {
    headers: {
      'Authorization': `Basic ${auth.authData}`,
    },
  }
  return _delete(`deactivate/account/${auth.accountId}`, options,
          (success => {
              const settings: Settings = {
                ...profile.settings,
                deactivateAccount: true
              }
              const dataProfile: Profile = {
                ...profile,
                settings: settings
              }
              return Result.Ok(dataProfile)
          }),
          (failure => {
            let data = failure.data;
            if(data.message && data.message.includes("Network request failed")){
              const errorConnection: Error = {
                connectionError: strings.no_internet_connection_when_requesting_alert_dialog_title,
                connectionErrorMessage: strings.no_internet_connection_when_requesting_alert_dialog_message,
                statusCode: -1
              }
              return Result.Error(errorConnection)
            }else{
              let status = parseInt(failure.statusCode);
              console.log('failureUpdate', failure)
              switch(status) {
                case 403:
                  if (data.message && data.message.includes("App is outdated. Please download latest app.")){
                    const error403SignOut: Error = {
                      signoutUser: true,
                      statusCode: status

                    }
                    return Result.Error(error403SignOut)
                  }else{
                    const errorDefault: Error = {
                      genericError: 'Something went wrong please try again',
                      statusCode: status
                    }
                    return Result.Error(errorDefault)
                  }
                case 404:
                  data = failure.data
                  const error404: Error = {
                      backendError: data.message ?  data.message : data.errorMessage,
                      statusCode: status
                  }
                  return Result.Error(error404)

                case 409:
                  let data409 = failure.data
                  const error409: Error = {
                      backendError: data409.message ?  data409.message : data409.errorMessage,
                      statusCode: status
                  }
                  return Result.Error(error409)

                default:
                  const errorDefault: Error = {
                      genericError: 'Something went wrong please try again',
                      statusCode: status
                  }
                  return Result.Error(errorDefault)
              }
            }


          }))
}

export default {
    getProfile,
    updateProfile,
    updateEmail,
    changePin,
    deactivateAccount,
}
