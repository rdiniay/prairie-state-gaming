import Result from 'folktale/result'
import { Auth, Profile, Settings } from '../../models'
import { get, put, post } from '../../api'
import { Rules, Error } from '../../models'
import { strings } from '../../localization/strings'

const verifyEmail = (emailAddress: string, code: string) => {
  const options = {
    body: JSON.stringify({
      email_address: emailAddress,
      code: code
    })
  }
  return post(`pendingEmail/verification`, options,
          (success => {
            console.log(success)
            return Result.Ok(true)
          }),
          (failure => {
            let data = failure.data;
            if(data.message && data.message.includes("Network request failed")){
              const errorConnection: Error = {
                connectionError: strings.no_internet_connection_when_requesting_alert_dialog_title,
                connectionErrorMessage: strings.no_internet_connection_when_requesting_alert_dialog_message,
                statusCode: -1
              }
              return Result.Error(errorConnection)
            }else{
              let status = parseInt(failure.statusCode);
              console.log('error-create: ', failure)
              switch(status){
                case 403:
                  let data = failure.data;
                  if (data.message && data.message.includes("App is outdated. Please download latest app.")){
                    const error403SignOut: Error = {
                      signoutUser: true,
                      statusCode: status

                    }
                    return Result.Error(error403SignOut)
                  }else{
                      const errorDefault: Error = {
                        api: 'pendingEmail/verification',
                        genericError: 'Something went wrong please try again',
                        statusCode: status
                      }
                      return Result.Error(errorDefault)
                  }
                case 404:
                  data = failure.data
                  const error404: Error = {
                      api: 'pendingEmail/verification',
                      backendError: data.message ?  data.message : data.errorMessage,
                      statusCode: status
                  }
                  return Result.Error(error404)

                default:
                  const errorDefault: Error = {
                      api: 'pendingEmail/verification',
                      genericError: 'Something went wrong please try again',
                      statusCode: status
                  }
                  return Result.Error(errorDefault)
              }
            }


          }))
}

const sendEmailVerification = (emailAddress: string, auth: Auth) => {
  const options = {
    body: JSON.stringify({
        email_address: emailAddress,
        accountId: auth.accountId
    })
  }
  return post(`pendingEmail`, options,
          (success => {
            console.log(success)
            
            return Result.Ok(true)
          }),
          (failure => {
            let data = failure.data;
            if(data.message && data.message.includes("Network request failed")){
              const errorConnection: Error = {
                connectionError: strings.no_internet_connection_when_requesting_alert_dialog_title,
                connectionErrorMessage: strings.no_internet_connection_when_requesting_alert_dialog_message,
                statusCode: -1
              }
              return Result.Error(errorConnection)
            }else{
              let status = parseInt(failure.statusCode);
              console.log('error-create: ', failure)
              switch(status){
                case 403:
                  let data = failure.data;
                  if (data.message && data.message.includes("App is outdated. Please download latest app.")){
                    const error403SignOut: Error = {
                      signoutUser: true,
                      statusCode: status

                    }
                    return Result.Error(error403SignOut)
                  }else{
                    const errorDefault: Error = {
                      genericError: 'Something went wrong please try again',
                      statusCode: status
                    }
                    return Result.Error(errorDefault)
                  }
                case 404:
                  data = failure.data
                  const error404: Error = {
                      backendError: data.message ?  data.message : data.errorMessage,
                      statusCode: status
                  }
                  return Result.Error(error404)

                default:
                  const errorDefault: Error = {
                      genericError: 'Something went wrong please try again',
                      statusCode: status
                  }
                  return Result.Error(errorDefault)
              }
            }


          }))
}

export default {
    verifyEmail,
    sendEmailVerification,
}