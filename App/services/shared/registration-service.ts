import Result from 'folktale/result'
import { Auth, Profile, Settings } from '../../models'
import { get, put, post } from '../../api'
import { User, Error } from '../../models'

const createAccount = (user: User) => {
  const data = {
    body: JSON.stringify({
          id: null,
          first_name: user.firstname,
          last_name: user.lastname,
          phone_number: user.phoneNumber,
          email_address: user.email,
          reg_type: "mobile",
          pin_number: user.pin,
          push_token: user.pushToken,
          device_id: user.deviceToken,
          referralCode: user.referral_code,
          enableSMSSessionReminders: user.earning_reminders
    }),
  }
  return post(`account`, data,
          (success => {
            console.log(success)
            return Result.Ok(success.data)
          }),
          (failure => {
            let status = parseInt(failure.statusCode);
            console.log('error-create: ', failure)
            switch(status){
              case 403: 
                let data = failure.data;
                if (data.message && data.message.includes("App is outdated. Please download latest app.")){
                  const error403SignOut: Error = { 
                    signoutUser: true,
                    statusCode: status
                    
                  }
                  return Result.Error(error403SignOut)
                }else{
                  const errorDefault: Error = {
                    genericError: 'Something went wrong please try again',
                    statusCode: status
                  }
                  return Result.Error(errorDefault)
                }
              case 404:
                data = failure.data
                const error404: Error = {
                    backendError: data.message ?  data.message : data.errorMessage,
                    statusCode: status
                }
                return Result.Error(error404)

              case 409:
                let data409 = failure.data
                const error409: Error = {
                    backendError: data409.message ?  data409.message : data409.errorMessage,
                    statusCode: status
                }
                return Result.Error(error409)
                  
              default:
                const errorDefault: Error = {
                    genericError: 'Something went wrong please try again',
                    statusCode: status
                }
                return Result.Error(errorDefault)
            }
            
          }))
}

export default {
    createAccount,
}