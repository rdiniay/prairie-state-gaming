import Result from 'folktale/result'
import { Auth, Profile, Settings } from '../../models'
import { get, put, post } from '../../api'
import { Bank, Entry, Coordinate, Error } from '../../models'
import { strings } from '../../localization/strings'

const toObjectBank= (bank: Bank): any => ({
    periods: [
        ...bank.periods
    ],
    coords: {
        lat: bank.coordinate.latitude,
        lng: bank.coordinate.longitude,
        accuracy: bank.coordinate.accuracy,
        altitude: bank.coordinate.altitude,
        status: bank.coordinate?.status,
    },
    checkinId: bank.checkinId,
    continueSession: bank.continueSession,
})

const toParseEntry = (data: any): Entry => {
  if (data.results) {
      const entry: Entry = {
        locationId: data.location_id,
        drawingId: data.drawing_id,
        ...data.results
      }
      return entry
  }
  else {
      const entry: Entry = {
        locationId: data.location_id,
        drawingId: data.drawing_id,
        entriesBanked: data.session,
        earnedEntryCount: data.entryCount
      }
      return entry
  }
}

// "drawing_id": 2515,
//         "entryCount": 1257,
//         "session": 112,
//         "location_id": 0,
//         "results": {
//             "entriesBanked": 112,
//             "earnedEntryCount": 1257,
//             "isActiveCheckinInvalid": false,
//             "multiplier": {
//                 "amount": 1,
//                 "endDate": "2021-08-15T14:25:52.000Z"
//             }
//         }

const addBankEntries = (bank: Bank, entries: Entry[], auth: Auth) => {
  const options = {
    headers: {
      'Authorization': `Basic ${auth.authData}`,
    },
    body: JSON.stringify(toObjectBank(bank))
  }
  return post(`entry/${auth.accountId}/bank`, options,
          (success => {
            let dataEntries: Entry[] = success.data.map((dataEntry: any) => toParseEntry(dataEntry))
            let refresh: Boolean = (bank.entries && bank.entries.length != dataEntries.length)
            let drawingEntries: Entry[] = dataEntries.map((dataEntry: Entry) => {
              const stateWideDrawingEntry = entries.find((drawingEntry: Entry) => !drawingEntry.locationId && !dataEntry.locationId && dataEntry.locationId === 0)
              const localDrawingEntry = entries.find((drawingEntry: Entry) => drawingEntry.locationId === dataEntry.locationId)
              if (stateWideDrawingEntry) {
                if (!refresh)
                  refresh = (dataEntry && dataEntry.multiplier && dataEntry?.multiplier?.endDate !== stateWideDrawingEntry?.multiplier?.endDate)
                  
                  // CHECK IF THE STATEWIDE ENTRIES ARE INCREMENTING
                  // OTHERWISE USER IS OUTSIDE FROM THE BEACON PROXIMITY CONSIDERING IT HAS STRONG INTERNET CONNECTION
                  // THEN PROCEED TO FORCE CHECKOUT
                  const clientCheckinIsActiveOne: Boolean = (stateWideDrawingEntry.earnedEntryCount < dataEntry.earnedEntryCount)
                  return ({ ...stateWideDrawingEntry, ...dataEntry, clientCheckinIsActiveOne })
              }
              if (localDrawingEntry) {
                if (!refresh)
                  refresh = (dataEntry && dataEntry.multiplier && dataEntry.multiplier.endDate !== localDrawingEntry.multiplier.endDate)

                  return ({ ...localDrawingEntry, ...dataEntry})
              }
              return dataEntry
            })
            let bankEntries: Bank = { ...bank, entries: drawingEntries, refresh }
            console.log(`addBankEntries: `, success.data)
            return Result.Ok(bankEntries)
          }),
          (failure => {
            let data = failure.data;
            if(data.message && data.message.includes("Network request failed")){
              const errorConnection: Error = {
                connectionError: strings.no_internet_connection_when_requesting_alert_dialog_title,
                connectionErrorMessage: strings.no_internet_connection_when_requesting_alert_dialog_message,
                statusCode: -1
              }
              return Result.Error(errorConnection)
            }else{
              let status = parseInt(failure.statusCode);
              console.log('error-create: ', failure)
              switch(status){
                case 200:
                  // EXPIRE SESSION
                  if ((data?.errorMessage || data?.message)){
                    let bankEntries: Bank = { ...bank, entries, refresh: true }
                    return Result.Ok(bankEntries)
                  }
                case 410:
                  // EXPIRE STATE DRAWING
                  let bankEntries: Bank = { ...bank, entries, refresh: true }
                  return Result.Ok(bankEntries)
                case 403: 
                  if (data.message && data.message.includes("App is outdated. Please download latest app.")){
                    const error403SignOut: Error = { 
                      signoutUser: true,
                      statusCode: status
                      
                    }
                    return Result.Error(error403SignOut)
                  }else{
                    const errorDefault: Error = {
                      genericError: 'Something went wrong please try again',
                      statusCode: status
                    }
                    return Result.Error(errorDefault)
                  }
                case 404:
                  data = failure.data
                  const error404: Error = {
                      backendError: data.message ?  data.message : data.errorMessage,
                      statusCode: status
                  }
                  return Result.Error(error404)
                    
                default:
                  const errorDefault: Error = {
                      genericError: 'Something went wrong please try again',
                      statusCode: status
                  }
                  return Result.Error(errorDefault)
              }
            }

            
            
          }))
}

const getBankEntries = (bank: Bank, auth: Auth) => {
  const coordinate: Coordinate = bank.coordinate
  const options = {
    headers: {
      'Authorization': `Basic ${auth.authData}`,
    },
  }
  return get(`entry/${auth.accountId}`, options,
          (success => {
            let successData = success.data.allSessions ? success.data.allSessions : success.data
            let currentEntries: Entry[] = bank.entries
            let dataEntries: Entry[] = successData.map((dataEntry: any) => toParseEntry(dataEntry))
            let drawingEntries: Entry[] = dataEntries.map((dataEntry: Entry) => {
              const stateWideDrawingEntry = currentEntries.find((drawingEntry: Entry) => !drawingEntry.locationId && !dataEntry.locationId && dataEntry.locationId === 0)
              const localDrawingEntry = currentEntries.find((drawingEntry: Entry) => drawingEntry.locationId === dataEntry.locationId)
              if (stateWideDrawingEntry)
                return ({ ...stateWideDrawingEntry, ...dataEntry})
              
              if (localDrawingEntry)
                return ({ ...localDrawingEntry, ...dataEntry})

              return dataEntry
            })
            
            let bankEntries: Bank = { ...bank, entries: drawingEntries }
            console.log('getBankEntries: ', bankEntries)
            return Result.Ok(bankEntries)
          }),
          (failure => {
            let data = failure.data;
            if(data.message && data.message.includes("Network request failed")){
              const errorConnection: Error = {
                connectionError: strings.no_internet_connection_when_requesting_alert_dialog_title,
                connectionErrorMessage: strings.no_internet_connection_when_requesting_alert_dialog_message,
                statusCode: -1
              }
              return Result.Error(errorConnection)
            }else{
              let status = parseInt(failure.statusCode);
            console.log('error-create: ', failure)
              switch(status){
                case 403: 
                  if (data.message && data.message.includes("App is outdated. Please download latest app.")){
                    const error403SignOut: Error = { 
                      signoutUser: true,
                      statusCode: status
                      
                    }
                    return Result.Error(error403SignOut)
                  }else{
                    const errorDefault: Error = {
                      genericError: 'Something went wrong please try again',
                      statusCode: status
                    }
                    return Result.Error(errorDefault)
                  }
                case 404:
                  data = failure.data
                  const error404: Error = {
                      backendError: data.message ?  data.message : data.errorMessage,
                      statusCode: status
                  }
                  return Result.Error(error404)
                    
                default:
                  const errorDefault: Error = {
                      genericError: 'Something went wrong please try again',
                      statusCode: status
                  }
                  return Result.Error(errorDefault)
              }
            }
            
            
          }))
}

export default {
    addBankEntries,
    getBankEntries,
}