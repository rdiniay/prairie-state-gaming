import React, { useEffect, useState } from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { RootState, RootDispatch } from '../../redux'
import { AuthSelectors } from '../../redux/auth-redux'
import {StackActions ,useNavigation, DrawerActions } from '@react-navigation/native'
import ContactUs from './ContactUsPanel'

const ContactUsContainer = (props: Props) => {
    const navigation = useNavigation()

    const onCancel = () => {
        navigation.goBack();
    }
    return (
        <ContactUs   onCancel={onCancel}
                auth={props.auth}
                {...props} />
    )
}

const mapStateToProps = (state: RootState) => ({
    auth: AuthSelectors.auth(state),
})

const mapDispatchToProps = (dispatch: RootDispatch) => ({
})

const connector = connect(mapStateToProps, mapDispatchToProps)
type Props = ConnectedProps<typeof connector>

export default connector(ContactUsContainer)
