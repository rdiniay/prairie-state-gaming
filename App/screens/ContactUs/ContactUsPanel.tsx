import React, { useState } from 'react'
import { ConnectedProps } from 'react-redux'
import { 
  View,
  TextInput,
  TouchableOpacity,
  Image,
  FlatList,
  SectionList,
  Dimensions,
  Linking,
  Platform,
  PixelRatio,
} from 'react-native'
import { Container, SwipeablePanel, Header, Text } from '../../components'
import { Colors } from '../../styles'
import { Auth } from '../../models'
import {styles, commons, commonsAssets} from './Styles'
const { width, height } = Dimensions.get('window');

type Props = {
    onCancel: () => void,
    auth: Auth,
}

const ContactUs = (props: Props) =>{
    const [panelProps, setPanelProps] = useState({
        fullWidth: true,
        openLarge: false,
        showCloseButton: false,
        onClose: () => null,
        // ...or any prop you want
    });

    const header = () => (
        <Header style={{ backgroundColor: 'transparent', }}
        // title={'Contact Us'} //'Prairie State Gaming'
                // titleStyle={{ fontSize: 18, color: Colors.black, paddingRight: 10, }}
                left={(
                   <TouchableOpacity    onPress={props.onCancel}
                                         style={commons.headerTouchableOpacityStyle}>
                         <Text style={commons.headerTextStyle}>
                             Cancel
                         </Text>
                   </TouchableOpacity>
                )}
                leftContainerStyle={commons.leftContainerStyle}
        />
    )

    

    return (
        <Container  safeAreaViewTopStyle={styles.safeArea}
                    statusBarStyle={'light'}
                    // scrollViewEnable={true}
                    header={header}
                    style={styles.container} >

            

                <View style={commons.contentLogoViewStyle}>
                    <Image resizeMode={'contain'} style={commons.contentLogoStyle} source={commonsAssets.contentImageSource} />
                </View>

                <SwipeablePanel titleValue={'contact us'} style={commons.contentSwipePanelStyle} {...panelProps} isActive={true}>
                        <View style={commons.contentInnerViewStyle}>
                            <View style={commons.contentInnerContentViewStyle}> 
                                <Text style={commons.contentInlineContentViewInnerHeaderViewStyle}>
                                    By Phone
                                </Text>
                                <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', paddingVertical: 10, }} onPress={() => Linking.openURL(`tel:${props.auth.contact.phone_no}`)}>
                                    <View style={{flexDirection:'row'}}>
                                        <Image source={commonsAssets.contentImageSourcePhone} style={commonsAssets.contentImagePreferredStyle}/>
                                        <Text style={commons.contentInlineContentViewInnerHeaderTextViewStyle}>
                                            {props.auth.contact.phone_no}
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                                <Text style={commons.contentInlineContentViewInnerHeaderViewStyle}>
                                    By Email
                                </Text>
                                <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', paddingVertical: 10, }} onPress={() => Linking.openURL(`mailto:${props.auth.contact.email}`)} >
                                    <View style={{flexDirection:'row'}}>
                                        <Image source={commonsAssets.contentImageSourceEmail} style={commonsAssets.contentImagePreferredStyle}/>
                                        <Text style={commons.contentInlineContentViewInnerHeaderTextViewStyle}>
                                            {props.auth.contact.email}
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                    </View>
                </SwipeablePanel>
              
        </Container>
    )
}

export default ContactUs