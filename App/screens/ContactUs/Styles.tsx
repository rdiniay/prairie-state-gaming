import { PixelRatio, Dimensions } from 'react-native'
import ScaledSheet from 'react-native-scaled-sheet'
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import { Colors, Commons } from '../../styles'
const { width, height } = Dimensions.get('window');

const styles = ScaledSheet.create({
    safeArea: {
        backgroundColor: Colors.red,
    },
    container: {
        flex: 1,
    },
    imagePanelMarginTopWithPhoneNumber: { 
        ...Commons.ContentLogoViewStyle(),
        marginTop: (height*.120) + useSafeAreaInsets().top,
    },
    imagePanelMarginTopWithoutPhoneNumber: { 
        ...Commons.ContentLogoViewStyle(),
        marginTop: (height*.140) + useSafeAreaInsets().top,
    }
})

const commons =  ScaledSheet.create({
    headerTouchableOpacityStyle: Commons.HeaderTouchableOpacityStyle(),
    headerTextStyle: Commons.HeaderBackButtonTextStyle(),
    leftContainerStyle:  Commons.HeaderContentLeftContainerStyle(),
    contentLogoViewStyle: Commons.ContentLogoViewStyle(),
    contentLogoStyle: Commons.ContentLogoStyle(),
    
    contentSwipePanelStyle: Commons.ContentSwipePanelStyle(),
    contentTitleStyle: Commons.ContentTitleStyle(),
    contentInnerViewStyle: Commons.InnerViewStyle(),
    contentInnerContentViewStyle: {
        ...Commons.InnerContentViewStyle(),
        paddingBottom: 50 * PixelRatio.get(),
    },
    contentInlineContentViewInnerHeaderViewStyle: { 
        textAlign: 'center', 
        fontSize: 20 / PixelRatio.getFontScale(), 
        padding: 2, 
        paddingVertical: 10, 
        fontFamily: "Raleway-Regular"
    },
    contentInlineContentViewInnerHeaderTextViewStyle: { 
        textAlign: 'center', 
        fontSize: 13 / PixelRatio.getFontScale(), 
        color: '#CA0D03' , 
        fontFamily: "Raleway-Regular"
    },
    inlineContentTextViewStyle: Commons.InlineContentViewInnerContentTextViewStyle(),


})
const imageSizeW = 20
const imageSizeH = 20
const commonsAssets = {
    contentImageSource: Commons.ContentImageSource(),
    contentImageSourcePhone: require('../../assets/contact_phone.png'),
    contentImageSourceEmail: require('../../assets/contact_email.png'),
    contentImagePreferredStyle: {
        width: PixelRatio.getPixelSizeForLayoutSize(imageSizeW / PixelRatio.get()),
        height: PixelRatio.getPixelSizeForLayoutSize(imageSizeH / PixelRatio.get()),
        marginRight: 5,
    }
}

export {
    styles,
    commons,
    commonsAssets,
}