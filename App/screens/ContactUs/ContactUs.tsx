import React from 'react'
import { ConnectedProps } from 'react-redux'
import { 
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  FlatList,
  SectionList,
  Dimensions,
  Linking,
} from 'react-native'
import { Container, Header } from '../../components'
import { Colors } from '../../styles'
import { Auth } from '../../models'
import styles from './Styles'
const { width, height } = Dimensions.get('window');

type Props = {
    onCancel: () => void,
    auth: Auth,
}

const ContactUs = (props: Props) =>{
    const header = () => (
        <Header title={'Contact Us'} //'Prairie State Gaming'
                titleStyle={{ fontSize: 18, color: Colors.black, paddingRight: 10, }}
                left={(
                   <TouchableOpacity    onPress={props.onCancel}
                                         style={{ flexDirection: "row", alignItems: 'center', paddingLeft: 15, paddingVertical: 6, }}>
                         <Text style={{ color: Colors.red, textAlign: 'center', fontSize: 18,  }}>
                             Cancel
                         </Text>
                   </TouchableOpacity>
                )}
                leftContainerStyle={{ flex: 0.35, }}
        />
    )

    return (
        <Container  safeAreaViewTopStyle={styles.safeArea}
                    statusBarStyle={'dark'}
                    scrollViewEnable={true}
                    header={header}
                    style={styles.container} >

              <View style={{ flex: 1, height: height*.60, justifyContent: 'center', alignItems: 'center', }}>
                    <View style={{ }}>
                        <Text style={{ textAlign: 'center', fontSize: 25, padding: 2, paddingVertical: 10, }}>
                            By Phone
                        </Text>
                        <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', paddingVertical: 10, }} onPress={() => Linking.openURL(`tel:${props.auth.contact.phone_no}`)}>
                            <Text style={{ textAlign: 'center', fontSize: 16, color: Colors.red }}>
                                {props.auth.contact.phone_no}
                            </Text>
                        </TouchableOpacity>
                        <Text style={{ textAlign: 'center', fontSize: 25, padding: 2, paddingVertical: 10, }}>
                            By Email
                        </Text>
                        <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', paddingVertical: 10, }} onPress={() => Linking.openURL(`mailto:${props.auth.contact.email}`)} >
                            <Text style={{ textAlign: 'center', fontSize: 16, color: Colors.red }}>
                                 {props.auth.contact.email}
                            </Text>
                        </TouchableOpacity>
                    </View>
              </View>
              
        </Container>
    )
}

export default ContactUs