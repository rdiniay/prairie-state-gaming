import React from 'react'
import { ConnectedProps } from 'react-redux'
import {
    View,
    TextInput,
    TouchableOpacity,
    Image,
    ScrollView,
    FlatList,
    SectionList,
    Dimensions, Platform
} from 'react-native'
import { Container, Header, Text, Button, LottieView, LoadingDialog } from '../../components'
import { Colors } from '../../styles'
import {styles, commonsAssets} from './Styles'
import { images, lotties } from '../../constants'
import {strings} from "../../localization/strings";

type Props = {
    onCancel: () => void,
    onDeactivateAccount: () => void,

    isLoading: boolean,
}

const Deactivate = (props: Props) =>{

    const header = () => (
      <Header   style={styles.safeArea}
                left={(
                  <TouchableOpacity    onPress={props.onCancel}
                                        style={styles.onCloseButton}>
                        <Text style={styles.onCloseText}>
                            Back
                        </Text>
                  </TouchableOpacity>
               )}
                leftContainerStyle={styles.onboardingHeaderSpace}
      />
    )

    const OnboardingMap = () => (
      <View style={styles.onboardingContainer}>
              <Text style={styles.onboardingMessage} >
                    {strings.deactivate_account_description}
              </Text>

              <LottieView style={styles.onboardingLocation}
                                  source={lotties.deactivateAccountLocation}
                                  animate={true}
                                  speed={.8}
                                                />
            
        </View>
    )

    return (
        <Container      safeAreaViewTopStyle={styles.safeArea}
                        statusBarStyle={'light'}
                        // scrollViewEnable={true}
                        header={header}
                        style={styles.container} >

          <LoadingDialog visible={props.isLoading} subtitle={'Please wait...'} />

          <ScrollView>
            <OnboardingMap  />
          </ScrollView>

          <View style={styles.onboardingButtonContainer}>
                <View style={styles.onboardingStackButton}>
                    
                    <Button title={strings.deactivate_button_deactivate}
                            onPress={props.onDeactivateAccount}
                            titleStyle={styles.onboardingTitleButton}
                            style={styles.onboardingButton} />
                </View>
            </View>
        </Container>
    )
}

export default Deactivate
