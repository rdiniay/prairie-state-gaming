import { PixelRatio, Dimensions } from 'react-native'
import ScaledSheet from 'react-native-scaled-sheet'
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import { Colors, Commons } from '../../styles'
const { width, height } = Dimensions.get('window');

const styles = ScaledSheet.create({
    safeArea: {
        backgroundColor: Colors.white,
    },
    container: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    onCloseButton: {
        flexDirection: "row", 
        alignItems: 'center',
        paddingLeft: 15, 
        paddingVertical: 6,
    },
    onCloseText: {
        color: Colors.red, 
        textAlign: 'center', 
        fontSize: 18, 
    },
    onboardingContainer: {
        flex: 1, 
        alignItems: 'center',
        paddingTop: 15,
    },
    onboardingHeaderSpace: {
        flex: .35
    },
    onboardingMessage: {
        marginBottom: 25,
        paddingHorizontal: 30,
        paddingVertical: 5,
        color: Colors.red,
        textAlign: 'justify',
        fontSize: 18,
    },
    onboardingLocation: {
        width: height*.53,
        left: -5,
    },
    onboardingButtonContainer: {
        flex: 1,
        width: width,
        backgroundColor: Colors.white,
    },
    onboardingStackButton: {
        flex: 1, 
        // flexDirection: 'row', 
        // justifyContent: 'space-around', 
        // alignItems: 'flex-end', 
        paddingBottom: 15,
    },
    onboardingTitleButton: {
        fontWeight: 'bold',
        color: Colors.red,
    },
    onboardingButton: {
        height: 50, 
        backgroundColor: Colors.transparent,
    },
    onboardingCircleIndicator: { 
        width: 15,
        height: 15, 
        backgroundColor: Colors.red, 
        borderRadius: 7.5, 
        marginBottom: 18,
    }
})

const commonsAssets = {
    contentImageSource: Commons.ContentImageSource(),
}

export {
    styles,
    commonsAssets,
}