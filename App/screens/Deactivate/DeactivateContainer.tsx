import React, { useEffect, useState, useCallback } from 'react'
import {Alert, Platform} from 'react-native'
import { connect, ConnectedProps } from 'react-redux'
import { RootState, RootDispatch } from '../../redux'
import {StackActions ,useNavigation, DrawerActions, CommonActions, useFocusEffect } from '@react-navigation/native'
import { Screen as RootScreen } from '../../navigation/routes/rootRoutes'
import Deactivate from './Deactivate'
import {PERMISSIONS, request, openSettings, requestMultiple, RESULTS} from 'react-native-permissions'
import { env } from '../../config'
import crashlytics from '@react-native-firebase/crashlytics';
import { getBrand, getSystemVersion  } from 'react-native-device-info'
import ProfileActions, { ProfileSelectors } from '../../redux/profile-redux'
import MenuActions from '../../redux/menu-redux'
import {strings} from "../../localization/strings";

const DeactivateContainer = (props: Props) => {
    const navigation = useNavigation()

    const onCancel = () => {
        navigation.goBack();
    }

    useFocusEffect(
        useCallback ( ()=> {
            if (props.profile?.settings?.deactivateAccount) {
                reset()
            }
        }, [props.profile])
    )

    const onDeactivateAccount = () => {
        props.deactivateAccount()
    }

    const reset = () => { 
        props.signOut()
        setTimeout(() => {
            const resetAction = CommonActions.reset({
                index: 0,
                routes: [{ name: RootScreen.MainNavigators, params: {  } }]
            });
            navigation.dispatch(resetAction);

        }, 1000)
    }

    return (
        <Deactivate   onCancel={onCancel}
                        onDeactivateAccount={onDeactivateAccount}
                        {...props} />
    )
}

const mapStateToProps = (state: RootState) => ({
    isLoading: ProfileSelectors.isLoading(state),
    profile: ProfileSelectors.profile(state),
})

const mapDispatchToProps = (dispatch: RootDispatch) => ({
    deactivateAccount: () => dispatch(ProfileActions.deactivateAccount()),
    signOut: () => dispatch(MenuActions.signOut()),
})

const connector = connect(mapStateToProps, mapDispatchToProps)
interface Props extends ConnectedProps<typeof connector> {
    route: any
}

export default connector(DeactivateContainer)
