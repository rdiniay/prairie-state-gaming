import React, { useState } from 'react'
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    SectionList,
    TextInput,
    ActivityIndicator,
    Dimensions,
    Platform,
} from  'react-native'
import { Container, SwipeablePanel,Header } from '../../components'
import {styles, commons, commonsAssets} from './Styles'
import { Colors } from '../../styles'
import {strings} from "../../localization/strings";
import { TextField } from 'react-native-material-textfield-upgraded'
import { LoadingDialog } from '../../components'
import { Auth } from '../../models'
const { width, height } = Dimensions.get('window');

type Props = {
    onCancel: () => void,
    onSendPromoCode: () => void,
    setReferralCode: (value: string) => void,

    referralCode: string,
    isLoading: boolean,
}

const ReferralCode = (props: Props) =>{
    const [toggleKeyboard, setToggleKeyboard] = useState(false)

    const [isPanelActive, setIsPanelActive] = useState(true);

    const [panelProps, setPanelProps] = useState({
        fullWidth: true,
        openLarge: false,
        showCloseButton: false,
        onClose: () => null,
        // ...or any prop you want
    });

    const openPanel = () => {
        setIsPanelActive(true);
    };

    const closePanel = () => {
        setIsPanelActive(false);
    };

    const toggleUp = () => {
        setPanelProps({...panelProps, openLarge: true})
        closePanel()
        setTimeout(() => {
            openPanel()
        }, 1);
    }

      const header = () => (
        <Header style={{ backgroundColor: Colors.red, }}
                left={(
                   <TouchableOpacity    onPress={props.onCancel}
                                        style={commons.headerTouchableOpacityStyle}>
                         <Text style={commons.headerTextStyle}>
                             Cancel
                         </Text>
                   </TouchableOpacity>
                )}
                leftContainerStyle={{ flex: Platform.OS === 'ios' ? .25 : .35, }}
        />
    )
    return(
        <Container      safeAreaViewTopStyle={styles.safeArea}
                        statusBarStyle={'light'}
                        // scrollViewEnable={true}
                        header={header}
                        style={styles.container} >

                <LoadingDialog visible={props.isLoading} subtitle={'Please wait...'} />


                <View style={commons.contentLogoViewStyle}>
                    <Image resizeMode={'contain'} style={commons.contentLogoStyle} source={commonsAssets.contentImageSource} />
                </View>
                <SwipeablePanel titleValue={strings.promos_referrals_title} style={commons.contentSwipePanelStyle} {...panelProps} isActive={isPanelActive}>
                    <View style={commons.contentInnerViewStyle}>

                        <View style={commons.contentInnerContentViewStyle}> 
                            <View style={commons.contentInlineContentViewInnerHeaderViewStyle}>
                                <Text style={commons.customHeadingTextViewStyle}>
                                    {strings.promos_referrals_heading_title}
                                </Text>
                                <Text style={{ ...commons.customHeadingTextViewStyle, color: Colors.black}}>
                                    {strings.promos_referrals_content_title}
                                </Text>
                            </View>
                            <View style={{ padding: 10, }} >
                                <View style={{ flex:1,  marginHorizontal: 15, paddingBottom: 20, }}>
                                    <TextField style={commons.customContentTextInputStyle}
                                        label={strings.promos_referrals_code_label}
                                        value={props.referralCode}
                                        tintColor={Colors.red}
                                        onChangeText={props.setReferralCode}
                                        autoFocus={toggleKeyboard}
                                        onBlur={() => { setToggleKeyboard(false) }}
                                        onFocus={() => { 
                                            toggleUp()
                                            setToggleKeyboard(true)
                                        }}
                                    />
                                </View>
                                <View style={{ justifyContent: 'flex-end', }}>
                                    <View style={{ flexDirection: 'row', paddingVertical: 5, }}>
                                        <View style={{ flex: 1, margin: 10 }} />
                                        <View style={{ flex: 1, margin: 10 }}>
                                            <TouchableOpacity style={commons.customTouchableOpacity}
                                                                onPress={props.onSendPromoCode}>
                                                <Text style={commons.customButtonTextStyle}>
                                                    {strings.promos_referrals_button_text}
                                                </Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                                
                            </View>
                        </View>
                    </View>
                </SwipeablePanel>
                
        </Container>
    )
}

export default ReferralCode