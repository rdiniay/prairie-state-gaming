import { PixelRatio, Dimensions } from 'react-native'
import ScaledSheet from 'react-native-scaled-sheet'
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import { Colors, Commons } from '../../styles'
const { width, height } = Dimensions.get('window');

const styles = ScaledSheet.create({
    safeArea: {
        backgroundColor: Colors.red,
    },
    container: {
        flex: 1,
    },
    imagePanelMarginTopWithPhoneNumber: { 
        ...Commons.ContentLogoViewStyle(),
        marginTop: (height*.120) + useSafeAreaInsets().top,
    },
    imagePanelMarginTopWithoutPhoneNumber: { 
        ...Commons.ContentLogoViewStyle(),
        marginTop: height*.140,
    }
})


const commons =  ScaledSheet.create({
    headerTouchableOpacityStyle: Commons.HeaderTouchableOpacityStyle(),
    headerTextStyle: Commons.HeaderBackButtonTextStyle(),
    leftContainerStyle:  Commons.HeaderContentLeftContainerStyle(),
    contentLogoViewStyle: Commons.ContentLogoViewStyle(),
    contentLogoStyle: Commons.ContentLogoStyle(),
    
    contentSwipePanelStyle: Commons.ContentSwipePanelStyle(),
    contentTitleStyle: Commons.ContentTitleStyle(),
    contentInnerViewStyle: Commons.InnerViewStyle(),
    contentInnerContentViewStyle: Commons.InnerContentViewStyle(),
    contentInlineContentViewInnerHeaderViewStyle: {
        ...Commons.InlineContentViewInnerHeaderViewStyle(),
        justifyContent: 'center',
        alignItems: 'center',
    
    },
    contentInlineContentViewInnerHeaderTextViewStyle: Commons.InlineContentViewInnerHeaderTextViewStyle(),
    inlineContentViewStyle: {
        paddingTop: 10, 
        borderBottomColor: Colors.darkgray, 
        borderBottomWidth: 0.5, 
        paddingVertical: 5, 
        marginHorizontal: 8,
    },
    inlineContentTextViewStyle: Commons.InlineContentViewInnerContentTextViewStyle(),
    customHeadingTextViewStyle : { 
        padding: 10, 
        fontSize: 18 / PixelRatio.getFontScale(), 
        color: Colors.red , 
        fontFamily: "Raleway-Regular",
        fontWeight: 'normal',
        fontStyle: 'normal',
        lineHeight: 14.09 / PixelRatio.getFontScale(),
    },
    customContentTextInputStyle :  {
        fontFamily: "Raleway-Regular",
        fontWeight: 'normal',
        fontStyle: 'normal',
        fontSize: 12 / PixelRatio.getFontScale(),
        lineHeight: 14.09 / PixelRatio.getFontScale(),
    },
    customTouchableOpacity: { 
        padding: 10, 
        backgroundColor: Colors.red, 
        borderRadius: 20,  
        justifyContent: 'center', 
        alignItems: 'center',  
    },
    customButtonTextStyle : { 
        color: 'white', 
        fontSize: 16 / PixelRatio.getFontScale(), 
        fontFamily: 'Raleway-Bold',
        textAlign: 'center'
    }
})

const commonsAssets = {
    contentImageSource: Commons.ContentImageSource(),
}

export {
    styles,
    commons,
    commonsAssets,
}