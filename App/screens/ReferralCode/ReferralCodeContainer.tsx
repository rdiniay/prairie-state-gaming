import React, { useEffect, useState } from 'react'
import { connect, ConnectedProps, useStore, } from 'react-redux'
import { RootState, RootDispatch } from '../../redux'
import ReferralActions, { ReferralSelectors } from '../../redux/referral-redux'
import {StackActions ,useNavigation, DrawerActions, CommonActions } from '@react-navigation/native'
import ReferralCode from './ReferralCodePanel'
import Toast from 'react-native-toast-message';
import MenuActions, { MenuSelectors } from '../../redux/menu-redux'
import {Screen as DashboardScreen} from '../../navigation/routes/drawerRoutes'
import {Screen as RootScreen} from '../../navigation/routes/rootRoutes'
import { env } from '../../config'
import {strings} from "../../localization/strings";

const ReferralCodeContainer = (props: Props) =>  {
    const navigation = useNavigation()
    const [referralCode, setReferralCode] = useState('')

    const onCancel = () => {
        navigation.goBack();
    }

    useEffect(() => {
        if (props.error) {
            if (props.error.statusCode == 403 && props.error.signoutUser){
                reset();
                return;
            }

            let errorMessage = props.error.genericError
            let errorStatus =  props.error.statusCode ? `Error: ${props.error.statusCode}` : ``
            if (props.error.backendError) 
                errorMessage = props.error.backendError

            if (props.error.connectionError) 
                errorMessage = props.error.connectionError

            if (__DEV__){
                Toast.show({ type: 'custom', text1: `${errorMessage} ${errorStatus}`, })
            }else{
                Toast.show({ type: 'custom', text1: `${errorMessage}`, })
            }
        }
    }, [props.error])

    useEffect(() => {
        if (props.referral) {
            const award = props.referral.award
            if (0 < award) {
                navigation.navigate(DashboardScreen.Contests)
                Toast.show({ type: 'custom', text1: strings.promos_referrals_earned_entries(award), })
            }
            else
                Toast.show({ type: 'custom', text1: strings.promos_referrals_already_claimed, })
        }
    }, [props.referral])
    
    const reset = () => { 
        props.signOut()
        setTimeout(() => {
            const resetAction = CommonActions.reset({
                index: 0,
                routes: [{ name: RootScreen.MainNavigators, params: {  } }]
            });
            navigation.dispatch(resetAction);

        }, 1000)
    }


    const onSendPromoCode = () => {
        if (referralCode) 
            props.sendPromoCode(referralCode)
    }

    return (
        <ReferralCode   onCancel={onCancel}
                        onSendPromoCode={onSendPromoCode}
                        referralCode={referralCode}
                        setReferralCode={setReferralCode}
                        { ...props } />
    )
}

const mapStateToProps = (state: RootState) => ({
    error: ReferralSelectors.error(state),
    isLoading: ReferralSelectors.isLoading(state),
    referral: ReferralSelectors.referral(state),
})
const mapDispatchToProps = (dispatch: RootDispatch) => ({
    sendPromoCode: (code: string) => dispatch(ReferralActions.sendPromoCode(code)),
    signOut: () => dispatch(MenuActions.signOut()),
})

const connector = connect(mapStateToProps, mapDispatchToProps)
type Props = ConnectedProps<typeof connector>

export default connector(ReferralCodeContainer)

