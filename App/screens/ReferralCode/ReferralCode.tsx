import React from 'react'
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    SectionList,
    TextInput,
    ActivityIndicator,
    Dimensions,
    Platform,
} from  'react-native'
import { Container, Header } from '../../components'
import styles from './Styles'
import { Colors } from '../../styles'
import {strings} from "../../localization/strings";
import { TextField } from 'react-native-material-textfield-upgraded'
import { LoadingDialog } from '../../components'
import { Auth } from '../../models'
const { width, height } = Dimensions.get('window');

type Props = {
    onCancel: () => void,
    onSendPromoCode: () => void,
    setReferralCode: (value: string) => void,

    referralCode: string,
    isLoading: boolean,
}

const ReferralCode = (props: Props) =>{

    const header = () => (
        <Header style={{ backgroundColor: Colors.red, }}
                title={'Promos & Referrals'} //'Prairie State Gaming'
                titleStyle={{ fontSize: 18, color: Colors.white, paddingRight: 10, }}
                left={(
                    <TouchableOpacity    onPress={props.onCancel}
                                          style={{ flexDirection: "row", alignItems: 'center', paddingLeft: 15, paddingVertical: 6, }}>
                          <Text style={{ color: Colors.white, textAlign: 'center', fontSize: 18,  }}>
                              Cancel
                          </Text>
                    </TouchableOpacity>
                 )}
                leftContainerStyle={{ flex: 0.35, }}
        />
    )
    return(
        <Container      safeAreaViewTopStyle={styles.safeArea}
                        statusBarStyle={'light'}
                        scrollViewEnable={true}
                        header={header}
                        style={styles.container} >

                <LoadingDialog visible={props.isLoading} subtitle={'Please wait...'} />

                <View style={{ flex: 1, margin: 10, marginTop: 20, borderRadius: 15, paddingTop: 15, paddingBottom: 5, paddingHorizontal: 10, backgroundColor: 'white' }}>

                    {/* <View style={{ marginTop: 10, height: 80,  alignItems: 'center' , }}>
                    <Image style={{width:237,}} source={require('../../assets/logo_text.png')} />
                    </View> */}
                    <View style={{ flex: 1, paddingBottom: 10, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ padding: 10, fontSize: 18, color: Colors.red }}>
                            {`Have a referral code? `}
                        </Text>
                        <Text style={{ padding: 10, fontSize: 18, }}>
                            {`Enter it below!`}
                        </Text>
                    </View>
                    <View style={{ padding: 10, }} >
                        <View style={{ flex:1,  marginHorizontal: 15, paddingBottom: 20, }}>
                            <TextField
                                label='Code'
                                value={props.referralCode}
                                tintColor={Colors.red}
                                onChangeText={props.setReferralCode}
                            />
                        </View>
                        <View style={{ height: height/3, justifyContent: 'flex-end', }}>
                            <View style={{ flexDirection: 'row', paddingVertical: 5, }}>
                                <View style={{ flex: 1, margin: 10 }} />
                                <View style={{ flex: 1, margin: 10 }}>
                                    <TouchableOpacity style={{ padding: 10, backgroundColor: Colors.red, borderRadius: 20,  justifyContent: 'center', alignItems: 'center', }}
                                                        onPress={props.onSendPromoCode}>
                                        <Text style={{ color: 'white', fontSize: 16, textAlign: 'center' }}>
                                            {`Send`}
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                        
                    </View>
                </View>
                
        </Container>
    )
}

export default ReferralCode