import ScaledSheet from 'react-native-scaled-sheet'
import { Colors } from '../../styles'

const styles = ScaledSheet.create({
    safeArea: {
        backgroundColor: Colors.red,
    },
    container: {
        flex: 1,
        backgroundColor: Colors.white,
        justifyContent: 'space-around',
    },
})

export default styles