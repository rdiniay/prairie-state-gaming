import React, { useEffect, useState } from 'react'
import { Platform, Alert } from 'react-native'
import { connect, ConnectedProps, } from 'react-redux'
import { RootState, RootDispatch } from '../../redux'
import {StackActions ,useNavigation, DrawerActions, CommonActions } from '@react-navigation/native'
import { Screen as MainScreen } from '../../navigation/routes/mainRoutes'
import { Screen } from '../../navigation/routes/rootRoutes'
import Menu from './Menu'
import MenuActions, { MenuSelectors } from '../../redux/menu-redux'
import { strings } from '../../localization/strings'
import Utils from '../../services/shared/utils/Utils'
import notifee from '@notifee/react-native'
import { getBrand } from 'react-native-device-info'

const MenuContainer = (props: Props) =>  {
    const navigation = useNavigation()
    const [isLoading, setIsLoading] = useState(false)

    const onNavigateMenuItem = (item: string) => {
        if(Platform.OS !== 'ios')
            navigation.dispatch(DrawerActions.toggleDrawer());
            
        setTimeout(() => {
            if (item === 'Profile')
                navigation.dispatch(StackActions.push(Screen.ProfileNavigators));
            else if (item === 'Locations')
                navigation.dispatch(StackActions.push(Screen.LocationNavigators));
            else if (item === 'About')
                navigation.dispatch(StackActions.push(Screen.AboutNavigators));
            else if (item === 'Contact Us')
                navigation.dispatch(StackActions.push(Screen.ContactUsNavigators));
            else if (item === 'Drawing Rules')
                navigation.dispatch(StackActions.push(Screen.DrawingRulesNavigators));
            else if (item === 'Tips & Info')
                navigation.dispatch(StackActions.push(Screen.TipsNavigators));
            else if (item === 'Promos & Referrals')
                navigation.dispatch(StackActions.push(Screen.ReferralCodeNavigators));
            else if (item === 'Settings')
                navigation.dispatch(StackActions.push(Screen.PreparationsNavigators, { settings: true }));
            else if (item === 'AutoStart' ){
                notifee.getPowerManagerInfo()
                    .then( powerManagerInfo => {
                        Utils.getAutoStartPermission()
                        .then(didUserDisabledBefore => {
                          if(powerManagerInfo.activity && didUserDisabledBefore){
                                Alert.alert(
                                    `${strings.permission_auto_start_android_title}`,
                                    `${strings.permission_auto_start_android_message}`,
                                    [
                                        {
                                            text: `${strings.commons_btn_ok}`,
                                            onPress: async () => {
                                                await notifee.openPowerManagerSettings()
                                                Utils.setAutoStartPermission(false)
                                            }
                                        },
                                        {
                                            text: `${strings.commons_btn_cancel}`,
                                            onPress: () => {
                                                console.log("Cancel Pressed")
                                            },
                                            style: 'cancel'
                                        }
                    
                                    // set auto start permission has been dimissed/ cancelled by the user
                                    ], { cancelable: false},
                                );
                            }
                        })
                    })
                
            }
            else if (item === 'Battery Restriction' ){
                notifee.isBatteryOptimizationEnabled()
                    .then(batteryOptimizationEnabled => {
                        if(batteryOptimizationEnabled){
                            var message = strings.permission_background_restriction_message
                            const brand = getBrand().toLowerCase()
                            console.log("Brand ", brand)
                            if (brand.includes("samsung")){
                                message = `${strings.permission_background_restriction_message} ${strings.permission_android_restriction_samsung}`
                            }
                            else if (brand.includes("huawei")){
                                message = `${strings.permission_background_restriction_message} ${strings.permission_android_restriction_huawei_common}`
                            }
                            else if (brand.includes("sony")){
                                message = `${strings.permission_background_restriction_message} ${strings.permission_android_restriction_sony}`
                            }
                            else if (brand.includes("xiaomi") || brand.includes("redmi")){
                                message = `${strings.permission_background_restriction_message} ${strings.permission_android_restriction_xiaomi}`
                            }
                            else if(brand.includes("oppo")){
                                message = `${strings.permission_background_restriction_message} ${strings.permission_android_restriction_oppo}`
                            }
                            else if (brand.includes("asus")){
                                message = `${strings.permission_background_restriction_message} ${strings.permission_android_restriction_asus}`
                            }

                            Alert.alert(
                                strings.permission_background_restriction_title,
                                message ,
                                [
                                  // 3. launch intent to navigate the user to the appropriate screen
                                  {
                                    text: strings.commons_btn_ok,
                                    onPress: async () => await notifee.openBatteryOptimizationSettings(),
                                  },
                                  {
                                    text: strings.commons_btn_cancel,
                                    onPress: () => [console.log("MenuContainer - Check Background Restriction - Cancel Pressed")],
                                    style: "cancel"
                                  },
                                ],
                                { cancelable: false }
                              )
                 
                            
                        }
                    })
            }
            else if (item === 'Sign Out')
                onSignOut()

        }, 500)
    }

    const onSignOut = () => {
        setIsLoading(true)
        props.signOut()
        setTimeout(() => {
            setIsLoading(false)
            const resetAction = CommonActions.reset({
                index: 0,
                routes: [{ name: Screen.MainNavigators, params: {  } }]
            });
            navigation.dispatch(resetAction);

        }, 1000)
    }

    return (
        <Menu   onNavigateMenuItem={onNavigateMenuItem}
                isLoading={isLoading}
                { ...props } />
    )
}

const mapStateToProps = (state: RootState) => ({

})
const mapDispatchToProps = (dispatch: RootDispatch) => ({
    signOut: () => dispatch(MenuActions.signOut())
})

const connector = connect(mapStateToProps, mapDispatchToProps)
type Props = ConnectedProps<typeof connector>

export default connector(MenuContainer)

