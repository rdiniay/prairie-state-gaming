import React from 'react'
import { ConnectedProps } from 'react-redux'
import { 
  View,
  TextInput,
  TouchableOpacity,
  Image,
  FlatList,
  SectionList,
  Platform,
} from 'react-native'
import { Container, Header, Text, LoadingDialog } from '../../components'
import { Colors } from '../../styles'
import styles from './Styles'
import notifee from '@notifee/react-native'
import Utils from '../../services/shared/utils/Utils'

type Props = {
  onNavigateMenuItem: (item: string) => void,

  isLoading: boolean,
}

const Menu = (props: Props) => {

    const header = () => (
        <Header style={{ backgroundColor: Colors.red,  }}
                title={'Menu'}
                titleStyle={{ fontSize: 18, color: Colors.white, padding: 4 }}
                
        />
    )
    
    const paddingHorizontal = Platform.OS === 'ios' ? 20 : 15
    const otherMenu = Platform.OS === 'ios' ? [] : [
      "Profile",
      "Locations"
    ]
    const arraMenu = [
      ...otherMenu,
      "Drawing Rules",
      "Contact Us",
      "About",
      "Tips & Info",
      "Promos & Referrals",
    ]
    const signOut =  'Sign Out'

    if(Platform.OS === 'android'){
      notifee.getPowerManagerInfo()
        .then( powerManagerInfo => {
          Utils.getAutoStartPermission()
            .then(didUserDisabledBefore => {
              if(powerManagerInfo.activity && didUserDisabledBefore){
                arraMenu.push("AutoStart")
              }
            })
         
        })
      
      notifee.isBatteryOptimizationEnabled()
      .then(batteryOptimizationEnabled => {
        if(batteryOptimizationEnabled){
          arraMenu.push('Battery Restriction')
        }
      })
      
    }

    setTimeout(() => {
      arraMenu.push('Settings')
    }, 50);
    
    return (
      <Container    safeAreaViewTopStyle={styles.safeArea}
                    statusBarStyle={'light'}
                    // scrollViewEnable={true}
                    header={header}
                    style={styles.container} >

              <LoadingDialog visible={props.isLoading} subtitle={'Please wait...'} />

              <FlatList 
                        // data={['Health & Beauty', 'TV & Home Appliances', 'Electronics Accessories', 'Fashion Accessories', 'Babies & Toys']}
                        data={arraMenu}
                        // numColumns={5}
                        ListFooterComponent={(
                          <TouchableOpacity onPress={Utils.multipleTapHandler(() => props.onNavigateMenuItem(signOut))} style={{ flex: 1, borderBottomColor: 'lightgray', borderBottomWidth: 0.5}}>
                              <Text numberOfLines={2} style={{ fontSize: 15, paddingHorizontal, paddingVertical: 18, }}> {signOut} </Text> 
                          </TouchableOpacity>
                        )}
                        renderItem={({ item }) => (
                          <TouchableOpacity onPress={Utils.multipleTapHandler(() => props.onNavigateMenuItem(item))} style={{ flex: 1, borderBottomColor: 'lightgray', borderBottomWidth: 0.5}}>
                              <Text numberOfLines={2} style={{ fontSize: 15, paddingHorizontal, paddingVertical: 18, }}> {item} </Text> 
                          </TouchableOpacity>
                        )}
                        keyExtractor={(item, index) => `flatlist-categories-${index}`}
                        />
              
        </Container>
    )
}

export default Menu