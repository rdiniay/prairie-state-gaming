import React, { useEffect, useState } from 'react'
import { 
  View,
  TouchableOpacity,
  Dimensions,
  Platform,
  ScrollView,
} from 'react-native'
import { Container, Text, Button, Header, PromotionItem, PromotionArtwork } from '../../components'
import { Colors } from '../../styles'
import styles from './Styles'
import { Mobile, Promotion, Drawing, Location, } from '../../models'
const { width, height } = Dimensions.get('window')
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { strings } from '../../localization/strings';            

type Props = {
  onCancel: () => void,
  onDrawingRules: (drawing: Drawing) => void,
  onDirection: (location: Location) => void,
  setPinch: (value: boolean) => void,

  mobile: Mobile,
  pinch: boolean,
  earningTickPercentage: number,
  promotionDetails: Promotion,
  monthlyPromotion: Promotion,
}

const Dashboard = (props: Props) => {
    const insets = useSafeAreaInsets();
    const [footerHeight, setFooterHeight] = useState(0)
    const promotionArtwork: Promotion = props.promotionDetails?.drawing ? props.promotionDetails : props.monthlyPromotion

    const header = () => (
        <Header style={{ backgroundColor: Colors.red, }}
                title={strings.drawing_title} //'Prairie State Gaming'   
                titleStyle={styles.headerTitle}
                left={(
                   <TouchableOpacity    onPress={props.onCancel}
                                         style={styles.backButtonContent}>
                         <Text style={styles.backButtonText}>
                             Back
                         </Text>
                   </TouchableOpacity>
                )}
                leftContainerStyle={{ flex: 0.35, }}
        />
    )

    return (
      <Container    safeAreaViewTopStyle={styles.safeArea}
                    statusBarStyle={"light"}
                    // scrollViewEnable={true}
                    header={header}
                    style={styles.container} >

            {props.promotionDetails && 
                <ScrollView onScroll={(evt) => { setFooterHeight(evt.nativeEvent.contentSize.height) }} >
                    <View style={styles.scrollContent} >
                        <PromotionItem theme={"dark"} 
                                            promotion={props.promotionDetails} />

                        {(props.promotionDetails?.drawing === null) &&
                            <View style={styles.descriptionContent}>
                                <Text style={styles.descriptionText}>
                                    {props.mobile ? strings.drawing_location_checked_in(promotionArtwork?.entry?.entriesBanked, promotionArtwork.location?.name) 
                                                    : strings.drawing_location_checked_out}
                                </Text>
                            </View>
                        }
                        {props.promotionDetails?.drawing &&
                            <View style={styles.descriptionContent}>
                                <Text style={styles.descriptionText}> 
                                    {strings.drawing_earned_today(props.promotionDetails?.entry?.entriesBanked)}  
                                </Text> 
                                
                                <Button style={[styles.descriptionButton, { marginBottom: props.promotionDetails?.location ? 15 : 30,  }]} 
                                    title={strings.drawing_rules}
                                    onPress={() => { props.onDrawingRules(props.promotionDetails.drawing) }}/>
                            
                            </View>
                        }
                        {props.promotionDetails?.location && 
                            <Button style={styles.descriptionButton} 
                                title={strings.drawing_directions}
                                onPress={() => { props.onDirection(props.promotionDetails?.location) }}/>
                        }

                        <PromotionArtwork promotion={promotionArtwork} />

                    </View>

                    {promotionArtwork &&
                        <View style={[styles.footerContent, { top: footerHeight, height: 0 < footerHeight ? height : 0, }]} />
                    }
                </ScrollView>
            }
              
        </Container>
    )
}

export default Dashboard