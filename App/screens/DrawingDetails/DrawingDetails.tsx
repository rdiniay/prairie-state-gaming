import React, { useState } from 'react'
import { ConnectedProps } from 'react-redux'
import { 
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  FlatList,
  SectionList,
  ActivityIndicator,
  Dimensions,
} from 'react-native'
import { Container, Header } from '../../components'
import { Colors } from '../../styles'
import styles from './Styles'
import { ScrollView } from 'react-native-gesture-handler'
import { WebView } from 'react-native-webview';
const { width, height } = Dimensions.get('window');
import { Rules } from '../../models'
import { images } from '../../constants'
import Lightbox from 'react-native-lightbox-v2';
import ImageZoom, { IOnMove } from 'react-native-image-pan-zoom'
import { Drawing } from '../../models'

type Props = {
    onCancel: () => void,
    setPinch: (value: boolean) => void,

    pinch: boolean,
    isLoading: boolean,
    drawingDetails: Drawing,
}

const DrawingDetails = (props: Props) =>{
    let closeFunction: () => {}
    const [backgroundColor, setBackgroundColor] = useState(`rgba(0, 0, 0, ${1.0})`)

    const header = () => (
        <Header style={{ backgroundColor: Colors.red, }}
                title={'Drawing Details'} //'Prairie State Gaming'   
                titleStyle={{ fontSize: 18, color: Colors.white, paddingRight: 10, }}
                left={(
                   <TouchableOpacity    onPress={props.onCancel}
                                         style={{ flexDirection: "row", alignItems: 'center', paddingLeft: 15, paddingVertical: 6, }}>
                         <Text style={{ color: Colors.white, textAlign: 'center', fontSize: 18,  }}>
                             Back
                         </Text>
                   </TouchableOpacity>
                )}
                leftContainerStyle={{ flex: 0.35, }}
        />
    )

    const array = [{
        title: `${props.drawingDetails ? props.drawingDetails.description : ''}`,
        subtile: `${props.drawingDetails ? props.drawingDetails.weeklyPrize : ''}`
    },
    {
        title: "Grand Prize",
        subtile: `${props.drawingDetails ? props.drawingDetails.grandPrize : ''}`
    }]

    return (
        <Container      safeAreaViewTopStyle={styles.safeArea}
                        statusBarStyle={'light'}
                        // scrollViewEnable={true}
                        header={header}
                        style={styles.container} >
            
            {props.isLoading && 
                <View style={{ flex:1, justifyContent: 'center', }}>
                    <ActivityIndicator size="large" color={'gray'}/>
                </View>
            }
            {!props.isLoading && 
                <View style={{ flex: 1, }}>
                    <View  style={{ height: '45%', width: width, backgroundColor: '#EBECF190', overflow: "hidden", alignItems: 'center', justifyContent: 'center', }}>
                        <Lightbox       underlayColor="white"
                                        onOpen={() => { props.setPinch(true) }}
                                        willClose={() => { props.setPinch(false) }}
                                        swipeToDismiss={false}
                                        backgroundColor={backgroundColor}
                                        renderHeader={close => {
                                            closeFunction = () => {
                                                close()
                                                setTimeout(() => {
                                                    setBackgroundColor(`rgba(0, 0, 0, ${1.0})`)
                                                }, 500)
                                                return close
                                            }
                                            return (
                                                <TouchableOpacity onPress={close} style={{ position: 'absolute', height: 100, width: 80, justifyContent: 'center', alignItems: 'center', alignSelf: 'flex-end', }}>
                                                    <View style={{ backgroundColor: '#00000080',  justifyContent: 'center', height: 50, width: 50, borderRadius: 25, alignItems: 'center', }}>
                                                        <View style={{ backgroundColor: '#FFFFFF20',  justifyContent: 'center', height: 50, width: 50, borderRadius: 25, alignItems: 'center', }}>
                                                            <Text style={{ fontSize: 30, color: 'white', textAlign: 'center', paddingBottom: 3, }}>
                                                                x
                                                            </Text>
                                                        </View>
                                                    </View>
                                                </TouchableOpacity>
                                            )
                                        }}
                                        >

                            <Image resizeMode="stretch" style={{ top: props.pinch ? 0 : height*.28, height: props.pinch ? 0 : height, width: props.pinch ? 0 : width,  }} 
                                                        source={{ uri: `${props.drawingDetails ? props.drawingDetails.grandPrizeUrl : ''}` }} />
                                        
                            {props.pinch &&
                                <ImageZoom cropWidth={Dimensions.get('window').width}
                                            cropHeight={Dimensions.get('window').height}
                                            imageWidth={width}
                                            imageHeight={height}
                                            enableSwipeDown={true}
                                            onMove={(position: IOnMove) => {
                                                const yPosition = 1 - (position.positionY / 1000)
                                                setBackgroundColor(`rgba(0, 0, 0, ${yPosition})`)
                                            }}
                                            onSwipeDown={() => { closeFunction() }}>
                                    <Image resizeMode="stretch" style={{ top: props.pinch ? 0 : height*.28, height: height, width: width,  }} source={{ uri: `${props.drawingDetails.grandPrizeUrl}` }}/>
                                </ImageZoom>
                            }
                        </Lightbox>
                    </View>
                    <FlatList 
                                // data={['Health & Beauty', 'TV & Home Appliances', 'Electronics Accessories', 'Fashion Accessories', 'Babies & Toys']}
                                data={array}
                                ListHeaderComponent={(
                                    <View style={{ flex: 1, paddingHorizontal: 15, paddingTop: 15  }}>
                                        <Text numberOfLines={2} style={{ fontSize: 22, paddingBottom: 0, }}>{props.drawingDetails ? props.drawingDetails.title : ''}</Text> 
                                    </View>
                                )}
                                renderItem={({ item }) => (
                                    <View style={{ flex: 1, borderBottomColor: 'lightgray', borderBottomWidth: 0.5, paddingHorizontal: 15, paddingVertical: 15 }}>
                                        <Text numberOfLines={2} style={{ fontSize: 15, }}>{item.title}</Text> 
                                        <Text numberOfLines={2} style={{ fontSize: 15, }}>{item.subtile}</Text> 
                                    </View>
                                )}
                                keyExtractor={(item, index) => `flatlist-categories-${index}`}
                                />
            </View>
        }
        </Container>
    )
}

export default DrawingDetails