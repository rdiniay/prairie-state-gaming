import ScaledSheet from 'react-native-scaled-sheet'
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import { Dimensions, PixelRatio, Platform } from 'react-native'
import { Colors, Commons } from '../../styles'
const { width, height } = Dimensions.get('window');

const styles = ScaledSheet.create({
    safeArea: {
        backgroundColor: Colors.red,
    },
    headerTitle: {
        fontSize: 18, 
        color: Colors.red, 
        paddingRight: 10,
    },
    backButtonContent: {
        flexDirection: "row", 
        alignItems: 'center', 
        paddingLeft: 15, 
        paddingVertical: 6,
    },
    backButtonText: {
        color: Colors.white, 
        textAlign: 'center', 
        fontSize: 18, 
    },
    container: {
        flex: 1,
        backgroundColor: Colors.red,
    },
    scrollContent: {
        flex: 1, 
        alignItems: 'center' ,
    },
    backgroundPanelContent: {
        flex: 1, 
        backgroundColor: Colors.red, 
        alignItems: 'center',
    },
    descriptionContent: {
        paddingHorizontal: 12, 
        alignItems: 'center',  
    },
    descriptionText: {
        textAlign: 'justify',
        color: Colors.white, 
        fontSize: 16,  
        paddingHorizontal: 15, 
        paddingBottom: 20, 
    },
    descriptionButton: {
        width: width * .8,
        marginBottom: 30,
    },
    footerContent: {
        position: 'absolute', 
        zIndex: 999, 
        backgroundColor: Colors.white , 
        alignSelf: 'center',  
        width: '100%',
    }
})

export default styles