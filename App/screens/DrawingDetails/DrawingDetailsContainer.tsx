import React, {useEffect, useRef, useState} from 'react'
import { Linking, Platform } from 'react-native'
import { connect, ConnectedProps } from 'react-redux'
import { RootState, RootDispatch } from '../../redux'
import {StackActions ,useNavigation, CommonActions } from '@react-navigation/native'
import MenuActions from '../../redux/menu-redux'
import DrawingDetails from './DrawingDetailsPanel'
import DrawingActions, { DrawingSelectors } from '../../redux/drawing-redux'
import { MobileSelectors } from '../../redux/mobile-redux'
import { BankSelectors } from '../../redux/bank-redux'
import { DashboardSelectors } from '../../redux/dashboard-redux'
import { LocationSelectors } from '../../redux/location-redux'
import {Screen as RootScreen} from '../../navigation/routes/rootRoutes'
import {
    Promotion,
    Drawing,
    Entry,
    Location,
    LocationCheckin
} from '../../models'

const DrawingDetailsContainer = (props: Props) => {
    //https://stackoverflow.com/questions/56450975/to-fix-cancel-all-subscriptions-and-asynchronous-tasks-in-a-useeffect-cleanup-f
    const mountedRef = useRef(true)
    const navigation = useNavigation()
    const [pinch, setPinch] = useState(false)

    const onCancel = () => {
        navigation.goBack();
    }

    useEffect(() => {
        return () => {
            props.getDrawingDetails(null)
            mountedRef.current = false
        }
    }, [])

    useEffect( () => {
        console.log('Container: ', props.error);
        if (props.error) {
            if (props.error.statusCode == 403 && props.error.signoutUser){
                reset();
            }
        }
        return () => {
            mountedRef.current = false
        }
    }, [props.error])

    const getMonthlyPromotion = (): Promotion => {
        const drawing: Drawing = props.recentDrawings ? props.recentDrawings.find((drawing) => !drawing.locationId) : null
        const entries: Entry[] = (props.bank && props.bank.entries) ? props.bank.entries : []
        const promotion: Promotion = ({ id: null, })
        if (drawing && 0 < entries.length) {
            const location = props.locations.find(location => props.mobile && props.mobile.locationId === location.id)
            const entry = entries.find((entry: Entry) => (entry.locationId === drawing.locationId) ||
                                                                (!entry.locationId && !drawing.locationId && entry.locationId === 0))
            return ({ ...promotion, location, drawing, entry, earningTickPercentage: props.earningTickPercentage })
        }
        return promotion
    }

    const getPromotionDetails = (): Promotion => {
        const drawing: Drawing = props.drawingDetails ? props.drawingDetails : null
        const entries: Entry[] = (props.bank && props.bank.entries) ? props.bank.entries : []
        const promotion: Promotion = ({ id: null, })
        if (drawing && 0 < entries.length) {
            const drawingLocation = props.locations.find(location => location.id === drawing.locationId)
            const drawingEntry = entries.find((entry: Entry) => (entry.drawingId === drawing.id))
            const entry = drawingEntry ? { ...drawingEntry, multiplier: drawing.multiplier } : null

            const isCheckedinLocalPromotion = (props.mobile && props.mobile.locationId === drawing.locationId)
            return ({ ...promotion,
                        drawing: drawing.id ? drawing : null,
                        entry,
                        location: drawingLocation,
                        earningTickPercentage: getAbandonedLocationCheckin(isCheckedinLocalPromotion, drawingLocation?.id) })
        }
        return promotion
    }

    const getAbandonedLocationCheckin = (isCheckedinLocalPromotion: Boolean, locationId: Number): Number => {
        const locationCheckin: LocationCheckin = props.locationCheckins ? props.locationCheckins.find((locationCheckin: LocationCheckin) => locationCheckin.id === locationId) : null
        const isCheckedinMonthlyPromotion = props.mobile && !locationId
        if (isCheckedinMonthlyPromotion)
            return props.earningTickPercentage
        else if (isCheckedinLocalPromotion)
            return props.earningTickPercentage

        if (locationCheckin && locationCheckin.lastCheckOutPeriodTime) {
            let lastCheckOutPeriodTime = Number(locationCheckin.lastCheckOutPeriodTime)
            let lastCheckInPeriodTime = Number(locationCheckin.lastCheckInPeriodTime)
            const duration = 120.88
            const elapse = ((lastCheckOutPeriodTime - lastCheckInPeriodTime) / 1000)
            const countedEntries = `${(elapse/duration)}`.includes('.99') ? Math.round(elapse/duration) : Math.floor(elapse/duration)
            const onGoingSeconds = ( (elapse/duration) - (countedEntries) ) * 1000
            const tickPercentage = (onGoingSeconds * 100) / 1000
            return tickPercentage
        }
        return 0
    }

    const onDrawingRules = (drawing: Drawing) => {
        props.getDrawingRulesSuccess(drawing)
        setTimeout(() => {
            navigation.dispatch(StackActions.push(RootScreen.DrawingRulesNavigators));
        }, 500)
    }

    const onDirection = (location: Location) => {
        if (location) {
            const scheme = Platform.select({ ios: 'maps:0,0?q=', android: 'geo:0,0?q=' });
            const latLng = `${location.coordinate.latitude},${location.coordinate.longitude}`;
            const label = `${location.name} ${location.completeAddress}`;
            const url = Platform.select({
                ios: `${scheme}${label}@${latLng}`,
                android: `${scheme}${latLng}(${label})`
            });

            Linking.openURL(url);
        }
    }

    const reset = () => {
        props.signOut()
        setTimeout(() => {
            const resetAction = CommonActions.reset({
                index: 0,
                routes: [{ name: RootScreen.MainNavigators, params: {  } }]
            });
            navigation.dispatch(resetAction);

        }, 1000)
    }

    return (
        <DrawingDetails     pinch={pinch}
                            setPinch={setPinch}
                            onCancel={onCancel}
                            onDrawingRules={onDrawingRules}
                            onDirection={onDirection}
                            promotionDetails={getPromotionDetails()}
                            monthlyPromotion={getMonthlyPromotion()}
                            {...props} />
    )
}

const mapStateToProps = (state: RootState) => ({
    isLoading: DrawingSelectors.isLoading(state),
    drawingDetails: DrawingSelectors.drawingDetails(state),
    error: DrawingSelectors.error(state),
    recentDrawings: DrawingSelectors.recentDrawings(state),
    mobile: MobileSelectors.mobile(state),
    bank: BankSelectors.bank(state),
    isBankLoading: BankSelectors.isLoading(state),
    entry: BankSelectors.entry(state),
    locations: LocationSelectors.locations(state),
    locationCheckins: LocationSelectors.locationCheckins(state),
    earningTickPercentage: DashboardSelectors.earningTick(state),
})

const mapDispatchToProps = (dispatch: RootDispatch) => ({
    signOut: () => dispatch(MenuActions.signOut()),
    getDrawingDetails: (drawing: Drawing) => dispatch(DrawingActions.getDrawingDetails(drawing)),
    getDrawingRulesSuccess: (drawing: Drawing) => dispatch(DrawingActions.getDrawingRulesSuccess(drawing)),
})

const connector = connect(mapStateToProps, mapDispatchToProps)
type Props = ConnectedProps<typeof connector>

export default connector(DrawingDetailsContainer)
