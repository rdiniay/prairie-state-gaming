import React, { useEffect, useState, useCallback } from 'react'
import { connect, ConnectedProps, useStore, } from 'react-redux'
import { RootState, RootDispatch } from '../../../redux'
import {StackActions ,useNavigation, DrawerActions, useFocusEffect } from '@react-navigation/native'
import { Alert } from 'react-native'
import { Screen } from '../../../navigation/routes/mainRoutes'
import ForgotPinUpdate from './ForgotPinUpdatePanel'
import LoginActions, { LoginSelectors } from '../../../redux/login-redux'
import { AuthSelectors } from '../../../redux/auth-redux'
import ForgotPinActions, { ForgotPinSelectors } from '../../../redux/forgot-pin-redux'
import { strings } from '../../../localization/strings';

const ForgotPinUpdateContainer = (props: Props) =>  {
    const navigation = useNavigation()
    const [newPin, setNewPin] = useState('')

    const onCancel = () => {
        navigation.dispatch(StackActions.popToTop());
    }

    useFocusEffect(
        useCallback ( ()=> {
            if (props.isEmailVerified) {
                Alert.alert(strings.forgot_pin_update_title, 
                            strings.forgot_pin_update_description,
                            [{
                                text: `${strings.commons_btn_ok}`,
                                onPress: () => onCancel()
                            }])
            }
        },[props.isEmailVerified])
    )

    const onVerifyPin = () => {
        if (newPin)
            props.updatePin(newPin)
    }

    return (
        <ForgotPinUpdate    newPin={newPin}
                            setNewPin={setNewPin}
                            onCancel={onCancel}
                            onVerifyPin={onVerifyPin}
                            { ...props } />
    )
}

const mapStateToProps = (state: RootState) => ({
    isEmailVerified: ForgotPinSelectors.isEmailVerified(state),
    isLoading: ForgotPinSelectors.isLoading(state),
})
const mapDispatchToProps = (dispatch: RootDispatch) => ({
    updatePin: (newPin: String) => dispatch(ForgotPinActions.updatePin(newPin))
})

const connector = connect(mapStateToProps, mapDispatchToProps)
type Props = ConnectedProps<typeof connector>

export default connector(ForgotPinUpdateContainer)

