import React from 'react'
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    SectionList,
    TextInput,
    ActivityIndicator,
    Dimensions,
} from  'react-native'
import { Container, Header, LoadingDialog } from '../../components'
import styles from './Styles'
import { Colors } from '../../styles'
import {strings} from "../../localization/strings";
import { TextField } from 'react-native-material-textfield-upgraded'
import { Auth } from '../../models'
const { width, height } = Dimensions.get('window');

type Props = {
    onBack: () => void,
    setEmail: (value: string) => void,

    onSendForgotPin: () => void,

    email: string,
    isValidEmail: boolean,
    isLoading: boolean,
}

const ForgotPin = (props: Props) =>{

    const header = () => (
        <Header style={{ backgroundColor: Colors.red, }}
                title={'Forgot PIN'}
                titleStyle={{ fontSize: 18, color: Colors.white }}
                left={(
                    <TouchableOpacity    onPress={props.onBack}
                                         style={{ alignItems: 'center', flexDirection: "row", paddingVertical: 5, }}>
                         <Text style={{ color: Colors.white, textAlign: 'center', fontSize: 18,  }}>
                             Back
                         </Text>
                   </TouchableOpacity>
                 )}
                 leftContainerStyle={{ paddingLeft: 10, }}
        />
    )

    return(
        <Container  safeAreaViewTopStyle={styles.safeArea}
                    statusBarStyle={'light'}
                    scrollViewEnable={true}
                    header={header}
                    style={styles.container} >

                <LoadingDialog visible={props.isLoading} subtitle={'Please wait...'} />
                
                <View style={{ flex: 1, margin: 25, marginTop: 20, borderRadius: 15, paddingTop: 15, paddingBottom: 5, paddingHorizontal: 10, backgroundColor: 'white' }}>

                    <View style={{ marginTop: 10, height: 80,  alignItems: 'center' , }}>
                        <Image style={{width:237,}} source={require('../../assets/logo_text.png')} />
                    </View>
                    <View style={{ flex: 1, padding: 20, paddingBottom: 0, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ paddingBottom: 5, fontSize: 18, color: Colors.black }}>
                            {`Please enter the email your account uses below and we'll send you a pin reset code.`}
                        </Text>
                    </View>
                    <View style={{ padding: 10, }} >
                        <View style={{ flex:1,  marginHorizontal: 15, paddingBottom: 20, }}>
                            <TextField
                                label='Email'
                                value={props.email}
                                tintColor={Colors.red}
                                onChangeText={props.setEmail}
                            />

                                {props.email === null &&
                                    <Text style={{ color: Colors.red, paddingBottom: 5, }}>
                                        {strings.required_text}
                                    </Text>
                                }

                                {props.email !== null && props.email.length !== 0 && !props.isValidEmail &&
                                    <Text style={{ color: Colors.red, paddingBottom: 5, }}>
                                        {strings.invalid_email}
                                    </Text>
                                }
                        </View>
                        
                        <View style={{ flex: 1,  flexDirection: 'row', paddingVertical: 5, }}>
                            <View style={{ flex: 1, margin: 10}} />
                            <View style={{ flex: 1, margin: 10}}>
                                <TouchableOpacity style={{ padding: 10, backgroundColor: Colors.red, borderRadius: 20,  justifyContent: 'center', alignItems: 'center', }}
                                                    onPress={props.onSendForgotPin}>
                                    <Text style={{ color: 'white', fontSize: 16, textAlign: 'center' }}>
                                        {`Send`}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>

                    </View>
                </View>
        </Container>
    )
}

export default ForgotPin