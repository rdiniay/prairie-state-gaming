import React from 'react'
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    SectionList,
    TextInput,
    ActivityIndicator,
    Dimensions,
} from  'react-native'
import { Container, Header, } from '../../../components'
import styles from '../Styles'
import { Colors } from '../../../styles'
import {strings} from "../../../localization/strings";
import { TextField } from 'react-native-material-textfield-upgraded'
import { Auth } from '../../../models'
const { width, height } = Dimensions.get('window');

type Props = {
    onCancel: () => void,
    setPinVerificationCode: (value: string) => void,
    onVerifyPin: () => void,

    pinVerificationCode: string,
}

const ForgotPinVerify = (props: Props) =>{

    const header = () => (
        <Header style={{ backgroundColor: Colors.red, }}
                title={'Forgot PIN'}
                titleStyle={{ fontSize: 18, color: Colors.white }}
                left={(
                    <View style={{ alignItems: 'center', flexDirection: "row", paddingVertical: 5, }} />
                 )}
                 leftContainerStyle={{ paddingLeft: 10, }}
        />
    )

    return(
        <Container  safeAreaViewTopStyle={styles.safeArea}
                    statusBarStyle={'light'}
                    scrollViewEnable={true}
                    header={header}
                    style={styles.container} >

                <View style={{ flex: 1, margin: 25, marginTop: 20, borderRadius: 15, paddingTop: 15, paddingBottom: 5, paddingHorizontal: 10, backgroundColor: 'white' }}>

                    <View style={{ marginTop: 10, height: 80,  alignItems: 'center' , }}>
                        <Image style={{width:237,}} source={require('../../../assets/logo_text.png')} />
                    </View>
                    <View style={{ flex: 1, padding: 20, paddingBottom: 0, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ paddingBottom: 5, fontSize: 18, color: Colors.black }}>
                            {`You should receive the email any second now. Enter the code it contains below.`}
                        </Text>
                    </View>
                    <View style={{ padding: 10, }} >
                        <View style={{ flex:1,  marginHorizontal: 15, paddingBottom: 20, }}>
                            <TextField
                                label='PIN reset code'
                                value={props.pinVerificationCode}
                                tintColor={Colors.red}
                                onChangeText={props.setPinVerificationCode}
                                keyboardType="number-pad"
                            />
                        </View>
                        
                        <View style={{ flex: 1,  flexDirection: 'row', paddingVertical: 5, }}>
                            <View style={{ flex: 1, margin: 10}}>
                                <TouchableOpacity style={{ padding: 10, backgroundColor: Colors.red, borderRadius: 20,  justifyContent: 'center', alignItems: 'center', }}
                                                    onPress={props.onVerifyPin}>
                                    <Text style={{ color: 'white', fontSize: 16, textAlign: 'center' }}>
                                        {`Verify`}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ flex: 1, margin: 10}}>
                                <TouchableOpacity   style={{ padding: 10, borderColor: Colors.red, borderWidth: 0.5, borderRadius: 20,  justifyContent: 'center', alignItems: 'center', }}
                                                    onPress={props.onCancel}>
                                    <Text style={{ color: Colors.red, fontSize: 16, textAlign: 'center' }}>
                                        {'Cancel'}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>

                    </View>
                </View>
                
        </Container>
    )
}

export default ForgotPinVerify