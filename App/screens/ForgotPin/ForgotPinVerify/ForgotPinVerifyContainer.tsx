import React, { useEffect, useState } from 'react'
import { connect, ConnectedProps, useStore, } from 'react-redux'
import { RootState, RootDispatch } from '../../../redux'
import {StackActions ,useNavigation, DrawerActions } from '@react-navigation/native'
import { Screen } from '../../../navigation/routes/mainRoutes'
import ForgotPinVerify from './ForgotPinVerifyPanel'
import LoginActions, { LoginSelectors } from '../../../redux/login-redux'
import { AuthSelectors } from '../../../redux/auth-redux'
import ForgotPinActions, { ForgotPinSelectors } from '../../../redux/forgot-pin-redux'

const ForgotPinVerifyContainer = (props: Props) =>  {
    const navigation = useNavigation()
    const [pinVerificationCode, setPinVerificationCode] = useState('')

    const onCancel = () => {
        navigation.dispatch(StackActions.popToTop());
    }

    const onVerifyPin = () => {
        if (pinVerificationCode) {
            props.setPinCode(pinVerificationCode)
            navigation.dispatch(StackActions.push(Screen.ForgotPinUpdate));
            setPinVerificationCode('')
        }
    }

    return (
        <ForgotPinVerify    pinVerificationCode={pinVerificationCode}
                            setPinVerificationCode={setPinVerificationCode}
                            onCancel={onCancel}
                            onVerifyPin={onVerifyPin}
                            { ...props } />
    )
}

const mapStateToProps = (state: RootState) => ({
})
const mapDispatchToProps = (dispatch: RootDispatch) => ({
    setPinCode: (code: String) => dispatch(ForgotPinActions.setPinCode(code))
})

const connector = connect(mapStateToProps, mapDispatchToProps)
type Props = ConnectedProps<typeof connector>

export default connector(ForgotPinVerifyContainer)

