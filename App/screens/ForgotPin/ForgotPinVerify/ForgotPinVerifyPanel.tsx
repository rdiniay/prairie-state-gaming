import React, { useState } from 'react'
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    SectionList,
    TextInput,
    ActivityIndicator,
    Dimensions,
} from  'react-native'
import { Container, SwipeablePanel, Header, } from '../../../components'
import styles from '../Styles'
import { Colors } from '../../../styles'
import {strings} from "../../../localization/strings";
import { TextField } from 'react-native-material-textfield-upgraded'
import { Auth } from '../../../models'
const { width, height } = Dimensions.get('window');

type Props = {
    onCancel: () => void,
    setPinVerificationCode: (value: string) => void,
    onVerifyPin: () => void,

    pinVerificationCode: string,
}

const ForgotPinVerify = (props: Props) =>{

    const header = () => (
        <Header style={{ paddingTop: 15,  paddingBottom: 20, shadowColor: '#FFF', backgroundColor: '#B43235' }} //backgroundColor: '#EBECF1' }}
                center={(
                    <View style={{ alignItems: 'flex-end' }}>
                      <Text style={{ color: 'white', fontWeight: 'bold' }}>
                        
                      </Text>
                    </View>
                )} 
                left={(
                    <TouchableOpacity style={{ flexDirection: "row", alignItems: 'center', paddingLeft: 10, }}>
                            <Text style={{ color: Colors.white, textAlign: 'center', fontSize: 18,  }}>
                             
                         </Text>
                    </TouchableOpacity>
                )}
        />
    )

    const [pinToggleKeyboard, setPinToggleKeyboard] = useState(false)

    const [isPanelActive, setIsPanelActive] = useState(true);

    const [panelProps, setPanelProps] = useState({
        fullWidth: true,
        openLarge: false,
        showCloseButton: false,
        onClose: () => closePanel(),
        onPressCloseButton: () => closePanel(),
        // ...or any prop you want
        allowTouchOutside: false,
        noBackgroundOpacity: true,
        barStyle: {
            backgroundColor: Colors.gray
        }
        // noBar: true,
      });
    
      const openPanel = () => {
        setIsPanelActive(true);
      };
    
      const closePanel = () => {
        setIsPanelActive(false);
      };

      const toggleUp = () => {
        setPanelProps({...panelProps, openLarge: true})
        closePanel()
        setTimeout(() => {
            openPanel()
        }, 1);
      }

    return(
        <Container  safeAreaViewTopStyle={styles.safeArea}
                    statusBarStyle={'light'}
                    // scrollViewEnable={true}
                    header={header}
                    style={styles.container} >
                        
            <View style={{ marginTop: height*.2, height: 80, }}>
                <Image resizeMode={'contain'} style={{width:200,}} source={require('../../../assets/new_logo_text.png')} />
            </View>

            <SwipeablePanel titleValue={'Forgot PIN'} style={{backgroundColor: '#E6DDCE', }} {...panelProps} isActive={isPanelActive}>
                <View style={{  borderTopLeftRadius: 20, borderTopRightRadius: 20, backgroundColor: '#F7F5EB' }} >
                    <View style={{ margin: 15, marginBottom: height*.35, marginTop: 25, borderColor: Colors.lightgray, borderWidth: 0.5, borderRadius: 15, paddingVertical: 15, paddingHorizontal: 10, backgroundColor: 'white'  }}>

                        <View style={{ flex: 1, padding: 20, paddingBottom: 0, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 18, color: Colors.black }}>
                                {`You should receive the email any second now. Enter the code it contains below.`}
                            </Text>
                        </View>
                        <View style={{ padding: 10, }} >
                            <View style={{ flex:1,  marginHorizontal: 15, paddingBottom: 20, }}>
                                
                                <View style={{ flex: 19, }}>
                                    <TextField
                                        // label={strings.login_email}
                                        value={props.pinVerificationCode ?? ""}
                                        baseColor={props.pinVerificationCode ? Colors.lightgray : Colors.lightgray}
                                        tintColor={Colors.lightgray}
                                        keyboardType="number-pad"
                                        onChangeText={props.setPinVerificationCode}
                                        autoFocus={pinToggleKeyboard}
                                        onBlur={() => { setPinToggleKeyboard(false) }}
                                        onFocus={() => { 
                                            toggleUp()
                                            setPinToggleKeyboard(true)
                                        }}
                                    />
                                    <Text style={{ paddingBottom: 5, color: Colors.lightgray }}>
                                        {`PIN reset code`}
                                    </Text>
                                </View>
                            </View>
                            
                            <View style={{ flex: 1,  flexDirection: 'row', paddingVertical: 5, }}>
                                <View style={{ flex: 1, margin: 10}}>
                                    <TouchableOpacity   style={{ padding: 8, backgroundColor: Colors.lightBrown, borderColor: Colors.brown, borderWidth: 2, borderRadius: 20,  justifyContent: 'center', alignItems: 'center', }}
                                                        onPress={props.onCancel}>
                                        <Text style={{ color: Colors.black, fontSize: 16, textAlign: 'center' }}>
                                            {'Cancel'}
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ flex: 1, margin: 10}}>
                                    <TouchableOpacity style={{ padding: 8, backgroundColor: Colors.brown, borderColor: Colors.brown, borderWidth: 2, borderRadius: 20,  justifyContent: 'center', alignItems: 'center', }}
                                                        onPress={props.onVerifyPin}>
                                        <Text style={{ color: Colors.black, fontSize: 16, textAlign: 'center' }}>
                                            {`Verify`}
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>

                        </View>
                    </View>
                </View>
            </SwipeablePanel>
                
        </Container>
    )
}

export default ForgotPinVerify