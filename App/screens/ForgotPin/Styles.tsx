import ScaledSheet from 'react-native-scaled-sheet'
import { Colors } from '../../styles'

const styles = ScaledSheet.create({
    safeArea: {
        backgroundColor: Colors.red,
    },
    container: {
        flex: 1,
        // backgroundColor: 'lightgray',
    },
    requiredTextStyle: {
        position: 'absolute', 
        paddingTop: 65, 
        width: 80, 
        color: Colors.red, 
        alignSelf: 'flex-end'
    },
    invalidEmail: {
        position: 'absolute',
        paddingTop: 65, 
        width: 200, 
        color: Colors.red, 
        alignSelf: 'flex-end'
    }
})

export default styles