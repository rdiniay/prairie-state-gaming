import React, { useEffect, useState } from 'react'
import {
    View,
    Image,
    TouchableOpacity,
    SectionList,
    TextInput,
    ActivityIndicator,
    Dimensions,
} from  'react-native'
import { Container, SwipeablePanel, Header, LoadingDialog, Text } from '../../components'
import styles from './Styles'
import { Colors } from '../../styles'
import {strings} from "../../localization/strings";
import { TextField } from 'react-native-material-textfield-upgraded'
import { Auth } from '../../models'
const { width, height } = Dimensions.get('window');

type Props = {
    onBack: () => void,
    setEmail: (value: string) => void,

    onSendForgotPin: () => void,

    email: string,
    isValidEmail: boolean,
    isLoading: boolean,
}

const ForgotPin = (props: Props) =>{

    const header = () => (
        <Header style={{ paddingTop: 15,  paddingBottom: 20, shadowColor: '#FFF', backgroundColor: '#B43235' }} //backgroundColor: '#EBECF1' }}
                center={(
                    <View style={{ alignItems: 'flex-end' }}>
                      <Text style={{ color: 'white', fontWeight: 'bold' }}>
                        
                      </Text>
                    </View>
                )} 
                left={(
                    <TouchableOpacity onPress={props.onBack}  style={{ flexDirection: "row", alignItems: 'center', paddingLeft: 10, }}>
                            <Text style={{ color: Colors.white, textAlign: 'center', fontSize: 18,  }}>
                             Back
                         </Text>
                    </TouchableOpacity>
                )}
        />
    )

    const [emailToggleKeyboard, setEmailToggleKeyboard] = useState(false)

    const [isPanelActive, setIsPanelActive] = useState(true);

    const [panelProps, setPanelProps] = useState({
        fullWidth: true,
        openLarge: false,
        showCloseButton: false,
        onClose: () => closePanel(),
        onPressCloseButton: () => closePanel(),
        // ...or any prop you want
        allowTouchOutside: false,
        noBackgroundOpacity: true,
        barStyle: {
            backgroundColor: Colors.gray
        }
        // noBar: true,
      });
    
      const openPanel = () => {
        setIsPanelActive(true);
      };
    
      const closePanel = () => {
        setIsPanelActive(false);
      };

      const toggleUp = () => {
        setPanelProps({...panelProps, openLarge: true})
        closePanel()
        setTimeout(() => {
            openPanel()
        }, 1);
      }
      
    return(
        <Container  safeAreaViewTopStyle={styles.safeArea}
                    statusBarStyle={'light'}
                    // scrollViewEnable={true}
                    header={header}
                    style={styles.container} >

            <LoadingDialog visible={props.isLoading} subtitle={'Please wait...'} />

            <View style={{ marginTop: height*.188, height: 80, }}>
                <Image resizeMode={'contain'} style={{width:200,}} source={require('../../assets/new_logo_text.png')} />
            </View>

            <SwipeablePanel titleValue={'Forgot PIN'} style={{backgroundColor: '#E6DDCE', }} {...panelProps} isActive={isPanelActive}>
                <View style={{  borderTopLeftRadius: 20, borderTopRightRadius: 20, backgroundColor: '#F7F5EB' }} >
                    <View style={{ margin: 15, marginBottom: height*.35, marginTop: 25, borderColor: Colors.lightgray, borderWidth: 0.5, borderRadius: 15, paddingVertical: 15, paddingHorizontal: 10, backgroundColor: 'white'  }}>

                        <View style={{ flex: 1, padding: 20, paddingBottom: 0, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ paddingBottom: 0, fontSize: 18, color: Colors.black }}>
                                {`Please enter the email your account uses below and we'll send you a pin reset code.`}
                            </Text>
                        </View>
                        <View style={{ padding: 10, paddingVertical: 0, paddingRight: 0, }} >
                            <View style={{ flex:1,   flexDirection: 'row',  }}>
                                <View style={{ flex: 19, }}>
                                    <TextField
                                        // label={strings.login_email}
                                        value={props.email ?? ""}
                                        baseColor={props.email ? Colors.lightgray : Colors.lightgray}
                                        tintColor={Colors.lightgray}
                                        keyboardType="email-address"
                                        onChangeText={props.setEmail}
                                        autoFocus={emailToggleKeyboard}
                                        onBlur={() => { setEmailToggleKeyboard(false) }}
                                        onFocus={() => { 
                                            toggleUp()
                                            setEmailToggleKeyboard(true)
                                        }}
                                    />
                                    <Text style={{ paddingBottom: 5, color: Colors.lightgray }}>
                                        {strings.login_email}
                                    </Text>
                                </View>
                                <View style={{ flex: 1, justifyContent: 'center', }}>
                                    {props.email === null &&
                                        <Text style={styles.requiredTextStyle}>
                                            {strings.required_text}
                                        </Text>
                                    }

                                    {props.email !== null && props.email.length !== 0 && !props.isValidEmail &&
                                        <Text style={styles.invalidEmail}>
                                            {strings.invalid_email}
                                        </Text>
                                    }
                                </View>
                                {/* <TouchableOpacity onPress={toggleUp} style={{ position: 'absolute', backgroundColor: 'blue', height:'100%', width: '100%', }} /> */}
                            </View>
                            
                            <View style={{ flex: 1,  flexDirection: 'row', paddingVertical: 5, }}>
                                <View style={{ flex: 1, margin: 10}} />
                                <View style={{ flex: 1, margin: 10}}>
                                    <TouchableOpacity style={{ padding: 10,backgroundColor: Colors.brown, borderRadius: 20,  justifyContent: 'center', alignItems: 'center', }}
                                                        onPress={props.onSendForgotPin}>
                                        <Text style={{ color: Colors.black, fontSize: 16, textAlign: 'center' }}>
                                            {`Send`}
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>

                    </View>
                </View>
            </SwipeablePanel>
        </Container>
    )
}

export default ForgotPin