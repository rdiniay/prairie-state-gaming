import React, { useEffect, useState } from 'react'
import { connect, ConnectedProps, useStore, } from 'react-redux'
import { RootState, RootDispatch } from '../../redux'
import {StackActions ,useNavigation, DrawerActions, CommonActions } from '@react-navigation/native'
import { Screen } from '../../navigation/routes/mainRoutes'
import ForgotPin from './ForgotPinPanel'
import LoginActions, { LoginSelectors } from '../../redux/login-redux'
import { AuthSelectors } from '../../redux/auth-redux'
import Toast from 'react-native-toast-message';
import ForgotPinActions, { ForgotPinSelectors } from '../../redux/forgot-pin-redux'
import MenuActions, { MenuSelectors } from '../../redux/menu-redux'
import {Screen as RootScreen} from '../../navigation/routes/rootRoutes'
import Utils from '../../services/shared/utils/Utils'
import Preference from "react-native-preference";


const ForgotPinContainer = (props: Props) =>  {
    const navigation = useNavigation()
    const [userName, setUserName] = useState('')
    const [isValidUserName, setIsValidUserName] = useState(true)


    const onBack = () => {
        navigation.dispatch(StackActions.pop());
    }

    useEffect(() => {
        props.clearForgotPin()
    }, [])

    useEffect(() => {
        if (props.error) {
            if (props.error.statusCode == 403 && props.error.signoutUser){
                reset();
                return
            }
            let errorMessage = props.error.genericError
            let errorStatus =  props.error.statusCode ? `Error: ${props.error.statusCode}` : ``
            if (props.error.backendError)
                errorMessage = props.error.backendError

            if (props.error.connectionError)
                errorMessage = props.error.connectionError

            if (props.error.api === 'pinResetRequest/verification') {
                onBack()
                setTimeout(() => {
                    if (__DEV__){
                        Toast.show({ type: 'custom', text1: `${errorMessage} ${errorStatus}`, })
                    }else{
                        Toast.show({ type: 'custom', text1: `${errorMessage}`, })
                    }
                }, 500);
            }
            else {
                if (__DEV__){
                    Toast.show({ type: 'custom', text1: `${errorMessage} ${errorStatus}`, })
                }else{
                    Toast.show({ type: 'custom', text1: `${errorMessage}`, })
                }
            }
        }
    }, [props.error])

    useEffect(() => {
        if (props.isEmailSent)
            navigation.dispatch(StackActions.push(Screen.ForgotPinVerify));
    }, [props.isEmailSent])

    const onSendForgotPin = async () => {

        if (!userName){
            setUserName(null);
            return;
        }
        let newUserName = userName;

        if(userName && userName.includes("--")){
            const splitEmail = userName.split("--", 2)
            const env = splitEmail[0]
            
            await Preference.set('ENV', env).then( () => {
                console.log('Preference has been set')
            })
            newUserName = splitEmail[1]
            console.log("ENV : ", env)
        }

        if(Utils.validateEmail(newUserName)){
            setIsValidUserName(true)
        }else{
            setIsValidUserName(false)
            return;
        }


        props.sendForgotPinVerification(newUserName)
    }

    const reset = () => {
        props.signOut()
        setTimeout(() => {
            const resetAction = CommonActions.reset({
                index: 0,
                routes: [{ name: RootScreen.MainNavigators, params: {  } }]
            });
            navigation.dispatch(resetAction);

        }, 1000)
    }

    return (
        <ForgotPin  email={userName}
                    setEmail={setUserName}
                    isValidEmail={isValidUserName}
                    onBack={onBack}
                    onSendForgotPin={onSendForgotPin}
                    { ...props } />
    )
}

const mapStateToProps = (state: RootState) => ({
    isEmailSent: ForgotPinSelectors.isEmailSent(state),
    isLoading: ForgotPinSelectors.isLoading(state),
    error: ForgotPinSelectors.error(state),
})
const mapDispatchToProps = (dispatch: RootDispatch) => ({
    clearForgotPin: () => dispatch(ForgotPinActions.clearForgotPin()),
    sendForgotPinVerification: (emailAddress: String) => dispatch(ForgotPinActions.sendForgotPinVerification(emailAddress)),
    signOut: () => dispatch(MenuActions.signOut()),
})

const connector = connect(mapStateToProps, mapDispatchToProps)
type Props = ConnectedProps<typeof connector>

export default connector(ForgotPinContainer)

