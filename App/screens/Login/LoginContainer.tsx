import React, { useCallback, useEffect, useState } from 'react'
import { connect, ConnectedProps, useStore, } from 'react-redux'
import { RootState, RootDispatch } from '../../redux'
import {StackActions ,useNavigation, DrawerActions, useFocusEffect, CommonActions} from '@react-navigation/native'
import { Screen } from '../../navigation/routes/mainRoutes'
import { Screen as RootScreen } from '../../navigation/routes/rootRoutes'
import Login from './LoginPanel'
import { AuthSelectors } from '../../redux/auth-redux'
import LoginActions, { LoginSelectors } from '../../redux/login-redux'
import ForgotPinActions from '../../redux/forgot-pin-redux'
import ProfileActions, { ProfileSelectors } from '../../redux/profile-redux'
import LocationActions, { LocationSelectors } from '../../redux/location-redux'
import MessagingActions from '../../redux/messaging-redux'
import Toast from 'react-native-toast-message';
import { Alert, Linking, Platform } from 'react-native'
import { strings } from '../../localization/strings'
import Utils from '../../services/shared/utils/Utils'
import Preference from 'react-native-preference'
import { AppStateSelectors } from '../../redux/app-state-redux'
import PreparationActions, { PreparationSelectors } from '../../redux/preparation-redux'
import {getSystemVersion} from "react-native-device-info";

const LoginContainer = (props: Props) =>  {
    const navigation = useNavigation()
    const [shouldNavigate, setShouldNavigate] = useState(true)
    const [userName, setUserName] = useState('')
    const [isValidUserName, setIsValidUserName] = useState(true)
    const [password, setPassword] = useState('')

    const onSignIn = () => {

        if (!userName){
            setUserName(null);
            return;
        }
        var newUserName = userName
        var env = ""

        if(userName && userName.includes("--")){
            const splitEmail = userName.split("--", 2)
            env = splitEmail[0]
            newUserName = splitEmail[1]
        }

        if (env.trim().length === 0) {
            continueSigningIn(newUserName)
        } else {
            // Set Preference
            Preference.set('ENV', env).then( () => {
                console.log('Preference has been set')
                console.log("ENV : ", env)
                continueSigningIn(newUserName)
            })
        }
    }

    const continueSigningIn = (newUserName: string) => {
        if(Utils.validateEmail(newUserName)){
            setIsValidUserName(true)
        }else if(Utils.validatePhoneNumber(newUserName)){
            setIsValidUserName(true)
        }else{
            setIsValidUserName(false)
            return;
        }

        if (!password){
            setPassword(null);
            return;
        }

        props.signIn(newUserName, password)
    }

    const onCreateOne = () => {
        navigation.dispatch(StackActions.push(Screen.Registration));
    }
    const onForgotPin = () => {
        navigation.dispatch(StackActions.push(Screen.ForgotPin));
    }

    const onLoad = () => {
        const env = Preference.get("ENV")
        const adhocTopic = env ? `${env}-adhoc`: 'adhoc'
        props.subscribeMessagingOnTopicMessage(adhocTopic)
        props.subscribeMessagingOnBackgroundMessage()
        props.clearForgotPin()
        props.getCurrentPromotionLocation()
        props.getProfile()
        setUserName("")
        setPassword("")


    }

    useFocusEffect(
        useCallback ( ()=> {
            props.getPreparations()
        }, [props.nextAppState])
    )

    useFocusEffect(
        useCallback ( ()=> {
            if((Platform.OS === 'android')) {
                if(parseInt(getSystemVersion()) >= 12) {
                    if ((!props.gps || !props.geoLocation || !props.bluetooth || !props.nearbyDevices || !props.network)) {
                        navigation.dispatch(StackActions.push(RootScreen.PreparationsNavigators));
                    }
                }
                else {
                    if ((!props.gps || !props.geoLocation || !props.bluetooth || !props.network)) {
                        navigation.dispatch(StackActions.push(RootScreen.PreparationsNavigators));
                    }
                }
            } else {
                if ((!props.gps || !props.bluetooth || !props.network)) {
                    navigation.dispatch(StackActions.push(RootScreen.PreparationsNavigators));
                }
            }

        }, [props.gps,
            props.geoLocation,
            props.bluetooth,
            props.network,
            props.nearbyDevices,
            props.timestamp]
        )
    )

    useFocusEffect(
        useCallback ( ()=> {
            if (props.isChangedEmail && props.profile)
                setUserName(props.profile?.emailAddress)


        }, [props.profile])
    )

    useFocusEffect(
        useCallback ( ()=> {
            if (props.route?.params?.username &&
                props.route?.params?.pin) {
                setUserName(props.route?.params?.username)
                setPassword(props.route?.params?.pin)
                onSignIn()
            }
        }, [props.route])
    )

    useFocusEffect(
        useCallback ( ()=> {

            var newUserName = userName

            if(userName && userName.includes("--")){
                const splitEmail = userName.split("--", 2)
                const env = splitEmail[0]
                // Set Preference
                newUserName = splitEmail[1]
            }

            if(Utils.validateEmail(newUserName)){
                setIsValidUserName(true)
            }else if(Utils.validatePhoneNumber(newUserName)){
                setIsValidUserName(true)
            }else{
                setIsValidUserName(false)
            }
        }, [userName])
    )

    useFocusEffect(
        useCallback ( ()=> {
            if (props.auth && shouldNavigate) {
                onLoad()

                setShouldNavigate(false)
                setTimeout(() => {

                    const resetToHome = CommonActions.reset({
                        index: 0,
                        routes: [{ name: Screen.DrawerNavigator, params: {  } }]
                    });
                    navigation.dispatch(resetToHome);
                }, 1000)
            }
            else if(!props.auth && !shouldNavigate)
                setShouldNavigate(true)

        }, [props.auth])
    )

    useEffect(() => {

        if (props.error) {

            if(props.error.forceAppUpdate){
                Alert.alert(strings.force_app_update_alert_dialog_title,
                    strings.force_app_update_alert_dialog_message,
                    [
                        {
                          text: `${strings.proceed_btn_proceed}`,
                          style: "default",
                          onPress: () =>  {
                            var url = props.error.forceAppUpdate.url;
                            if (!url.includes("https://")){
                                url = "https://" + props.error.forceAppUpdate.url;
                            }
                            Linking.canOpenURL(url).then(supported => {
                              if (supported) {
                                Linking.openURL(url);
                              } else {
                                console.log("Don't know how to open URI: " + url);
                              }
                            });
                          },

                        }
                      ]
                );

            }else{
                let errorMessage = props.error.genericError
                let errorStatus =  props.error.statusCode ? `Error: ${props.error.statusCode}` : ``
                if (props.error.backendError)
                    errorMessage = props.error.backendError

                if (props.error.connectionError)
                    errorMessage = props.error.connectionError

                setTimeout(() => {
                    if (__DEV__){
                        Toast.show({ type: 'custom', text1: `${errorMessage} ${errorStatus}`, })
                    }else{
                        Toast.show({ type: 'custom', text1: `${errorMessage}`, })
                    }
                }, 500);
            }
        }
    }, [props.error])

    const onHandleUserName = (value: string) => {
        var cleaned = ('' + value).replace(/\D/g, '')
        var match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/)
        if (match) {
            var usernameEnv = ''
            if (value && value.includes("--")) {
                const splitUserEmail = value.split("--", 2)
                usernameEnv = `${splitUserEmail[0].toLowerCase()}--`
            }
            var intlCode = (match[1] ? '+1 ' : ''),
                number = [intlCode, `${usernameEnv}(`, match[2], ') ', match[3], '-', match[4]].join('');

            setUserName(number)
            return;
        }
        if (value && value.includes("--")) {
            const splitUserEmail = value.split("--", 2)
            const usernameEnv = `${splitUserEmail[0].toLowerCase()}--`
            setUserName(`${usernameEnv}${splitUserEmail[1]}`)
            return
        }
        setUserName(value)
    }

    return (
        <Login  onSignIn={onSignIn}
                onCreateOne={onCreateOne}
                onForgotPin={onForgotPin}
                isValidUserName={isValidUserName}
                userName={userName}
                password={password}
                setUserName={setUserName}
                setPassword={setPassword}
                onHandleUserName={onHandleUserName}
                { ...props } />
    )
}

const mapStateToProps = (state: RootState) => ({
    coordinate: LocationSelectors.coordinate(state),
    auth: AuthSelectors.auth(state),
    isChangedEmail: ProfileSelectors.isChangedEmail(state),
    profile: ProfileSelectors.profile(state),
    error: LoginSelectors.error(state),
    isLoading: LoginSelectors.isLoading(state),
    nextAppState: AppStateSelectors.nextAppState(state),
    gps: PreparationSelectors.gps(state),
    geoLocation: PreparationSelectors.geoLocation(state),
    bluetooth: PreparationSelectors.bluetooth(state),
    network: PreparationSelectors.network(state),
    nearbyDevices: PreparationSelectors.nearbyDevices(state),
    notification: PreparationSelectors.notification(state),
    timestamp: PreparationSelectors.timestamp(state),
})
const mapDispatchToProps = (dispatch: RootDispatch) => ({
    subscribeMessagingOnTopicMessage: (topic: String) => dispatch(MessagingActions.subscribeMessagingOnTopicMessage(topic)),
    subscribeMessagingOnBackgroundMessage: () => dispatch(MessagingActions.subscribeMessagingOnBackgroundMessage()),
    getCurrentPromotionLocation: () => dispatch(LocationActions.getCurrentPromotionLocation()),
    clearForgotPin: () => dispatch(ForgotPinActions.clearForgotPin()),
    getProfile: () => dispatch(ProfileActions.getProfile()),
    signIn: (userName: string, pin: string) => dispatch(LoginActions.signIn(userName, pin)),
    signInSuccess: () => dispatch(LoginActions.signInSuccess(null)),
    getPreparations: () => dispatch(PreparationActions.getPreparations()),
})

const connector = connect(mapStateToProps, mapDispatchToProps)
interface Props extends ConnectedProps<typeof connector> {
    route: any
}

export default connector(LoginContainer)

