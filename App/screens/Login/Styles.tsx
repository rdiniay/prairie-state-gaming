import ScaledSheet from 'react-native-scaled-sheet'
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import { Colors, Commons } from '../../styles'
import { Dimensions, PixelRatio, } from  'react-native'
const { width, height } = Dimensions.get('window');

const styles = ScaledSheet.create({
    safeArea: {
        backgroundColor: Colors.red,
    },
    container: {
        flex: 1,
    },
    invalidUsernameText: {
        textAlign: 'center', 
        position: 'absolute', 
        bottom: -15, 
        width: width*.52, 
        color: Colors.red, 
        alignSelf: 'flex-end' 
    },
    requiredText: {
        position: 'absolute', 
        bottom: -15, 
        width: 80, 
        color: Colors.red, 
        alignSelf: 'flex-end' 
    },
    imagePanelMarginTopWithPhoneNumber: { 
        ...Commons.ContentLogoViewStyle(),
        marginTop: (height*.140) + useSafeAreaInsets().top,
    },
    imagePanelMarginTopWithoutPhoneNumber: { 
        ...Commons.ContentLogoViewStyle(),
        marginTop: (height*.180) + useSafeAreaInsets().top,
    }
})

const commons =  ScaledSheet.create({
    headerTouchableOpacityStyle: Commons.HeaderTouchableOpacityStyle(),
    headerTextStyle: Commons.HeaderBackButtonTextStyle(),
    contentHeaderTextViewStyle: Commons.ContentHeaderTextViewStyle(),
    contentHeaderSubTextViewStyle: Commons.ContentHeaderSubTextViewStyle(),
    leftContainerStyle:  Commons.HeaderContentLeftContainerStyle(),
    contentLogoViewStyle: Commons.ContentLogoViewStyle(),
    contentLogoStyle: Commons.ContentLogoStyle(),
    
    contentSwipePanelStyle: Commons.ContentSwipePanelStyle(),
    contentTitleStyle: Commons.ContentTitleStyle(),
    contentInnerViewStyle: Commons.InnerViewStyle(),
    contentInnerContentViewStyle: Commons.InnerContentViewStyle(),
    contentInlineContentViewInnerHeaderViewStyle: Commons.InlineContentViewInnerHeaderViewStyle(),
    contentInlineContentViewInnerHeaderTextViewStyle: {
        textAlign: 'center'  ,  
        ...Commons.InlineContentViewInnerHeaderTextViewStyle(),
        fontSize: 13 / PixelRatio.getFontScale(),
    },
    inlineContentViewStyle: {
        paddingTop: 10, 
        borderBottomColor: Colors.darkgray, 
        borderBottomWidth: 0.5, 
        paddingVertical: 5, 
        marginHorizontal: 8,
    },
    inlineContentTextViewStyle: {
        ...Commons.InlineContentViewInnerContentTextViewStyle(),
        marginHorizontal: 15,
    },
    contentInlineContextTextInputStyle : {
        padding: 8, 
        marginHorizontal: 7, 
        fontSize: 12 / PixelRatio.getFontScale(), 
        fontFamily: 'Raleway-Regular'
    },
    contentInlineClickableTextViewStyle :  { 
        marginHorizontal: 15, 
        color: Colors.black, 
        paddingVertical: 8,
        fontFamily: 'Raleway-Regular',
        fontSize: 12 / PixelRatio.getFontScale(), 
    },


})

const commonsAssets = {
    contentImageSource: Commons.ContentImageSource(),
}

export {
    styles,
    commons,
    commonsAssets,
}