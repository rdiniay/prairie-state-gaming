import React from 'react'
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    SectionList,
    TextInput,
    ActivityIndicator,
    Dimensions,
} from  'react-native'
import { env } from '../../config'
import { Container, Header } from '../../components'
import styles from './Styles'
import { Colors } from '../../styles'
import {strings} from "../../localization/strings";
import { TextField } from 'react-native-material-textfield-upgraded'
import { Auth } from '../../models'
const { width, height } = Dimensions.get('window');
import { LoadingDialog } from '../../components'
import { getSystemVersion, getReadableVersion  } from 'react-native-device-info'

type Props = {
    onSignIn: () => void,
    onCreateOne: () => void,
    onForgotPin: () => void,
    setEmail: (value: string) => void,
    setPassword: (value: string) => void,

    auth: Auth,
    email: string,
    isValidEmail: boolean,
    password: string,
    isLoading: boolean,
}

const Login = (props: Props) =>{

    const header = () => (
        <Header style={{ paddingTop: 15,  paddingBottom: 20, shadowColor: '#FFF', backgroundColor: '#295FEA' }} //backgroundColor: '#EBECF1' }}
                center={(
                    <View style={{ alignItems: 'flex-end' }}>
                      <Text style={{ color: Colors.red, fontWeight: 'bold' }}>
                        
                      </Text>
                    </View>
                )} 
                // right={(
                //    <View style={{ flex: 0.1, top: -18, alignItems: 'center', }}>
                //     <View style={{ borderRadius: 20, height: 40, width: 40, backgroundColor: '#EBECF160', overflow: 'hidden' ,}} >
                //       <Image resizeMode="cover" source={images.cityOfBaguioSeal} style={{ flex: 3,  height: 40, width: 40, }} /> 
                //     </View>
                //   </View>
                // )}
        />
    )
    return(
        <Container  // safeAreaViewTopStyle={styles.safeArea}
                    statusBarStyle={'dark'}
                    scrollViewEnable={true}
                    // header={header}
                    style={styles.container} >

                {props.auth &&
                    <View style={{ flex: 1, height: height, justifyContent: 'center', alignItems: 'center', }}>
                        <ActivityIndicator size="large" color={'white'}/>
                    </View>
                }
                {props.isLoading && !props.auth && 
                    <LoadingDialog visible={props.isLoading} subtitle={'Please wait...'} />
                }
                {!props.auth &&
                    <View style={{ flex: 1, margin: 25, marginTop: 30, borderRadius: 15, paddingVertical: 15, paddingHorizontal: 10, backgroundColor: 'white' }}>

                        <View style={{ marginTop: 10, height: 80,  alignItems: 'center' , }}>
                           <Image style={{width:237,}} source={require('../../assets/logo_text.png')} />
                        </View>

                        <View style={{ padding: 10, }} >
                            <View style={{ flex:1,  marginHorizontal: 15, }}>
                                <TextField
                                    label={strings.login_email}
                                    value={props.email}
                                    baseColor={props.email ? Colors.red : Colors.lightgray}
                                    tintColor={Colors.red}
                                    keyboardType="email-address"
                                    onChangeText={props.setEmail}
                                />
                                {props.email === null &&
                                    <Text style={{ color: Colors.red, paddingBottom: 5, }}>
                                        {strings.required_text}
                                    </Text>
                                }

                                {props.email !== null && props.email.length !== 0 && !props.isValidEmail &&
                                    <Text style={{ color: Colors.red, paddingBottom: 5, }}>
                                        {strings.invalid_email}
                                    </Text>
                                }
                            </View>
                            <View style={{ flex: 1, marginHorizontal: 15, }}>
                                <TextField
                                    label={strings.login_pin}
                                    value={props.password}
                                    maxLength={4}
                                    baseColor={props.password ? Colors.red : Colors.lightgray}
                                    tintColor={Colors.red}
                                    keyboardType='number-pad'
                                    onChangeText={props.setPassword}
                                />
                                {props.password === null &&
                                    <Text style={{ color: Colors.red, paddingBottom: 5, }}>
                                        {strings.required_text}
                                    </Text>
                                }
                            </View>
                            <View style={{ flex: 1, marginRight: 10, bottom: 15,  alignItems: 'flex-end' }}>
                                <TouchableOpacity   style={{ padding: 15, paddingBottom: 30, justifyContent: 'center', alignItems: 'center', }}
                                                    onPress={props.onForgotPin}>
                                    <Text style={{ color: Colors.lightgray, fontSize: 13, textAlign: 'center' }}>
                                        {strings.login_forgot_your_pin}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                           
                            <View style={{ flex: 1,  flexDirection: 'row'}}>

                                <View style={{ flex: 1, margin: 10}}>
                                    <TouchableOpacity   style={{ padding: 10, borderColor: Colors.red, borderWidth: 0.5, borderRadius: 20,  justifyContent: 'center', alignItems: 'center', }}
                                                        onPress={props.onCreateOne}>
                                        <Text style={{ color: Colors.red, fontSize: 16, textAlign: 'center' }}>
                                            {strings.login_create_one}
                                        </Text>
                                    </TouchableOpacity>
                                </View>

                                <View style={{ flex: 1, margin: 10}}>
                                <TouchableOpacity style={{ padding: 10, backgroundColor: Colors.red, borderRadius: 20,  justifyContent: 'center', alignItems: 'center', }}
                                                    onPress={props.onSignIn}>
                                    <Text style={{ color: 'white', fontSize: 16, textAlign: 'center' }}>
                                        {strings.login_sign_in}
                                    </Text>
                                </TouchableOpacity>
                                </View>
                            </View>

                        </View>
                    </View>
                }
                {`${env.PSG_BASE_URL}`.includes('-api') &&  
                    <Text style={{ fontSize: 14, color: Colors.whiteOpacity, alignSelf: 'center'}}>
                        {`Version: ${getReadableVersion()}`}
                    </Text>
                }
        </Container>
    )
}

export default Login