import React, { useEffect, useState } from 'react'
import {
    View,
    Image,
    TouchableOpacity,
    SectionList,
    TextInput,
    ActivityIndicator,
    Dimensions,
    ScrollViewProps,
} from  'react-native'
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { env } from '../../config'
import Utils from '../../services/shared/utils/Utils'
import { Container, SwipeablePanel, Header, Text, } from '../../components'
import {styles, commons, commonsAssets} from './Styles'
import { Colors } from '../../styles'
import {strings} from "../../localization/strings";
import { TextField } from 'react-native-material-textfield-upgraded'
import { Auth } from '../../models'
const { width, height } = Dimensions.get('window');
import { LoadingDialog } from '../../components'
import { getSystemVersion, getReadableVersion  } from 'react-native-device-info'

type Props = {
    onSignIn: () => void,
    onCreateOne: () => void,
    onForgotPin: () => void,
    setUserName: (value: string) => void,
    setPassword: (value: string) => void,
    onHandleUserName: (value: string) => void,

    auth: Auth,
    userName: string,
    isValidUserName: boolean,
    password: string,
    isLoading: boolean,
}

const Login = (props: Props) =>{
    const insets = useSafeAreaInsets()

    const [emailToggleKeyboard, setEmailToggleKeyboard] = useState(false)
    const [pinToggleKeyboard, setPinToggleKeyboard] = useState(false)

    const [isPanelActive, setIsPanelActive] = useState(true);

    const [panelProps, setPanelProps] = useState({
        fullWidth: true,
        openLarge: false,
        showCloseButton: false,
        onClose: () => closePanel(),
        onPressCloseButton: () => closePanel(),
        // ...or any prop you want
        noBackgroundOpacity: true,
      });

      const openPanel = () => {
        setIsPanelActive(true);
      };

      const closePanel = () => {
        setIsPanelActive(false);
      };

      const toggleUp = () => {
        if(props.userName === null)
            props.setUserName("")

        setPanelProps({...panelProps, openLarge: true})
        closePanel()
        setTimeout(() => {
            openPanel()
        }, 1);
      }

      const toggleDown = () => {
        if(props.userName === null)
            props.setUserName("")

        setPanelProps({...panelProps, openLarge: false})
        closePanel()
        setTimeout(() => {
            openPanel()
        }, 1);

      }

    return(
        <Container  safeAreaViewTopStyle={styles.safeArea}
                    statusBarStyle={'light'}
                    // scrollViewEnable={true}
                    // header={header}
                    style={styles.container} >

        {props.auth &&
            <View style={{ flex: 1, height: height, justifyContent: 'center', alignItems: 'center', }}>
                <ActivityIndicator size="large" color={'white'}/>
            </View>
        }
        {props.isLoading && !props.auth &&
            <LoadingDialog visible={props.isLoading} subtitle={'Please wait...'} />
        }

        {!props.auth &&
            <View style={styles.imagePanelMarginTopWithoutPhoneNumber}>
                <Image resizeMode={'contain'} style={commons.contentLogoStyle} source={commonsAssets.contentImageSource} />
            </View>
        }
        {!props.auth &&
            <SwipeablePanel titleValue={'login'} style={{backgroundColor: '#E6DDCE', }} {...panelProps} isActive={isPanelActive}>
                <View style={{ height: height*.83, borderTopLeftRadius: 20, borderTopRightRadius: 20, backgroundColor: '#F7F5EB' }} >
                    <View style={{ height: height*.45, margin: 15, marginTop: 25, borderColor: Colors.lightgray, borderWidth: 0.5, borderRadius: 15, paddingVertical: 15, paddingHorizontal: 10, backgroundColor: 'white'  }}>

                        <View style={{ flex: 1, padding: 10, paddingRight: 0, }} >
                            <View style={{ flex:1,  marginHorizontal: 0, marginTop: -10, flexDirection: 'row',  }}>
                                <View style={{ flex: 19, }}>
                                    <TextField
                                        // label={strings.login_email}
                                        value={props.userName}
                                        baseColor={props.userName ? Colors.lightgray : Colors.lightgray}
                                        tintColor={Colors.lightgray}
                                        keyboardType="email-address"
                                        testID="txtUsername"
                                        accessibilityLabel="txtUsername"
                                        onChangeText={props.onHandleUserName}
                                        autoFocus={emailToggleKeyboard}
                                        onBlur={() => { setEmailToggleKeyboard(false) }}
                                        onFocus={() => {
                                            toggleUp()
                                            setEmailToggleKeyboard(true)
                                        }}
                                    />
                                    <Text style={{ paddingBottom: 5, color: Colors.lightgray }}>
                                        {strings.login_user_name}
                                    </Text>
                                </View>
                                <View style={{ flex: 1, justifyContent: 'flex-end', }}>
                                    {props.userName === null &&
                                        <Text style={styles.requiredText}>
                                            {strings.required_text}
                                        </Text>
                                    }

                                    {props.userName !== null && props.userName.length !== 0 && !props.isValidUserName &&
                                        <Text style={styles.invalidUsernameText}>
                                            {strings.invalid_email}
                                        </Text>
                                    }
                                </View>
                                {/* <TouchableOpacity onPress={toggleUp} style={{ position: 'absolute', backgroundColor: 'blue', height:'100%', width: '100%', }} /> */}
                            </View>
                            <View style={{ flex:1,  marginHorizontal: 0, marginTop: -0, flexDirection: 'row',  }}>
                                <View style={{ flex: 19, }}>
                                    <TextField
                                        // label={strings.login_pin}
                                        value={props.password}
                                        maxLength={4}
                                        baseColor={props.password ? Colors.lightgray : Colors.lightgray}
                                        tintColor={Colors.lightgray}
                                        keyboardType='number-pad'
                                        testID="txtPin"
                                        accessibilityLabel="txtPin"
                                        onChangeText={props.setPassword}
                                        autoFocus={pinToggleKeyboard}
                                        onBlur={() => { setPinToggleKeyboard(false) }}
                                        onFocus={() => {
                                            toggleUp()
                                            setPinToggleKeyboard(true)
                                        }}
                                    />
                                    <Text style={{ paddingBottom: 5, color: Colors.lightgray }}>
                                        {strings.login_pin}
                                    </Text>
                                </View>
                                <View style={{ flex: 1, justifyContent: 'center'}}>
                                    {props.password === null &&
                                        <Text style={styles.requiredText}>
                                            {strings.required_text}
                                        </Text>
                                    }
                                </View>
                                {/* <TouchableOpacity onPress={toggleUp} style={{ position: 'absolute', backgroundColor: 'blue', height:'100%', width: '100%', }} /> */}
                            </View>
                            <View style={{ flex: 1, marginRight: 10, bottom: 15,  alignItems: 'flex-end' }}>
                                <TouchableOpacity   style={{ padding: 15, paddingTop: 30, justifyContent: 'center', alignItems: 'center', }}
                                                    onPress={() => { props.onForgotPin() }}>
                                    <Text style={{ color: Colors.lightgray, fontSize: 13, textAlign: 'center' }}>
                                        {strings.login_forgot_your_pin}
                                    </Text>
                                </TouchableOpacity>
                            </View>

                            <View style={{ flex: 1,  flexDirection: 'row'}}>

                                <View style={{ flex: 1, margin: 10}}>
                                    <TouchableOpacity   style={{ padding: 8, backgroundColor: Colors.lightBrown, borderColor: Colors.brown, borderWidth: 2, borderRadius: 20,  justifyContent: 'center', alignItems: 'center', }}
                                                        onPress={Utils.multipleTapHandler(() => props.onCreateOne())}>
                                        <Text style={{ color: Colors.black, fontSize: 16, textAlign: 'center' }}>
                                            {strings.login_create_one}
                                        </Text>
                                    </TouchableOpacity>
                                </View>

                                <View style={{ flex: 1, margin: 10}}>
                                <TouchableOpacity style={{ padding: 8, backgroundColor: Colors.brown, borderColor: Colors.brown, borderWidth: 2, borderRadius: 20,  justifyContent: 'center', alignItems: 'center', }}
                                                    onPress={Utils.multipleTapHandler(() => props.onSignIn())}>
                                    <Text style={{ color: Colors.black, fontSize: 16, textAlign: 'center' }}>
                                        {strings.login_sign_in}
                                    </Text>
                                </TouchableOpacity>
                                </View>
                            </View>

                        </View>
                    </View>
                    {`${env.PSG_BASE_URL}`.includes('-api') &&
                        <Text style={{ fontSize: 14, color: Colors.lightgray, alignSelf: 'center'}}>
                            {`Version: ${getReadableVersion()}`}
                        </Text>
                    }
                </View>

            </SwipeablePanel>
        }
        </Container>
    )
}

export default Login
