import React, { useEffect, useState, useCallback } from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { RootState, RootDispatch } from '../../redux'
import { Alert } from 'react-native'
import {StackActions ,useNavigation, DrawerActions, CommonActions, useFocusEffect } from '@react-navigation/native'
import ChangeEmail from './ChangeEmailPanel'

import ProfileActions, { ProfileSelectors } from '../../redux/profile-redux'
import Toast from 'react-native-toast-message';
import MenuActions, { MenuSelectors } from '../../redux/menu-redux'
import {Screen as RootScreen} from '../../navigation/routes/rootRoutes'
import { strings } from '../../localization/strings'
import Utils from '../../services/shared/utils/Utils'
import { env } from '../../config'


const ChangeEmailContainer = (props: Props) => {
    const navigation = useNavigation()
    const [newEmail, setNewEmail] = useState('')
    const [isValidEmail, setIsValidEmail] = useState(true)

    const onCancel = () => {
        navigation.goBack();
    }

    const onChangeEmail = () => {
        console.log("onChangeEmail:", newEmail, !newEmail, !Utils.validateEmail(newEmail))
        if (!newEmail) {
            setNewEmail(null)
            return
        } 

        if(!Utils.validateEmail(newEmail)){
            setIsValidEmail(false)
            return;
        }else{
            setIsValidEmail(true)
        }


        if (newEmail) {
            props.updateEmail(newEmail)
            return
        } 

        if (!newEmail)
            setNewEmail(null)
    }

    useFocusEffect(
        useCallback ( ()=> {
            if(!Utils.validateEmail(newEmail)){
                setIsValidEmail(false)
            }else{
                setIsValidEmail(true)
            }
        }, [newEmail])
    )

    useEffect(() => {
        if (props.isChangedEmail) {
            Alert.alert(strings.change_email_success, 
                strings.change_email_message,
                [{
                    text: `${strings.commons_btn_ok}`,
                    onPress: () => {}
                }])
            reset()
        }
    }, [props.isChangedEmail])

    useFocusEffect (
        useCallback( () => {
            console.log("Change Email Container", props.error, props.isConnected)
            if (props.error) {
                if (props.error.statusCode == 403 && props.error.signoutUser){
                    reset();
                    return;
                }
                else if (!props.isConnected){
                    Utils.getGlobalConnectionDialogIsShown().then( (result) => {
                        if(!result){
                            setTimeout(() => {
                                Alert.alert(
                                    `${strings.no_internet_connection_alert_dialog_title}`,
                                    `${strings.no_internet_connection_alert_dialog_message}`,
                                    [
                                    {
                                        text: `${strings.commons_btn_ok}`,
                                        onPress: () => {
                                            Utils.setGlobalConnectionDialogIsShown(false);
                                        }
                                    }
                                    ], { cancelable: true, onDismiss: () => Utils.setGlobalConnectionDialogIsShown(false)},
                                    
                                );
                            },1000)
                        }
                    })
                    return
                }else if (props.error.connectionError){
                    Utils.getGlobalConnectionDialogIsShown().then( (result) => {
                        if(!result){
                            Utils.setGlobalConnectionDialogIsShown(true);
                            setTimeout(() => {
                                Alert.alert(
                                    props.error.connectionError,
                                    props.error.connectionErrorMessage,
                                    [
                                    {
                                        text: `${strings.commons_btn_ok}`,
                                        onPress: () => {
                                            Utils.setGlobalConnectionDialogIsShown(false);
                                        }
                                    }
                                    ], { cancelable: true, onDismiss: () => Utils.setGlobalConnectionDialogIsShown(false)},
                                    
                                );
                            },1000)
                        }
                    })
                    return
                }

                let errorMessage = props.error.genericError
                let errorStatus =  props.error.statusCode ? `Error: ${props.error.statusCode}` : ``
                if (props.error.backendError) 
                    errorMessage = props.error.backendError

                if (props.error.connectionError) 
                    errorMessage = props.error.connectionError

                setTimeout(() => {
                    if (__DEV__){
                        Toast.show({ type: 'custom', text1: `${errorMessage} ${errorStatus}`, })
                    }else{
                        Toast.show({ type: 'custom', text1: `${errorMessage}`, })
                    }
                }, 500);
                

                
                }
            }, [props.error])
    )


    const reset = () => { 
        props.signOut()
        setTimeout(() => {
            const resetAction = CommonActions.reset({
                index: 0,
                routes: [{ name: RootScreen.MainNavigators, params: {  } }]
            });
            navigation.dispatch(resetAction);

        }, 1000)
    }


    return (
        <ChangeEmail  onCancel={onCancel}
                    newEmail={newEmail}
                    isValidEmail={isValidEmail}
                    setNewEmail={setNewEmail}
                    onChangeEmail={onChangeEmail}
                    {...props} />
    )
}

const mapStateToProps = (state: RootState) => ({
    error: ProfileSelectors.error(state),
    isLoading: ProfileSelectors.isLoading(state),
    isChangedEmail: ProfileSelectors.isChangedEmail(state),
    isConnected: state.network.isConnected,
})

const mapDispatchToProps = (dispatch: RootDispatch) => ({
    updateEmail: (newEmail: string) => dispatch(ProfileActions.updateEmail(newEmail)),
    signOut: () => dispatch(MenuActions.signOut()),
    
})

const connector = connect(mapStateToProps, mapDispatchToProps)
type Props = ConnectedProps<typeof connector>

export default connector(ChangeEmailContainer)
