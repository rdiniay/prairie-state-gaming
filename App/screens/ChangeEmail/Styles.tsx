import { PixelRatio } from 'react-native'
import ScaledSheet from 'react-native-scaled-sheet'
import { Colors, Commons } from '../../styles'

const styles = ScaledSheet.create({
    safeArea: {
        backgroundColor: Colors.red,
    },
    container: {
        flex: 1,
    }
})


const commons =  ScaledSheet.create({
    headerTouchableOpacityStyle: Commons.HeaderTouchableOpacityStyle(),
    headerTextStyle: Commons.HeaderBackButtonTextStyle(),
    leftContainerStyle:  Commons.HeaderContentLeftContainerStyle(),
    contentLogoViewStyle: Commons.ContentLogoViewStyle(),
    contentLogoStyle: Commons.ContentLogoStyle(),
    
    contentSwipePanelStyle: Commons.ContentSwipePanelStyle(),
    contentTitleStyle: Commons.ContentTitleStyle(),
    contentInnerViewStyle: Commons.InnerViewStyle(),
    contentInnerContentViewStyle: Commons.InnerContentViewStyle(),
    contentInlineContentViewInnerHeaderViewStyle: Commons.InlineContentViewInnerHeaderViewStyle(),
    contentInlineContentViewInnerHeaderTextViewStyle: Commons.InlineContentViewInnerHeaderTextViewStyle(),
    contentInlineContentViewInnerContentErrorTextViewStyle: Commons.InlineContentViewInnerContentErrorTextViewStyle(),
    contentInlineContentViewInnerContentTextInputViewStyle : {
        ...Commons.InlineContentViewInnerContentTextViewStyle(),
        fontSize: 18 / PixelRatio.getFontScale(),

    },
    inlineContentViewStyle: {
        paddingTop: 10, 
        paddingVertical: 5, 
        marginHorizontal: 8,
        justifyContent: 'center'
    },
    inlineContentTextViewStyle: Commons.InlineContentViewInnerContentTextViewStyle(),
    customHeadingTextViewStyle : { 
        fontSize: 16 / PixelRatio.getFontScale(), 
        color: Colors.black,
        fontFamily: "Raleway-Regular",
        fontWeight: 'normal',
        fontStyle: 'normal',
        lineHeight: 14.09 / PixelRatio.getFontScale(),
    },
    customContentTextInputStyle :  {
        fontFamily: "Raleway-Regular",
        fontWeight: 'normal',
        fontStyle: 'normal',
        fontSize: 12 / PixelRatio.getFontScale(),
        lineHeight: 14.09 / PixelRatio.getFontScale(),
    },
    customTouchableOpacity: { 
        padding: 10, 
        backgroundColor: Colors.red, 
        borderRadius: 20,  
        justifyContent: 'center', 
        alignItems: 'center',  
    },
    customButtonTextStyle : { 
        color: 'white', 
        fontSize: 16 / PixelRatio.getFontScale(), 
        fontFamily: 'Raleway-Bold',
        textAlign: 'center'
    }
})

const commonsAssets = {
    contentImageSource: Commons.ContentImageSource(),
}

export {
    styles,
    commons,
    commonsAssets,
}