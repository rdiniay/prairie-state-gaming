import React from 'react'
import { ConnectedProps } from 'react-redux'
import { 
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  FlatList,
  SectionList,
  Platform,
} from 'react-native'
import { Container, Header, LoadingDialog } from '../../components'
import { Colors } from '../../styles'
import styles from './Styles'
import { strings } from '../../localization/strings'

type Props = {
    onCancel: () => void,
    setNewEmail: (value: string) => void,
    onChangeEmail: () => void,
    isValidEmail: boolean,
    newEmail: string,
    isLoading: boolean,
}

const ChangeEmail = (props: Props) =>{
    const header = () => (
        <Header title={'Change your Email'} //'Prairie State Gaming'
                titleStyle={{ fontSize: 18, color: Colors.black, paddingRight: 10, }}
                left={(
                   <TouchableOpacity    onPress={props.onCancel}
                                         style={{ flexDirection: "row", alignItems: 'center', paddingLeft: 15, paddingVertical: 6, }}>
                         <Text style={{ color: Colors.red, textAlign: 'center', fontSize: 18,  }}>
                             Cancel
                         </Text>
                   </TouchableOpacity>
                )}
                leftContainerStyle={{ flex: 0.35, }}
        />
    )

    return (
        <Container  safeAreaViewTopStyle={styles.safeArea}
                    statusBarStyle={'dark'}
                    scrollViewEnable={true}
                    header={header}
                    style={styles.container} >

            <LoadingDialog visible={props.isLoading} subtitle={'Please wait...'} />

            <View style={{ flex: 1, padding: 15,  }}>
                <View style={{ paddingTop: 10, marginHorizontal: 15, }}>
                    <View style={{ flex:1, flexDirection: 'row', justifyContent: 'space-between'}}>
                        <Text style={{ flex: 1, fontSize: 18, color: Colors.black, paddingBottom: 5, }}>
                            {`Now we just need to make sure we can reach you. So we'll send and email to your address below with a code.`}
                        </Text>
                    </View>
                </View>
                <View style={{ paddingTop: 10, borderBottomColor: Colors.lightgray, borderBottomWidth: 0.5, paddingVertical: 5, marginHorizontal: 15, }}>
                    <View style={{ flex:1, flexDirection: 'row', justifyContent: 'space-between'}}>
                        <Text style={{ flex: 1, color: Colors.red, paddingBottom: 5, }}>
                            {'Email'}
                        </Text>
                        {props.newEmail === null &&
                            <Text style={{ color: Colors.red, paddingBottom: 5, }}>
                                {strings.required_text}
                            </Text>
                        }

                        {props.newEmail.length !== 0 && !props.isValidEmail &&
                            <Text style={{ color: Colors.red, paddingBottom: 5, }}>
                                {strings.invalid_email}
                            </Text>
                        }
                    </View>
                    <TextInput onChangeText={props.setNewEmail} autoCapitalize='none'  keyboardType="email-address" style={{ fontSize: 18, }}/>
                </View>
                <View style={{ flex: 1, margin: 70, marginTop: 35, marginBottom: 0, }}>
                    <TouchableOpacity style={{ padding: 10, backgroundColor: Colors.red, borderRadius: 20,  justifyContent: 'center', alignItems: 'center', }}
                                        onPress={props.onChangeEmail}>
                        <Text style={{ color: 'white', fontSize: 16, textAlign: 'center' }}>
                            Change Email
                        </Text>
                    </TouchableOpacity>
                </View> 
            </View>
              
        </Container>
    )
}

export default ChangeEmail