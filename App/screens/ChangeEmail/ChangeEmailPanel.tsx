import React, { useEffect, useState } from 'react'
import { ConnectedProps } from 'react-redux'
import { 
  View,
  TouchableOpacity,
  Image,
  FlatList,
  SectionList,
  Dimensions,
  Platform,
} from 'react-native'
import { Container, SwipeablePanel, Header, LoadingDialog, Text, TextInput } from '../../components'
import { Colors } from '../../styles'
import {styles, commons, commonsAssets} from './Styles'
import { strings } from '../../localization/strings'
import { string } from 'yargs'
const { width, height } = Dimensions.get('window');

type Props = {
    onCancel: () => void,
    setNewEmail: (value: string) => void,
    onChangeEmail: () => void,
    isValidEmail: boolean,
    newEmail: string,
    isLoading: boolean,
}

const ChangeEmail = (props: Props) =>{
    const [toggleKeyboard, setToggleKeyboard] = useState(false)

    const [isPanelActive, setIsPanelActive] = useState(true);

    const [panelProps, setPanelProps] = useState({
        fullWidth: true,
        openLarge: false,
        showCloseButton: false,
        onClose: () => null,
        // ...or any prop you want
    });

    const openPanel = () => {
        setIsPanelActive(true);
    };

    const closePanel = () => {
        setIsPanelActive(false);
    };

    const toggleUp = () => {
        setPanelProps({...panelProps, openLarge: true})
        closePanel()
        setTimeout(() => {
            openPanel()
        }, 1);
    }

    const header = () => (
        <Header style={{ backgroundColor: 'transparent', }}
                // title={'Change your PIN'} //'Prairie State Gaming'
                // titleStyle={{ fontSize: 18, color: Colors.black, paddingRight: 10, }}
                left={(
                   <TouchableOpacity    onPress={props.onCancel}
                   style={commons.headerTouchableOpacityStyle}>
                         <Text style={commons.headerTextStyle}>
                             Cancel
                         </Text>
                   </TouchableOpacity>
                )}
                // leftContainerStyle={{ flex: 0.35, }}
        />
    )

    return (
        <Container  safeAreaViewTopStyle={styles.safeArea}
                    statusBarStyle={'light'}
                    // scrollViewEnable={true}
                    header={header}
                    style={styles.container} >

            <LoadingDialog visible={props.isLoading} subtitle={'Please wait...'} />

            <View style={commons.contentLogoViewStyle}>
                <Image resizeMode={'contain'} style={commons.contentLogoStyle} source={commonsAssets.contentImageSource} />
            </View>

            <SwipeablePanel titleValue={strings.change_email_title} style={commons.contentSwipePanelStyle} {...panelProps} isActive={isPanelActive}>
                <View style={commons.contentInnerViewStyle}>
                    <View style={commons.contentInnerContentViewStyle}>  
                        <View style={{ paddingTop: 10, marginHorizontal: 15, }}>
                            <View style={commons.inlineContentViewStyle}>
                                <Text style={commons.customHeadingTextViewStyle}>
                                    {strings.change_email_heading_title}
                                </Text>
                            </View>
                        </View>
                        <View style={{ paddingTop: 10, borderBottomColor: Colors.lightgray, borderBottomWidth: 0.5, paddingVertical: 5, marginHorizontal: 15, }}>
                            <View style={{ flex:1, flexDirection: 'row', justifyContent: 'space-between'}}>
                                <Text style={{...commons.customInlineTextViewStyle, color: Colors.red}}>
                                    {strings.change_email_label}
                                </Text>
                                {props.newEmail === null &&
                                    <Text style={commons.contentInlineContentViewInnerContentErrorTextViewStyle}>
                                        {strings.required_text}
                                    </Text>
                                }

                                {props.newEmail !== null && props.newEmail.length !== 0  && !props.isValidEmail &&
                                    <Text style={commons.contentInlineContentViewInnerContentErrorTextViewStyle}>
                                        {strings.invalid_email}
                                    </Text>
                                }
                            </View>
                            <TextInput 
                                onChangeText={props.setNewEmail} 
                                autoCapitalize='none'  
                                keyboardType="email-address" 
                                style={commons.contentInlineContentViewInnerContentTextInputViewStyle}
                                autoFocus={toggleKeyboard}
                                onBlur={() => { setToggleKeyboard(false) }}
                                onFocus={() => { 
                                    toggleUp()
                                    setToggleKeyboard(true)
                                }}
                                />
                        </View>
                        <View style={{ flex: 1, margin: 70, marginTop: 35, marginBottom: 0, }}>
                            <TouchableOpacity style={commons.customTouchableOpacity}
                                                onPress={props.onChangeEmail}>
                                <Text style={commons.customButtonTextStyle}>
                                    {strings.change_email_button_text}
                                </Text>
                            </TouchableOpacity>
                        </View> 
                    </View>
                </View>
            </SwipeablePanel>
              
        </Container>
    )
}

export default ChangeEmail