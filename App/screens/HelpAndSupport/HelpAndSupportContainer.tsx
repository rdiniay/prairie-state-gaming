import React, { useCallback, useEffect, useState}  from 'react'
import {Alert} from 'react-native'
import { connect, ConnectedProps } from 'react-redux'
import { RootState, RootDispatch } from '../../redux'
import { useNavigation, useFocusEffect } from '@react-navigation/native'
import HelpAndSupport from './HelpAndSupport'
import HelpSupportAction, { HelpSupportSelectors } from '../../redux/help-support-redux'
import { strings } from '../../localization/strings'
import Utils from '../../services/shared/utils/Utils'

/**
 * Scenario: 
 * If they get passed on all the required app settings and are still not able to get check-in
 * 
 * Solution:
 * A feature button like <Please send my information>
 * A feature that would allow the user to send diagnostic information to the app team. 
 * A piece of information include
 * - Mobile device used
 * - App version
 * - Location 
 * - App settings 
 * - etc.
 * 
 * Diagnostic logs should only be streamlined and NOT collect too much information otherwise only capture important key settings. 
 * A standard of 15-20 pieces of information in a JSON format send to the email and log it to the server
 */
const SupportContainer = (props: Props) => {
    const navigation = useNavigation()
    const [processing, setProcessing] = useState(false)
    const [showAlertSuccess, setShowAlertSuccess] = useState(false)
    const onCancel = () => {
        navigation.goBack();
    }

    useFocusEffect (
        useCallback(() => {
            if(processing && props.success) {
                setProcessing(false)
                setShowAlertSuccess(true)
                props.clearSuccess()
            }
        }, [props.success])
    )

    useFocusEffect (
        useCallback(() => {
            if (processing && props.error) {
                setProcessing(false)
                let title = strings.diagnostic_error
                let errorMessage = strings.genericError
                if (props.error.backendError) 
                    errorMessage = props.error.backendError

                if (props.error.connectionError) {
                    title = strings.no_internet_connection_alert_dialog_title
                    errorMessage = strings.no_internet_connection_alert_dialog_message
                }
                    

                props.clearError()
                
                Alert.alert(
                    title, 
                    errorMessage,
                    [{
                        text: `${strings.commons_btn_ok}`,
                        onPress: () => {}
                    }])
            }
        }, [props.error])
    )

    const sendDiagnosticLogs = async () => {
        props.sendDiagnosticLogs()
        setProcessing(true)
    }

    return (
        <HelpAndSupport onCancel={onCancel}
                        onSend={sendDiagnosticLogs}
                        setShowAlertSuccess={setShowAlertSuccess}
                        showAlertSuccess={showAlertSuccess}
                        {...props}/>
    )
}

const mapStateToProps = (state: RootState) => ({
    success: HelpSupportSelectors.success(state),
    error: HelpSupportSelectors.error(state),
    isLoading: HelpSupportSelectors.isLoading(state),
    isConnected: state.network.isConnected,
})

const mapDispatchToProps = (dispatch: RootDispatch) => ({
    sendDiagnosticLogs: () => dispatch(HelpSupportAction.sendDiagnosticLogs()),
    clearSuccess: () => dispatch(HelpSupportAction.sendDiagnosticLogsSuccess(null)),
    clearError: () => dispatch(HelpSupportAction.sendDiagnosticLogsFailure(null)),
})

const connector = connect(mapStateToProps, mapDispatchToProps)
type Props = ConnectedProps<typeof connector>

export default connector(SupportContainer)
