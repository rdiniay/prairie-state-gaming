import { Dimensions } from 'react-native'
import ScaledSheet from 'react-native-scaled-sheet'
import { Colors, Commons } from '../../styles'

const styles = ScaledSheet.create({
    safeArea: {
        backgroundColor: Colors.red,
    },
    container: {
        flex: 1,
    },
    backButton: {
        flexDirection: "row", 
        alignItems: 'center', 
        paddingLeft: 15, 
        paddingVertical: 6,
    },
    backButtonText: {
        color: Colors.white, 
        textAlign: 'center', 
        fontSize: 18,
    },
    header: {
        backgroundColor: Colors.red
    },
    headerTitle: {
        fontSize: 18, 
        color: Colors.white, 
        padding: 4,
    },
    panelContent: {
        backgroundColor: '#F7F5EB', 
        height: '100%',
    },
    rowContent: {
        margin: 15, 
        marginTop: 10, 
        borderColor: Colors.lightgray, 
        borderWidth: 0.5, 
        borderRadius: 15, 
        paddingVertical: 15, 
        paddingHorizontal: 10, 
        backgroundColor: 'white',
    },
    loadingView: {
        flex:0.7, 
        paddingTop: 150, 
        justifyContent: 'center', 
    },
    dialogTitle: {
        color: Colors.black, 
    },
    dialogContent: {
        textAlign: 'left',
        color: Colors.black, 
    }
})

const commons =  ScaledSheet.create({
    headerTouchableOpacityStyle: Commons.HeaderTouchableOpacityStyle(),
    headerTextStyle: Commons.HeaderBackButtonTextStyle(),
    leftContainerStyle:  Commons.HeaderContentLeftContainerStyle(),
    contentLogoViewStyle: Commons.ContentLogoViewStyle(),
    contentLogoStyle: Commons.ContentLogoStyle(),
    
    contentSwipePanelStyle: Commons.ContentSwipePanelStyle(),
    contentTitleStyle: Commons.ContentTitleStyle(),
    contentInnerViewStyle: Commons.InnerViewStyle(),
    contentInnerContentViewStyle: Commons.InnerContentViewStyle(),
    contentInlineContentViewInnerHeaderViewStyle: Commons.InlineContentViewInnerHeaderViewStyle(),
    contentInlineContentViewInnerHeaderTextViewStyle: Commons.InlineContentViewInnerHeaderTextViewStyle(),
    inlineContentTextViewStyle: Commons.InlineContentViewInnerContentTextViewStyle(),
})

const commonsAssets = {
    contentImageSource: Commons.ContentImageSource(),
}

export {
    styles,
    commons,
    commonsAssets,
}