import React from 'react'
import {
    View,
    TouchableOpacity,
    StyleSheet,
    ActivityIndicator
} from 'react-native'
import { Container, Header, Text, Button } from '../../components'
import { Colors , } from '../../styles'
import {styles, commons, commonsAssets} from './Styles'
import { strings } from '../../localization/strings'
import Dialog from 'react-native-dialog'

type Props = {
    onCancel: () => void,
    onSend: () => void,
    setShowAlertSuccess: (value: boolean) => void,
    success: any,
    isLoading: boolean,
    showAlertSuccess: boolean,
}
  
const Support = (props: Props) =>{
    const header = () => (
        <Header style={styles.header}
                title={strings.diagnostic_help_and_support}
                titleStyle={styles.headerTitle}
                left={
                (
                   <TouchableOpacity   style={styles.backButton} onPress={props.onCancel}>
                         <Text style={styles.backButtonText}>
                             Cancel
                         </Text>
                   </TouchableOpacity>
                )}
                leftContainerStyle={{ flex: .35 }}
        />
    )

    return (
        <Container      safeAreaViewTopStyle={styles.safeArea}
                        statusBarStyle={'light'}
                        header={header}
                        style={styles.container} >

        <View style={styles.panelContent}>
            {!props.isLoading &&
                <View>
                        <View style={{paddingTop: 20}}/>
                        <View style={styles.rowContent}>
                            <View>
                                <View style={{  paddingBottom: 10, paddingHorizontal: 10, }}>
                                    <Text style={{ color: Colors.black, fontSize: 16, fontWeight: 'bold' }}>
                                        {strings.diagnostic_having_issues_checking_in}
                                    </Text>
                                </View>
                                <View style={{paddingHorizontal: 10, paddingBottom: 20}}>
                                    <Text style={{ color: Colors.lightgray, fontSize: 14 }}>
                                        {strings.diagnostic_instruction_subtext}
                                    </Text>
                                </View>
                                <Button style={{ backgroundColor: Colors.red, 
                                                padding: 15, borderRadius: 10, borderWidth: 2, 
                                                borderColor: Colors.white }}
                                                titleStyle={{ fontWeight: 'bold', fontSize: 16 }}
                                                title={strings.diagnostic_send_button}
                                                onPress={props.onSend}/>
                            </View>
                        </View>
                </View>
            }
            {props.isLoading &&
                <View style={styles.loadingView}>
                    <ActivityIndicator size="large" color={'gray'}/>
                </View>
            }
            <View>
                <Dialog.Container visible={props.showAlertSuccess}>
                    <Dialog.Title style={styles.dialogTitle}>{strings.diagnostic_success}</Dialog.Title>
                    <Dialog.Description style={styles.dialogContent}>
                        {strings.diagnostic_success_msg}
                    </Dialog.Description>
                    <Dialog.Button onPress={() => { props.setShowAlertSuccess(false) }} label="Ok" />
                </Dialog.Container>
            </View>
        
        </View>
        </Container>
    )
}

export default Support
