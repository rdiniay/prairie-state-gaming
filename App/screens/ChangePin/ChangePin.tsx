import React from 'react'
import { ConnectedProps } from 'react-redux'
import { 
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  FlatList,
  SectionList
} from 'react-native'
import { Container, Header, LoadingDialog } from '../../components'
import { Colors } from '../../styles'
import styles from './Styles'

type Props = {
    onCancel: () => void,
    setCurrentPin: (value: string) => void,
    setNewPin: (value: string) => void,
    onChangePin: () => void,

    currentPin: string,
    newPin: string,
    isLoading: boolean,
}

const ChangePin = (props: Props) =>{
    const header = () => (
        <Header title={'Change your PIN'} //'Prairie State Gaming'
                titleStyle={{ fontSize: 18, color: Colors.black, paddingRight: 10, }}
                left={(
                   <TouchableOpacity    onPress={props.onCancel}
                                         style={{ flexDirection: "row", alignItems: 'center', paddingLeft: 15, paddingVertical: 6, }}>
                         <Text style={{ color: Colors.red, textAlign: 'center', fontSize: 18,  }}>
                             Cancel
                         </Text>
                   </TouchableOpacity>
                )}
                leftContainerStyle={{ flex: 0.35, }}
        />
    )

    return (
        <Container  safeAreaViewTopStyle={styles.safeArea}
                    statusBarStyle={'dark'}
                    scrollViewEnable={true}
                    header={header}
                    style={styles.container} >

            <LoadingDialog visible={props.isLoading} subtitle={'Please wait...'} />

            <View style={{ flex: 1, padding: 15,  }}>
                <View style={{ paddingTop: 10, borderBottomColor: Colors.lightgray, borderBottomWidth: 0.5, paddingVertical: 5, marginHorizontal: 15, }}>
                    <View style={{ flex:1, flexDirection: 'row', justifyContent: 'space-between'}}>
                        <Text style={{ flex: 1, color: Colors.red, paddingBottom: 5, }}>
                            {'Current PIN'}
                        </Text>
                        {props.currentPin === null &&
                            <Text style={{ color: Colors.red, paddingBottom: 5, }}>
                                Needed
                            </Text>
                        }
                    </View>
                    <TextInput maxLength={4} onChangeText={props.setCurrentPin} autoCapitalize='none' keyboardType='number-pad' style={{ fontSize: 18, }}/>
                </View>
                <View style={{ paddingTop: 10, borderBottomColor: Colors.lightgray, borderBottomWidth: 0.5, paddingVertical: 5, marginHorizontal: 15, }}>
                    <View style={{ flex:1, flexDirection: 'row', justifyContent: 'space-between'}}>
                        <Text style={{ flex: 1, color: Colors.red, paddingBottom: 5, }}>
                            {'New 4 digit PIN'}
                        </Text>
                        {props.newPin === null &&
                            <Text style={{ color: Colors.red, paddingBottom: 5, }}>
                                Needed
                            </Text>
                        }
                    </View>
                    <TextInput  maxLength={4} onChangeText={props.setNewPin} autoCapitalize='none' keyboardType='number-pad' style={{ fontSize: 18, }}/>
                </View>
                <View style={{ flex: 1, margin: 70, marginTop: 35, marginBottom: 0, }}>
                    <TouchableOpacity style={{ padding: 10, backgroundColor: Colors.red, borderRadius: 20,  justifyContent: 'center', alignItems: 'center', }}
                                        onPress={props.onChangePin}>
                        <Text style={{ color: 'white', fontSize: 16, textAlign: 'center' }}>
                            Change PIN
                        </Text>
                    </TouchableOpacity>
                </View> 
            </View>
              
        </Container>
    )
}

export default ChangePin