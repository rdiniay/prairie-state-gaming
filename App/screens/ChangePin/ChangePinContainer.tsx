import React, { useEffect, useState, useCallback } from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { RootState, RootDispatch } from '../../redux'
import {StackActions ,useNavigation, DrawerActions, CommonActions, useFocusEffect } from '@react-navigation/native'
import { Alert } from 'react-native'
import ChangePin from './ChangePinPanel'
import ProfileActions, { ProfileSelectors } from '../../redux/profile-redux'
import Toast from 'react-native-toast-message';

import MenuActions, { MenuSelectors } from '../../redux/menu-redux'
import {Screen as RootScreen} from '../../navigation/routes/rootRoutes'

import Utils from '../../services/shared/utils/Utils'
import { strings } from '../../localization/strings'
import { env } from '../../config'

const ChangePinContainer = (props: Props) => {
    const navigation = useNavigation()
    const [currentPin, setCurrentPin] = useState('')
    const [newPin, setNewPin] = useState('')

    const onCancel = () => {
        navigation.goBack();
    }

    const onChangePin = () => {
        if (currentPin && newPin) {
            const params: any = { currentPin, newPin }
            props.changePin(params)
            return
        } 
        if (!currentPin)
            setCurrentPin(null)
        if (!newPin)
            setNewPin(null)
    }

    useEffect(() => {
        if (props.isChangedPin) {
            setTimeout(() => {
                navigation.goBack();
            }, 500);
        }

    }, [props.isChangedPin])

    useFocusEffect (
        useCallback(() => {
            if (props.error) {
                if (props.error.statusCode == 403 && props.error.signoutUser){
                    reset();
                    return;
                }
                else if (!props.isConnected){
                    Utils.getGlobalConnectionDialogIsShown().then( (result) => {
                        if(!result){
                            setTimeout(() => {
                                Alert.alert(
                                    `${strings.no_internet_connection_alert_dialog_title}`,
                                    `${strings.no_internet_connection_alert_dialog_message}`,
                                    [
                                    {
                                        text: `${strings.commons_btn_ok}`,
                                        onPress: () => {
                                            Utils.setGlobalConnectionDialogIsShown(false);
                                        }
                                    }
                                    ], { cancelable: true, onDismiss: () => Utils.setGlobalConnectionDialogIsShown(false)},
                                    
                                );
                            },1000)
                        }
                    })
                    return
                }else if (props.error.connectionError){
                    Utils.getGlobalConnectionDialogIsShown().then( (result) => {
                        if(!result){
                            Utils.setGlobalConnectionDialogIsShown(true);
                            setTimeout(() => {
                                Alert.alert(
                                    props.error.connectionError,
                                    props.error.connectionErrorMessage,
                                    [
                                    {
                                        text: `${strings.commons_btn_ok}`,
                                        onPress: () => {
                                            Utils.setGlobalConnectionDialogIsShown(false);
                                        }
                                    }
                                    ], { cancelable: true, onDismiss: () => Utils.setGlobalConnectionDialogIsShown(false)},
                                    
                                );
                            },1000)
                        }
                    })
                    return
                }
                let errorMessage = props.error.genericError
                let errorStatus =  props.error.statusCode ? `Error: ${props.error.statusCode}` : ``
                if (props.error.backendError) 
                    errorMessage = props.error.backendError
    
                if (props.error.connectionError) 
                    errorMessage = props.error.connectionError
    
                setTimeout(() => {
                    if (__DEV__){
                        Toast.show({ type: 'custom', text1: `${errorMessage} ${errorStatus}`, })
                    }else{
                        Toast.show({ type: 'custom', text1: `${errorMessage}`, })
                    }
                }, 500);
            }
        }, [props.error])
    )
    
    const reset = () => { 
        props.signOut()
        setTimeout(() => {
            const resetAction = CommonActions.reset({
                index: 0,
                routes: [{ name: RootScreen.MainNavigators, params: {  } }]
            });
            navigation.dispatch(resetAction);

        }, 1000)
    }

    return (
        <ChangePin  onCancel={onCancel}
                    currentPin={currentPin}
                    newPin={newPin}
                    setCurrentPin={setCurrentPin}
                    setNewPin={setNewPin}
                    onChangePin={onChangePin}
                    {...props} />
    )
}

const mapStateToProps = (state: RootState) => ({
    isChangedPin: ProfileSelectors.isChangedPin(state),
    isLoading: ProfileSelectors.isLoading(state),
    error: ProfileSelectors.error(state),
    isConnected: state.network.isConnected,
})

const mapDispatchToProps = (dispatch: RootDispatch) => ({
    changePin: (params: any) => dispatch(ProfileActions.changePin(params)),
    signOut: () => dispatch(MenuActions.signOut()),
})

const connector = connect(mapStateToProps, mapDispatchToProps)
type Props = ConnectedProps<typeof connector>

export default connector(ChangePinContainer)
