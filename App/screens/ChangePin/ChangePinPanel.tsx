import React, { useEffect, useState } from 'react'
import { ConnectedProps } from 'react-redux'
import { 
  View,
  TouchableOpacity,
  Image,
  FlatList,
  Dimensions,
  SectionList,
  PixelRatio
} from 'react-native'
import { Container,SwipeablePanel, Header, LoadingDialog, Text, TextInput } from '../../components'
import { Colors } from '../../styles'
import {styles, commons, commonsAssets} from './Styles'
import { strings } from '../../localization/strings'
const { width, height } = Dimensions.get('window');

type Props = {
    onCancel: () => void,
    setCurrentPin: (value: string) => void,
    setNewPin: (value: string) => void,
    onChangePin: () => void,

    currentPin: string,
    newPin: string,
    isLoading: boolean,
}

const ChangePin = (props: Props) =>{
    const [toggleKeyboard, setToggleKeyboard] = useState(false)

    const [isPanelActive, setIsPanelActive] = useState(true);

    const [panelProps, setPanelProps] = useState({
        fullWidth: true,
        openLarge: false,
        showCloseButton: false,
        onClose: () => null,
        // ...or any prop you want
    });

    const openPanel = () => {
        setIsPanelActive(true);
    };

    const closePanel = () => {
        setIsPanelActive(false);
    };

    const toggleUp = () => {
        setPanelProps({...panelProps, openLarge: true})
        closePanel()
        setTimeout(() => {
            openPanel()
        }, 1);
    }

    const header = () => (
        <Header style={{ backgroundColor: 'transparent', }}
                // title={'Change your PIN'} //'Prairie State Gaming'
                // titleStyle={{ fontSize: 18, color: Colors.black, paddingRight: 10, }}
                left={(
                   <TouchableOpacity    onPress={props.onCancel}
                   style={commons.headerTouchableOpacityStyle}>
                         <Text style={commons.headerTextStyle}>
                             Cancel
                         </Text>
                   </TouchableOpacity>
                )}
                // leftContainerStyle={{ flex: 0.35, }}
        />
    )

    return (
        <Container  safeAreaViewTopStyle={styles.safeArea}
                    statusBarStyle={'light'}
                    // scrollViewEnable={true}
                    header={header}
                    style={styles.container} >

            <LoadingDialog visible={props.isLoading} subtitle={'Please wait...'} />

            <View style={commons.contentLogoViewStyle}>
                <Image resizeMode={'contain'} style={commons.contentLogoStyle} source={commonsAssets.contentImageSource} />
            </View>
            <SwipeablePanel titleValue={strings.change_pin_title} style={commons.contentSwipePanelStyle} {...panelProps} isActive={isPanelActive}>
                <View style={commons.contentInnerViewStyle}>
                    <View style={commons.contentInnerContentViewStyle}> 
                        <View style={commons.inlineContentViewStyle}>
                            <View style={commons.customInlineViewStyle}>
                                <Text style={commons.customInlineTextViewStyle}>
                                    {strings.change_pin_header_title_current_pin}
                                </Text>
                                {props.currentPin === null &&
                                    <Text style={{...commons.contentInlineContentViewInnerContentErrorTextViewStyle}}>
                                        {strings.required_text}
                                    </Text>
                                }
                            </View>
                            <TextInput 
                                maxLength={4} 
                                onChangeText={props.setCurrentPin} 
                                autoCapitalize='none' 
                                keyboardType='number-pad' 
                                style={{ ...commons.contentInlineContentViewInnerContentTextInputViewStyle }}
                                autoFocus={toggleKeyboard}
                                onBlur={() => { setToggleKeyboard(false) }}
                                onFocus={() => { 
                                    toggleUp()
                                    setToggleKeyboard(true)
                                }}
                                />
                        </View>
                        <View style={commons.inlineContentViewStyle}>
                            <View style={commons.customInlineViewStyle}>
                                <Text style={commons.customInlineTextViewStyle}>
                                    {strings.change_pin_header_title_new_pin}
                                </Text>
                                {props.newPin === null &&
                                    <Text style={{...commons.contentInlineContentViewInnerContentErrorTextViewStyle}}>
                                         {strings.required_text}
                                    </Text>
                                }
                            </View>
                            <TextInput  
                                maxLength={4} 
                                onChangeText={props.setNewPin} 
                                autoCapitalize='none' 
                                keyboardType='number-pad' 
                                style={{ ...commons.contentInlineContentViewInnerContentTextInputViewStyle }}
                                autoFocus={toggleKeyboard}
                                onBlur={() => { setToggleKeyboard(false) }}
                                onFocus={() => { 
                                    toggleUp()
                                    setToggleKeyboard(true)
                                }}
                                />
                        </View>
                        <View style={commons.customViewStyle}>
                            <TouchableOpacity style={commons.customTouchableOpacity}
                                                onPress={props.onChangePin}>
                                <Text style={commons.customButtonTextStyle}>
                                    {strings.change_pin_button_text}
                                </Text>
                            </TouchableOpacity>
                        </View> 
                    </View>
                </View>
            </SwipeablePanel>
              
        </Container>
    )
}

export default ChangePin