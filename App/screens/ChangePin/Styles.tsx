import { PixelRatio } from 'react-native'
import ScaledSheet from 'react-native-scaled-sheet'
import { Colors, Commons } from '../../styles'

const styles = ScaledSheet.create({
    safeArea: {
        backgroundColor: Colors.red,
    },
    container: {
        flex: 1,
    }
})

const commons =  ScaledSheet.create({
    headerTouchableOpacityStyle: Commons.HeaderTouchableOpacityStyle(),
    headerTextStyle: Commons.HeaderBackButtonTextStyle(),
    leftContainerStyle:  Commons.HeaderContentLeftContainerStyle(),
    contentLogoViewStyle: Commons.ContentLogoViewStyle(),
    contentLogoStyle: Commons.ContentLogoStyle(),
    
    contentSwipePanelStyle: Commons.ContentSwipePanelStyle(),
    contentTitleStyle: Commons.ContentTitleStyle(),
    contentInnerViewStyle: Commons.InnerViewStyle(),
    contentInnerContentViewStyle: Commons.InnerContentViewStyle(),
    contentInlineContentViewInnerHeaderViewStyle: Commons.InlineContentViewInnerHeaderViewStyle(),
    contentInlineContentViewInnerHeaderTextViewStyle: Commons.InlineContentViewInnerHeaderTextViewStyle(),
    inlineContentViewStyle: {
        paddingTop: 10, 
        borderBottomColor: Colors.darkgray, 
        borderBottomWidth: 0.5, 
        paddingVertical: 5, 
        marginHorizontal: 8,
    },
    inlineContentTextViewStyle: Commons.InlineContentViewInnerContentTextViewStyle(),
    customInlineViewStyle: { 
        flex:1, 
        flexDirection: 'row', 
        justifyContent: 'space-between',
    },
    customInlineTextViewStyle: { 
        flex: 1,
        color: Colors.red, 
        paddingBottom: 5,
        fontFamily: 'Raleway-Regular',
        fontSize: 12 / PixelRatio.getFontScale(),
    },
    contentInlineContentViewInnerContentErrorTextViewStyle: Commons.InlineContentViewInnerContentErrorTextViewStyle(),
    contentInlineContentViewInnerContentTextInputViewStyle : {
        ...Commons.InlineContentViewInnerContentTextViewStyle(),
        fontSize: 18 / PixelRatio.getFontScale(),

    },
    customViewStyle : { 
        flex: 1, 
        margin: 70, 
        marginTop: 35, 
        marginBottom: 0, 
    },

    customTouchableOpacity: { 
        padding: 10, 
        backgroundColor: Colors.red, 
        borderRadius: 20,  
        justifyContent: 'center', 
        alignItems: 'center', 
    },

    customButtonTextStyle : { 
        color: 'white', 
        fontSize: 16 / PixelRatio.getFontScale(), 
        fontFamily: 'Raleway-Bold',
        textAlign: 'center'
    }
})

const commonsAssets = {
    contentImageSource: Commons.ContentImageSource(),
}

export {
    styles,
    commons,
    commonsAssets,
}