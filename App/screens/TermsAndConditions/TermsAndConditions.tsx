import React from 'react'
import { ConnectedProps } from 'react-redux'
import { 
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  FlatList,
  SectionList,
  ActivityIndicator,
  Dimensions,
} from 'react-native'
import { Container, Header } from '../../components'
import { Colors } from '../../styles'
import styles from './Styles'
import { ScrollView } from 'react-native-gesture-handler'
import { WebView } from 'react-native-webview';
const { width, height } = Dimensions.get('window');
import { Rules, TOS } from '../../models'
import {strings} from "../../localization/strings";

type Props = {
    onCancel: () => void,
    TOS: TOS,
    isLoading: boolean,
}

const TermsAndConditions = (props: Props) =>{
    const header = () => (
        <Header title={strings.tos_title} //'Prairie State Gaming'
                titleStyle={{ fontSize: 18, color: Colors.black, paddingRight: 10, }}
                left={(
                   <TouchableOpacity    onPress={props.onCancel}
                                         style={{ flexDirection: "row", alignItems: 'center', paddingLeft: 15, paddingVertical: 6, }}>
                         <Text style={{ color: Colors.red, textAlign: 'center', fontSize: 18,  }}>
                             Cancel
                         </Text>
                   </TouchableOpacity>
                )}
                leftContainerStyle={{ flex: 0.35, }}
        />
    )

    return (
        <Container      safeAreaViewTopStyle={styles.safeArea}
                        statusBarStyle={'dark'}
                        // scrollViewEnable={true}
                        header={header}
                        style={styles.container} >
            {props.isLoading &&
                <View style={{ flex:0.7, justifyContent: 'center', }}>
                    <ActivityIndicator size="large" color={'gray'}/>
                </View>
            }
            {!props.isLoading &&
                <View style={{ flex:1, }}>
                    <WebView
                        style={{ backgroundColor: Colors.whiteOpacity}}
                        originWhitelist={['*']}
                        source={{ html: `<html><head><meta name="viewport" content="width=device-width, initial-scale=0.9"></head>
                                            <body style="padding: 15; ">
                                               ${props.TOS ? props.TOS.rules.value : '<p/>'}
                                            </body>
                                        </html>` }}
                    />
                    
                </View>
            }
              
        </Container>
    )
}

export default TermsAndConditions