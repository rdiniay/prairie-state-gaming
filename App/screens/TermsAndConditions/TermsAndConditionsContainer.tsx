import React, { useEffect, useState } from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { RootState, RootDispatch } from '../../redux'
import {StackActions ,useNavigation, DrawerActions } from '@react-navigation/native'
import TermsAndConditions from './TermsAndConditionsPanel'
import TOSActions, { TOSSelectors } from '../../redux/tos-redux'

const TermsAndConditionsContainer = (props: Props) => {
    const navigation = useNavigation()

    const onCancel = () => {
        navigation.goBack();
    }

    useEffect(() => {
        props.getTOSContent()
    }, [])
    
    return (
        <TermsAndConditions   onCancel={onCancel}
                        TOS={props.tosContent}
                        {...props} />
    )
}

const mapStateToProps = (state: RootState) => ({
    tosContent: TOSSelectors.tosContent(state),
    isLoading: TOSSelectors.isLoading(state),
})

const mapDispatchToProps = (dispatch: RootDispatch) => ({
    getTOSContent: () => dispatch(TOSActions.getTOSContent()),
})

const connector = connect(mapStateToProps, mapDispatchToProps)
type Props = ConnectedProps<typeof connector>

export default connector(TermsAndConditionsContainer)
