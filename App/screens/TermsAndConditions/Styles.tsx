import { PixelRatio, Dimensions } from 'react-native'
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import ScaledSheet from 'react-native-scaled-sheet'
import { Colors, Commons } from '../../styles'
const { width, height } = Dimensions.get('window');

const styles = ScaledSheet.create({
    safeArea: {
        backgroundColor: Colors.red,
    },
    container: {
        flex: 1,
    },
    imagePanelMarginTopWithPhoneNumber: { 
        ...Commons.ContentLogoViewStyle(),
        marginTop: (height*.120) + useSafeAreaInsets().top,
    },
    imagePanelMarginTopWithoutPhoneNumber: { 
        ...Commons.ContentLogoViewStyle(),
        marginTop: (height*.140) + useSafeAreaInsets().top,
    },
    loadingView: {
        flex:0.7, 
        paddingTop: 150, 
        justifyContent: 'center', 
    }
})

const commons =  ScaledSheet.create({
    headerTouchableOpacityStyle: Commons.HeaderTouchableOpacityStyle(),
    headerTextStyle: Commons.HeaderBackButtonTextStyle(),
    leftContainerStyle:  Commons.HeaderContentLeftContainerStyle(),
    contentLogoViewStyle: Commons.ContentLogoViewStyle(),
    contentLogoStyle: Commons.ContentLogoStyle(),
    
    contentSwipePanelStyle: Commons.ContentSwipePanelStyle(),
    contentTitleStyle: Commons.ContentTitleStyle(),
    contentInnerViewStyle: Commons.InnerViewStyle(),
    contentInnerContentViewStyle: Commons.InnerContentViewStyle(),
    contentInlineContentViewInnerHeaderViewStyle: Commons.InlineContentViewInnerHeaderViewStyle(),
    contentInlineContentViewInnerHeaderTextViewStyle: Commons.InlineContentViewInnerHeaderTextViewStyle(),
    inlineContentTextViewStyle: Commons.InlineContentViewInnerContentTextViewStyle(),
    contentInlineWebViewTextStyle: { 
        // height: height*.65, 
        flex: 1, 
        backgroundColor: Colors.whiteOpacity, 
        fontFamily:'Raleway',
        fontSize: 12 / PixelRatio.getFontScale(),
    },
    
    contentHeaderTextViewStyle: Commons.ContentHeaderTextViewStyle(),
    
    inlineContentViewStyle: {
        paddingTop: 10, 
        borderBottomColor: Colors.darkgray, 
        borderBottomWidth: 0.5, 
        paddingVertical: 5, 
        marginHorizontal: 8,
    },
    
    contentInlineContextTextInputStyle : {
        padding: 8, 
        marginHorizontal: 7, 
        fontSize: 12 / PixelRatio.getFontScale(), 
        fontFamily: 'Raleway-Regular'
    },
    contentInlineClickableTextViewStyle :  { 
        marginHorizontal: 15, 
        color: Colors.black, 
        paddingVertical: 8,
        fontFamily: 'Raleway-Regular',
        fontSize: 12 / PixelRatio.getFontScale(), 
    },
})

const commonsAssets = {
    contentImageSource: Commons.ContentImageSource(),
}

export {
    styles,
    commons,
    commonsAssets,
}