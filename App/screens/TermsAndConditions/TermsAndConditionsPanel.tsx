import React, { useEffect, useState } from 'react'
import { ConnectedProps } from 'react-redux'
import { 
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  FlatList,
  SectionList,
  ActivityIndicator,
  Dimensions,
} from 'react-native'
import { Container, SwipeablePanel, Header } from '../../components'
import { Colors } from '../../styles'
import {styles, commons, commonsAssets} from './Styles'
import { ScrollView } from 'react-native-gesture-handler'
import RenderHtml from 'react-native-render-html';
const { width, height } = Dimensions.get('window');
import { Rules, TOS } from '../../models'
import {strings} from "../../localization/strings";

type Props = {
    onCancel: () => void,
    TOS: TOS,
    isLoading: boolean,
}

const TermsAndConditions = (props: Props) =>{
    const [panelProps, setPanelProps] = useState({
        fullWidth: true,
        openLarge: false,
        showCloseButton: false,
        onClose: () => null,
        // ...or any prop you want
    });

    const header = () => (
        <Header style={{ backgroundColor: 'transparent', }}
                // title={strings.tos_title} //'Prairie State Gaming'
                // titleStyle={{ fontSize: 18, color: Colors.black, paddingRight: 10, }}
                left={(
                   <TouchableOpacity    onPress={props.onCancel}
                                         style={{ flexDirection: "row", alignItems: 'center', paddingLeft: 15, paddingVertical: 6, }}>
                         <Text style={{ color: Colors.white, textAlign: 'center', fontSize: 18,  }}>
                             Cancel
                         </Text>
                   </TouchableOpacity>
                )}
                // leftContainerStyle={{ flex: 0.35, }}
        />
    )

    return (
        <Container      safeAreaViewTopStyle={styles.safeArea}
                        statusBarStyle={'light'}
                        // scrollViewEnable={true}
                        header={header}
                        style={styles.container} >

            <View style={{ marginTop: height*.125, height: 80 }}>
                <Image resizeMode={'contain'} style={{width:200,}} source={require('../../assets/new_logo_text.png')} />
            </View>

            <SwipeablePanel titleValue={'terms of service'} style={{backgroundColor: '#E6DDCE',}} {...panelProps} isActive={true}>
                <View style={commons.contentInnerViewStyle}>
                        {!props.isLoading &&
                            <View style={commons.contentInnerContentViewStyle}>
                                
                                <View  style={commons.contentInlineWebViewTextStyle}>
                                    <RenderHtml
                                        contentWidth={width}
                                        source={{ html: `<html><head><meta name="viewport" content="width=device-width, initial-scale=1"></head>
                                                            <body style="padding: 5; ">
                                                                <p>${props.TOS ? props.TOS.rules.value : '<p/>'}
                                                            </body>
                                                        </html>` }}
                                    />
                                    
                                </View>
                            </View>
                        }
                        {props.isLoading &&
                            <View style={styles.loadingView}>
                                <ActivityIndicator size="large" color={'gray'}/>
                            </View>
                        }
                    </View>
            
            </SwipeablePanel>
              
        </Container>
    )
}

export default TermsAndConditions