import React, { useRef, useEffect, useState, useCallback } from 'react'
import { Alert, Platform, AppState, Linking } from 'react-native'
import { connect, ConnectedProps, } from 'react-redux'
import { RootState, RootDispatch } from '../../redux'
import {StackActions ,useNavigation, DrawerActions, CommonActions, useFocusEffect } from '@react-navigation/native'
import DashboardActions, { DashboardSelectors } from '../../redux/dashboard-redux'
import { AuthSelectors } from '../../redux/auth-redux'
import MobileActions, { MobileSelectors } from '../../redux/mobile-redux'
import BankActions, { BankSelectors } from '../../redux/bank-redux'
import DrawingActions, { DrawingSelectors } from '../../redux/drawing-redux'
import LocationActions, { LocationSelectors } from '../../redux/location-redux'
import { MessagingSelectors } from '../../redux/messaging-redux'
import MenuActions, { MenuSelectors } from '../../redux/menu-redux'
import { AppStateSelectors } from '../../redux/app-state-redux'
import { ProfileSelectors } from '../../redux/profile-redux'
import Geocoder from 'react-native-geocoding'
import Toast from 'react-native-toast-message';
import { Screen as MainScreen } from '../../navigation/routes/mainRoutes'
import { Screen as RootScreen } from '../../navigation/routes/rootRoutes'
import { LocationPermissionStatus, Error, Promotion, Bank, Entry, Drawing, Location, LocationCheckin, Beacon, Coordinate } from '../../models'
import { strings } from '../../localization/strings'
import { PERMISSIONS, request, openSettings, requestMultiple, checkMultiple, checkLocationAccuracy } from 'react-native-permissions'
import Utils  from '../../services/shared/utils/Utils'
import { env } from '../../config'
import crashlytics from '@react-native-firebase/crashlytics';
import Dashboard from './Dashboard'
import AndroidOpenSettings from 'react-native-android-open-settings';
import BluetoothStateManager from 'react-native-bluetooth-state-manager';
import { getBrand, getSystemVersion  } from 'react-native-device-info'
import { globals } from '../../constants'
import notifee from '@notifee/react-native'
import Immutable from 'seamless-immutable'
import BeaconManagerActions, { BeaconManagerSelectors} from '../../redux/beacon-manager-redux'
import BackgroundGeolocation, { Geofence } from "react-native-background-geolocation";
import PreparationActions, { PreparationSelectors } from '../../redux/preparation-redux'
import HelpSupportAction, { HelpSupportSelectors } from '../../redux/help-support-redux'

const DashboardContainer = (props: Props) =>  {
    const navigation = useNavigation()
    const [shouldVerify, setShouldVerify] = useState(false)
    const [verificationCode, setVerificationCode] = useState('')
    const [nearbyDevicesPermission, setNearbyDevicesPermission] = useState(true)
    const [bluetoothPermission, setBluetoothPermission] = useState(true)
    const [locationPermission, setLocationPermission] = useState(true)
    const [locationPermissionNever, setLocationPermissionNever] = useState(false)
    const [locationPermissionWhileUsing, setLocationPermissionWhileUsing] = useState(false)
    const [isDashboardLoading, setIsDashboardLoading] = useState(false)
    const [sendingDiagnostic, setSendingDiagnostic] = useState(false)

    useEffect(() => {
        // SHOW LOADING FOR THE FIRST TIME OF THE APP IS OPENED
        const isSyncing = (props.isBankLoading || props.isDrawingLoading || props.isLocationLoading)
        const currentPromotions = [...getLocalPromotions(), getMonthlyPromotion()]
        const entriesIsLoading = 0 === currentPromotions.filter(item => item.entry).length
        setIsDashboardLoading(entriesIsLoading && isSyncing)
    }, [props.isLocationLoading, props.isDrawingLoading, props.isBankLoading])

    useEffect(() => {
        const apiKey = Platform.OS === 'ios' ? 'AIzaSyDNiG0mG4z2d-JcXgCElRDtliQmZkJSXrI' : 'AIzaSyDm7SLurt6cQbO35Pra1pUFjv8-cgE0N4k'
        Geocoder.init(apiKey)
    }, [])

 // Use focus effect to allow function to run when screen is focused
 useFocusEffect (
    useCallback(() => {
        console.log("DashboardContainer: ", props.error)
        let error: Error = props.errorLocation ?? props.errorDrawing ?? props.errorBank ?? null
        if (error) {
            if (error.statusCode == 403 && error.signoutUser){
                reset();
                return
            }
        }
    }, [props.errorLocation, props.errorBank, props.errorDrawing])
)

    useFocusEffect (
        useCallback(() => {
            console.log("DashboardContainer: ", props.error)
            if (props.error) {
                if (!props.isConnected){
                    Utils.getGlobalConnectionDialogIsShown().then( (result) => {
                        if(!result){
                            Utils.setGlobalConnectionDialogIsShown(true);
                            setTimeout(() => {
                                Alert.alert(
                                    `${strings.no_internet_connection_alert_dialog_title}`,
                                    `${strings.no_internet_connection_alert_dialog_message}`,
                                    [
                                    {
                                        text: `${strings.commons_btn_ok}`,
                                        onPress: () => {
                                            Utils.setGlobalConnectionDialogIsShown(false);
                                        }
                                    }
                                    ], { cancelable: true, onDismiss: () => Utils.setGlobalConnectionDialogIsShown(false)},

                                );
                            },1000)

                        }
                    })
                    return
                }else if (props.error.connectionError){
                    Utils.getGlobalConnectionDialogIsShown().then( (result) => {
                        if(!result){
                            Utils.setGlobalConnectionDialogIsShown(true);
                            setTimeout(() => {
                                Alert.alert(
                                    props.error.connectionError,
                                    props.error.connectionErrorMessage,
                                    [
                                    {
                                        text: `${strings.commons_btn_ok}`,
                                        onPress: () => {
                                            Utils.setGlobalConnectionDialogIsShown(false);
                                        }
                                    }
                                    ], { cancelable: true, onDismiss: () => Utils.setGlobalConnectionDialogIsShown(false)},

                                );
                            },1000)
                        }
                    })
                    return
                }

                let errorMessage = props.error.genericError
                let errorStatus =  props.error.statusCode ? `Error: ${props.error.statusCode}` : ``
                if (props.error.backendError)
                    errorMessage = props.error.backendError

                if (props.error.connectionError)
                    errorMessage = props.error.connectionError

                setTimeout(() => {
                    if (__DEV__){
                        Toast.show({ type: 'custom', text1: `${errorMessage} ${errorStatus}`, })
                    }else{
                        Toast.show({ type: 'custom', text1: `${errorMessage}`, })
                    }
                    if (props.error.api === 'pendingEmail/verification')
                        showVerifyEmail()
                }, 500);
            }
        }, [props.error])
    )

    const reset = () => {
        props.signOut()
        setTimeout(() => {
            const resetAction = CommonActions.reset({
                index: 0,
                routes: [{ name: RootScreen.MainNavigators, params: {  } }]
            });
            navigation.dispatch(resetAction);

        }, 1000)
    }


    const checkLocationPermission = async () => {
        if (env.IS_DEBUG){
            console.log("DashboardContainer: Check Location Permission called()");
        }
        BackgroundGeolocation.getProviderState(async (data) => {
            let permissionStatus = data?.status
            setLocationPermission(permissionStatus === LocationPermissionStatus.Always)
            setLocationPermissionNever(permissionStatus === LocationPermissionStatus.Never)
            setLocationPermissionWhileUsing(permissionStatus === LocationPermissionStatus.WhileUsingTheApp)
        })
    }

    const showVerifyEmail = () => {
        setVerificationCode('')
        setShouldVerify(true)
    }

    const hideVerifyEmail = () => {
        setShouldVerify(false)
    }

    const toggleDrawer = () => {
        navigation.dispatch(DrawerActions.toggleDrawer())
    }

    const goToSettings = () => {
        if (!locationPermission) {
            openSettings().catch((e) => {
                crashlytics().recordError(e)
                console.warn('cannot open settings')
            });
        }
        else if (bluetoothPermission === null) {
            openSettings().catch((e) => {
                crashlytics().recordError(e)
                console.warn('cannot open settings')
            });
        }
        else if (!bluetoothPermission) {
            goToBToothSettings()
        }
        else if (!nearbyDevicesPermission){
            openSettings().catch((e) => {
                crashlytics().recordError(e)
                console.warn('cannot open settings')
            });
        }

    }

    const goToBToothSettings = () => {
        Platform.OS === 'ios'
        ? Linking.openURL('App-Prefs:Bluetooth')
        : AndroidOpenSettings.bluetoothSettings();
    }

    const onVerifyEmail = () => {
        hideVerifyEmail()
        props.verifyEmail(verificationCode)
    }

    const onSendVerification = () => {
        props.sendEmailVerification()
        showVerifyEmail()
    }

    const sendDiagnosticLogs = async () => {
        props.sendDiagnosticLogs()
        setSendingDiagnostic(true)
    }

    const onPromotionDetails = (promotion: Promotion) => {
        if (promotion?.drawing)
            props.getDrawingDetails(promotion.drawing)
        else {
            props.getDrawingDetails({ locationId: promotion.location.id })
        }
        setTimeout(() => {
            navigation.dispatch(StackActions.push(MainScreen.DrawingDetails));
        }, 500)
    }

    const getAbandonedLocationCheckin = (isCheckedinLocalPromotion: Boolean, locationId: Number): Number => {
        const locationCheckin: LocationCheckin = props.locationCheckins ? props.locationCheckins.find((locationCheckin: LocationCheckin) => locationCheckin.id === locationId) : null
        if (props.mobile && isCheckedinLocalPromotion)
            return props.earningTickPercentage

        if (locationCheckin && locationCheckin.lastCheckOutPeriodTime) {
            let lastCheckOutPeriodTime = Number(locationCheckin.lastCheckOutPeriodTime)
            let lastCheckInPeriodTime = Number(locationCheckin.lastCheckInPeriodTime)
            const duration = 120.88
            const elapse = ((lastCheckOutPeriodTime - lastCheckInPeriodTime) / 1000)
            const countedEntries = `${(elapse/duration)}`.includes('.99') ? Math.round(elapse/duration) : Math.floor(elapse/duration)
            const onGoingSeconds = ( (elapse/duration) - (countedEntries) ) * 1000
            const tickPercentage = (onGoingSeconds * 100) / 1000
            return tickPercentage
        }
        return 0
    }

    const getLocalPromotions = (): Promotion[] => {
        const auth = props.auth
        const eligibleLocations = props.locations.filter((location: Location) => {
            if (auth?.hideTestLocations && location.clientExcluded) {
                // HIDE TEST LOCATIONS
                return false
            }
            return true
        })
        const nearLocations = eligibleLocations.filter(location => location.isNear)
        const localPromotions: Promotion[] = nearLocations.map((location, index) => {
            const drawing: Drawing = props.recentDrawings ? props.recentDrawings.find((recent: Drawing) => location.id === recent.locationId) : null
            const entries: Entry[] = (props.bank && props.bank.entries) ? props.bank.entries : []
            const promotion: Promotion = ({ id: location.distance, location: location, })
            const currentTime = Number(Date.now())
            const elapse = (Number(new Date(`${drawing?.endDate}`)) - currentTime) / 1000
            if (drawing && 0 < elapse && 0 < entries.length) {
                const drawingEntry = entries.find((entry: Entry) => (entry.drawingId === drawing.id))
                const entry = drawingEntry ? { ...drawingEntry, multiplier: drawing.multiplier } : null
                const promotionId = 0 < drawingEntry?.earnedEntryCount ? -(drawingEntry.earnedEntryCount) : location.distance
                const isCheckedinLocalPromotion = (props.mobile && props.mobile.locationId === drawing.locationId)
                return ({ ...promotion,
                            drawing,
                            entry,
                            id: promotionId, isCheckedin: isCheckedinLocalPromotion,
                            earningTickPercentage: getAbandonedLocationCheckin(isCheckedinLocalPromotion, location.id) })
            }
            return ({ ...promotion, })
        })
        return Immutable.asMutable(localPromotions)
                        .sort((a, b) =>  Number(a.id) -  Number(b.id) )
    }

    const getMonthlyPromotion = (): Promotion => {
        const drawing: Drawing = props.recentDrawings ? props.recentDrawings.find((drawing) => !drawing.locationId) : null
        const entries: Entry[] = (props.bank && props.bank.entries) ? props.bank.entries : []
        const promotion: Promotion = ({ id: null, })
        const currentTime = Number(Date.now())
        const elapse = (Number(new Date(`${drawing?.endDate}`)) - currentTime) / 1000
        if (drawing && 0 < elapse && 0 < entries.length) {
            const drawingEntry = entries.find((entry: Entry) => (entry.locationId === drawing.locationId) ||
                                                                (!entry.locationId && !drawing.locationId && entry.locationId === 0))

            return ({ ...promotion, drawing: drawing, entry: drawingEntry, earningTickPercentage: props.earningTickPercentage })
        }
        return promotion
    }

    useFocusEffect(
        useCallback ( ()=> {
            if((Platform.OS === 'android')) {
                props.startEarningTimer()
                
                if(parseInt(getSystemVersion()) >= 12) {
                    if ((!props.gps || !props.geoLocation || !props.bluetooth || !props.nearbyDevices || !props.network)) {
                        navigation.dispatch(StackActions.push(RootScreen.PreparationsNavigators));
                    }
                }
                else {
                    if ((!props.gps || !props.geoLocation || !props.bluetooth || !props.network)) {
                        navigation.dispatch(StackActions.push(RootScreen.PreparationsNavigators));
                    }
                }
            } else {
                if ((!props.gps || !props.bluetooth || !props.network)) {
                    navigation.dispatch(StackActions.push(RootScreen.PreparationsNavigators));
                }
            }

        }, [props.nextAppState,
            props.gps,
            props.geoLocation,
            props.bluetooth,
            props.timestamp,
            props.nearbyDevices]
        )
    )

    // ONLY PERFORM WHEN THERE IS REFRESH OR NEW CYCLE/EARNING ENTRY TO BANK
    // FOR A PERIOD OF TIME
    useFocusEffect(
        useCallback ( ()=> {
            console.log("NextApp State: ", props.nextAppState)
            props.getPreparations()
            
            if (props.nextAppState === "active") {
                props.stopCountDownStateWideDrawing()
                props.startCountDownStateWideDrawing()

                checkLocationPermission()
            }
            else {
                // TODO
                // CURRENTLY THIS IS ONLY FOR UI CHANGES.
                // SO WE STOP IN BACKGROUND
                props.stopCountDownStateWideDrawing()
            }

        }, [props.nextAppState])
    )

    useFocusEffect (
        useCallback(() => {
            if(sendingDiagnostic && props.diagnosticSuccess) {
                setSendingDiagnostic(false)
                props.clearDiagnosticSuccess()
                let title = strings.diagnostic_success
                let successMessage = strings.diagnostic_success_msg
                Alert.alert(
                    title, 
                    successMessage,
                    [{
                        text: `${strings.commons_btn_ok}`,
                        onPress: () => {}
                    }])
            }
        }, [props.diagnosticSuccess])
    )

    useFocusEffect (
        useCallback(() => {
            if (sendingDiagnostic && props.diagnosticError) {
                setSendingDiagnostic(false)
                let title = strings.diagnostic_error
                let errorMessage = strings.genericError
                if (props.diagnosticError.backendError) 
                    errorMessage = props.diagnosticError.backendError

                if (props.diagnosticError.connectionError) 
                    errorMessage = props.diagnosticError.connectionError

                props.clearDiagnosticError()
                
                Alert.alert(
                    title, 
                    errorMessage,
                    [{
                        text: `${strings.commons_btn_ok}`,
                        onPress: () => {}
                    }])
            }
        }, [props.diagnosticError])
    )

    const onRetryPromotions = () => {
        const monthlyPromotion = getMonthlyPromotion()
        const hasMonthlyPromotion = monthlyPromotion?.drawing && monthlyPromotion?.entry
        if (!hasMonthlyPromotion)
            setTimeout(() => props.getRecentDrawings(), 500)
    }

    const onRefresh = () => {
        setTimeout(() => {
            props.getCurrentPromotionLocation()
        }, 500)
    }

    const onShowHelp = () => {
        navigation.dispatch(StackActions.push(RootScreen.HelpAndSupportNavigators));
    }

    return (
        <Dashboard   toggleDrawer={toggleDrawer}
                isPromotionLoading={(props.isBankLoading || props.isDrawingLoading || props.isLocationLoading)}
                isDashboardLoading={isDashboardLoading}
                goToSettings={goToSettings}
                onRefresh={onRefresh}
                onShowHelp={onShowHelp}
                onRetryPromotions={onRetryPromotions}
                onVerifyEmail={onVerifyEmail}
                onSendVerification={onSendVerification}
                onPromotionDetails={onPromotionDetails}
                onSendDiagnostic={sendDiagnosticLogs}
                isEmailVerified={props.auth && props.auth.verified}
                isLocationGranted={locationPermission}
                isBluetoothGranted={bluetoothPermission}
                isNearbyDevicesGranted={nearbyDevicesPermission}
                setVerificationCode={setVerificationCode}
                verificationCode={verificationCode}
                hideVerifyEmail={hideVerifyEmail}
                shouldVerify={shouldVerify}
                shouldStartEarning={(props.mobile && props.mobile.beacon && props.mobile.checkinId) ? true : false}
                bankEntry={props.entry}
                monthlyPromotion={getMonthlyPromotion()}
                localPromotions={getLocalPromotions()}
                locationPermissionNever={locationPermissionNever}
                locationPermissionWhileUsing={locationPermissionWhileUsing}
                showDiagnostic={false}//{(props.diagnosticCheckInAttempDone && !(props.mobile && props.mobile.beacon && props.mobile.checkinId))}
                isDiagnosticLoading={props.diagnosticIsLoading}
                { ...props } />
    )
}

const mapStateToProps = (state: RootState) => ({
    isCurrentlyMonitoring: BeaconManagerSelectors.isRunning(state),
    isLocationLoading: LocationSelectors.isLoading(state),
    coordinate: LocationSelectors.coordinate(state),
    locations: LocationSelectors.locations(state),
    locationCheckins: LocationSelectors.locationCheckins(state),
    profile: ProfileSelectors.profile(state),
    nextAppState: AppStateSelectors.nextAppState(state),
    fcmToken: MessagingSelectors.token(state),
    recentDrawings: DrawingSelectors.recentDrawings(state),
    isDrawingLoading: DrawingSelectors.isLoading(state),
    mobile: MobileSelectors.mobile(state),
    lastCheckInPeriodTime: MobileSelectors.lastCheckInPeriodTime(state),
    bank: BankSelectors.bank(state),
    isBankLoading: BankSelectors.isLoading(state),
    entry: BankSelectors.entry(state),
    auth: AuthSelectors.auth(state),
    errorLocation: LocationSelectors.error(state),
    errorBank: BankSelectors.error(state),
    errorDrawing: DrawingSelectors.error(state),
    error: DashboardSelectors.error(state),
    isEmailSent: DashboardSelectors.isEmailSent(state),
    isLoading: DashboardSelectors.isLoading(state),
    earningTickPercentage: DashboardSelectors.earningTick(state),
    isConnected: state.network.isConnected,
    gps: PreparationSelectors.gps(state),
    geoLocation: PreparationSelectors.geoLocation(state),
    nearbyDevices: PreparationSelectors.nearbyDevices(state),
    bluetooth: PreparationSelectors.bluetooth(state),
    network: PreparationSelectors.network(state),
    notification: PreparationSelectors.notification(state),
    timestamp: PreparationSelectors.timestamp(state),

    diagnosticSuccess: HelpSupportSelectors.success(state),
    diagnosticError: HelpSupportSelectors.error(state),
    diagnosticIsLoading: HelpSupportSelectors.isLoading(state),
    diagnosticCheckInAttempDone: BeaconManagerSelectors.checkInAttemptDone(state),
})
const mapDispatchToProps = (dispatch: RootDispatch) => ({
    startEarningTimer: () => dispatch(DashboardActions.startEarningTimer()),
    stopEarningTimer: () => dispatch(DashboardActions.stopEarningTimer()),
    sendEmailVerification: () => dispatch(DashboardActions.sendEmailVerification()),
    verifyEmail: (code: string) => dispatch(DashboardActions.verifyEmail(code)),

    startCountDownStateWideDrawing: () => dispatch(DrawingActions.startCountDownStateWideDrawing()),
    stopCountDownStateWideDrawing: () => dispatch(DrawingActions.stopCountDownStateWideDrawing()),
    getDrawingDetails: (drawing: Drawing) => dispatch(DrawingActions.getDrawingDetails(drawing)),
    getRecentDrawings: () => dispatch(DrawingActions.getRecentDrawings()),
    getDrawingRulesSuccess: (drawing: Drawing) => dispatch(DrawingActions.getDrawingRulesSuccess(drawing)),
    getCurrentLocation: () => dispatch(LocationActions.getCurrentLocation()),
    getLocationBeacons: (coordinate: Coordinate) => dispatch(LocationActions.getLocationBeacons(coordinate)),
    getCurrentPromotionLocation: () => dispatch(LocationActions.getCurrentPromotionLocation()),
    signOut: () => dispatch(MenuActions.signOut()),
    updateBankEnries: () => dispatch(BankActions.updateBankEntries()),

    requestBankLoading: () => dispatch(BankActions.requestBankLoading()),
    checkOut: (isBluetoothEnabled: Boolean) => dispatch(MobileActions.checkOut(isBluetoothEnabled, false, true, "dashboardContainer:mapStateToProps", null)),
    checkOutSuccess: () => dispatch(MobileActions.checkOutSuccess(null, null)),

    stopBeaconMonitoring: () => dispatch(BeaconManagerActions.stopBeaconMonitoring()),
    startBeaconMonitoring: () => dispatch(BeaconManagerActions.startBeaconMonitoring()),

    getPreparations: () => dispatch(PreparationActions.getPreparations()),

    sendDiagnosticLogs: () => dispatch(HelpSupportAction.sendDiagnosticLogs()),
    clearDiagnosticSuccess: () => dispatch(HelpSupportAction.sendDiagnosticLogsSuccess(null)),
    clearDiagnosticError: () => dispatch(HelpSupportAction.sendDiagnosticLogsFailure(null)),
})

const connector = connect(mapStateToProps, mapDispatchToProps)
type Props = ConnectedProps<typeof connector>

export default connector(DashboardContainer)
