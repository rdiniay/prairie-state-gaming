import React, { useEffect, useState } from 'react'
import { ConnectedProps } from 'react-redux'
import {
  View,
  TextInput,
  TouchableOpacity,
  Image,
  FlatList,
  SectionList,
  ActivityIndicator,
  ScrollView,
  Dimensions,
  Platform,
  PixelRatio,
} from 'react-native'
import { images, lotties } from '../../constants'
import { Container,
    SwipeablePanel,
    Header,
    Text,
    Button,
    VerificationButton,
    PromotionItem,
    LottieView
} from '../../components'
import { Colors } from '../../styles'
import {styles, commons, commonsAssets} from './Styles'
import Dialog from "react-native-dialog";
import { Auth, Profile, Promotion, Drawing, Entry, } from '../../models'
import DateString from 'moment'
const { width, height } = Dimensions.get('window')
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import { strings } from '../../localization/strings'
import { format } from "date-fns";


type Props = {
  toggleDrawer: () => void,
  onRefresh: () => void,
  onRetryPromotions: () => void,
  goToSettings: () => void,
  onSendVerification: () => void,
  onPromotionDetails: (drawing: Drawing) => void,

  onVerifyEmail: () => void,
  hideVerifyEmail: () => void,
  setVerificationCode: (value: string) => void,
  onSendDiagnostic: () => void,
  onShowHelp: () => void,

  auth: Auth,
  profile: Profile,
  isPromotionLoading: boolean,
  isDashboardLoading: boolean,
  isLoading: boolean,
  earningTickPercentage: number,
  isEmailVerified: boolean,
  isLocationGranted: boolean,
  isBluetoothGranted: boolean,
  isNearbyDevicesGranted: boolean,
  verificationCode: string,
  shouldVerify: boolean,
  shouldStartEarning: boolean,
  bankEntry: Entry,
  recentDrawings: Drawing[],
  monthlyPromotion: Promotion,
  localPromotions: Promotion[],
  locationPermissionNever: boolean,
  locationPermissionWhileUsing: boolean,
  showDiagnostic: boolean,
  isDiagnosticLoading: boolean,
}

const Dashboard = (props: Props) => {
    const insets = useSafeAreaInsets();
    const [footerHeight, setFooterHeight] = useState(0)
    const isLoaded = !props.isDashboardLoading && props.localPromotions && 0 < props.localPromotions.length
    const hasMonthlyPromotion = isLoaded && props.monthlyPromotion?.drawing && props.monthlyPromotion?.entry

    const BlueHeader = () => {
        var color = Colors.red
        var message = ""
        if(!props.isLocationGranted || !props.isBluetoothGranted || (!props.isNearbyDevicesGranted && Platform.OS === 'android')){
            color = Colors.noticeColor;
            if (props.locationPermissionNever)
                message = Platform.OS === 'ios' ? strings.ios_location_not_granted : strings.android_location_not_granted
            else if (props.locationPermissionWhileUsing)
                message = strings.location_while_using_the_app;
            else {
                if (!props.isLocationGranted){
                    message = (Platform.OS === 'ios') ? (strings.ios_location_not_granted) : (strings.android_location_not_granted);
                }else if(!props.isBluetoothGranted){
                    message = strings.bluetooth_not_granted;
                }
                else if(!props.isNearbyDevicesGranted && Platform.OS === 'android'){
                    message = strings.nearby_devices_not_granted;
                }
            }
        }

        return (
            <View style={{display: message ? 'flex':'none', backgroundColor: color,flexDirection: 'row', justifyContent:'center', alignItems: 'center'}}>
                <Text style={{color:Colors.white, paddingBottom: 10, flex: 0.85}}>{message}</Text>
                <TouchableOpacity onPress={props.goToSettings}>
                    <View style={{ 
                        padding: 10, 
                        alignItems: 'center', 
                        justifyContent: 'center', 
                        overflow: 'hidden', 
                        borderWidth: 1, 
                        borderColor: Colors.white, 
                        borderRadius: 15
                    }}>
                        <Text style={commons.customHeaderRightTextViewStyle}>
                            {`Settings`}
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
    const header = () => {
        var isAndroid = Platform.OS === 'android'
        var flex = (props.locationPermissionNever || props.locationPermissionWhileUsing) ? isAndroid ? .2 : .1 : .35;
        var textAlign = (props.locationPermissionNever || props.locationPermissionWhileUsing) ? 'justify' : 'center'
        var height = 20
        var color = Colors.red
        var showNotif = false

        if(!props.isLocationGranted || !props.isBluetoothGranted || (!props.isNearbyDevicesGranted && Platform.OS === 'android')){
            height = 40;
            color = Colors.noticeColor;
            showNotif = true
        }

        return  <Header style={{ backgroundColor: color, }}
                title={null} //'Prairie State Gaming'
                titleStyle={{ ...commons.customHeaderNavMessageStyle, color: showNotif ? Colors.white : Colors.red, textAlign, }}
                left={Platform.OS === 'ios' ? ( <View style={{ height: height }}/> )
                :
                (
                    <TouchableOpacity    onPress={props.toggleDrawer}
                                            style={styles.hamburgerMenuButton}>
                            <View style={styles.hamburgerMenuView} >
                                <Image resizeMode="contain" source={images.hamburgerMenu} style={styles.hamburgerMenuImage} />
                            </View>
                    </TouchableOpacity>
                )}
                leftContainerStyle={{ flex: flex }}
                center={(
                    <Image resizeMode={'contain'} style={{ height: 40, width: 120, flexDirection:'row', marginStart: showNotif? 45 : 15}} source={commonsAssets.contentImageSource} />
                )}
                centerContainerStyle={{ flex: .425, }}
                right={ (
                        <View  style={styles.refreshContainer}>
                            <View style={props.showDiagnostic ? styles.refreshLeftContent : styles.refreshLeftContent2} />
                            { props.showDiagnostic &&
                                <TouchableOpacity onPress={props.onShowHelp}>
                                {!props.isDashboardLoading &&
                                    <Image resizeMode="contain" source={images.helpIcon} style={styles.helpIcon} />
                                }
                                </TouchableOpacity>
                            }
                            
                            <TouchableOpacity style={styles.refreshRightContent} onPress={props.onRefresh}>
                                {!props.isDashboardLoading &&
                                    <LottieView style={styles.refreshIcon}
                                                source={lotties.refresh()}
                                                animate={props.isPromotionLoading}
                                            />
                                }
                            </TouchableOpacity>
                        </View>
                )}
                rightContainerStyle={{ flex: .35, marginRight: 0, }}
            />
    }

    const [panelProps, setPanelProps] = useState({
        fullWidth: true,
        openLarge: true,
        showCloseButton: false,
        onClose: () => null,
        // ...or any prop you want
    });

    const NoAvailablePromotion = () => props.isDashboardLoading ? <View /> : (
        <TouchableOpacity style={styles.noAvailablePromotionContent}>
            <Text  style={styles.noAvailablePromotionText}>
                {`${strings.dashboard_no_monthly_promo}`}
            </Text>
            <Button style={styles.noAvailablePromotionButton}
                    title={strings.commons_btn_retry}
                    onPress={props.onRetryPromotions}/>
        </TouchableOpacity>
    )

    return (
      <Container    safeAreaViewTopStyle={styles.safeArea}
                    statusBarStyle={(!props.isLocationGranted || !props.isBluetoothGranted || (!props.isNearbyDevicesGranted && Platform.OS === 'android') )? 'notice-light' : "light"}
                    // scrollViewEnable={true}
                    header={header}
                    style={styles.container} >
            <BlueHeader/>
            <View>
                <Dialog.Container visible={props.shouldVerify}>
                    <Dialog.Title>{strings.dashboard_verification_sent}</Dialog.Title>
                    <Dialog.Description>
                        {strings.dashboard_verification_description_sent}
                    </Dialog.Description>
                    {Platform.OS === 'ios' &&
                        <Dialog.Input keyboardType="number-pad" onChangeText={props.setVerificationCode}>
                            {props.verificationCode}
                        </Dialog.Input>
                    }
                    {Platform.OS !== 'ios' &&
                        <View style={styles.androidVerificationInputContent}>
                            <TextInput onChangeText={props.setVerificationCode}
                                        autoCapitalize='none'
                                        keyboardType='number-pad'
                                        style={styles.androidVerificationTextInput}/>
                        </View>
                    }
                    <Dialog.Button onPress={props.hideVerifyEmail} label="Cancel" />
                    <Dialog.Button onPress={props.onVerifyEmail} label="Submit" />

                </Dialog.Container>
            </View>

            <View style={styles.container}>
                {!isLoaded && <ActivityIndicator style={styles.panelLoadingContent} size="large" color={Colors.white}/> }

                {isLoaded &&
                   <FlatList
                        data={props.localPromotions}
                        onScroll={(evt) => { setFooterHeight(evt.nativeEvent.contentSize.height) }}
                        ListHeaderComponent={(
                            <>
                                {hasMonthlyPromotion &&
                                    <PromotionItem  truncated={true}
                                                    theme={"dark"}
                                                    promotion={props.monthlyPromotion}
                                                    showDiagnostic={props.showDiagnostic}
                                                    onPress={() => { props.onPromotionDetails(props.monthlyPromotion) }} 
                                                    onSendDiagnostic={()=> props.onSendDiagnostic()}
                                                    isDiagnosticLoading={props.isDiagnosticLoading} />
                                }
                                {!hasMonthlyPromotion &&
                                    <NoAvailablePromotion />
                                }
                            </>
                        )}
                        renderItem={({ item, index }) => (
                            <View style={styles.panelContent(index)} >
                                {!(props.auth?.verified) && index === 0 &&
                                    <VerificationButton onPress={props.onSendVerification} />
                                }
                                <PromotionItem  truncated={true}
                                                        theme={"light"}
                                                        promotion={item}
                                                        showDiagnostic={false}
                                                        onPress={() => { props.onPromotionDetails(item) }} />
                            </View>
                        )}
                        // Warning: Each child in a list should have a unique "key" prop.
                        keyExtractor={(item, index) => `${item.location?.id}`}
                        style={styles.container}
                        ListFooterComponent={(<View style={[styles.footerContent, { height: 0 < footerHeight ? height : 0, }]} />)}
                    />
                }
            </View>
        </Container>
    )
}

export default Dashboard
