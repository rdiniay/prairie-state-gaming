import ScaledSheet from 'react-native-scaled-sheet'
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import { Dimensions, PixelRatio, Platform } from 'react-native'
import { Colors, Commons } from '../../styles'
const { width, height } = Dimensions.get('window');

const styles = ScaledSheet.create({
    safeArea: {
        backgroundColor: Colors.red,
    },
    container: {
        flex: 1,
    },
    footerContent: {
        position: 'absolute', 
        zIndex: 999, 
        backgroundColor: Colors.lightBrown , 
        alignSelf: 'center',  
        width: width,
    },
    backgroundPanelContent: {
        flex: 1, 
        backgroundColor: Colors.red, 
        alignItems: 'center',
        justifyContent: 'center',
    },
    androidVerificationInputContent: { 
        marginHorizontal: 10, 
        borderBottomColor: Colors.black,
        borderBottomWidth: 1, bottom: 12 
    },
    androidVerificationTextInput: {
        fontSize: 18,
    },
    panelContent: (index: number) => ({
        borderTopLeftRadius: index === 0 ? 20 : 0, 
        borderTopRightRadius: index === 0 ? 20 : 0, 
        paddingTop: index === 0 ? 15 : 0,
        backgroundColor: Colors.lightBrown, 
    }),
    panelLoadingContent: {
        flex: 1, 
        backgroundColor: Colors.red, 
    },
    panelLoadingBottomContent: {
        flex: 1,
        backgroundColor: Colors.lightBrown
    },
    noAvailablePromotionContent: {
        padding: 20, 
        paddingVertical: 40,
    },
    noAvailablePromotionText: {
        fontSize: 18, 
        color: Colors.whiteOpacity, 
        fontWeight: 'bold',
        textAlign: 'justify',
        paddingHorizontal: 10,
    },
    noAvailablePromotionButton: {
        marginTop: 20, 
        paddingHorizontal: 10,
    },
    refreshContainer: {
        flex: 1,
        flexDirection: 'row',  
        paddingTop: 5, 
    },
    refreshLeftContent: {
        marginLeft: 20,
    },
    refreshLeftContent2: {
        marginLeft: 80,
    },
    refreshRightContent: {
        flex: .6,
    },
    hamburgerMenuButton: {
        flexDirection: "row", 
        alignItems: 'center', 
        paddingLeft: 10, 
    },
    hamburgerMenuView: {
        height: 30, 
        overflow: 'hidden',
    },
    hamburgerMenuImage: {
        flex: 1, 
        width: 20, 
        tintColor: Colors.white,
    },
    image: {
        flex: 1,
        height: 180,
        // backgroundColor: '#EBECF190', 
        overflow: "hidden", alignItems: 'center', justifyContent: 'center',
    },
    imagePanelMarginTopWithPhoneNumber: { 
        ...Commons.ContentLogoViewStyle(),
        marginTop: (height*.120) + useSafeAreaInsets().top,
    },
    imagePanelMarginTopWithoutPhoneNumber: { 
        ...Commons.ContentLogoViewStyle(),
        marginTop: (height*.140) + useSafeAreaInsets().top,
    },
    helpIcon: {
        flex: 1, 
        tintColor: Colors.white,
        alignItems: 'center', 
        marginLeft: 15, 
        margin: 2
    },
    refreshIcon: {
        opacity: 0.9,
    },
})


const commons =  ScaledSheet.create({
    headerTouchableOpacityStyle: Commons.HeaderTouchableOpacityStyle(),
    headerTextStyle: Commons.HeaderBackButtonTextStyle(),
    contentHeaderTextViewStyle: Commons.ContentHeaderTextViewStyle(),
    contentHeaderSubTextViewStyle: Commons.ContentHeaderSubTextViewStyle(),
    leftContainerStyle:  Commons.HeaderContentLeftContainerStyle(),
    contentLogoViewStyle: Commons.ContentLogoViewStyle(),
    contentLogoStyle: Commons.ContentLogoStyle(),
    
    contentSwipePanelStyle: Commons.ContentSwipePanelStyle(),
    contentTitleStyle: Commons.ContentTitleStyle(),
    contentInnerViewStyle: Commons.InnerViewStyle(),
    contentInnerContentViewStyle: Commons.InnerContentViewStyle(),
    contentInlineContentViewInnerHeaderViewStyle: Commons.InlineContentViewInnerHeaderViewStyle(),
    contentInlineContentViewInnerHeaderTextViewStyle: {
        textAlign: 'center'  ,  
        ...Commons.InlineContentViewInnerHeaderTextViewStyle(),
        fontSize: 13 / PixelRatio.getFontScale(),
    },
    inlineContentViewStyle: {
        paddingTop: 10, 
        borderBottomColor: Colors.darkgray, 
        borderBottomWidth: 0.5, 
        paddingVertical: 5, 
        marginHorizontal: 8,
    },
    inlineContentTextViewStyle: {
        ...Commons.InlineContentViewInnerContentTextViewStyle(),
        marginHorizontal: 15,
    },
    contentInlineContextTextInputStyle : {
        padding: 8, 
        marginHorizontal: 7, 
        fontSize: 12 / PixelRatio.getFontScale(), 
        fontFamily: 'Raleway-Regular'
    },
    contentInlineClickableTextViewStyle :  { 
        marginHorizontal: 15, 
        color: Colors.black, 
        paddingVertical: 8,
        fontFamily: 'Raleway-Regular',
        fontSize: 12 / PixelRatio.getFontScale(), 
    },
    customHeaderNavMessageStyle : { 
        fontSize: 16 / PixelRatio.getFontScale(),
        padding: 0,
        fontFamily: 'Raleway-Regular',
    },
    customHeaderRightViewStyle: { 
        height: 35, 
        padding: 2, 
        alignItems: 'center', 
        justifyContent: 'center', 
        overflow: 'hidden', 
        borderWidth: 1, 
        borderColor: Colors.white, 
        borderRadius: 15
    },
    customHeaderRightTextViewStyle: { 
        textAlign: 'center', 
        fontFamily: 'Raleway-Regular',
        fontSize: 15 / PixelRatio.getFontScale(), 
        color: Colors.white,
    },


    customEntriesTextStyle :  {
        textAlign: 'center', 
        fontSize: 28 / PixelRatio.getFontScale(), 
        fontFamily: 'Raleway-Bold', 
        fontWeight: 'bold', 
        paddingVertical: 0,
    },

    customEntriesSubTextStyle: (shouldShowMultiplier: Boolean, color: string) => {
        return {
            textAlign: 'center',
            fontFamily: 'Raleway-Bold',
            fontWeight: 'bold',
            color: color,
            paddingVertical: 5,
            fontSize: shouldShowMultiplier ? 10 / PixelRatio.getFontScale() : 12/ PixelRatio.getFontScale(),
        };

    },


    customVerificationHeaderTextStyle : { 
        textAlign: 'center', 
        fontSize: 18 / PixelRatio.getFontScale(), 
        fontFamily: 'Raleway-Bold' 
    },

    customVerificationHeaderSubTextStyle : { 
        textAlign: 'center', 
        fontSize: 16 / PixelRatio.getFontScale(), 
        color: Colors.lightgray, 
        fontFamily: 'Raleway-Regular',
    },

    customVerificationBottomTextStyle : { 
        textAlign: 'center', 
        fontSize: 20/ PixelRatio.getFontScale(), 
        color: Colors.red 
    },

    customDashboardContentHeaderTextStyle: { 
        fontSize: 15 / PixelRatio.getFontScale(), 
        fontWeight: 'bold',
        fontFamily: 'Raleway-Bold',
    },
    customDashboardContentSubHeaderTextStyle : { 
        fontSize: 12 / PixelRatio.getFontScale(),
        fontFamily: 'Raleway-Regular',
    },
    customDashboardContentBottomTextStyle : { 
        color: Colors.lightgray, 
        fontSize: 12 / PixelRatio.getFontScale(),
        fontFamily: 'Raleway-Regular'
    },
    customDashboardEmptyMessageTextStyle : { 
        textAlign: 'center', 
        padding: 20, 
        color: 'gray', 
        fontSize: 20 / PixelRatio.getFontScale() , 
        fontFamily: "Raleway-Regular", 
        lineHeight: 14.09 / PixelRatio.getFontScale() 
    },
    customDashboardEmptyButtonTextStyle: { 
        color: 'white', 
        fontSize: 16 / PixelRatio.getFontScale(), 
        textAlign: 'center', 
        fontFamily: "Raleway-Regular", 
        lineHeight: 14.09 / PixelRatio.getFontScale(),
    }
})

const commonsAssets = {
    contentImageSource: Commons.ContentImageSource(),
}

export {
    styles,
    commons,
    commonsAssets,
}