import React, { useState } from 'react'
import { ConnectedProps } from 'react-redux'
import { 
  View,
  TextInput,
  TouchableOpacity,
  Image,
  FlatList,
  SectionList,
  ActivityIndicator,
  Dimensions,
  Platform,
} from 'react-native'
import { Container, SwipeablePanel, Header, Text } from '../../components'
import { Colors } from '../../styles'
import {styles, commons, commonsAssets} from './Styles'
import { ScrollView } from 'react-native-gesture-handler'
import { WebView } from 'react-native-webview';

import RenderHtml from 'react-native-render-html';
const { width, height } = Dimensions.get('window');
import { Rules } from '../../models'

type Props = {
    onCancel: () => void,
    rules: Rules,
    isLoading: boolean,
}

const DrawingRules = (props: Props) =>{
    const header = () => (
        <Header style={{ backgroundColor: Colors.red, }}
                left={(
                   <TouchableOpacity    onPress={props.onCancel}
                                         style={commons.headerTouchableOpacityStyle}>
                         <Text style={commons.headerTextStyle}>
                             Cancel
                         </Text>
                   </TouchableOpacity>
                )}
                leftContainerStyle={commons.leftContainerStyle}
        />
    )

    const [panelProps, setPanelProps] = useState({
        fullWidth: true,
        openLarge: false,
        showCloseButton: false,
        onClose: () => null,
        // ...or any prop you want
    });

    return (
        <Container      safeAreaViewTopStyle={styles.safeArea}
                        statusBarStyle={'light'}
                        // scrollViewEnable={true}
                        header={header}
                        style={styles.container} >
            
            <View style={styles.imagePanelMarginTopWithoutPhoneNumber}>
                <Image resizeMode={'contain'} style={commons.contentLogoStyle} source={commonsAssets.contentImageSource} />
            </View>

            <SwipeablePanel titleValue={'Drawing Rules'} style={commons.contentSwipePanelStyle} {...panelProps} isActive={true}>
                <View style={commons.contentInnerViewStyle}>
                        {!props.isLoading &&
                            <View style={commons.contentInnerContentViewStyle}>
                                
                                <View  style={commons.contentInlineWebViewTextStyle}>
                                    <RenderHtml
                                        contentWidth={width}
                                        source={{ html: `<html><head><meta name="viewport" content="width=device-width, initial-scale=1"></head>
                                                            <body style="padding: 5; ">
                                                                <p>${props.rules ? props.rules.value : '<p/>'}
                                                            </body>
                                                        </html>` }}
                                    />
                                    
                                </View>
                            </View>
                        }
                        {props.isLoading &&
                            <View style={{ flex:0.7, paddingTop: 150, justifyContent: 'center', }}>
                                <ActivityIndicator size="large" color={'gray'}/>
                            </View>
                        }
                    </View>
            </SwipeablePanel>
        </Container>
    )
}

export default DrawingRules