import React, { useEffect, useState } from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { RootState, RootDispatch } from '../../redux'
import {StackActions ,useNavigation, DrawerActions, CommonActions } from '@react-navigation/native'
import DrawingRules from './DrawingRulesPanel'
import DrawingActions, { DrawingSelectors } from '../../redux/drawing-redux'
import MenuActions, { MenuSelectors } from '../../redux/menu-redux'
import {Screen as RootScreen} from '../../navigation/routes/rootRoutes'

const DrawingRulesContainer = (props: Props) => {
    const navigation = useNavigation()

    const onCancel = () => {
        navigation.goBack();
    }

    useEffect(() => {
        if (!props.drawing)
            props.getDrawingRules()

        return () => {
            props.clearDrawingRules()
        }
    }, [])


    useEffect( () => {
        console.log('Container: ', props.error);
        if (props.error) {
            if (props.error.statusCode == 403 && props.error.signoutUser){
                reset();
            }

        }
    }, [props.error])

    const reset = () => { 
        props.signOut()
        setTimeout(() => {
            const resetAction = CommonActions.reset({
                index: 0,
                routes: [{ name: RootScreen.MainNavigators, params: {  } }]
            });
            navigation.dispatch(resetAction);

        }, 1000)
    }

    return (
        <DrawingRules   onCancel={onCancel}
                        rules={props.drawing ? props.drawing.rules : null}
                        {...props} />
    )
}

const mapStateToProps = (state: RootState) => ({
    drawing: DrawingSelectors.drawingRules(state),
    isLoading: DrawingSelectors.isLoading(state),
    error: DrawingSelectors.error(state),
})

const mapDispatchToProps = (dispatch: RootDispatch) => ({
    clearDrawingRules: () => dispatch(DrawingActions.clearDrawingRules()),
    getDrawingRules: () => dispatch(DrawingActions.getDrawingRules()),
    signOut: () => dispatch(MenuActions.signOut()),
})

const connector = connect(mapStateToProps, mapDispatchToProps)
type Props = ConnectedProps<typeof connector>

export default connector(DrawingRulesContainer)
