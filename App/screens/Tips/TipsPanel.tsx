import React, { useState } from 'react'
import { ConnectedProps } from 'react-redux'
import { 
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  FlatList,
  Dimensions,
  SectionList,
  Platform,
  PixelRatio,
} from 'react-native'
import { Container, SwipeablePanel, Header } from '../../components'
import { Colors } from '../../styles'
import {styles, commons, commonsAssets} from './Styles'
const { width, height } = Dimensions.get('window');


type Props = {
    onCancel: () => void,
}

const Tips = (props: Props) =>{
    const [panelProps, setPanelProps] = useState({
        fullWidth: true,
        openLarge: false,
        showCloseButton: false,
        onClose: () => null,
        // ...or any prop you want
    });

      const header = () => (
        <Header style={{ backgroundColor: Colors.red, }}
                left={(
                   <TouchableOpacity    onPress={props.onCancel}
                                         style={commons.headerTouchableOpacityStyle}>
                         <Text style={commons.headerTextStyle}>
                             Cancel
                         </Text>
                   </TouchableOpacity>
                )}
                leftContainerStyle={commons.leftContainerStyle}
        />
    )

    return (
        <Container      safeAreaViewTopStyle={styles.safeArea}
                        statusBarStyle={'light'}
                        // scrollViewEnable={true}
                        header={header}
                        style={styles.container} >

            <View style={commons.contentLogoViewStyle}>
                <Image resizeMode={'contain'} style={commons.contentLogoStyle} source={commonsAssets.contentImageSource} />
            </View>

            <SwipeablePanel titleValue={'tips & info'} style={commons.contentSwipePanelStyle} {...panelProps} isActive={true}>
                <View style={commons.contentInnerViewStyle}>
                        <View style={commons.contentInnerContentViewStyle}>
                            <View style={commons.contentInlineContentViewInnerHeaderViewStyle}>
                                <Text style={commons.contentInlineContentViewInnerHeaderTextViewStyle}>
                                    Earning entries
                                </Text>
                            </View>
                            <View style={commons.inlineContentViewStyle}>
                                <Text style={commons.inlineContentTextViewStyle}>
                                Go to one of our locations to check in and start earning entries. The longer you stay, the more you'll earn.
                                </Text>
                            </View>
                            <View style={commons.inlineContentViewStyle}>
                                <Text style={commons.inlineContentTextViewStyle}>
                                If you believe you're in a valid location but are not checked in, click on the refresh button to the right of the entry counters to double-check.
                                </Text>
                            </View>
                            <View style={commons.inlineContentViewStyle}>
                                <Text style={commons.inlineContentTextViewStyle}>
                                When a multiplier is active, you'll earn entries multiplied by the multiplier displayed.
                                </Text>
                            </View>
                            <View style={commons.inlineContentViewStyle}>
                                <Text style={commons.inlineContentTextViewStyle}>
                                Daily bonus entry: you also earn a bonus entry along with the first entry you earn each day!
                                </Text>
                            </View>
                        </View>

                        <View style={commons.contentInnerContentViewStyle}>
                            <View style={commons.contentInlineContentViewInnerHeaderViewStyle}>
                                <Text style={commons.contentInlineContentViewInnerHeaderTextViewStyle}>
                                    Checking in and earning when app is not on-screen
                                </Text>
                            </View>
                            <View style={commons.inlineContentViewStyle}>
                                <Text style={commons.inlineContentTextViewStyle}>
                                Please refrain from "killing the app" as this disables the auto-checkin and earning features. If you do need to "kill the app" for some reason, please re-open it to ensure the features are re-enabled.
                                </Text>
                            </View>
                            <View style={commons.inlineContentViewStyle}>
                                <Text style={commons.inlineContentTextViewStyle}>
                                    If the app has trouble determining if you are still earning or not and you've enabled reminders. we'll send you a text message to ask you to open the app ensure you keep earning.
                                </Text>
                            </View>
                            <View style={commons.inlineContentViewStyle}>
                                <Text style={commons.inlineContentTextViewStyle}>
                                If you're having trouble checking in, please make sure your device is not surrounded by too many items that may interface with the signal such as deep inside of a bag.
                                </Text>
                            </View>
                        </View>
                </View>
            </SwipeablePanel>
              
        </Container>
    )
}

export default Tips