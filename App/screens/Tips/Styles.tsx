import { PixelRatio, Dimensions } from 'react-native'
import ScaledSheet from 'react-native-scaled-sheet'
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import { Colors, Commons } from '../../styles'
const { width, height } = Dimensions.get('window');

const styles = ScaledSheet.create({
    safeArea: {
        backgroundColor: Colors.red,
    },
    container: {
        flex: 1,
    },
    imagePanelMarginTopWithPhoneNumber: { 
        ...Commons.ContentLogoViewStyle(),
        marginTop: (height*.120) + useSafeAreaInsets().top,
    },
    imagePanelMarginTopWithoutPhoneNumber: { 
        ...Commons.ContentLogoViewStyle(),
        marginTop: (height*.140) + useSafeAreaInsets().top,
    }
})

const commons =  ScaledSheet.create({
    headerTouchableOpacityStyle: Commons.HeaderTouchableOpacityStyle(),
    headerTextStyle: Commons.HeaderBackButtonTextStyle(),
    leftContainerStyle:  Commons.HeaderContentLeftContainerStyle(),
    contentLogoViewStyle: Commons.ContentLogoViewStyle(),
    contentLogoStyle: Commons.ContentLogoStyle(),
    
    contentSwipePanelStyle: Commons.ContentSwipePanelStyle(),
    contentTitleStyle: Commons.ContentTitleStyle(),
    contentInnerViewStyle: Commons.InnerViewStyle(),
    contentInnerContentViewStyle: Commons.InnerContentViewStyle(),
    contentInlineContentViewInnerHeaderViewStyle: Commons.InlineContentViewInnerHeaderViewStyle(),
    contentInlineContentViewInnerHeaderTextViewStyle: Commons.InlineContentViewInnerHeaderTextViewStyle(),
    inlineContentViewStyle: {
        paddingTop: 10, 
        borderBottomColor: Colors.darkgray, 
        borderBottomWidth: 0.5, 
        paddingVertical: 5, 
        marginHorizontal: 8,
    },
    inlineContentTextViewStyle: Commons.InlineContentViewInnerContentTextViewStyle(),
})

const commonsAssets = {
    contentImageSource: Commons.ContentImageSource(),
}

export {
    styles,
    commons,
    commonsAssets,
}