import React from 'react'
import { ConnectedProps } from 'react-redux'
import { 
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  FlatList,
  SectionList
} from 'react-native'
import { Container, Header } from '../../components'
import { Colors } from '../../styles'
import styles from './Styles'

type Props = {
    onCancel: () => void,
}

const Tips = (props: Props) =>{
    const header = () => (
        <Header title={'Tips & Info'} //'Prairie State Gaming'
                titleStyle={{ fontSize: 18, color: Colors.black, paddingRight: 10, }}
                left={(
                   <TouchableOpacity    onPress={props.onCancel}
                                         style={{ flexDirection: "row", alignItems: 'center', paddingLeft: 15, paddingVertical: 6, }}> 
                         <Text style={{ color: Colors.red, textAlign: 'center', fontSize: 18,  }}>
                             Cancel
                         </Text>
                   </TouchableOpacity>
                )}
                leftContainerStyle={{ flex: 0.35, }}
        />
    )

    return (
        <Container      safeAreaViewTopStyle={styles.safeArea}
                        statusBarStyle={'dark'}
                        scrollViewEnable={true}
                        header={header}
                        style={styles.container} >

            <View style={{ flex: 1, backgroundColor: Colors.whiteOpacity }}>
                <View style={{ flex: 1, marginHorizontal: 20, marginVertical: 5, padding: 20, paddingTop: 10, borderRadius: 15, borderWidth: 0.5, borderColor: Colors.lightgray }}>
                    <View style={{ paddingTop: 10, paddingVertical: 5, marginHorizontal: 8, }}>
                        <Text style={{ color: Colors.black, paddingBottom: 5, fontSize: 16, }}>
                            Earning entries
                        </Text>
                    </View>
                    <View style={{ paddingTop: 10, borderBottomColor: Colors.darkgray, borderBottomWidth: 0.5, paddingVertical: 5, marginHorizontal: 8, }}>
                        <Text style={{ color: Colors.black, paddingBottom: 5, fontSize: 16, }}>
                           Go to one of our locations to check in and start earning entries. The longer you stay, the more you'll earn.
                        </Text>
                    </View>
                    <View style={{  paddingTop: 10, borderBottomColor: Colors.darkgray, borderBottomWidth: 0.5, paddingVertical: 5, marginHorizontal: 8, }}>
                        <Text style={{ color: Colors.black, paddingBottom: 5, fontSize: 16, }}>
                           If you believe you're in a valid location but are not checked in, click on the refresh button to the right of the entry counters to double-check.
                        </Text>
                    </View>
                    <View style={{ paddingTop: 10, borderBottomColor: Colors.darkgray, borderBottomWidth: 0.5, paddingVertical: 5, marginHorizontal: 8, }}>
                        <Text style={{ color: Colors.black, paddingBottom: 5, fontSize: 16, }}>
                           When a multiplier is active, you'll earn entries multiplied by the multiplier displayed.
                        </Text>
                    </View>
                    <View style={{ paddingTop: 10, paddingVertical: 5, marginHorizontal: 8, }}>
                        <Text style={{ color: Colors.black, paddingBottom: 5, fontSize: 16, }}>
                           Daily bonus entry: you also earn a bonus entry along with the first entry you earn each day!
                        </Text>
                    </View>
                </View>

                <View style={{ flex: 1, marginHorizontal: 20, marginVertical: 5, padding: 20, paddingTop: 10, borderRadius: 15, borderWidth: 0.5, borderColor: Colors.lightgray }}>
                    <View style={{ paddingTop: 10, paddingVertical: 5, marginHorizontal: 8, }}>
                        <Text style={{ color: Colors.black, paddingBottom: 5, fontSize: 16, }}>
                            Checking in and earning when app is not on-screen
                        </Text>
                    </View>
                    <View style={{ paddingTop: 10, borderBottomColor: Colors.darkgray, borderBottomWidth: 0.5, paddingVertical: 5, marginHorizontal: 8, }}>
                        <Text style={{ color: Colors.black, paddingBottom: 5, fontSize: 16, }}>
                           Please refrain from "killing the app" as this disables the auto-checkin and earning features. If you do need to "kill the app" for some reason, please re-open it to ensure the features are re-enabled.
                        </Text>
                    </View>
                    <View style={{  paddingTop: 10, borderBottomColor: Colors.darkgray, borderBottomWidth: 0.5, paddingVertical: 5, marginHorizontal: 8, }}>
                        <Text style={{ color: Colors.black, paddingBottom: 5, fontSize: 16, }}>
                            If the app has trouble determining if you are still earning or not and you've enabled reminders. we'll send you a text message to ask you to open the app ensure you keep earning.
                        </Text>
                    </View>
                    <View style={{ paddingTop: 10, paddingVertical: 5, marginHorizontal: 8, }}>
                        <Text style={{ color: Colors.black, paddingBottom: 5, fontSize: 16, }}>
                           If you're having trouble checking in, please make sure your device is not surrounded by too many items that may interface with the signal such as deep inside of a bag.
                        </Text>
                    </View>
                </View>
                
            </View>
            <View style={{ flex: 1, height: 70,}}/>
              
        </Container>
    )
}

export default Tips