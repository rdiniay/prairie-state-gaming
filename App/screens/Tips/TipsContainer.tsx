import React, { useEffect, useState } from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { RootState, RootDispatch } from '../../redux'
import {StackActions ,useNavigation, DrawerActions } from '@react-navigation/native'
import Tips from './TipsPanel'

const TipsContainer = (props: Props) => {
    const navigation = useNavigation()

    const onCancel = () => {
        navigation.goBack();
    }
    return (
        <Tips   onCancel={onCancel}
                {...props} />
    )
}

const mapStateToProps = (state: RootState) => ({
    
})

const mapDispatchToProps = (dispatch: RootDispatch) => ({
    
})

const connector = connect(mapStateToProps, mapDispatchToProps)
type Props = ConnectedProps<typeof connector>

export default connector(TipsContainer)
