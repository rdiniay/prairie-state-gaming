import React from 'react'
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    TouchableHighlight,
    Switch,
    SectionList,
    TextInput
} from  'react-native'
import { Container, Header } from '../../components'
import styles from './Styles'
import { Colors } from '../../styles'

type Props = {
    onBackToTop: () => void,
    setPhoneNumber: (value: string) => void,
    onCreateAccount: () => void,
}

const Verification = (props: Props) =>{

    const header = () => (
        <Header style={{ paddingTop: 15,  paddingBottom: 20, shadowColor: '#FFF', backgroundColor: '#295FEA' }} //backgroundColor: '#EBECF1' }}
                center={(
                    <View style={{ alignItems: 'flex-end' }}>
                      <Text style={{ color: 'white', fontWeight: 'bold' }}>
                        
                      </Text>
                    </View>
                )} 
                // right={(
                //    <View style={{ flex: 0.1, top: -18, alignItems: 'center', }}>
                //     <View style={{ borderRadius: 20, height: 40, width: 40, backgroundColor: '#EBECF160', overflow: 'hidden' ,}} >
                //       <Image resizeMode="cover" source={images.cityOfBaguioSeal} style={{ flex: 3,  height: 40, width: 40, }} /> 
                //     </View>
                //   </View>
                // )}
        />
    )
    return(
        <Container  // safeAreaViewTopStyle={styles.safeArea}
                    statusBarStyle={'dark'}
                    scrollViewEnable={true}
                    // header={header}
                    style={styles.container} >
                <View style={{ flex: 1, margin: 25, marginTop: 30, borderRadius: 15, paddingVertical: 5, paddingHorizontal: 10, backgroundColor: 'white' }}>

                        <View style={{ flex: 1, marginTop: 20, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ paddingBottom: 5, fontSize: 18, }}>
                                We need your phone number to contact you if you're a winner
                            </Text>
                        </View>
                        <View style={{ padding: 10, paddingTop: 10, }} >
                            <View style={{ borderBottomColor: Colors.lightgray, borderBottomWidth: 0.5, paddingVertical: 5, marginHorizontal: 15, }}>
                                <Text style={{ color: Colors.red, paddingBottom: 5, }}>
                                    Phone number
                                </Text>
                                <TextInput onChangeText={props.setPhoneNumber} keyboardType='email-address' spellCheck={false} style={{ fontSize: 16, }}/>
                            </View>
                            <View style={{ paddingTop: 15, paddingVertical: 5, marginHorizontal: 15, }}>
                                <Text style={{ flex: 1, color: Colors.darkgray }}>
                                    Keep reminders enabled to ensure your earning session stays active. We'll only text you if we can't communicate with the app.
                                </Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingVertical: 5, marginHorizontal: 15, }}>
                                <Text style={{ color: Colors.red, }}>
                                    Earning Reminder
                                </Text>
                                <TouchableHighlight underlayColor={`${Colors.white}`} onPress={button => { }}>
                                    <Switch
                                        style={{ marginBottom: 0 }}
                                        trackColor={{ false: Colors.lightgray, true: Colors.red }}
                                        thumbColor={"#f4f3f4"}
                                        ios_backgroundColor={`${Colors.lightgray}`}
                                        // onValueChange={toggle => { props.onSwitchDefaultAddress() }}
                                        value={true}
                                        />
                                </TouchableHighlight>
                            </View>
                            <View style={{ flex: 1, margin: 45, marginTop: 20, marginBottom: 0, }}>
                                <TouchableOpacity   style={{ padding: 10, borderColor: Colors.red, borderWidth: 0.5, borderRadius: 20,  justifyContent: 'center', alignItems: 'center', }}
                                                    >
                                    <Text style={{ color: Colors.red, fontSize: 16, textAlign: 'center' }}>
                                        Have a referral code?
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ flex: 1, margin: 25, marginBottom: 10, }}>
                                <TouchableOpacity style={{ padding: 10, backgroundColor: Colors.red, borderRadius: 20,  justifyContent: 'center', alignItems: 'center', }}
                                                    onPress={props.onCreateAccount}>
                                    <Text style={{ color: Colors.white, fontSize: 16, textAlign: 'center' }}>
                                        Create your account!
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ flex: 1, marginHorizontal: 50,  }}>
                                <Text style={{ color: Colors.darkgray, fontSize: 12, textAlign: 'center' }}>
                                    {`By clicking above you agree to our `}
                                    <Text style={{ color: Colors.red,}}>
                                       Terms of Service.
                                    </Text>
                                </Text>
                            </View>
                            <View style={{ flex: 1, margin: 25, marginTop: 15, marginBottom: 0, }}>
                                <TouchableOpacity   style={{ padding: 10, borderRadius: 20,  justifyContent: 'center', alignItems: 'center', }}
                                                    onPress={props.onBackToTop}>
                                    <Text style={{ color: Colors.red, fontSize: 16, textAlign: 'center' }}>
                                        Cancel
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            
                        </View>
                </View>
        </Container>
    )
}

export default Verification