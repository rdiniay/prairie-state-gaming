import React, { useEffect, useState } from 'react'
import { connect, ConnectedProps, } from 'react-redux'
import { RootState, RootDispatch } from '../../redux'
import {StackActions ,useNavigation, DrawerActions } from '@react-navigation/native'
import Verification from './Verification'
import { User } from '../../models'
import RegistrationActions, { RegistrationSelectors } from '../../redux/registration-redux'

const VerificationContainer = (props: Props) =>  {
    const [phoneNumber, setPhoneNumber] = useState('')
    const navigation = useNavigation()

    const onBackToTop = () => {
        navigation.dispatch(StackActions.popToTop());
    }

    const onCreateAccount = () => {
        const user: User = { ...props.user, phoneNumber: Number(phoneNumber) }
        props.createAccount(user)
    }

    useEffect(() => {
        const user: User = props.user
        if (user.id) {
            alert('successfully created')
        }
    }, [props.user])

    return (
        <Verification   onBackToTop={onBackToTop}
                        setPhoneNumber={setPhoneNumber}
                        onCreateAccount={onCreateAccount}
                        { ...props } />
    )
}

const mapStateToProps = (state: RootState) => ({
    user: RegistrationSelectors.user(state),
})
const mapDispatchToProps = (dispatch: RootDispatch) => ({
    createAccount: (user: User) => dispatch(RegistrationActions.createAccount(user))
})

const connector = connect(mapStateToProps, mapDispatchToProps)
type Props = ConnectedProps<typeof connector>

export default connector(VerificationContainer)

