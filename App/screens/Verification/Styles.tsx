import ScaledSheet from 'react-native-scaled-sheet'

const styles = ScaledSheet.create({
    safeArea: {
        backgroundColor: '#295FEA',
    },
    container: {
        flex: 1,
        backgroundColor: 'lightgray',
    },
})

export default styles