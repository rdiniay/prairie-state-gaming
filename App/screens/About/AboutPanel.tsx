import React, { useEffect, useState } from 'react'
import { ConnectedProps } from 'react-redux'
import { 
  View,
  TextInput,
  TouchableOpacity,
  Image,
  FlatList,
  SectionList,
  Dimensions,
  Platform,
  ActivityIndicator,
} from 'react-native'
import { Container, SwipeablePanel, Header, Text } from '../../components'
import { Colors } from '../../styles'
import {styles, commons, commonsAssets } from './Styles'
import { Rules } from '../../models'
import RenderHtml from 'react-native-render-html';
const { width, height } = Dimensions.get('window')

type Props = {
    onCancel: () => void,

    isLoading: boolean,
    about: Rules,
    aboutFooter: Rules,
}

const About = (props: Props) =>{
    const header = () => (
        <Header style={{ backgroundColor: Colors.red, }}
                left={(
                   <TouchableOpacity    onPress={props.onCancel}
                                         style={commons.headerTouchableOpacityStyle}>
                         <Text style={commons.headerTextStyle}>
                             Cancel
                         </Text>
                   </TouchableOpacity>
                )}
                leftContainerStyle={commons.leftContainerStyle}
        />
    )

    const [panelProps, setPanelProps] = useState({
        fullWidth: true,
        openLarge: false,
        showCloseButton: false,
        onClose: () => null,
        // ...or any prop you want
    });

    return (
        <Container      safeAreaViewTopStyle={styles.safeArea}
                        statusBarStyle={'light'}
                        // scrollViewEnable={true}
                        header={header}
                        style={styles.container} >

            <View style={commons.contentLogoViewStyle}>
                <Image resizeMode={'contain'} style={commons.contentLogoStyle} source={commonsAssets.contentImageSource} />
            </View>

            <SwipeablePanel titleValue={'about'} style={commons.contentSwipePanelStyle} {...panelProps} isActive={true}>
                <View style={commons.contentInnerViewStyle}>
                    {!props.isLoading &&
                            <View style={commons.contentInnerContentViewStyle}>
                                
                                <View  style={commons.contentInlineWebViewTextStyle}>
                                    <RenderHtml
                                        contentWidth={width}
                                        source={{ html: `<html><head><meta name="viewport" content="width=device-width, initial-scale=1"></head>
                                                            <body style="padding: 5; ">
                                                                <p>${props.about ? props.about.value : '<p/>'}
                                                                ${props.aboutFooter ? props.aboutFooter.value : ''}
                                                            </body>
                                                        </html>` }}
                                    />
                                    
                                </View>
                            </View>
                        }
                        {props.isLoading &&
                            <View style={styles.loadingView}>
                                <ActivityIndicator size="large" color={'gray'}/>
                            </View>
                        }
                </View>
            </SwipeablePanel>
              
        </Container>
    )
}

export default About