import React from 'react'
import { ConnectedProps } from 'react-redux'
import { 
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  FlatList,
  SectionList
} from 'react-native'
import { Container, Header } from '../../components'
import { Colors } from '../../styles'
import styles from './Styles'

type Props = {
    onCancel: () => void,

    about: string,
}

const About = (props: Props) =>{
    const header = () => (
        <Header title={'About'} //'Prairie State Gaming'
                titleStyle={{ fontSize: 18, color: Colors.black, paddingRight: 10, }}
                left={(
                   <TouchableOpacity    onPress={props.onCancel}
                                         style={{ flexDirection: "row", alignItems: 'center', paddingLeft: 15, paddingVertical: 6, }}>
                         <Text style={{ color: Colors.red, textAlign: 'center', fontSize: 18,  }}>
                             Cancel
                         </Text>
                   </TouchableOpacity>
                )}
                leftContainerStyle={{ flex: 0.35, }}
        />
    )

    return (
        <Container      safeAreaViewTopStyle={styles.safeArea}
                        statusBarStyle={'dark'}
                        scrollViewEnable={true}
                        header={header}
                        style={styles.container} >

              <Text style={{ flex: 1, textAlign: 'center', padding: 20, paddingTop: 50, fontSize: 14, }}>
                    {props.about ?? ''}
              </Text>
              
        </Container>
    )
}

export default About