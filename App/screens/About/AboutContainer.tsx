import React, { useEffect, useState } from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { RootState, RootDispatch } from '../../redux'
import {StackActions ,useNavigation, DrawerActions } from '@react-navigation/native'
import About from './AboutPanel'
import MenuActions, { MenuSelectors } from '../../redux/menu-redux'
import AboutActions, { AboutSelectors } from '../../redux/about-redux'

const AboutContainer = (props: Props) => {
    const navigation = useNavigation()

    const onCancel = () => {
        navigation.goBack();
    }

    useEffect(() => {
        props.getAboutContent()
        props.getAboutFooter()
    }, [])

    useEffect(() => {
        console.log('props.aboutFooter ', props.aboutFooter)
    }, [props.aboutFooter])

    return (
        <About   onCancel={onCancel}
                {...props} />
    )
}

const mapStateToProps = (state: RootState) => ({
    about: AboutSelectors.aboutContent(state),
    aboutFooter: AboutSelectors.aboutFooter(state),
    isLoading: AboutSelectors.isLoading(state),
})

const mapDispatchToProps = (dispatch: RootDispatch) => ({
    getAboutContent: () => dispatch(AboutActions.getAboutContent()),
    getAboutFooter: () => dispatch(AboutActions.getAboutFooter()),
})

const connector = connect(mapStateToProps, mapDispatchToProps)
type Props = ConnectedProps<typeof connector>

export default connector(AboutContainer)
