import ScaledSheet from 'react-native-scaled-sheet'
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import { Colors, Commons } from '../../styles'
import { Dimensions, PixelRatio, } from  'react-native'
const { width, height } = Dimensions.get('window');

const styles = ScaledSheet.create({
    safeArea: {
        backgroundColor: Colors.red,
    },
    container: {
        flex: 1,
        backgroundColor: Colors.lightBrown,
    },
    backButton: {
        flexDirection: "row", 
        alignItems: 'center', 
        paddingLeft: 15, 
        paddingVertical: 6,
    },
    backButtonText: {
        color: Colors.white, 
        textAlign: 'center', 
        fontSize: 18,
    },
    header: {
        backgroundColor: Colors.red
    },
    headerTitle: {
        fontSize: 18, 
        color: Colors.white, 
        padding: 4,
    },

    listHeader: {
        paddingTop: height * .45 
    },
    promotionItem: {
        borderRadius: 25, 
        backgroundColor: Colors.lightBrown
    },
    panelContent: {
        borderTopLeftRadius: 20, 
        borderTopRightRadius: 20, 
        backgroundColor: Colors.lightBrown, 
        paddingTop: 38,
    },
    mapViewContent: {
        height: height * .55, 
        width: width, 
        alignSelf: 'center', 
        position: 'absolute',
    },
    markerCalloutText: {
        padding: 8, 
        textAlign: 'center', 
        fontSize: 18, 
        backgroundColor: '#36B6F1', 
        color: Colors.white 
    },
    imagePanelMarginTopWithPhoneNumber: { 
        ...Commons.ContentLogoViewStyle(),
        marginTop: (height*.120) + useSafeAreaInsets().top,
    },
    imagePanelMarginTopWithoutPhoneNumber: { 
        ...Commons.ContentLogoViewStyle(),
        marginTop: height*.140,
    }
})

const commons =  ScaledSheet.create({
    headerTouchableOpacityStyle: Commons.HeaderTouchableOpacityStyle(),
    headerTextStyle: Commons.HeaderBackButtonTextStyle(),
    contentHeaderTextViewStyle: Commons.ContentHeaderTextViewStyle(),
    contentHeaderSubTextViewStyle: Commons.ContentHeaderSubTextViewStyle(),
    leftContainerStyle:  Commons.HeaderContentLeftContainerStyle(),
    contentLogoViewStyle: Commons.ContentLogoViewStyle(),
    contentLogoStyle: Commons.ContentLogoStyle(),
    
    contentSwipePanelStyle: Commons.ContentSwipePanelStyle(),
    contentTitleStyle: Commons.ContentTitleStyle(),
    contentInnerViewStyle: Commons.InnerViewStyle(),
    contentInnerContentViewStyle: Commons.InnerContentViewStyle(),
    contentInlineContentViewInnerHeaderViewStyle: Commons.InlineContentViewInnerHeaderViewStyle(),
    contentInlineContentViewInnerHeaderTextViewStyle: {
        textAlign: 'center'  ,  
        ...Commons.InlineContentViewInnerHeaderTextViewStyle(),
        fontSize: 13 / PixelRatio.getFontScale(),
    },
    inlineContentViewStyle: {
        paddingTop: 10, 
        borderBottomColor: Colors.darkgray, 
        borderBottomWidth: 0.5, 
        paddingVertical: 5, 
        marginHorizontal: 8,
    },
    inlineContentTextViewStyle: {
        ...Commons.InlineContentViewInnerContentTextViewStyle(),
        marginHorizontal: 15,
    },
    contentInlineContextTextInputStyle : {
        padding: 8, 
        marginHorizontal: 7, 
        fontSize: 12 / PixelRatio.getFontScale(), 
        fontFamily: 'Raleway-Regular'
    },
    contentInlineClickableTextViewStyle :  { 
        marginHorizontal: 15, 
        color: Colors.black, 
        paddingVertical: 8,
        fontFamily: 'Raleway-Regular',
        fontSize: 12 / PixelRatio.getFontScale(), 
    },


})

const commonsAssets = {
    contentImageSource: Commons.ContentImageSource(),
}

export {
    styles,
    commons,
    commonsAssets,
}