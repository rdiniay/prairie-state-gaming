import React, { useState } from 'react'
import { ConnectedProps } from 'react-redux'
import { 
  View,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
  Image,
  FlatList,
  SectionList,
  Dimensions,
  Platform,
  ScrollView,
} from 'react-native'
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import { images } from '../../constants'
import ProgressCircle from 'react-native-progress-circle'
import { Container, SwipeablePanel, Header, Text, PromotionItem } from '../../components'
import { Colors } from '../../styles'
import {styles, commons, commonsAssets} from './Styles'
import MapView, { Marker, Callout } from 'react-native-maps';
import { Promotion, Location } from '../../models'
const { width, height } = Dimensions.get('window')

type Props = {
    onBack: () => void,
    onDirection: (location: Location) => void,
    onPromotionDetails: (promotion: Promotion) => void,

    coordinate: any,
    region: any,
    locations: Location[],
    localPromotions: Promotion[],
    firstname: string,
    lastname: string,
    phoneNumber: string,
}

const LocationView = (props: Props) => {
    const insets = useSafeAreaInsets()
    
    const header = () => (
        <Header style={styles.header}
                title={'Locations'} //'Prairie State Gaming'   
                titleStyle={styles.headerTitle}
                left={Platform.OS === 'ios' ? (  <View style={{ height: 0 }}/> )
                :
                (
                   <TouchableOpacity   style={styles.backButton} onPress={props.onBack}>
                         <Text style={styles.backButtonText}>
                             Cancel
                         </Text>
                   </TouchableOpacity>
                )}
                leftContainerStyle={{ flex: Platform.OS === 'ios' ? .25 : .35, }}
        />
    )

    const [panelProps, setPanelProps] = useState({
        fullWidth: true,
        openLarge: false,
        showCloseButton: true,
        onClose: () => null,
        // ...or any prop you want
        allowTouchOutside: Platform.OS === 'ios',
        smallPanelHeight: (height - insets.top - insets.bottom) / 1.8,
    });

    return (
      <Container  safeAreaViewTopStyle={styles.safeArea}
                  statusBarStyle={'light'}
                //   scrollViewEnable={true}
                  header={header}
                  style={styles.container} >

            {props.region &&
                <MapView style={styles.mapViewContent}
                        initialRegion={props.region}>
                    {props.localPromotions && props.localPromotions.map(localPromo => {
                        const location: Location = localPromo.location
                        return (
                        <Marker 
                            key={`${location?.id}`}
                            draggable
                            coordinate={{ latitude: Number(location.coordinate.latitude), longitude: Number(location.coordinate.longitude) }}>
                                <Callout onPress={() => { props.onDirection(location) }}>
                                    <Text style={styles.markerCalloutText}>
                                        {`${location.completeAddress}\n${location.name}`}
                                    </Text>
                                </Callout>
                        </Marker>
                    )})}
                </MapView>
            }
            <SwipeablePanel  style={styles.container} {...panelProps} isActive={true}>
                <View style={styles.panelContent} >
                    {props.localPromotions && 
                        props.localPromotions.map(promotion => 
                            <View key={`${promotion?.location?.id}`} >
                                <PromotionItem  truncated={true}
                                            theme={"light"}
                                            promotion={promotion}
                                            onPress={() => { props.onPromotionDetails(promotion)}} />
                            </View>
                        )
                    }
                </View>
            </SwipeablePanel>
              
        </Container>
    )
}

export default LocationView