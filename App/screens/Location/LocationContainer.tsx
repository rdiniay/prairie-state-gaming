import React, { useEffect, useState } from 'react'
import { Linking, Platform } from 'react-native'
import { connect, ConnectedProps, } from 'react-redux'
import { RootState, RootDispatch } from '../../redux'
import {StackActions ,useNavigation, DrawerActions } from '@react-navigation/native'
import { Screen as MainScreen } from '../../navigation/routes/mainRoutes'
import { Screen as LocationScreen } from '../../navigation/routes/locationRoutes'
import { Screen } from '../../navigation/routes/rootRoutes'
import LocationView from './Location'
import LocationPanel from './LocationPanel'
import { AuthSelectors } from '../../redux/auth-redux'
import LocationActions, { LocationSelectors } from '../../redux/location-redux'
import DrawingActions, { DrawingSelectors } from '../../redux/drawing-redux'
import { BankSelectors } from '../../redux/bank-redux'
import { MobileSelectors } from '../../redux/mobile-redux'
import { ProfileSelectors } from '../../redux/profile-redux'
import { DashboardSelectors } from '../../redux/dashboard-redux'
import { Drawing, Entry, Coordinate, LocationCheckin, Promotion, Location } from '../../models'
import Immutable from 'seamless-immutable'


const LocationContainer = (props: Props) =>  {
    const Location = Platform.OS === 'android' ? LocationView : LocationPanel
    const navigation = useNavigation()
    const [region, setRegion] = useState(null)

    useEffect(() => {
        const coordinate = props.coordinate
        if (coordinate) {
            setRegion({
                latitude: Number(coordinate.latitude),
                longitude: Number(coordinate.longitude),
                latitudeDelta: 0.0442,
                longitudeDelta: 0.0442,
            })
        }
    }, [])

    useEffect(() => {
        const coordinate = props.coordinate
        if (Platform.OS === 'ios') {
            if (coordinate) 
                props.getLocationBeacons(coordinate)
        }
        // LOAD ONCE (Especially on Android that always change locations in the forground)
        else if (!region && coordinate) {
            props.getLocationBeacons(coordinate)
        }
    }, [props.coordinate])

    const onBack = () => {
        navigation.dispatch(StackActions.pop());
    }
    
    const getAbandonedLocationCheckin = (isCheckedinLocalPromotion: Boolean, locationId: Number): Number => {
        const locationCheckin: LocationCheckin = props.locationCheckins ? props.locationCheckins.find((locationCheckin: LocationCheckin) => locationCheckin.id === locationId) : null
        if (props.mobile && isCheckedinLocalPromotion)
            return props.earningTickPercentage
        
        if (locationCheckin && locationCheckin.lastCheckOutPeriodTime) {
            let lastCheckOutPeriodTime = Number(locationCheckin.lastCheckOutPeriodTime)
            let lastCheckInPeriodTime = Number(locationCheckin.lastCheckInPeriodTime)
            const duration = 120.88
            const elapse = ((lastCheckOutPeriodTime - lastCheckInPeriodTime) / 1000)
            const countedEntries = `${(elapse/duration)}`.includes('.99') ? Math.round(elapse/duration) : Math.floor(elapse/duration)
            const onGoingSeconds = ( (elapse/duration) - (countedEntries) ) * 1000
            const tickPercentage = (onGoingSeconds * 100) / 1000
            return tickPercentage
        } 
        return 0
    }

    const getLocalPromotions = (): Promotion[] => {
        const auth = props.auth
        const eligibleLocations = props.locations.filter((location: Location) => {
            if (auth?.hideTestLocations && location.clientExcluded) {
                // HIDE TEST LOCATIONS
                return false
            }
            return true
        })
        const nearLocations = eligibleLocations.filter(location => location.isNear)
        const localPromotions: Promotion[] = nearLocations.map((location, index) => {
            const drawing: Drawing = props.recentDrawings ? props.recentDrawings.find((recent: Drawing) => location.id === recent.locationId) : null
            const entries: Entry[] = (props.bank && props.bank.entries) ? props.bank.entries : []
            const promotion: Promotion = ({ id: location.distance, location: location, })
            const currentTime = Number(Date.now())
            const elapse = (Number(new Date(`${drawing?.endDate}`)) - currentTime) / 1000
            if (drawing && 0 < elapse && 0 < entries.length) {
                const drawingEntry = entries.find((entry: Entry) => (entry.drawingId === drawing.id))
                const isCheckedinLocalPromotion = (props.mobile && props.mobile.locationId === drawing.locationId)
                return ({ ...promotion, drawing, entry: { ...drawingEntry, multiplier: drawing.multiplier } ,
                            isCheckedin: isCheckedinLocalPromotion, 
                            earningTickPercentage: getAbandonedLocationCheckin(isCheckedinLocalPromotion, location.id) })
            }
            return ({ ...promotion, })
        })
        return Immutable.asMutable(localPromotions)
                        .sort((a, b) =>  Number(a.id) -  Number(b.id) )
    }

    const onPromotionDetails = (promotion: Promotion) => {
        if (promotion?.drawing)
            props.getDrawingDetails(promotion.drawing)
        else {
            props.getDrawingDetails({ locationId: promotion.location.id })
        }
        setTimeout(() => {
            let DrawingDetails = Platform.OS === 'ios' ? MainScreen.DrawingDetails : LocationScreen.DrawingDetails
            navigation.dispatch(StackActions.push(DrawingDetails));
        }, 500)
    }

    const onDirection = (location: Location) => {
        const scheme = Platform.select({ ios: 'maps:0,0?q=', android: 'geo:0,0?q=' });
        const latLng = `${location.coordinate.latitude},${location.coordinate.longitude}`;
        const label = `${location.name} ${location.completeAddress}`;
        const url = Platform.select({
            ios: `${scheme}${label}@${latLng}`,
            android: `${scheme}${latLng}(${label})`
        });

        Linking.openURL(url);
    }

    return (
        <Location   onBack={onBack}
                    region={region}
                    localPromotions={getLocalPromotions()}
                    onDirection={onDirection}
                    onPromotionDetails={onPromotionDetails}
                    firstname={props.profile ? props.profile.firstname : ""}
                    lastname={props.profile ? props.profile.lastname : ""}
                    phoneNumber={props.profile ? props.profile.phoneNumber : ""}
                    { ...props } />
    )
}

const mapStateToProps = (state: RootState) => ({
    auth: AuthSelectors.auth(state),
    profile: ProfileSelectors.profile(state),
    coordinate: LocationSelectors.coordinate(state),
    locations: LocationSelectors.locations(state),
    locationCheckins: LocationSelectors.locationCheckins(state),
    bank: BankSelectors.bank(state),
    mobile: MobileSelectors.mobile(state),
    recentDrawings: DrawingSelectors.recentDrawings(state),
    earningTickPercentage: DashboardSelectors.earningTick(state),
})
const mapDispatchToProps = (dispatch: RootDispatch) => ({
    getCurrentLocation: () => dispatch(LocationActions.getCurrentLocation()),
    getLocationBeacons: (coordinate: Coordinate) => dispatch(LocationActions.getLocationBeacons(coordinate)),
    getDrawingDetails: (drawing: Drawing) => dispatch(DrawingActions.getDrawingDetails(drawing)),
})

const connector = connect(mapStateToProps, mapDispatchToProps)
type Props = ConnectedProps<typeof connector>

export default connector(LocationContainer)

