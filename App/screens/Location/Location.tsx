import React from 'react'
import { ConnectedProps } from 'react-redux'
import { 
  View,
  TextInput,
  TouchableOpacity,
  Image,
  FlatList,
  SectionList,
  Dimensions,
  Platform,
} from 'react-native'
import { Container, SwipeablePanel, Header, Text, PromotionItem } from '../../components'
import { Colors } from '../../styles'
import {styles, commons, commonsAssets} from './Styles'
import MapView, { Marker, Callout } from 'react-native-maps';
import { Promotion, Location } from '../../models'
const { width, height } = Dimensions.get('window')

type Props = {
    onBack: () => void,
    onDirection: (location: Location) => void,
    onPromotionDetails: (promotion: Promotion) => void,

    coordinate: any,
    region: any,
    locations: Location[],
    localPromotions: Promotion[],
    firstname: string,
    lastname: string,
    phoneNumber: string,
}

const LocationView = (props: Props) => {
    const header = () => (
        <Header style={styles.header}
                title={'Locations'} //'Prairie State Gaming'   
                titleStyle={styles.headerTitle}
                left={Platform.OS === 'ios' ? (  <View style={{ height: 0 }}/> )
                :
                (
                   <TouchableOpacity   style={styles.backButton} onPress={props.onBack}>
                         <Text style={styles.backButtonText}>
                             Cancel
                         </Text>
                   </TouchableOpacity>
                )}
                leftContainerStyle={{ flex: Platform.OS === 'ios' ? .25 : .35, }}
        />
    )

    return (
      <Container  safeAreaViewTopStyle={styles.safeArea}
                  statusBarStyle={'light'}
                //   scrollViewEnable={true}
                  header={header}
                  style={styles.container} >

            {props.region &&
                <FlatList   
                    ListHeaderComponent={
                        <View style={styles.listHeader}>
                            <MapView style={styles.mapViewContent}
                                initialRegion={props.region}>
                                {props.localPromotions && props.localPromotions.map(localPromo => {
                                    const location: Location = localPromo.location
                                    return (
                                    <Marker 
                                        key={`${location?.id}`}
                                        draggable
                                        coordinate={{ latitude: Number(location.coordinate.latitude), longitude: Number(location.coordinate.longitude) }}>
                                            <Callout onPress={() => { props.onDirection(location) }}>
                                                <Text style={styles.markerCalloutText}>
                                                    {`${location.completeAddress}\n${location.name}`}
                                                </Text>
                                            </Callout>
                                    </Marker>
                                )})}
                            </MapView>
                        </View>
                    }
                    data={props.localPromotions}
                    style={styles.container}
                    renderItem={({ item }) => (
                        <View style={styles.promotionItem}>
                            <PromotionItem  truncated={true}
                                            theme={"light"}
                                            promotion={item}
                                            onPress={() => { props.onPromotionDetails(item)}} />
                        </View>
                    )}
                    keyExtractor={(item, index) => `${item.location?.id}`}
                />
            }
        </Container>
    )
}

export default LocationView