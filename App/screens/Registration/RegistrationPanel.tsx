import React, { useEffect, useState} from 'react'
import {
    View,
    Image,
    TouchableOpacity,
    SectionList,
    Dimensions,
    Linking,
    Platform,
} from  'react-native'
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import CheckBox from '@react-native-community/checkbox'
import { Container, SwipeablePanel, Header, Text, TextInput, LoadingDialog } from '../../components'
import {styles, commons, commonsAssets} from './Styles'
import { Colors } from '../../styles'
import {strings} from "../../localization/strings";
const { width, height } = Dimensions.get('window');

type Props = {
    // REQUIRES SPACE TO ENABLE PROPS FUNCTION CALL
    onBack: () => void,
    onRegister: () => void,
    onTOS: () => void,
    setFirstname: (value: string) => void,
    setLastname: (value: string) => void,
    setEmailAddress: (value: string) => void,
    setPin: (value: string) => void,
    setReferralCode: (value: string) => void,
    setPhoneNumber: (value: string) => void,
    setEarningReminders: (value: boolean) => void,
    isFirstnameValid: boolean,
    isLastnameValid: boolean,
    isPhoneNumberValid: boolean,
    firstname: string,
    lastname: string,
    email: string,
    isValidEmail: boolean,
    pin: string,
    referral_code: string,
    phone: string,
    earning_reminders: boolean,
    isLoading: boolean,
}

const Registration = (props: Props) =>{
    const insets = useSafeAreaInsets()

    const header = () => (
        <Header style={{ paddingTop: 15,  paddingBottom: 20, shadowColor: '#FFF', backgroundColor: '#B43235' }} //backgroundColor: '#EBECF1' }}
                center={(
                    <View style={{ alignItems: 'flex-end' }}>
                      <Text style={{ color: 'white', fontWeight: 'bold' }}>

                      </Text>
                    </View>
                )}
                left={(
                    <TouchableOpacity onPress={props.onBack} style={{ flexDirection: "row", alignItems: 'center', paddingLeft: 10}}>
                            <Text style={{ color: Colors.white, textAlign: 'center', fontSize: 18,  }}>
                             Back
                         </Text>
                    </TouchableOpacity>


                )}
        />
    )
    const [toggleKeyboard, setToggleKeyboard] = useState(false)

    const [isPanelActive, setIsPanelActive] = useState(true);

    const [panelProps, setPanelProps] = useState({
        fullWidth: true,
        openLarge: false,
        showCloseButton: false,
        onClose: () => closePanel(),
        onPressCloseButton: () => closePanel(),
        // ...or any prop you want
        allowTouchOutside: false,
        // noBar: true,
      });

      const openPanel = () => {
        setIsPanelActive(true);
      };

      const closePanel = () => {
        setIsPanelActive(false);
      };

      const toggleUp = () => {
        setPanelProps({...panelProps, openLarge: true})
        closePanel()
        setTimeout(() => {
            openPanel()
        }, 1);
      }


    return(
        <Container  safeAreaViewTopStyle={styles.safeArea}
                    statusBarStyle={'light'}
                    // scrollViewEnable={true}
                    header={header}
                    style={styles.container} >


            <LoadingDialog visible={props.isLoading} subtitle={'Please wait...'} />

            <View style={styles.imagePanelMarginTopWithoutPhoneNumber}>
                <Image resizeMode={'contain'} style={commons.contentLogoStyle} source={commonsAssets.contentImageSource} />
            </View>

            <SwipeablePanel titleValue={'registration'} style={{backgroundColor: '#E6DDCE' }} {...panelProps} isActive={isPanelActive}>
                    <View style={{  borderTopLeftRadius: 20, borderTopRightRadius: 20, backgroundColor: '#F7F5EB' }} >
                        <View style={{ margin: 15, marginBottom: 140, marginTop: 25, borderColor: Colors.lightgray, borderWidth: 0.5, borderRadius: 15, paddingVertical: 15, paddingHorizontal: 10, backgroundColor: 'white'  }}>

                                    <View style={{ flex: 1, marginTop: 10, justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ paddingBottom: 5, fontSize: 16, color: Colors.darkgray }}>
                                            {strings.register_header_title}
                                        </Text>
                                    </View>
                                    <View style={{ padding: 10, paddingTop: 10,  }} >
                                        <View style={{ borderBottomColor: 'lightgray', flexDirection: 'column-reverse', }}>
                                            <View style={{ flex:1, flexDirection: 'row', justifyContent: 'space-between'}}>
                                                <Text style={{ flex: 1, color: Colors.lightgray, paddingTop: 5, }}>
                                                    {strings.register_firstname_placeholder}
                                                </Text>
                                                {props.firstname === null &&
                                                    <Text style={styles.highlightedText}>
                                                        {strings.required_text}
                                                    </Text>
                                                }
                                                {!props.isFirstnameValid &&
                                                    <Text style={styles.highlightedText}>
                                                        {strings.invalid_firstname}
                                                    </Text>
                                                }
                                            </View>
                                            <View style={{ borderBottomColor: 'lightgray', borderBottomWidth: 0.5,  }}>

                                                <TextInput
                                                    value={props.firstname}
                                                    onChangeText={props.setFirstname}
                                                    keyboardType='email-address'
                                                    spellCheck={false}
                                                    style={{ flex: 1, paddingVertical: 4, fontSize: 16, }}
                                                    autoFocus={toggleKeyboard}
                                                    onBlur={() => { setToggleKeyboard(false) }}
                                                    onFocus={() => {
                                                        toggleUp()
                                                        setToggleKeyboard(true)
                                                    }}
                                                    />
                                             </View>
                                        </View>
                                        <View style={{ paddingTop: 10, flexDirection: 'column-reverse', }}>
                                            <View style={{ flex:1, flexDirection: 'row', justifyContent: 'space-between'}}>
                                                <Text style={{ color: Colors.lightgray, paddingTop: 5, }}>
                                                    {strings.register_lastname_placeholder}
                                                </Text>
                                                    {props.lastname === null &&
                                                        <Text style={styles.highlightedText}>
                                                            {strings.required_text}
                                                        </Text>
                                                    }
                                                    {!props.isLastnameValid &&
                                                        <Text style={styles.highlightedText}>
                                                            {strings.invalid_lastname}
                                                        </Text>
                                                    }
                                            </View>
                                            <View style={{ borderBottomColor: 'lightgray',  borderBottomWidth: 0.5,  }}>
                                                <TextInput
                                                    value={props.lastname}
                                                    onChangeText={props.setLastname}
                                                    keyboardType='email-address'
                                                    style={{ paddingVertical: 2, fontSize: 16, }}
                                                    autoFocus={toggleKeyboard}
                                                    onBlur={() => { setToggleKeyboard(false) }}
                                                    onFocus={() => {
                                                        toggleUp()
                                                        setToggleKeyboard(true)
                                                    }}
                                                    />
                                            </View>
                                        </View>
                                        <View style={{ paddingTop: 10, flexDirection: 'column-reverse', }}>
                                            <View style={{ flex:1, flexDirection: 'row', justifyContent: 'space-between'}}>
                                                <Text style={{ color: Colors.lightgray, paddingTop: 5, }}>
                                                    {strings.register_email_placeholder}
                                                </Text>
                                                    {props.email === null &&
                                                        <Text style={styles.highlightedText}>
                                                            {strings.required_text}
                                                        </Text>
                                                    }

                                                    {props.email !== null && props.email.length !== 0 && !props.isValidEmail &&
                                                        <Text style={styles.highlightedText}>
                                                            {strings.invalid_email}
                                                        </Text>
                                                    }
                                            </View>
                                            <View style={{ borderBottomColor: 'lightgray',  borderBottomWidth: 0.5,  }}>
                                                <TextInput
                                                    value={props.email}
                                                    onChangeText={props.setEmailAddress}
                                                    autoCapitalize='none'
                                                    keyboardType='email-address'
                                                    spellCheck={false}
                                                    style={{ paddingVertical: 4, fontSize: 16, }}
                                                    autoFocus={toggleKeyboard}
                                                    onBlur={() => { setToggleKeyboard(false) }}
                                                    onFocus={() => {
                                                        toggleUp()
                                                        setToggleKeyboard(true)
                                                    }}
                                                    />
                                            </View>
                                        </View>
                                        <View style={{ paddingTop: 10, flexDirection: 'column-reverse', }}>
                                            <View style={{ flex:1, flexDirection: 'row', justifyContent: 'space-between'}}>
                                                <Text style={{ color: Colors.lightgray, paddingTop: 5, }}>
                                                    {strings.register_pin_placeholder}
                                                </Text>
                                                    {props.pin === null &&
                                                        <Text style={styles.highlightedText}>
                                                            {strings.required_text}
                                                        </Text>
                                                    }


                                            </View>
                                            <View style={{ borderBottomColor: 'lightgray',  borderBottomWidth: 0.5,  }}>
                                                <TextInput
                                                    maxLength={4}
                                                    value={props.pin}
                                                    onChangeText={props.setPin}
                                                    autoCapitalize='none'
                                                    secureTextEntry={true}
                                                    keyboardType='number-pad'
                                                    style={{ paddingVertical: 4, fontSize: 16, }}
                                                    autoFocus={toggleKeyboard}
                                                    onBlur={() => { setToggleKeyboard(false) }}
                                                    onFocus={() => {
                                                        toggleUp()
                                                        setToggleKeyboard(true)
                                                    }}
                                                    />
                                            </View>
                                        </View>

                                        <View style={{ paddingTop: 10, flexDirection: 'column-reverse', }}>
                                            <View style={{ flex:1, flexDirection: 'row', justifyContent: 'space-between'}}>
                                                <Text style={{ color: Colors.lightgray, paddingTop: 5, }}>
                                                    {strings.register_referral_code_placeholder}
                                                </Text>
                                                    {props.referral_code === null &&
                                                        <Text style={styles.highlightedText}>
                                                            {strings.required_text}
                                                        </Text>
                                                    }
                                            </View>
                                            <View style={{ borderBottomColor: 'lightgray',  borderBottomWidth: 0.5,  }}>
                                                <TextInput
                                                    value={props.referral_code}
                                                    onChangeText={props.setReferralCode}
                                                    autoCapitalize='none'
                                                    keyboardType='default'
                                                    style={{ paddingVertical: 2, fontSize: 16, }}
                                                    autoFocus={toggleKeyboard}
                                                    onBlur={() => { setToggleKeyboard(false) }}
                                                    onFocus={() => {
                                                        toggleUp()
                                                        setToggleKeyboard(true)
                                                    }}
                                                    />
                                            </View>
                                        </View>


                                        <View style={{ paddingTop: 10, flexDirection: 'column-reverse',  }}>
                                            <View style={{ flex:1, flexDirection: 'row', justifyContent: 'space-between'}}>
                                                <Text style={{ color: Colors.lightgray, paddingTop: 5, }}>
                                                    {strings.register_phone_number_placeholder}
                                                </Text>
                                                    {props.phone === null &&
                                                        <Text style={styles.highlightedText}>
                                                            {strings.required_text}
                                                        </Text>
                                                    }
                                                    {!props.isPhoneNumberValid &&
                                                        <Text style={styles.highlightedText}>
                                                            {strings.invalid_phoneNumber}
                                                        </Text>
                                                    }
                                            </View>
                                            <View style={{ borderBottomColor: 'lightgray',  borderBottomWidth: 0.5,  }}>
                                                <TextInput
                                                    value={props.phone}
                                                    onChangeText={props.setPhoneNumber}
                                                    autoCapitalize='none'
                                                    // maxLength={14}
                                                    keyboardType='phone-pad'
                                                    style={{ paddingVertical: 4, fontSize: 16, }}
                                                    autoFocus={toggleKeyboard}
                                                    onBlur={() => { setToggleKeyboard(false) }}
                                                    onFocus={() => {
                                                        toggleUp()
                                                        setToggleKeyboard(true)
                                                    }}
                                                    />
                                            </View>
                                        </View>

                                        <View style={{ flex: 1, paddingHorizontal: 5, marginTop: 30, flexDirection: 'row', }}>
                                            <View style={{flex:1, marginRight: 10, flexDirection: 'row', alignItems: 'center' }}>
                                                <CheckBox
                                                    lineWidth={2}
                                                    boxType={'square'}
                                                    value={props.earning_reminders}
                                                    onValueChange={props.setEarningReminders}
                                                    onCheckColor={'red'}
                                                    tintColor={'red'}
                                                    onTintColor={'red'}
                                                />

                                                <Text style={{ padding: 10, textAlign: 'center', fontSize: 14, }}>
                                                    {strings.register_earning_reminders_chk_text}
                                                </Text>
                                            </View>
                                        </View>

                                        <View style={{ flex: 1, paddingHorizontal: 5, marginTop: 30, flexDirection: 'row', justifyContent: 'space-around'}}>
                                            <View style={{flex:1}} />
                                            <View style={{ flex: 1, marginLeft: 10, marginRight: 5, }}>
                                                <TouchableOpacity   style={{ padding: 10, backgroundColor: Colors.brown, borderRadius: 20,  justifyContent: 'center', alignItems: 'center', }}
                                                                    onPress={props.onRegister}>
                                                    <Text style={{ color: Colors.black, fontSize: 16, textAlign: 'center' }}>
                                                        {strings.register_register_button_text}
                                                    </Text>
                                                </TouchableOpacity>
                                            </View>

                                        </View>

                                        <View style={{ flex: 1, paddingHorizontal: 5, marginTop: 40, flexDirection: 'row', justifyContent: 'space-around'}}>
                                            <Text style={{ color: Colors.lightgray, paddingBottom: 5, justifyContent: "center"}}>
                                                {strings.register_footer_text}
                                            </Text>

                                        </View>

                                        <View style={{ flex: 1, paddingHorizontal: 5, flexDirection: 'row', justifyContent: 'space-around'}}>
                                            <TouchableOpacity   onPress={props.onTOS}>
                                                <Text style={{ color: Colors.red, paddingBottom: 5, justifyContent: "center"}}  >
                                                    {strings.register_footer_text_link}
                                                </Text>
                                            </TouchableOpacity>
                                        </View>



                                    </View>
                            </View>

                        <View style={styles.footer}/>
                    </View>
            </SwipeablePanel>

        </Container>
    )
}

export default Registration
