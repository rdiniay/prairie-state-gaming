import React, { useCallback, useEffect, useState } from 'react'
import { connect, ConnectedProps, } from 'react-redux'
import { RootState, RootDispatch } from '../../redux'
import {StackActions ,useNavigation, DrawerActions, useFocusEffect } from '@react-navigation/native'
import Registration from './RegistrationPanel'
import { Screen } from '../../navigation/routes/mainRoutes'
import { Screen as RootScreen } from '../../navigation/routes/rootRoutes'
import { Auth, User } from '../../models'
import AuthActions, { AuthSelectors } from '../../redux/auth-redux'
import ProfileActions from '../../redux/profile-redux'
import RegistrationActions, { RegistrationSelectors } from '../../redux/registration-redux'
import Toast from 'react-native-toast-message';
import Utils from '../../services/shared/utils/Utils'
import { env } from '../../config'
import LoginActions from '../../redux/login-redux'
import Preference from 'react-native-preference'

const RegistrationContainer = (props: Props) =>  {
    const navigation = useNavigation()
    const [shouldNavigate, setShouldNavigate] = useState(true)
    const [firstname, setFirstname] = useState('')
    const [lastname, setLastname] = useState('')
    const [email, setEmail] = useState('')
    const [isValidEmail, setIsValidEmail] = useState(true)
    const [pin, setPin] = useState('')
    const [referral_code, setReferralCode] = useState('')
    const [phoneNumber, setPhone] = useState('')
    const [earning_reminders, setEarningReminders] = useState(true)
    const [username, setUsername] = useState('')

    const isFirstnameValid = firstname ? Utils.validateFirstname(firstname) : true

    const isLastnameValid = lastname ? Utils.validateLastname(lastname) : true

    const isPhoneNumberValid = phoneNumber ? Utils.validateUniversalPhoneNumber(phoneNumber) : true

    const onBack = () => {
        console.log("Back Pressed")
        navigation.dispatch(StackActions.pop());
    }

    const onRegister = async () => {
        let shouldReturn = false
        if (!firstname) { 
            setFirstname(null)
            shouldReturn = true
        }
        if (!lastname){
            setLastname(null)
            shouldReturn = true
        }
        if (!email){
            setEmail(null)
            shouldReturn = true
        }
        if (!phoneNumber){
            setPhone(null)
            shouldReturn = true
        }
        if (!pin){
            setPin(null)
            shouldReturn = true
        }

        if (!isFirstnameValid)
            shouldReturn = true
        if (!isLastnameValid)
            shouldReturn = true
        if (!isPhoneNumberValid)
            shouldReturn = true

        var newEmail: string = email

        if(email && email.includes("--")){
            const splitEmail = email.split("--", 2)
            const env = splitEmail[0]
            // Set Preference
            await Preference.set('ENV', env).then( () => {
                console.log(`Preference has been set for env: ${env}`)
            })
            newEmail = splitEmail[1]
        }

        if(!Utils.validateEmail(newEmail)){
            setIsValidEmail(false)
            return;
        }else{
            setIsValidEmail(true)
        }

        if (shouldReturn) return
        
       
        const user: User = {
            id: null,
            firstname,
            lastname,
            phoneNumber,
            referral_code,
            email: newEmail,
            pin,
            earning_reminders,
        }
        console.log("User: ",user)
        setUsername(newEmail)
        props.saveAccount(user)
    }


    useFocusEffect(
        useCallback ( ()=> {
            var newEmail: string = email

            if(email && email.includes("--")){
                const splitEmail = email.split("--", 2)
                const env = splitEmail[0]

                newEmail = splitEmail[1]
                console.log("ENV : ", env)
            }
            if(!Utils.validateEmail(newEmail)){
                setIsValidEmail(false)
            }else{
                setIsValidEmail(true)
            }
        }, [email])
    )

    useEffect(() => {
        const auth: Auth = props.auth
        if (auth && auth.accountId && shouldNavigate) {
            setShouldNavigate(false)
            setTimeout(() => {
                navigation.dispatch(StackActions.push(Screen.Login, { 
                    username: username,
                    pin: pin
                }));
            }, 500)
        }
        else if (!auth)
            setShouldNavigate(true)
    }, [props.auth])

    useEffect(() => {
        if (props.error) {
            let errorMessage = props.error.genericError
            let errorStatus =  props.error.statusCode ? `Error: ${props.error.statusCode}` : ``
            if (props.error.backendError) 
                errorMessage = props.error.backendError

            if (props.error.connectionError) 
                errorMessage = props.error.connectionError

            setTimeout(() => {
                if (__DEV__){
                    Toast.show({ type: 'custom', text1: `${errorMessage} ${errorStatus}`, })
                }else{
                    Toast.show({ type: 'custom', text1: `${errorMessage}`, })
                }
            }, 500);
        }
    }, [props.error])

    const setEmailAddress = (value: string) => {
        if (value && value.includes("--")) {
            const splitUserEmail = value.split("--", 2)
            const usernameEnv = `${splitUserEmail[0].toLowerCase()}--`
            setEmail(`${usernameEnv}${splitUserEmail[1]}`)
            return
        }
        setEmail(value)
    }

    const setPhoneNumber = (value: string) => {
        if(25 < value.length) return
        setPhone(value)
        /*
        var cleaned = ('' + value).replace(/\D/g, '')
        var match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/)
        if (match) {
            var usernameEnv = ''
            if (value && value.includes("--")) {
                const splitUserEmail = value.split("--", 2)
                usernameEnv = `${splitUserEmail[0].toLowerCase()}--`
            }
            var intlCode = (match[1] ? '+1 ' : ''),
                number = [intlCode, `${usernameEnv}(`, match[2], ') ', match[3], '-', match[4]].join('');
    
            setPhone(number)
            return;
        }
        if (value && value.includes("--")) {
            const splitUserEmail = value.split("--", 2)
            const usernameEnv = `${splitUserEmail[0].toLowerCase()}--`
            setPhone(`${usernameEnv}${splitUserEmail[1]}`)
            return
        }
        setPhone(value)
        */
    }

    const onTOS = () => {
        setTimeout(() => {
            navigation.dispatch(StackActions.push(RootScreen.TOSNavigators));
        },500)

    }

    return (
        <Registration  
                        onBack={onBack}
                        onRegister={onRegister}
                        onTOS={onTOS}
                        isFirstnameValid={isFirstnameValid}
                        isLastnameValid={isLastnameValid}
                        isPhoneNumberValid={isPhoneNumberValid}
                        firstname={firstname}
                        lastname={lastname}
                        email={email}
                        isValidEmail={isValidEmail}
                        pin={pin}
                        referral_code={referral_code}
                        phone={phoneNumber}
                        earning_reminders={earning_reminders}
                        setFirstname={setFirstname}
                        setLastname={setLastname}
                        setEmailAddress={setEmailAddress}
                        setPin={setPin}
                        setReferralCode={setReferralCode}
                        setPhoneNumber={setPhoneNumber}
                        setEarningReminders={setEarningReminders}
                        { ...props } />
    )
}

const mapStateToProps = (state: RootState) => ({
    auth: AuthSelectors.auth(state),
    error: RegistrationSelectors.error(state),
    isLoading: RegistrationSelectors.isLoading(state),
})
const mapDispatchToProps = (dispatch: RootDispatch) => ({
    getProfile: () => dispatch(ProfileActions.getProfile()),
    signInSuccess: (auth: Auth) => dispatch(LoginActions.signInSuccess(auth)),
    saveAccount: (user: User) => dispatch(RegistrationActions.createAccount(user))
})

const connector = connect(mapStateToProps, mapDispatchToProps)
type Props = ConnectedProps<typeof connector>

export default connector(RegistrationContainer)

