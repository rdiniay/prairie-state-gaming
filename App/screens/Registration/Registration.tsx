import React from 'react'
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    SectionList,
    TextInput,
    Linking
} from  'react-native'
import CheckBox from '@react-native-community/checkbox'
import { Container, Header } from '../../components'
import styles from './Styles'
import { Colors } from '../../styles'
import {strings} from "../../localization/strings";
import { string } from 'yargs'
import { LoadingDialog } from '../../components'

type Props = {
    // REQUIRES SPACE TO ENABLE PROPS FUNCTION CALL
    onRegister: () => void,
    onTOS: () => void,
    setFirstname: (value: string) => void,
    setLastname: (value: string) => void,
    setEmail: (value: string) => void,
    setPin: (value: string) => void,
    setReferralCode: (value: string) => void,
    setPhoneNumber: (value: string) => void,
    setEarningReminders: (value: boolean) => void,
    firstname: string,
    lastname: string,
    email: string,
    isValidEmail: boolean,
    pin: string,
    referral_code: string,
    phone: string,
    earning_reminders: boolean,
    isLoading: boolean,
}

const Registration = (props: Props) =>{

    const header = () => (
        <Header style={{ paddingTop: 15,  paddingBottom: 20, shadowColor: '#FFF', backgroundColor: '#295FEA' }} //backgroundColor: '#EBECF1' }}
                center={(
                    <View style={{ alignItems: 'flex-end' }}>
                      <Text style={{ color: 'white', fontWeight: 'bold' }}>
                        
                      </Text>
                    </View>
                )} 
                // right={(
                //    <View style={{ flex: 0.1, top: -18, alignItems: 'center', }}>
                //     <View style={{ borderRadius: 20, height: 40, width: 40, backgroundColor: '#EBECF160', overflow: 'hidden' ,}} >
                //       <Image resizeMode="cover" source={images.cityOfBaguioSeal} style={{ flex: 3,  height: 40, width: 40, }} /> 
                //     </View>
                //   </View>
                // )}
        />
    )
    return(
        <Container  // safeAreaViewTopStyle={styles.safeArea}
                    statusBarStyle={'dark'}
                    scrollViewEnable={true}
                    // header={header}
                    style={styles.container} >

                <LoadingDialog visible={props.isLoading} subtitle={'Please wait...'} />

                <View style={{ flex: 1, flexDirection: 'column', margin: 25, marginTop: 30, borderRadius: 15, paddingVertical: 10, paddingHorizontal: 10, backgroundColor: 'white', }}>

                        <View style={{ height: 80,  alignItems: 'center' , }}>
                           <Image style={{width:237,}} source={require('../../assets/logo_text.png')} />
                        </View>

                        <View style={{ flex: 1, marginTop: 10, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ paddingBottom: 5, fontSize: 18, }}>
                                {strings.register_header_title}
                            </Text>
                        </View>
                        <View style={{ padding: 10, paddingTop: 10, }} >
                            <View style={{ borderBottomColor: 'lightgray', borderBottomWidth: 0.5, paddingVertical: 5, marginHorizontal: 15, }}>
                                <View style={{ flex:1, flexDirection: 'row', justifyContent: 'space-between'}}>
                                    <Text style={{ flex: 1, color: Colors.red, paddingBottom: 5, }}>
                                        {strings.register_firstname_placeholder}
                                    </Text>
                                    {props.firstname === null &&
                                        <Text style={{ color: Colors.red, paddingBottom: 5, }}>
                                            {strings.required_text}
                                        </Text>
                                    }
                                </View>
                                <TextInput onChangeText={props.setFirstname} keyboardType='email-address' spellCheck={false} style={{ flex: 1, paddingVertical: 2, fontSize: 16, }}/>
                            </View>
                            <View style={{ paddingTop: 5, borderBottomColor: Colors.lightgray, borderBottomWidth: 0.5, paddingVertical: 5, marginHorizontal: 15, }}>
                                <View style={{ flex:1, flexDirection: 'row', justifyContent: 'space-between'}}>
                                    <Text style={{ color: Colors.red, paddingBottom: 5, }}>
                                        {strings.register_lastname_placeholder}
                                    </Text>
                                        {props.lastname === null &&
                                            <Text style={{ color: Colors.red, paddingBottom: 5, }}>
                                                {strings.required_text}
                                            </Text>
                                        }
                                </View>
                                <TextInput onChangeText={props.setLastname} keyboardType='email-address' style={{ paddingVertical: 2, fontSize: 16, }}/>
                            </View>
                            <View style={{ paddingTop: 5, borderBottomColor: Colors.lightgray, borderBottomWidth: 0.5, paddingVertical: 5, marginHorizontal: 15, }}>
                                <View style={{ flex:1, flexDirection: 'row', justifyContent: 'space-between'}}>
                                    <Text style={{ color: Colors.red, paddingBottom: 5, }}>
                                        {strings.register_email_placeholder}
                                    </Text>
                                        {props.email === null &&
                                            <Text style={{ color: Colors.red, paddingBottom: 5, }}>
                                                {strings.required_text}
                                            </Text>
                                        }

                                        {props.email !== null && props.email.length !== 0 && !props.isValidEmail &&
                                            <Text style={{ color: Colors.red, marginTop: 5 }}>
                                                {strings.invalid_email}
                                            </Text>
                                        }
                                </View>
                                <TextInput onChangeText={props.setEmail} autoCapitalize='none' keyboardType='email-address' spellCheck={false} style={{ paddingVertical: 2, fontSize: 16, }}/>
                            </View>
                            <View style={{ paddingTop: 5, borderBottomColor: Colors.lightgray, borderBottomWidth: 0.5, paddingVertical: 5, marginHorizontal: 15, }}>
                                <View style={{ flex:1, flexDirection: 'row', justifyContent: 'space-between'}}>
                                    <Text style={{ color: Colors.red, paddingBottom: 5, }}>
                                        {strings.register_pin_placeholder}
                                    </Text>
                                        {props.pin === null &&
                                            <Text style={{ color: Colors.red, paddingBottom: 5, }}>
                                                {strings.required_text}
                                            </Text>
                                        }


                                </View>
                                <TextInput maxLength={4} onChangeText={props.setPin} autoCapitalize='none' secureTextEntry={true} keyboardType='number-pad' style={{ paddingVertical: 2, fontSize: 16, }}/>
                            </View>

                            <View style={{ paddingTop: 5, borderBottomColor: Colors.lightgray, borderBottomWidth: 0.5, paddingVertical: 5, marginHorizontal: 15, }}>
                                <View style={{ flex:1, flexDirection: 'row', justifyContent: 'space-between'}}>
                                    <Text style={{ color: Colors.red, paddingBottom: 5, }}>
                                        {strings.register_referral_code_placeholder}
                                    </Text>
                                        {props.referral_code === null &&
                                            <Text style={{ color: Colors.red, paddingBottom: 5, }}>
                                                {strings.required_text}
                                            </Text>
                                        }
                                </View>
                                <TextInput onChangeText={props.setReferralCode} autoCapitalize='none' keyboardType='phone-pad' style={{ paddingVertical: 2, fontSize: 16, }}/>
                            </View>


                            <View style={{ paddingTop: 5, borderBottomColor: Colors.lightgray, borderBottomWidth: 0.5, paddingVertical: 5, marginHorizontal: 15, }}>
                                <View style={{ flex:1, flexDirection: 'row', justifyContent: 'space-between'}}>
                                    <Text style={{ color: Colors.red, paddingBottom: 5, }}>
                                        {strings.register_phone_number_placeholder}
                                    </Text>
                                        {props.phone === null &&
                                            <Text style={{ color: Colors.red, paddingBottom: 5, }}>
                                                {strings.required_text}
                                            </Text>
                                        }
                                </View>
                                <TextInput value={props.phone} onChangeText={props.setPhoneNumber} autoCapitalize='none' maxLength={14} keyboardType='phone-pad' style={{ paddingVertical: 2, fontSize: 16, }}/>
                            </View>

                            <View style={{ flex: 1, paddingHorizontal: 10, marginTop: 10, flexDirection: 'row', }}>
                                <View style={{flex:1, marginLeft: 5, marginRight: 10, flexDirection: 'row', alignItems: 'center' }}>
                                    <CheckBox
                                        lineWidth={2}
                                        boxType={'square'}
                                        value={props.earning_reminders}
                                        onValueChange={props.setEarningReminders}
                                        onCheckColor={'red'}
                                        tintColor={'red'}
                                        onTintColor={'red'}
                                    />

                                    <Text style={{ padding: 10, textAlign: 'center', fontSize: 14, }}>
                                        {strings.register_earning_reminders_chk_text}
                                    </Text>
                                </View>
                            </View>

                            <View style={{ flex: 1, paddingHorizontal: 5, marginTop: 30, flexDirection: 'row', justifyContent: 'space-around'}}>
                                <View style={{flex:1}} />
                                <View style={{ flex: 1, marginLeft: 10, marginRight: 5, }}>
                                    <TouchableOpacity   style={{ padding: 10, backgroundColor: Colors.red, borderRadius: 20,  justifyContent: 'center', alignItems: 'center', }}
                                                        onPress={props.onRegister}>
                                        <Text style={{ color: Colors.white, fontSize: 16, textAlign: 'center' }}>
                                            {strings.register_register_button_text}
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            
                            </View>

                            <View style={{ flex: 1, paddingHorizontal: 5, marginTop: 40, flexDirection: 'row', justifyContent: 'space-around'}}>
                                <Text style={{ color: Colors.lightgray, paddingBottom: 5, justifyContent: "center"}}>
                                    {strings.register_footer_text}
                                </Text>

                            </View>

                            <View style={{ flex: 1, paddingHorizontal: 5, flexDirection: 'row', justifyContent: 'space-around'}}>
                                <TouchableOpacity   onPress={props.onTOS}>
                                    <Text style={{ color: Colors.red, paddingBottom: 5, justifyContent: "center"}}  > 
                                        {strings.register_footer_text_link}
                                    </Text>
                                </TouchableOpacity>
                            </View>

                        
                            
                        </View>
                </View>
        </Container>
    )
}

export default Registration