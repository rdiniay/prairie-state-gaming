import ScaledSheet from 'react-native-scaled-sheet'
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import { Colors, Commons } from '../../styles'
import { Dimensions, PixelRatio, } from  'react-native'
const { width, height } = Dimensions.get('window');

const styles = ScaledSheet.create({
    safeArea: {
        backgroundColor: Colors.red,
    },
    container: {
        flex: 1,
    },
    footer: {
        height: 100,
    },
    highlightedText: {
        color: Colors.red, 
        paddingTop: 5,
    },
    imagePanelMarginTopWithPhoneNumber: { 
        ...Commons.ContentLogoViewStyle(),
        marginTop: (height*.120) + useSafeAreaInsets().top,
    },
    imagePanelMarginTopWithoutPhoneNumber: { 
        ...Commons.ContentLogoViewStyle(),
        marginTop: (height*.140) + useSafeAreaInsets().top,
    }
})

const commons =  ScaledSheet.create({
    headerTouchableOpacityStyle: Commons.HeaderTouchableOpacityStyle(),
    headerTextStyle: Commons.HeaderBackButtonTextStyle(),
    contentHeaderTextViewStyle: Commons.ContentHeaderTextViewStyle(),
    contentHeaderSubTextViewStyle: Commons.ContentHeaderSubTextViewStyle(),
    leftContainerStyle:  Commons.HeaderContentLeftContainerStyle(),
    contentLogoViewStyle: Commons.ContentLogoViewStyle(),
    contentLogoStyle: Commons.ContentLogoStyle(),
    
    contentSwipePanelStyle: {
        flex:1, 
        backgroundColor: Colors.lightBrown,
    },
    contentTitleStyle: {
        ...Commons.ContentTitleStyle(),
        marginBottom: 200 / PixelRatio.get(),
    },
    contentInnerViewStyle: Commons.InnerViewStyle(),
    contentInnerContentViewStyle: Commons.InnerContentViewStyle(),
    contentInlineContentViewInnerHeaderViewStyle: Commons.InlineContentViewInnerHeaderViewStyle(),
    contentInlineContentViewInnerHeaderTextViewStyle: {
        textAlign: 'center'  ,  
        ...Commons.InlineContentViewInnerHeaderTextViewStyle(),
        fontSize: 13 / PixelRatio.getFontScale(),
    },
    inlineContentViewStyle: {
        paddingTop: 10, 
        borderBottomColor: Colors.darkgray, 
        borderBottomWidth: 0.5, 
        paddingVertical: 5, 
        marginHorizontal: 8,
    },
    inlineContentTextViewStyle: {
        ...Commons.InlineContentViewInnerContentTextViewStyle(),
        marginHorizontal: 15,
    },
    contentInlineContextTextInputStyle : {
        padding: 8, 
        marginHorizontal: 7, 
        fontSize: 12 / PixelRatio.getFontScale(), 
        fontFamily: 'Raleway-Regular'
    },
    contentInlineClickableTextViewStyle :  { 
        marginHorizontal: 15, 
        color: Colors.black, 
        paddingVertical: 8,
        fontFamily: 'Raleway-Regular',
        fontSize: 12 / PixelRatio.getFontScale(), 
    },
    customHeaderNavMessageStyle : { 
        fontSize: 16 / PixelRatio.getFontScale(),
        padding: 0,
        fontFamily: 'Raleway-Regular',
    },
    customHeaderRightViewStyle: { 
        height: 35, 
        padding: 2, 
        alignItems: 'center', 
        justifyContent: 'center', 
        overflow: 'hidden', 
        borderWidth: 1, 
        borderColor: Colors.white, 
        borderRadius: 15
    },
    customHeaderRightTextViewStyle: { 
        textAlign: 'center', 
        fontFamily: 'Raleway-Regular',
        fontSize: 15 / PixelRatio.getFontScale(), 
        color: Colors.white,
    },

    customEntryProgressViewStyle: { 
        zIndex: 1, 
        position: 'absolute', 
    },

    customEntriesTextStyle :  {
        textAlign: 'center', 
        fontSize: 16 / PixelRatio.getFontScale(), 
        fontFamily: 'Raleway-Bold', 
        fontWeight: 'bold', 
        paddingVertical: 0,
    },

    customEntriesSubTextStyle: (shouldShowMultiplier: Boolean, color: string) => {
        return {
            textAlign: 'center',
            fontFamily: 'Raleway-Bold',
            fontWeight: 'bold',
            color: color,
            paddingVertical: 5,
            fontSize: shouldShowMultiplier ? 10 / PixelRatio.getFontScale() : 12/ PixelRatio.getFontScale(),
        }

    },

    customPromotionItemTextStyl1: { 
        color: Colors.lightgray, 
        fontSize: 15 / PixelRatio.getFontScale(),
        fontFamily: 'Raleway-Regular',
        fontWeight: 'normal',
        fontStyle: 'normal'
    },

    customPromotionItemTextStyl2: { 
        color: Colors.lightgray, 
        fontSize: 18 / PixelRatio.getFontScale(),
        fontFamily: 'Raleway-Bold',
        fontStyle: 'normal',
        fontWeight: 'normal',
        textTransform: 'uppercase',

    },

    customPromotionItemTextStyl3: { 
        color: Colors.lightgray, 
        fontSize: 12 / PixelRatio.getFontScale(),
        fontFamily: 'Raleway-Regular',
        fontStyle: 'normal',
        fontWeight: 'normal',
    },

    customLocalEntriesTextStyle: { 
        position: 'absolute', 
        fontFamily: 'Raleway-Regular' ,
        fontSize: 15 / PixelRatio.getFontScale(), 
        fontWeight: 'bold', 
    }

})

const commonsAssets = {
    contentImageSource: Commons.ContentImageSource(),
}

export {
    styles,
    commons,
    commonsAssets,
}