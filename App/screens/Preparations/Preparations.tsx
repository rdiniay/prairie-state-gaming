import React from 'react'
import { ConnectedProps } from 'react-redux'
import {
    View,
    TextInput,
    TouchableOpacity,
    Image,
    ScrollView,
    FlatList,
    SectionList,
    Dimensions, Platform
} from 'react-native'
import { Container, Header, Text, Button, LottieView } from '../../components'
import { Colors } from '../../styles'
import {styles, commonsAssets} from './Styles'
import { images, lotties } from '../../constants'
import {strings} from "../../localization/strings";
import { getSystemVersion  } from 'react-native-device-info'
import {PreparationsDataSource, Props} from './PreparationsDataSource'
const { width, height } = Dimensions.get('window')

const Onboarding = (props: Props) =>{

    const header = () => (
      <Header   style={styles.safeArea}
                center={(
                    <Image resizeMode={'contain'}
                          style={styles.onboardingLogo}
                          source={commonsAssets.contentImageSource} />
                )}
                leftContainerStyle={styles.onboardingHeaderSpace}
                centerContainerStyle={styles.onboardingHeaderSpace}
                rightContainerStyle={styles.onboardingHeaderSpace}
                left={props.isSettings ? (
                    <Button style={{ right: 25, backgroundColor: Colors.transparent, }}
                            title={`Back`}
                            titleStyle={{ fontSize: 18, }}
                            onPress={props.onCancel}
                             />
                 ): <View />}
      />
    )
    const onboardingData = {
      index: 0,
      title: strings.onboarding_title,
      message: Platform.OS === 'android'? strings.onboarding_message_android : strings.onboarding_message,
      lottiePhoto: lotties.onBoardingLocation,
    }

    const PreparationItem = (item: any) => {
      const isNearby = (item?.title?.includes("Nearby") || item?.title?.includes("Bluetooth") || item?.title?.includes("Data"))
      return (
        <View style={{ flex: 1, flexDirection: 'row', paddingBottom: 12, }}>
              <View style={{ flex: .1, paddingLeft: 10, }}>
                  <View style={{ backgroundColor: Colors.darkRed, alignItems: 'center', paddingVertical: isNearby ? 8 : 10, borderRadius: isNearby ? 40 : 30,}}>
                      <Image source={item?.icon} style={{ tintColor: Colors.white, width: isNearby ? 20 : 15, height: isNearby ? 20 : 15, }} />
                  </View>
              </View>
              <View style={{ flex: item?.isGranted ? .725 : .625, padding: 5, paddingHorizontal: 10, paddingBottom: 8, }}>
                  <Text style={{ color: Colors.white, paddingBottom: 3, fontWeight: 'bold' }}>
                      {item?.title}
                  </Text>
                  <Text numberOfLines={3} style={{ color: Colors.white, }}>
                      {item?.description}
                  </Text>
              </View>
              <View style={{ flex: item?.isGranted ? .25 :.35, }}>
                    {item?.isGranted &&
                        <Button style={{ position: 'absolute', alignSelf: 'center', backgroundColor: Colors.green, padding: 18, borderRadius: 10, borderWidth: 2, borderColor: Colors.white }}
                                disabled={true}
                                iconStyle={{ left: 5, }}
                                icon={<Image source={images.checkIcon} style={{tintColor: Colors.white, width: 28, height:  28, }} />}
                            />
                    }
                    {!item?.isGranted && 
                        <Button style={{ backgroundColor: Colors.lightRed, padding: 8, borderRadius: 10, borderWidth: 2, borderColor: Colors.white }}
                                titleStyle={{ fontWeight: 'bold', paddingLeft: 30, fontSize: 12, textAlign: 'left' }}
                                title={`OPEN\nSETTINGS`} 
                                iconStyle={{ left: 8, }}
                                icon={<Image source={images.crossIcon} style={{tintColor: Colors.white, width: 25, height:  25, }} />}
                                onPress={item?.onPress} />
                    }
              </View>
        </View>
      )
    }

    const renderPermissionList = (props: Props) => {
        const list = PreparationsDataSource.getPermissionList(props)
        return list.map(item => {
            return <PreparationItem  
                    key={item.key}
                    icon={item.image}
                    title= {item.title}
                    description= {item.description}
                    onPress={item.onPress}
                    isGranted={item.isGranted}
                />;
        });
    };

    return (
        <Container      safeAreaViewTopStyle={styles.safeArea}
                        statusBarStyle={'light'}
                        // scrollViewEnable={true}
                        header={header}
                        style={styles.container} >
          <Text style={styles.onboardingTitle} >
              {`${props.isSettings ? `` : `REQUIRED `}SETTINGS`}
          </Text>
          <ScrollView style={{ paddingHorizontal: 10, paddingTop: height *.06, }}>
                <View style={{ flex: 1, flexDirection: 'row'  }}>
                    <View style={{ flex: .7 }} />
                    <View style={{ flex: .3, justifyContent: 'flex-end', alignItems: 'center' }}>
                        <Text   value={`STATUS`}
                                style={{ color: Colors.white, fontWeight: 'bold', fontSize: 18, textDecorationLine: 'underline', marginBottom: 25, }}/>
                    </View>
                </View>
                {renderPermissionList(props)}
          </ScrollView>

          {!props.isSettings &&
                <Button style={{ backgroundColor: props.enableNextButton? Colors.green : Colors.gray, padding: 15, margin: 20, marginBottom: 40, }}
                        title={'Next'} 
                        onPress={() => props.enableNextButton ? props.onNext() : {}} />
          }
        </Container>
    )
}

export default Onboarding
