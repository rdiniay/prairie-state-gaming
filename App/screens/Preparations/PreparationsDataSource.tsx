import {
    Platform,
} from 'react-native'
import { images } from '../../constants'
import { Colors } from '../../styles'
import {strings} from "../../localization/strings";
import { string } from 'yargs';

/**
 * An array of JSON data that corresponds to the location permission UI per
 * android version
 *
 * locationInstructions['A8']
 *      - These instructions are for Android 8 and 9 since both OS have the
 *        same location permission screens
 *
 * locationInstructions['A10']
 *      - These instructions are for Android 10 location permission screens
 *
 * locationInstructions['A11']
 *      - These instructions are for Android 11 location permission screens
 *
 * locationInstructions['A12']
 *      - These instructions are for Android 12 location permission screens
 *      - In Android 12 the location permission have two steps,
 *        (Precise -> While Using the app)
 */
const locationInstructions = {
    'A12': [
        {
            key: 'one',
            header: strings.permission_follow_steps_provided,
            title: strings.permission_step_1,
            text: strings.permission_step_select_precise,
            image: images.locationPermissionA12Step1,
            footer: '',
            backgroundColor: Colors.red,
          },
          {
            key: 'two',
            header: '',
            title: strings.permission_step_2,
            text: strings.permission_step_select_while_using_the_app,
            image: images.locationPermissionA12Step2,
            footer: strings.permission_continue_if_step_is_done,
            backgroundColor: Colors.red,
          },
          {
            key: 'three',
            header: '',
            title: strings.permission_step_3,
            text: strings.permission_step_select_enable_geolocation,
            image: images.geolocationPermissionStep,
            footer: strings.permission_click_turn_on_button,
            backgroundColor: Colors.red,
          }
    ],
    'A11' : [
        {
            key: 'one',
            header: strings.permission_follow_steps_provided,
            title: strings.permission_step_1,
            text: strings.permission_step_select_while_using_the_app,
            image: images.locationPermissionA11,
            footer: strings.permission_continue_if_step_is_done,
            backgroundColor: Colors.red,
        },
        {
          key: 'two',
          header: '',
          title: strings.permission_step_2,
          text: strings.permission_step_select_enable_geolocation,
          image: images.geolocationPermissionStep,
          footer: strings.permission_click_turn_on_button,
          backgroundColor: Colors.red,
        }
    ],
    'A10' : [
        {
            key: 'one',
            header: strings.permission_follow_steps_provided,
            title: strings.permission_step_1,
            text: strings.permission_step_select_allow_only_while_using_the_app,
            image: images.locationPermissionA10,
            footer: strings.permission_continue_if_step_is_done,
            backgroundColor: Colors.red,
        },
        {
          key: 'two',
          header: '',
          title: strings.permission_step_2,
          text: strings.permission_step_select_enable_geolocation,
          image: images.geolocationPermissionStep,
          footer: strings.permission_click_turn_on_button,
          backgroundColor: Colors.red,
        }
    ],
    'A8' : [
        {
            key: 'one',
            header: strings.permission_follow_steps_provided,
            title: strings.permission_step_1,
            text: strings.permission_step_select_allow,
            image: images.locationPermissionA8,
            footer: strings.permission_continue_if_step_is_done,
            backgroundColor: Colors.red,
        },
        {
          key: 'two',
          header: '',
          title: strings.permission_step_2,
          text: strings.permission_step_select_enable_geolocation,
          image: images.geolocationPermissionStep,
          footer: strings.permission_click_turn_on_button,
          backgroundColor: Colors.red,
        }
    ],
}

const nearbyDevicesInstructions = {
    'A12': [
        {
            key: 'one',
            header: strings.permission_follow_steps_provided,
            title: strings.permission_step_1,
            text: strings.permission_step_under_app_settings,
            image: images.nearbyDevicesPermission12Step1,
            footer: strings.permission_step_locate_find_permissions,
            backgroundColor: Colors.red,
        },
        {
            key: 'two',
            header: '',
            title: strings.permission_step_2,
            text: strings.permission_step_select_nearby_devices,
            image: images.nearbyDevicesPermission12Step2,
            footer: '',
            backgroundColor: Colors.red,
        },
        {
            key: 'three',
            header: '',
            title: strings.permission_step_3,
            text: strings.permission_step_allow_quoted,
            image: images.nearbyDevicesPermission12Step3,
            footer: '',
            backgroundColor: Colors.red,
        }
    ]
}

const openSettingsInstructions = [
    {
        key: 'one',
        header: strings.permission_allow_precise_loction_in_app_settings,
        title: strings.permission_step_1,
        text: strings.permission_click_permissions_in_app_info,
        image: images.openSettingsAStep1,
        footer: '',
        backgroundColor: Colors.red,
      },
      {
        key: 'two',
        header: '',
        title: strings.permission_step_2,
        text: strings.permission_look_for_location_in_allowed_section,
        image: images.openSettingsAStep2,
        footer: '',
        backgroundColor: Colors.red,
      },
      {
        key: 'three',
        header: '',
        title: strings.permission_step_3,
        text: strings.permission_choose_allow_all_the_time,
        image: images.openSettingsAStep3,
        footer: '',
        backgroundColor: Colors.red,
      },
      {
        key: 'four',
        header: '',
        title: strings.permission_step_4,
        text: strings.permission_step_select_enable_geolocation,
        image: images.geolocationPermissionStep,
        footer: '',
        backgroundColor: Colors.red,
      }
]

export type Props = {
    onCancel: () => void,
    onNext: () => void,

    onLocationPermission: () => void,
    onNearByDevicesPermission: () => void,
    onBluetoothConnection: () => void,
    onNetworkConnection: () => void,
    onNotificationPermission: () => void,

    locationPermission: boolean,
    nearbyDevicesPermission: boolean,
    bluetoothConnection: boolean,
    networkConnection: boolean,
    notificationPermission: boolean,
    enableNextButton: boolean,
    isSettings: boolean,
}

interface PermissionsData {
    key: number,
    image: any;
    title: string;
    description: string;
    onPress: () => void;
    isGranted: boolean;
}

const PermissionList = (props: Props) => {
    const allPermissions: PermissionsData[] = [];

    const location : PermissionsData = {
        key: 1,
        image: images.locationPinIcon,
        title: strings.permission_gps_title,
        description: props?.locationPermission ? strings.permission_correct_gps_description : strings.permission_incorrect_gps_description,
        onPress: props?.onLocationPermission,
        isGranted: props?.locationPermission,
    };

    allPermissions.push(location);

    if ( Platform.Version >= 31 ) {
        const nearby: PermissionsData = {
            key: 2,
            image: images.nearbyDevicesIcon,
            title: strings.permission_open_nearby_devices_title,
            description: props?.nearbyDevicesPermission ? strings.permission_correct_nearby_devices_description : strings.permission_incorrect_nearby_devices_description,
            onPress: props?.onNearByDevicesPermission,
            isGranted: props?.nearbyDevicesPermission,
        }

        allPermissions.push(nearby);
    }

    const bluetooth : PermissionsData = {
        key: 3,
        image: images.bluetoothIcon,
        title: strings.permission_bluetooth_title,
        description: props?.bluetoothConnection ? strings.permission_correct_bluetooth_description : strings.permission_incorrect_bluetooth_description,
        onPress: props?.onBluetoothConnection,
        isGranted: props?.bluetoothConnection,
    };

    allPermissions.push(bluetooth);

    const network : PermissionsData = {
        key: 3,
        image: images.dataAccessIcon,
        title: strings.permission_data_access_title,
        description: props?.networkConnection ? strings.permission_correct_data_access_description : strings.permission_incorrect_data_access_description,
        onPress: props?.onNetworkConnection,
        isGranted: props?.networkConnection,
    };

    allPermissions.push(network);

    if (props.isSettings) {
        const notification : PermissionsData = {
            key: 4,
            image: images.notificationIcon,
            title: strings.permission_notifications_title,
            description: props?.notificationPermission ? strings.permission_correct_notification_description : strings.permission_incorrect_notification_description,
            onPress: props?.onNotificationPermission,
            isGranted: props?.notificationPermission,
        };
    
        allPermissions.push(notification);
    }

    return allPermissions
}

/**
 * Instruction Helper Class
 */
const PreparationsDataSource = {

    /**
     * Helper function that checks the android OS verson and returns the
     * instruction data depending on the steps on how to enable the location
     * permission.
     *
     * Android 8 and 9, have the same location permission UI, thus we can use
     * the default value locationInstructions['A8']
     *
     * @returns An array of JSON data
     */
    getLocationPermisionInstructions: function(){
        // Android 8 and 9
        var data = locationInstructions['A8']

        if (Platform.Version === 29) {
            // Android 10
            data = locationInstructions['A10']
        }
        else if (Platform.Version === 30) {
            // Android 11
            data = locationInstructions['A11']
        }
        else if (Platform.Version >= 31) {
            // Android 12 and Up
            data = locationInstructions['A12']
        }

        return data
    },

    getNearbyDevicesPermissionInstructions: function(){
        return nearbyDevicesInstructions['A12']

    },

    getOpenSettingsInstruction: function() {
        return openSettingsInstructions
    },

    getPermissionList: function(props: Props) {
        return PermissionList(props)
    }
}

export {PreparationsDataSource}
