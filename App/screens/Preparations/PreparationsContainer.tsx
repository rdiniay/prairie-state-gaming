import React, { useEffect, useState, useCallback } from 'react'
import {Alert, Platform, BackHandler} from 'react-native'
import { connect, ConnectedProps } from 'react-redux'
import { RootState, RootDispatch } from '../../redux'
import {StackActions ,useNavigation, DrawerActions, useFocusEffect } from '@react-navigation/native'
import { Screen } from '../../navigation/routes/preparationsRoutes'
import Preparations from './Preparations'
import {PERMISSIONS, request, openSettings, requestMultiple, RESULTS} from 'react-native-permissions'
import { env } from '../../config'
import crashlytics from '@react-native-firebase/crashlytics';
import { getBrand, getSystemVersion  } from 'react-native-device-info'
import AppLaunchActions, { AppLaunchSelectors } from '../../redux/app-launch-redux'
import {strings} from "../../localization/strings";
import BackgroundGeolocation, { Geofence } from "react-native-background-geolocation";
import { LocationPermissionStatus } from '../../models'
import { AuthSelectors } from '../../redux/auth-redux'
import { AppStateSelectors } from '../../redux/app-state-redux'
import PreparationActions, { PreparationSelectors } from '../../redux/preparation-redux'
import BluetoothStateManager from 'react-native-bluetooth-state-manager';
import Utils from "../../services/shared/utils/Utils";
import MessagingActions, { MessagingSelectors } from '../../redux/messaging-redux'
import LocationActions from '../../redux/location-redux'


const PreparationsContainer = (props: Props) => {
    const navigation = useNavigation()
    const [locationPermission, setLocationPermission] = useState(false)
    const [haveRequestedPermission, setHaveRequestedPermission] = useState(false)
    const [haveRequestedNearbyDevicesPermission, setHaveRequestedNearbyDevicesPermission] = useState(false)
    const [nearbyDevicesPermission, setNearbyDevicesPermission] = useState(false)
    const [bluetoothPermission, setBluetoothPermission] = useState(true)
    const [enableNextButton, setEnableNextButton] = useState(false)

    const onCancel = () => {
        navigation.goBack();
    }

    const onNext = () => {
        props.onboardingLocationSuccess()
        navigation.dispatch(StackActions.popToTop());
    }

    const onLocationPermission = () => {
        if (!haveRequestedPermission) {
            navigation.dispatch(StackActions.push(Screen.LocationPermission))
            setHaveRequestedPermission(true)
        }
        else {
            if (haveRequestedPermission && !locationPermission ) {
                navigation.dispatch(StackActions.push(Screen.OpenSettingsInstruction))
            }
        }
    }

    const onBluetoothConnection = () => {
        // Simplify BLuetooth permission request
        // navigation.dispatch(StackActions.push(Screen.BluetoothConnection))
        if (env.IS_DEBUG){
            console.log("Check Bluetooth Permission called()");
        }
        if (Platform.OS === 'ios'){
            navigation.dispatch(StackActions.push(Screen.BluetoothConnection))
        }
        else
        {
            if(!props.nearbyDevices && parseInt(getSystemVersion()) >= 12){
                Alert.alert(
                    `${strings.permission_bluetooth_requires_nearby_devices_title}`,
                    `${strings.permission_bluetooth_requires_nearby_devices_description}`,
                    [
                        {
                            text: `${strings.permission_open_nearby_devices_title}`,
                            onPress: () => {
                                onNearByDevicesPermission()
                            }
                        }
                    ], { cancelable: true},

                );
            }else{
                BluetoothStateManager.requestToEnable().then( (isEnabled) => {
                    console.log("Android Bluetooth Permission: ", isEnabled.valueOf())
                    setBluetoothPermission(isEnabled.valueOf())
                })
                    .catch( (exception) => {
                        crashlytics().recordError(exception)
                        console.log("Bluetooth Permission Exception ", exception)
                        setBluetoothPermission(false)
                    })
            }

        }
    }

    const onNetworkConnection = () => {
        navigation.dispatch(StackActions.push(Screen.NetworkConnection))
    }

    const onNotificationPermission = () => {
        props.requestMessagingPermission()
    }

    const onNearByDevicesPermission = () => {
        if(Platform.OS === 'ios') {
            return
        }

        if (parseInt(getSystemVersion()) >= 12 ){
            if (!haveRequestedNearbyDevicesPermission) {
                requestMultiple(
                    [ PERMISSIONS.ANDROID.BLUETOOTH_CONNECT,
                        PERMISSIONS.ANDROID.BLUETOOTH_SCAN,
                    ]
                )
                    .then( results => {
                        console.log('PreparationsContainer: ', results)
                        if (results['android.permission.BLUETOOTH_CONNECT']
                            && results['android.permission.BLUETOOTH_SCAN'] === 'granted') {
                            console.log('PreparationsContainer: Android Nearby Devices Permissions: ', results)
                            setNearbyDevicesPermission(true)
                        }else{
                            setNearbyDevicesPermission(false)
                            if(haveRequestedNearbyDevicesPermission){
                                navigation.dispatch(StackActions.push(Screen.NearbyDevicesPermission))
                            }
                        }
                    })
                    .catch( exception => {
                        crashlytics().recordError(exception)
                        console.log("Nearby Devices Permission Exception ", exception)
                        setNearbyDevicesPermission(false)
                    })

                setHaveRequestedNearbyDevicesPermission(true)
            }else{
                navigation.dispatch(StackActions.push(Screen.NearbyDevicesPermission))
            }


        }
    }

    useFocusEffect(
        useCallback ( ()=> {
            BackgroundGeolocation.getProviderState((data) => {
                let permissionStatus = data?.status
                let isPreciseLocation: Boolean = !data?.accuracyAuthorization
                if (permissionStatus === LocationPermissionStatus.Never)
                    setHaveRequestedPermission(true)
                else if (permissionStatus === LocationPermissionStatus.AskNextTime)
                    setHaveRequestedPermission(false)
                else if (!isPreciseLocation)
                    setHaveRequestedPermission(true)
                else
                    setHaveRequestedPermission(false)
            })

            const handleBackButton = () => {
                return true;
            }

            BackHandler.addEventListener('hardwareBackPress', handleBackButton);
            return () => {

                BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
            }
        }, [])
    )


    useFocusEffect(
        useCallback ( ()=> {
           props.getPreparations()
        }, [props.nextAppState])
    )

    // useFocusEffect(
    //     useCallback ( ()=> {
    //         props.getCurrentLocation()
    //     }, [props.geoLocation])
    // )

    useFocusEffect(
        useCallback ( ()=> {
            if((Platform.OS === 'android')) {
                if(parseInt(getSystemVersion()) >= 12) {
                    setEnableNextButton((props.gps && props.geoLocation && props.bluetooth && props.nearbyDevices && props.network))
                }
                else {
                    setEnableNextButton((props.gps && props.geoLocation && props.bluetooth && props.network))
                }
            } else {
                setEnableNextButton((props.gps && props.bluetooth && props.network))
            }

        }, [props.gps,
            props.geoLocation,
            props.bluetooth,
            props.network,
            props.nearbyDevices,
            props.timestamp])
    )

    return (
        <Preparations   onCancel={onCancel}
                        onNext={onNext}
                        onLocationPermission={onLocationPermission}
                        onNearByDevicesPermission={onNearByDevicesPermission}
                        onBluetoothConnection={onBluetoothConnection}
                        onNetworkConnection={onNetworkConnection}
                        onNotificationPermission={onNotificationPermission}
                        locationPermission={props.gps && props.geoLocation}
                        bluetoothConnection={props.bluetooth}
                        networkConnection={props.network}
                        notificationPermission={props.notification}
                        nearbyDevicesPermission={props.nearbyDevices}
                        enableNextButton={enableNextButton}
                        isSettings={props.settings && props.gps && props.bluetooth}
                        {...props} />
    )
}

const mapStateToProps = (state: RootState) => ({
    onboardingSuccess: AppLaunchSelectors.onboardingSuccess(state),
    gps: PreparationSelectors.gps(state),
    geoLocation: PreparationSelectors.geoLocation(state),
    network: PreparationSelectors.network(state),
    bluetooth: PreparationSelectors.bluetooth(state),
    notification: PreparationSelectors.notification(state),
    nearbyDevices: PreparationSelectors.nearbyDevices(state),
    timestamp: PreparationSelectors.timestamp(state),
    auth: AuthSelectors.auth(state),
    nextAppState: AppStateSelectors.nextAppState(state),
})

const mapDispatchToProps = (dispatch: RootDispatch) => ({
    requestMessagingPermission: () => dispatch(MessagingActions.requestMessagingPermission()),
    onboardingLocationSuccess: () => dispatch(AppLaunchActions.onboardingLocationSuccess()),
    getPreparations: () => dispatch(PreparationActions.getPreparations()),
    getCurrentLocation: () => dispatch(LocationActions.getCurrentLocation()),
})

const connector = connect(mapStateToProps, mapDispatchToProps)
interface Props extends ConnectedProps<typeof connector> {
    route: any
    navigation: any,
    settings: boolean,
}

export default connector(PreparationsContainer)
