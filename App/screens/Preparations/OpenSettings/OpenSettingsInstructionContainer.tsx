import React, { useCallback, useEffect, useState } from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { RootState, RootDispatch } from '../../../redux'
import {useNavigation, useFocusEffect } from '@react-navigation/native'
import OpenSettingsInstruction from './OpenSettingsInstruction'
import { AuthSelectors } from '../../../redux/auth-redux'
import BackgroundGeolocation, { Geofence } from "react-native-background-geolocation";
import { AppStateSelectors } from '../../../redux/app-state-redux'
import { LocationPermissionStatus } from '../../../models'
import LocationActions from '../../../redux/location-redux'
import { PreparationSelectors } from '../../../redux/preparation-redux'

const OpenSettingsInstructionContainer = (props: Props) => {
    const navigation = useNavigation()

    const onBack = () => {
        navigation.goBack();
    }

    useFocusEffect(
        useCallback ( ()=> {
            if (props.nextAppState === "active") {
                BackgroundGeolocation.getProviderState((data) => {
                    let permissionStatus = data?.status
                    let isPreciseLocation: Boolean = !data?.accuracyAuthorization
                    let isGeolocation: Boolean =  data?.enabled
                    let isGranted: Boolean = (permissionStatus === LocationPermissionStatus.Always || 
                                                permissionStatus === LocationPermissionStatus.WhileUsingTheApp)
                           
                    if (isGranted && isPreciseLocation) {
                        if (isGeolocation)
                            onBack()
                        else
                            props.getCurrentLocation()
                    }
                })
            }
            
        }, [props.nextAppState])
    )

    useFocusEffect(
        useCallback ( ()=> {
            if (props.geoLocation && props.gps) {
                onBack()
            }
        }, [props.geoLocation, props.gps])
    )

    return (
        <OpenSettingsInstruction 
            onBack={onBack}
        />
    )
}

const mapStateToProps = (state: RootState) => ({
    auth: AuthSelectors.auth(state),
    nextAppState: AppStateSelectors.nextAppState(state),
    geoLocation: PreparationSelectors.geoLocation(state),
    gps: PreparationSelectors.gps(state),
})

const mapDispatchToProps = (dispatch: RootDispatch) => ({
    getCurrentLocation: () => dispatch(LocationActions.getCurrentLocation()),
})

const connector = connect(mapStateToProps, mapDispatchToProps)
interface Props extends ConnectedProps<typeof connector> {
    route: any
}

export default connector(OpenSettingsInstructionContainer)
