import React from 'react'
import {
    View,
    Image,
    Platform,
} from 'react-native'
import { Container, Header, Text, Button, LottieView } from '../../../components'
import { Colors } from '../../../styles'
import {styles, commonsAssets} from './Styles'
import AppIntroSlider from 'react-native-app-intro-slider';
import {PreparationsDataSource} from '../PreparationsDataSource'
import { openSettings } from 'react-native-permissions'
import {StackActions ,useNavigation, DrawerActions, useFocusEffect } from '@react-navigation/native'

type Props = {
    onBack: () => void,
}
  
const OpenSettingsInstuction = (props: Props) =>{
    const navigation = useNavigation()
    const header = () => (
      <Header   style={styles.safeArea}
                center={(
                    <Image resizeMode={'contain'}
                          style={styles.onboardingLogo}
                          source={commonsAssets.contentImageSource} />
                )}
                leftContainerStyle={{ flex: .25 }}
                centerContainerStyle={{ flex: .5, alignItems: 'center' }}
                rightContainerStyle={{ flex: .25 }}
                left={(
                    <Button style={{ backgroundColor: Colors.transparent, }}
                            title={`Back`}
                            titleStyle={{ fontSize: 18, }}
                            onPress={props.onBack}
                             />
                 )}
      />
    )

    const _renderItem = ({item}) => {
        return (
            <View
                style={[
                styles.slide,
                {
                    backgroundColor: item.bg,
                },
            ]}>
                <Text style={styles.header}>{item.header}</Text>
                <View style={styles.card}>
                    <Text style={styles.title}>{item.title}</Text>
                    <Text style={styles.text}>{item.text}</Text>
                    <Image source={item.image} style={styles.image} resizeMode={'contain'}/>
                </View>
                <Text style={styles.footer}>{item.footer}</Text>
            </View>
        );
    }

    const _onDone = () => {
        openSettings()
    }

    const _doneLabel = 'Open Settings'

    return (
        <Container      safeAreaViewTopStyle={styles.safeArea}
                        statusBarStyle={'light'}
                        // scrollViewEnable={true}
                        header={header}
                        style={styles.container} >

          <AppIntroSlider renderItem={_renderItem} data={PreparationsDataSource.getOpenSettingsInstruction()} onDone={_onDone} doneLabel={_doneLabel}/>
        </Container>
    )
}

export default OpenSettingsInstuction
