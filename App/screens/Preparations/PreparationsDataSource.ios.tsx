import {
    Platform,
} from 'react-native'
import { images } from '../../constants'
import { Colors } from '../../styles'
import {strings} from "../../localization/strings";

const locationInstructions = {
    'I15': [
        {
            key: 'one',
            header: strings.permission_follow_steps_provided,
            title: strings.permission_step_1,
            text: strings.permission_step_select_precise_on,
            image: images.locationPermissionI15Step1,
            footer: '',
            backgroundColor: Colors.red,
          },
          {
            key: 'two',
            header: '',
            title: strings.permission_step_2,
            text: strings.permission_ios_step_select_while_using_the_app,
            image: images.locationPermissionI15Step2,
            footer: '',
            backgroundColor: Colors.red,
          },
    ],
    'I13': [
        {
            key: 'one',
            header: strings.permission_follow_steps_provided,
            title: strings.permission_step_1,
            text: strings.permission_ios_step_select_while_using_the_app,
            image: images.locationPermissionI13,
            footer: '',
            backgroundColor: Colors.red,
          },
    ],
}

const openSettingsInstructions = [
    {
        key: 'one',
        header: strings.permission_allow_precise_loction_in_app_settings,
        title: strings.permission_step_1,
        text: strings.permission_click_location_in_app_Settings,
        image: images.openSettingsIStep1,
        footer: '',
        backgroundColor: Colors.red,
      },
      {
        key: 'two',
        header: '',
        title: strings.permission_step_2,
        text: strings.permission_select_always_in_allow_location_access_and_enable_precise_location,
        image: images.openSettingsIStep2,
        footer: '',
        backgroundColor: Colors.red,
      },
]

export type Props = {
    onCancel: () => void,
    onNext: () => void,
    
    onLocationPermission: () => void,
    onBluetoothConnection: () => void,
    onNetworkConnection: () => void,
    onNotificationPermission: () => void,

    locationPermission: boolean,
    networkConnection: boolean,
    bluetoothConnection: boolean,
    notificationPermission: boolean,
    enableNextButton: boolean,
    isSettings: boolean,
}
interface PermissionsData {
    key: number,
    image: any;
    title: string;
    description: string;
    onPress: () => void;
    isGranted: boolean;
}

const PermissionList = (props: Props) => {
    const allPermissions: PermissionsData[] = [];

    const location : PermissionsData = {
        key: 1,
        image: images.locationPinIcon,
        title: strings.permission_gps_title,
        description: props?.locationPermission ? strings.permission_correct_gps_description : strings.permission_incorrect_gps_description,
        onPress: props?.onLocationPermission,
        isGranted: props?.locationPermission,
    };

    allPermissions.push(location);
    
    const bluetooth : PermissionsData = {
        key: 2,
        image: images.bluetoothIcon,
        title: strings.permission_bluetooth_title,
        description: props?.bluetoothConnection ? strings.permission_correct_bluetooth_description : strings.permission_incorrect_bluetooth_description,
        onPress: props?.onBluetoothConnection,
        isGranted: props?.bluetoothConnection,
    };

    allPermissions.push(bluetooth);

    const network : PermissionsData = {
        key: 3,
        image: images.dataAccessIcon,
        title: strings.permission_data_access_title,
        description: props?.networkConnection ? strings.permission_correct_data_access_description : strings.permission_incorrect_data_access_description,
        onPress: props?.onNetworkConnection,
        isGranted: props?.networkConnection,
    };

    allPermissions.push(network);

    if (props.isSettings) {
        const notification : PermissionsData = {
            key: 3,
            image: images.notificationIcon,
            title: strings.permission_notifications_title,
            description: props?.notificationPermission ? strings.permission_correct_notification_description : strings.permission_incorrect_notification_description,
            onPress: props?.onNotificationPermission,
            isGranted: props?.notificationPermission,
        };
    
        allPermissions.push(notification);
    }

    return allPermissions
}

/**
 * Data Source Class, is a class that predefined data to allow UI/Views to display
 * dynamically. This can be used to display platform dependent UI/Views.
 */
const PreparationsDataSource = {
    getLocationPermisionInstructions: function(){
        const majorVersionIOS = parseInt(Platform.Version.toString(), 10);
        
        var data = locationInstructions['I13']
        
        if (majorVersionIOS >= 14 ) {
            data = locationInstructions['I15']
        }

        return data
    },

    getOpenSettingsInstruction: function() {
        return openSettingsInstructions
    },

    getPermissionList: function(props: any) {
        return PermissionList(props)
    },
    
}

export {PreparationsDataSource}