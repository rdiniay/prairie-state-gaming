import React, { useCallback, useEffect, useState } from 'react'
import { Platform, Linking, Alert} from 'react-native'
import { connect, ConnectedProps } from 'react-redux'
import { RootState, RootDispatch } from '../../../redux'
import { useNavigation, useFocusEffect } from '@react-navigation/native'
import BluetoothConnection from './BluetoothConnection'
import {PERMISSIONS, openSettings, request } from 'react-native-permissions'
import { env } from '../../../config'
import crashlytics from '@react-native-firebase/crashlytics';
import AppLaunchActions from '../../../redux/app-launch-redux'
import BackgroundGeolocation from "react-native-background-geolocation";
import { AuthSelectors } from '../../../redux/auth-redux'
import BluetoothStateManager from 'react-native-bluetooth-state-manager';
import AndroidOpenSettings from 'react-native-android-open-settings';
import PreparationActions, { PreparationSelectors } from '../../../redux/preparation-redux'
import { AppStateSelectors } from '../../../redux/app-state-redux'

const BluetoothConnectionContainer = (props: Props) => {
    const navigation = useNavigation()
    const [bluetoothPermission, setBluetoothPermission] = useState(false)

    const onBack = () => {
        navigation.goBack();
    }

    const requestBluetoothPermission = async () => {
        if (env.IS_DEBUG){
            console.log("Check Bluetooth Permission called()");
        }

        if (Platform.OS === 'ios'){
            request(
                Platform.select({
                    ios: PERMISSIONS.IOS.BLUETOOTH_PERIPHERAL,
                })
                ).then( (res) => {
                    console.log("iOS Bluetooth Permission: ", res)
                    const isBluetoothEnabled = res === 'granted'
                    setBluetoothPermission(isBluetoothEnabled)
                    
                    if (res === "blocked") {
                        setBluetoothPermission(null)
                    }
                })
                .catch( (exception) => {
                    crashlytics().recordError(exception)
                    console.log("Bluetooth Permission Exception ", exception)
                    setBluetoothPermission(false)
                })
        }
        else
        {
            BluetoothStateManager.requestToEnable().then( (isEnabled) => {
                console.log("Android Bluetooth Permission: ", isEnabled.valueOf())
                setBluetoothPermission(isEnabled.valueOf())
            })
            .catch( (exception) => {
                crashlytics().recordError(exception)
                console.log("Bluetooth Permission Exception ", exception)
                setBluetoothPermission(false)
            })
        }
    }

    const goToBToothSettings = () => {
        Platform.OS === 'ios'
        ? Linking.openURL('App-Prefs:Bluetooth')
        : AndroidOpenSettings.bluetoothSettings();
    }

    useFocusEffect(
        useCallback ( ()=> {
           props.getPreparations()
        }, [props.nextAppState])
    )

    useFocusEffect(
        useCallback(() => {
            setBluetoothPermission(props.bluetooth)
        }, [props.bluetooth])
    )

    useFocusEffect(
        useCallback(() => {
            BluetoothStateManager.getState().then( (bluetoothState) => {
                switch (bluetoothState) {
                    case 'Unauthorized': {
                        openSettings()
                        setTimeout(() => onBack(), 500);
                        break;
                    }
                }
            })
        }, [])
    )

    return (
        <BluetoothConnection     
            onBack={onBack}
            requestBluetoothPermission={requestBluetoothPermission}
            bluetoothPermission={bluetoothPermission}
            turnOnBluetooth={goToBToothSettings}
            {...props} />
    )
}

const mapStateToProps = (state: RootState) => ({
    bluetooth: PreparationSelectors.bluetooth(state),
    auth: AuthSelectors.auth(state),
    nextAppState: AppStateSelectors.nextAppState(state),
})

const mapDispatchToProps = (dispatch: RootDispatch) => ({
    onboardingLocationSuccess: () => dispatch(AppLaunchActions.onboardingLocationSuccess()),
    getPreparations: () => dispatch(PreparationActions.getPreparations()),
})

const connector = connect(mapStateToProps, mapDispatchToProps)
interface Props extends ConnectedProps<typeof connector> {
    route: any
}

export default connector(BluetoothConnectionContainer)
