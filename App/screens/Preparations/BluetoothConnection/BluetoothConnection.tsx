import React, { useState, useCallback } from 'react'
import { ConnectedProps } from 'react-redux'
import {
    View,
    TextInput,
    TouchableOpacity,
    Image,
    ScrollView,
    FlatList,
    SectionList,
    Dimensions, Platform
} from 'react-native'
import { Container, Header, Text, Button, LottieView } from '../../../components'
import { Colors } from '../../../styles'
import {styles, commonsAssets} from './Styles'
import { images, lotties } from '../../../constants'
import {strings} from "../../../localization/strings";
import { useFocusEffect } from '@react-navigation/native'
import DeviceInfo from 'react-native-device-info'
const { width, height } = Dimensions.get('window');

type Props = {
    onBack: () => void,
    requestBluetoothPermission: () => void,
    bluetoothPermission: boolean,
    turnOnBluetooth: () => void,
}

const BluetoothConnection = (props: Props) =>{
    const [switchValue, setSwitch] = useState(false)
    const header = () => (
      <Header   style={styles.safeArea}
                center={<View />}
                leftContainerStyle={{ flex: .25 }}
                centerContainerStyle={{ flex: .4, alignItems: 'center' }}
                rightContainerStyle={{ flex: .4, }}
                left={(
                    <Button style={{ backgroundColor: Colors.transparent, }}
                            title={`Back`}
                            titleStyle={{ fontSize: 18, }}
                            onPress={props.onBack}
                             />
                 )}
                 right={!DeviceInfo.hasNotch() ? <View /> : (
                    <Text style={{ marginVertical: -5, bottom: 20, fontSize: 20, color: Colors.white, textAlign: 'center'  }} >
                        {`SWIPE DOWN!\n`}
                        <Text style={{ fontSize: 14, color: Colors.white,}}>
                            {`Outside from the screen`}
                        </Text>
                    </Text>
                 )}
      />
    )

    useFocusEffect(
        useCallback(() => {
            setSwitch(props.bluetoothPermission)
        }, [props.bluetoothPermission])
    )

    return (
        <Container      safeAreaViewTopStyle={styles.safeArea}
                        statusBarStyle={'light'}
                        // scrollViewEnable={true}
                        header={header}
                        style={styles.container} >

            <Text style={styles.onboardingTitle} >
                {`BLUETOOTH CONNECTION`}
            </Text>
            <View style={{ flex: 1, paddingTop: 20, paddingHorizontal: 15, }}>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View style={{ flex: .75, padding: 5, paddingHorizontal: 10, }}>
                        <Text style={{ color: Colors.white, paddingBottom: 3, fontWeight: 'bold' }}>
                            {`Bluetooth`}
                        </Text>
                        <Text numberOfLines={3} style={{ color: Colors.white, }}>
                            {props.bluetoothPermission ? strings.permission_correct_bluetooth_description :
                                 `Incorrect Bluetooth setting. To Enable Bluetooth on your device, Please see the instruction below.`}
                        </Text>
                    </View>
                    <View style={{ flex: .25, paddingTop: 5 }}>
                        <TouchableOpacity activeOpacity={1} 
                                            onPress={() => props.turnOnBluetooth()}
                                            style={{ flexDirection: props.bluetoothPermission ? 'row-reverse' : 'row', alignItems: 'center', padding: 7, width: 70, borderRadius: 25, backgroundColor: switchValue ? Colors.green : Colors.gray}}>
                            <View style={{ height: 25, width: 25, borderRadius: 12.5, backgroundColor: Colors.white }} />
                            <Text value={props.bluetoothPermission ? 'On' : 'Off'} style={{ flex: 1, textAlign: 'center', fontSize: 14, fontWeight: switchValue ? 'bold' : '400', color: Colors.white }} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ flex: 4,}}>
                    {Platform.OS === 'ios' && !props.bluetoothPermission &&
                        <Image source={images.bluetoothConnectioniOS} 
                                resizeMode={'contain'}
                                style={{ position: 'absolute', alignSelf: 'center', width: width, height: height*.45}} />
                    }
                </View>
            </View>
            {!props.bluetoothPermission && !DeviceInfo.hasNotch() &&
                <>
                    <Text style={{ marginVertical: 2, fontSize: 16, color: Colors.white, textAlign: 'center'  }} >
                        {`Outside from the screen!`}
                    </Text>
                    <Text style={{ marginVertical: -5, fontSize: 30, color: Colors.white, textAlign: 'center'  }} >
                        {`SWIPE UP HERE!`}
                    </Text>
                </>
            }
        </Container>
    )
}

export default BluetoothConnection
