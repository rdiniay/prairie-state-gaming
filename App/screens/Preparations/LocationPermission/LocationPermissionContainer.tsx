import React, { useCallback, useEffect, useState } from 'react'
import {Alert, Platform} from 'react-native'
import { connect, ConnectedProps } from 'react-redux'
import { RootState, RootDispatch } from '../../../redux'
import {StackActions ,useNavigation, DrawerActions, useFocusEffect } from '@react-navigation/native'
import { Screen } from '../../../navigation/routes/mainRoutes'
import LocationPermission from './LocationPermission'
import {PERMISSIONS, request, openSettings, requestMultiple, RESULTS} from 'react-native-permissions'
import { env } from '../../../config'
import crashlytics from '@react-native-firebase/crashlytics';
import { getBrand, getSystemVersion  } from 'react-native-device-info'
import AppLaunchActions from '../../../redux/app-launch-redux'
import {strings} from "../../../localization/strings";
import BackgroundGeolocation, { Geofence } from "react-native-background-geolocation";
import { LocationPermissionStatus } from '../../../models'
import { AuthSelectors } from '../../../redux/auth-redux'
import LocationActions from '../../../redux/location-redux'
import { PreparationSelectors } from '../../../redux/preparation-redux'

const LocationPermissionContainer = (props: Props) => {
    const navigation = useNavigation()
    const [permissionEnabled, setPermissionEnabled] = useState<Boolean>(false)

    const onBack = () => {
        navigation.goBack();
    }
        
    const checkLocationPermission = () => {
        setTimeout(() => {
            BackgroundGeolocation.getProviderState((data) => {
                let permissionStatus = data?.status
                let isPreciseLocation: Boolean = !data?.accuracyAuthorization
                let isGranted: Boolean = (permissionStatus === LocationPermissionStatus.Always || 
                                            permissionStatus === LocationPermissionStatus.WhileUsingTheApp)
                       
                //setPermissionEnabled(data?.enabled && props.geoLocation)
                onBack()
            })
        }, 300);
    }

    const requestLocationPermission = async () => {
        if (env.IS_DEBUG){
            console.log("LocationPermissionContainer: Check Location Permission called()");
        }

        if (Platform.OS === 'ios'){
            const permission = parseInt(Platform.Version, 10) < 13
                            ? PERMISSIONS.IOS.LOCATION_ALWAYS
                            : PERMISSIONS.IOS.LOCATION_WHEN_IN_USE;
   
            request(permission).then( (result) => {
                checkLocationPermission()
            })
        }else{
            request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
            .then(result => {
                props.getCurrentLocation()
            })
        }
    }

    useFocusEffect(
        useCallback ( ()=> {
            if (props.geoLocation && props.gps) {
                onBack()
            }
        }, [props.geoLocation, props.gps])
    )

    useFocusEffect(
        useCallback(() => {
            BackgroundGeolocation.getProviderState((data) => {
                let permissionEnabled: Boolean = data?.enabled
                setPermissionEnabled(permissionEnabled)
            })
        }, [])
    )

    return (
        <LocationPermission     onBack={onBack}
                                requestLocationPermission={requestLocationPermission}
                                {...props} />
    )
}

const mapStateToProps = (state: RootState) => ({
    auth: AuthSelectors.auth(state),
    gps: PreparationSelectors.gps(state),
    geoLocation: PreparationSelectors.geoLocation(state),
    timestamp: PreparationSelectors.timestamp(state),
})

const mapDispatchToProps = (dispatch: RootDispatch) => ({
    onboardingLocationSuccess: () => dispatch(AppLaunchActions.onboardingLocationSuccess()),
    getCurrentLocation: () => dispatch(LocationActions.getCurrentLocation()),
})

const connector = connect(mapStateToProps, mapDispatchToProps)
interface Props extends ConnectedProps<typeof connector> {
    route: any
}

export default connector(LocationPermissionContainer)
