import { PixelRatio, Dimensions } from 'react-native'
import ScaledSheet from 'react-native-scaled-sheet'
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import { Colors, Commons } from '../../styles'
const { width, height } = Dimensions.get('window');

const styles = ScaledSheet.create({
    safeArea: {
        backgroundColor: Colors.red,
    },
    container: {
        flex: 1,
        backgroundColor: Colors.red,
    },
    onboardingContainer: {
        flex: 1, 
        alignItems: 'center',
        paddingTop: 15,
    },
    onboardingLogo: {
        height: 40,
        width: 120,
    },
    onboardingHeaderSpace: {
        flex: .35
    },
    onboardingTitle: {
        marginTop: 25,
        fontSize: 25,
        color: Colors.white,
        textAlign: 'center',
        fontWeight: 'bold',
    },
    onboardingMessage: {
        marginBottom: 25,
        paddingHorizontal: 30,
        color: Colors.white,
        textAlign: 'justify',
    },
    onboardingLocation: {
        width: height*.36,
    },
    onboardingPermission: {
        height: '55%', 
        width: '100%'
    },
    onboardingButtonContainer: {
        flex: 1,
        width: width,
    },
    onboardingStackButton: {
        flex: 1, 
        flexDirection: 'row', 
        justifyContent: 'space-around', 
        alignItems: 'flex-end', 
        paddingBottom: 15,
    },
    onboardingTitleButton: {
        fontWeight: 'bold',
    },
    onboardingButton: {
        height: 50, 
        backgroundColor: Colors.transparent,
    },
    onboardingCircleIndicator: { 
        width: 15,
        height: 15, 
        backgroundColor: Colors.white, 
        borderRadius: 7.5, 
        marginBottom: 18,
    }
})

const commonsAssets = {
    contentImageSource: Commons.ContentImageSource(),
}

export {
    styles,
    commonsAssets,
}