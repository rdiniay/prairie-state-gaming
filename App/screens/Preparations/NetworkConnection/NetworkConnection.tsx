import React, { useState, useCallback } from 'react'
import { ConnectedProps } from 'react-redux'
import {
    View,
    TextInput,
    TouchableOpacity,
    Image,
    ScrollView,
    FlatList,
    SectionList,
    Dimensions, Platform
} from 'react-native'
import { Container, Header, Text, Button, LottieView } from '../../../components'
import { Colors } from '../../../styles'
import {styles, commonsAssets} from './Styles'
import { images, lotties } from '../../../constants'
import {strings} from "../../../localization/strings";
import { useFocusEffect } from '@react-navigation/native'
import DeviceInfo from 'react-native-device-info'
const { width, height } = Dimensions.get('window');

type Props = {
    onBack: () => void,
    networkConnection: boolean,
}

const NetworkConnection = (props: Props) =>{
    const [switchValue, setSwitch] = useState(false)
    const header = () => (
      <Header   style={styles.safeArea}
                center={<View />}
                leftContainerStyle={{ flex: .25 }}
                centerContainerStyle={{ flex: .4, alignItems: 'center' }}
                rightContainerStyle={{ flex: .4, }}
                left={(
                    <Button style={{ backgroundColor: Colors.transparent, }}
                            title={`Back`}
                            titleStyle={{ fontSize: 18, }}
                            onPress={props.onBack}
                             />
                 )}
                 right={ Platform.OS === 'ios' ? 
                    ( !DeviceInfo.hasNotch() ? <View /> : 
                        (<Text style={{ marginVertical: -5, bottom: 20, fontSize: 20, color: Colors.white, textAlign: 'center'  }} >
                            {`SWIPE DOWN!\n`}
                            <Text style={{ fontSize: 14, color: Colors.white,}}>
                                {`Outside from the screen`}
                            </Text>
                        </Text>
                 )) : 
                    (<Text style={{ marginVertical: -5, bottom: 8, fontSize: 16, color: Colors.white, textAlign: 'center'  }} >
                        {`SWIPE DOWN!\n`}
                        <Text style={{ fontSize: 14, color: Colors.white,}}>
                            {`Outside from the screen`}
                        </Text>
                    </Text>
                )}
      />
    )

    useFocusEffect(
        useCallback(() => {
            setSwitch(props.networkConnection)
        }, [props.networkConnection])
    )

    return (
        <Container      safeAreaViewTopStyle={styles.safeArea}
                        statusBarStyle={'light'}
                        // scrollViewEnable={true}
                        header={header}
                        style={styles.container} >

            <Text style={styles.onboardingTitle} >
                {strings.permission_data_access_title.toUpperCase()}
            </Text>
            <View style={{ flex: 1, paddingTop: 20, paddingHorizontal: 15, }}>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View style={{ flex: 1, padding: 5, paddingHorizontal: 10, }}>
                        <Text style={{ color: Colors.white, paddingBottom: 3, fontWeight: 'bold' }}>
                            {strings.permission_data_access_title}
                        </Text>
                        <Text numberOfLines={3} style={{ color: Colors.white, }}>
                            {props.networkConnection ? strings.permission_correct_data_access_description : strings.permission_incorrect_data_access_description}
                        </Text>
                    </View>
                </View>
                <View style={{ flex: 4,}}>
                    {!props.networkConnection &&
                        <Image source={ Platform.OS === 'ios' ? images.internetConnectioniOS : images.internetConnectionAndroid}
                                resizeMode={'contain'}
                                style={{ position: 'absolute', alignSelf: 'center', width: width, height: height*.45}} />
                    }
                </View>
            </View>
            {!props.networkConnection && !DeviceInfo.hasNotch() && Platform.OS === 'ios' &&
                <>
                    <Text style={{ marginVertical: 2, fontSize: 16, color: Colors.white, textAlign: 'center'  }} >
                        {`Outside from the screen!`}
                    </Text>
                    <Text style={{ marginVertical: -5, fontSize: 30, color: Colors.white, textAlign: 'center'  }} >
                        {`SWIPE UP HERE!`}
                    </Text>
                </>
            }
        </Container>
    )
}

export default NetworkConnection
