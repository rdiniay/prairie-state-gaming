import React, { useCallback, useEffect, useState } from 'react'
import { Platform, Linking, Alert} from 'react-native'
import { connect, ConnectedProps } from 'react-redux'
import { RootState, RootDispatch } from '../../../redux'
import { useNavigation, useFocusEffect } from '@react-navigation/native'
import NetworkConnection from './NetworkConnection'
import { env } from '../../../config'
import AppLaunchActions from '../../../redux/app-launch-redux'
import BackgroundGeolocation from "react-native-background-geolocation";
import { AuthSelectors } from '../../../redux/auth-redux'
import AndroidOpenSettings from 'react-native-android-open-settings';
import PreparationActions, { PreparationSelectors } from '../../../redux/preparation-redux'
import { AppStateSelectors } from '../../../redux/app-state-redux'

const NetworkConnectionContainer = (props: Props) => {
    const navigation = useNavigation()
    const [networkConnection, setNetworkConnection] = useState(false)

    const onBack = () => {
        navigation.goBack();
    }

    useFocusEffect(
        useCallback ( ()=> {
           props.getPreparations()
        }, [props.nextAppState])
    )

    useFocusEffect(
        useCallback(() => {
            setNetworkConnection(props.network)
        }, [props.network])
    )

    return (
        <NetworkConnection     
            onBack={onBack}
            networkConnection={networkConnection}
            {...props} />
    )
}

const mapStateToProps = (state: RootState) => ({
    network: PreparationSelectors.network(state),
    auth: AuthSelectors.auth(state),
    nextAppState: AppStateSelectors.nextAppState(state),
})

const mapDispatchToProps = (dispatch: RootDispatch) => ({
    onboardingLocationSuccess: () => dispatch(AppLaunchActions.onboardingLocationSuccess()),
    getPreparations: () => dispatch(PreparationActions.getPreparations()),
})

const connector = connect(mapStateToProps, mapDispatchToProps)
interface Props extends ConnectedProps<typeof connector> {
    route: any
}

export default connector(NetworkConnectionContainer)
