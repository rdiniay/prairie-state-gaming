import React, { useCallback, useEffect, useState } from 'react'
import {Alert, Platform} from 'react-native'
import { connect, ConnectedProps } from 'react-redux'
import { RootState, RootDispatch } from '../../../redux'
import {StackActions ,useNavigation, DrawerActions, useFocusEffect } from '@react-navigation/native'
import { Screen } from '../../../navigation/routes/mainRoutes'
import NearbyDevicesPermission from './NearbyDevicesPermission'
import {PERMISSIONS, request, openSettings, requestMultiple, RESULTS} from 'react-native-permissions'
import { env } from '../../../config'
import crashlytics from '@react-native-firebase/crashlytics';
import { getBrand, getSystemVersion  } from 'react-native-device-info'
import AppLaunchActions from '../../../redux/app-launch-redux'
import {strings} from "../../../localization/strings";
import BackgroundGeolocation, { Geofence } from "react-native-background-geolocation";
import { LocationPermissionStatus } from '../../../models'
import { AuthSelectors } from '../../../redux/auth-redux'

const NearbyDevicesPermissionContainer = (props: Props) => {
    const navigation = useNavigation()

    const onBack = () => {
        navigation.goBack();
    }

    const requestNearbyPermission = async () => {
        if (env.IS_DEBUG){
            console.log("NearbyDevicesPermissionContainer: Check Nearby Devices Permission called()");
        }

        // Currently, requesting permission nearby devices will trigger an exception hence we open settings for the user.
        openSettings()
            .catch((e) => {
                crashlytics().recordError(e)
                console.warn('cannot open settings')
            })
            .finally(() => {
                onBack()
            });
    }

    useFocusEffect(
        useCallback(() => {
            // BackgroundGeolocation.getProviderState((data) => {
            //     let permissionEnabled: Boolean = data?.enabled
            //     setPermissionEnabled(permissionEnabled)
            // })
        }, [])
    )

    return (
        <NearbyDevicesPermission     onBack={onBack}
                                requestNearbyPermission={requestNearbyPermission}
                                {...props} />
    )
}

const mapStateToProps = (state: RootState) => ({
    auth: AuthSelectors.auth(state),
})

const mapDispatchToProps = (dispatch: RootDispatch) => ({
    // NA
})

const connector = connect(mapStateToProps, mapDispatchToProps)
interface Props extends ConnectedProps<typeof connector> {
    route: any
}

export default connector(NearbyDevicesPermissionContainer)
