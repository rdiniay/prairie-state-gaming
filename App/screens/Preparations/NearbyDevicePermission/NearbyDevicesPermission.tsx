import React from 'react'
import {
    View,
    Image,
    Platform,
} from 'react-native'
import { Container, Header, Text, Button, LottieView } from '../../../components'
import { Colors } from '../../../styles'
import {styles, commonsAssets} from './Styles'
import { images, lotties } from '../../../constants'
import {strings} from "../../../localization/strings";
import AppIntroSlider from 'react-native-app-intro-slider';
import {PreparationsDataSource} from '../PreparationsDataSource'

type Props = {
    onBack: () => void,
    requestNearbyPermission: () => void,
}

const Onboarding = (props: Props) =>{

    const header = () => (
      <Header   style={styles.safeArea}
                center={(
                    <Image resizeMode={'contain'}
                          style={styles.onboardingLogo}
                          source={commonsAssets.contentImageSource} />
                )}
                leftContainerStyle={{ flex: .25 }}
                centerContainerStyle={{ flex: .5, alignItems: 'center' }}
                rightContainerStyle={{ flex: .25 }}
                left={(
                    <Button style={{ backgroundColor: Colors.transparent, }}
                            title={`Back`}
                            titleStyle={{ fontSize: 18, }}
                            onPress={props.onBack}
                             />
                 )}
      />
    )

    const _renderItem = ({item}) => {
        return (
            <View
                style={[
                styles.slide,
                {
                    backgroundColor: item.bg,
                },
            ]}>
                <Text style={styles.header}>{item.header}</Text>
                <View style={styles.card}>
                    <Text style={styles.title}>{item.title}</Text>
                    <Text style={styles.text}>{item.text}</Text>
                    <Image source={item.image} style={styles.image}/>
                </View>
                <Text style={styles.footer}>{item.footer}</Text>
            </View>
        );
    }

    const _onDone = () => {
        props?.requestNearbyPermission()
    }

    const _doneLabel = 'App Settings'

    return (
        <Container      safeAreaViewTopStyle={styles.safeArea}
                        statusBarStyle={'light'}
                        // scrollViewEnable={true}
                        header={header}
                        style={styles.container} >

          <AppIntroSlider renderItem={_renderItem} data={PreparationsDataSource.getNearbyDevicesPermissionInstructions()} onDone={_onDone} doneLabel={_doneLabel}/>

          {/* <Button style={{ padding: 15, margin: 20, marginBottom: 20, }}
                        title={'Turn On'}
                        onPress={props?.requestLocationPermission} /> */}
        </Container>
    )
}

export default Onboarding
