import { PixelRatio, Dimensions } from 'react-native'
import ScaledSheet from 'react-native-scaled-sheet'
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import { Colors, Commons } from '../../../styles'
const { width, height } = Dimensions.get('window');

const styles = ScaledSheet.create({
    safeArea: {
        backgroundColor: Colors.red,
    },
    container: {
        flex: 1,
        backgroundColor: Colors.red,
    },
    slide: {
        flex: 1,
        paddingTop: 20,
        alignItems: 'center',
        backgroundColor: 'blue',
    },
    card: {
        backgroundColor: '#E6DDCE',
        borderRadius: 10,
        overflow: "hidden",
        padding: 20,
        opacity: 0.7,

    },
    header: {
        fontSize: 22,
        color: 'white',
        textAlign: 'center',
        paddingBottom: 20,
    },
    title: {
        fontSize: 20,
        color: Colors.darkRed,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    text: {
        color: Colors.darkRed,
        textAlign: 'left',
        fontWeight: 'bold',
        fontSize: 18,
        paddingTop: 20,
        paddingBottom: 10,
    },
    image: {
        width: 320,
        height: 200,
        resizeMode: 'contain'
    },
    footer: {
        fontSize: 20,
        color: 'white',
        textAlign: 'center',
        paddingTop: 20,
    },
})

const commonsAssets = {
    contentImageSource: Commons.ContentImageSource(),
}

export {
    styles,
    commonsAssets,
}
