import React, { useEffect, useState } from 'react'
import {Alert, Platform} from 'react-native'
import { connect, ConnectedProps } from 'react-redux'
import { RootState, RootDispatch } from '../../redux'
import {StackActions ,useNavigation, DrawerActions } from '@react-navigation/native'
import { Screen } from '../../navigation/routes/mainRoutes'
import Onboarding from './Onboarding'
import {PERMISSIONS, request, openSettings, requestMultiple, RESULTS} from 'react-native-permissions'
import { env } from '../../config'
import crashlytics from '@react-native-firebase/crashlytics';
import { getBrand, getSystemVersion  } from 'react-native-device-info'
import AppLaunchActions from '../../redux/app-launch-redux'
import {strings} from "../../localization/strings";
import BackgroundGeolocation, { Geofence } from "react-native-background-geolocation";
import { LocationPermissionStatus } from '../../models'
import { AuthSelectors } from '../../redux/auth-redux'

const OnboardingContainer = (props: Props) => {
    const navigation = useNavigation()
    const [locationPermission, setLocationPermission] = useState(false)

    const onCancel = () => {
        navigation.goBack();
    }

    const navigateToSignIn = () => {
        props.onboardingLocationSuccess()
        navigation.dispatch(StackActions.push(Screen.Login));
    }

    const navigateToPreprations = () => {
        navigation.dispatch(StackActions.push(Screen.PreparationsNavigators));
    }

    useEffect(() => {
        if (locationPermission)
            navigateToSignIn()
    }, [locationPermission])

    const checkLocationPermission = async () => {
        if (env.IS_DEBUG){
            console.log("LocationPermissionContainer: Check Location Permission called()");
        }

        if (Platform.OS === 'ios'){
            const permission = parseInt(Platform.Version, 10) < 13
                            ? PERMISSIONS.IOS.LOCATION_ALWAYS
                            : PERMISSIONS.IOS.LOCATION_WHEN_IN_USE;

            request(permission).then( (result) => {
                setLocationPermission(true)
            })
        }else{
            request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
            .then(result => {
                setLocationPermission(true)
            })
        }
    }

    return (
        <Onboarding   onCancel={onCancel}
                        navigateToSignIn={navigateToSignIn}
                        checkLocationPermission={checkLocationPermission}
                        {...props} />
    )
}

const mapStateToProps = (state: RootState) => ({
    auth: AuthSelectors.auth(state),
})

const mapDispatchToProps = (dispatch: RootDispatch) => ({
    onboardingLocationSuccess: () => dispatch(AppLaunchActions.onboardingLocationSuccess())
})

const connector = connect(mapStateToProps, mapDispatchToProps)
interface Props extends ConnectedProps<typeof connector> {
    route: any
}

export default connector(OnboardingContainer)
