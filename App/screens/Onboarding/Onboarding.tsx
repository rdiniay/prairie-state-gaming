import React from 'react'
import { ConnectedProps } from 'react-redux'
import {
    View,
    TextInput,
    TouchableOpacity,
    Image,
    ScrollView,
    FlatList,
    SectionList,
    Dimensions, Platform
} from 'react-native'
import { Container, Header, Text, Button, LottieView } from '../../components'
import { Colors } from '../../styles'
import {styles, commonsAssets} from './Styles'
import { images, lotties } from '../../constants'
import {strings} from "../../localization/strings";

type Props = {
    onCancel: () => void,
    navigateToSignIn: () => void,
    checkLocationPermission: () => void,
}

const Onboarding = (props: Props) =>{

    const header = () => (
      <Header   style={styles.safeArea}
                center={(
                    <Image resizeMode={'contain'}
                          style={styles.onboardingLogo}
                          source={commonsAssets.contentImageSource} />
                )}
                leftContainerStyle={styles.onboardingHeaderSpace}
                centerContainerStyle={styles.onboardingHeaderSpace}
                rightContainerStyle={styles.onboardingHeaderSpace}
      />
    )
    const onboardingData = {
      index: 0,
      title: strings.onboarding_title,
      message: Platform.OS === 'android'? strings.onboarding_message_android : strings.onboarding_message,
      lottiePhoto: lotties.onBoardingLocation,
    }

    const OnboardingMap = (onboarding: any) => (
      <View style={styles.onboardingContainer}>
            <Text style={styles.onboardingTitle} >
                  {onboarding.title}
            </Text>
            <Text style={styles.onboardingMessage} >
                  {onboarding.message}
            </Text>
            {onboarding.lottiePhoto &&
              <LottieView style={styles.onboardingLocation}
                                  source={onboarding.lottiePhoto}
                                  animate={true}
                                  speed={.8}
                                                />
            }
            {onboarding.imagePhoto &&
              <Image resizeMode='contain'
                    source={onboarding.imagePhoto}
                    style={styles.onboardingPermission} />
            }
        </View>
    )

    return (
        <Container      safeAreaViewTopStyle={styles.safeArea}
                        statusBarStyle={'light'}
                        // scrollViewEnable={true}
                        header={header}
                        style={styles.container} >

          <ScrollView>
            <OnboardingMap
                            {...onboardingData} />
          </ScrollView>

          <View style={styles.onboardingButtonContainer}>
                <View style={styles.onboardingStackButton}>

                    <Button onPress={props.navigateToSignIn}
                            titleStyle={styles.onboardingTitleButton}
                            style={styles.onboardingButton} />

                    <View style={styles.onboardingCircleIndicator} />
                    
                    <Button title={strings.onboarding_button_turn_on}
                            onPress={props.checkLocationPermission}
                            titleStyle={styles.onboardingTitleButton}
                            style={styles.onboardingButton} />
                </View>
            </View>
        </Container>
    )
}

export default Onboarding
