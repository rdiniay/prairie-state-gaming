import React, {useCallback, useEffect, useState} from 'react'
import {Alert, Platform} from 'react-native'
import { connect, ConnectedProps } from 'react-redux'
import { RootState, RootDispatch } from '../../redux'
import {StackActions, useNavigation, DrawerActions, useFocusEffect} from '@react-navigation/native'
import { Screen } from '../../navigation/routes/mainRoutes'
import Onboarding from './Onboarding'
import {PERMISSIONS, request, openSettings, requestMultiple, RESULTS} from 'react-native-permissions'
import { env } from '../../config'
import crashlytics from '@react-native-firebase/crashlytics';
import { getBrand, getSystemVersion  } from 'react-native-device-info'
import AppLaunchActions from '../../redux/app-launch-redux'
import {strings} from "../../localization/strings";
import BackgroundGeolocation, { Geofence } from "react-native-background-geolocation";
import { LocationPermissionStatus } from '../../models'
import { AuthSelectors } from '../../redux/auth-redux'
import {AppStateSelectors} from "../../redux/app-state-redux";
import { PreparationSelectors } from '../../redux/preparation-redux'
import LocationActions, { LocationSelectors } from '../../redux/location-redux'
import Preference from 'react-native-preference'

const OnboardingContainer = (props: Props) => {
    const navigation = useNavigation()
    const [requestGeoLocationPermission, setRequestGeoLocationPermission] = useState(false)
    const [locationPermission, setLocationPermission] = useState(false)
    const [nearbyDevicesPermission, setNearbyDevicesPermission] = useState(false)
    const [haveRequestedNearbyPermission, setHaveRequestedNearbyPermission] = useState(false)


    const onCancel = () => {
        navigation.goBack();
    }

    const navigateToSignIn = () => {
        props.onboardingLocationSuccess()
        navigation.dispatch(StackActions.push(Screen.Login));
    }

    const navigateToPreprations = () => {
        navigation.dispatch(StackActions.push(Screen.PreparationsNavigators));
    }

    useEffect(() => {
        if (locationPermission)
            navigateToPreprations()
    }, [locationPermission])

    useFocusEffect(
        useCallback ( ()=> {
            Preference.clear("ENV").then(r => {
                console.log("Preference clear ENV ", r)
            })
        }, [])
    )

    useFocusEffect(
        useCallback ( ()=> {
            console.log("NextApp State: ", props.nextAppState , nearbyDevicesPermission)
            if (!haveRequestedNearbyPermission) {
                if (props.nextAppState === "active" 
                    && !nearbyDevicesPermission ) {
                    checkNearbyDevicesPermission()
                }
            }
        }, [props.nextAppState])
    )

    useFocusEffect(
        useCallback(() => {
            if (Platform.OS === 'android' && requestGeoLocationPermission)
                setLocationPermission(true)
        }, [props.geoLocation, props.timestamp])
    )

    const checkLocationPermission = async () => {
        if (env.IS_DEBUG){
            console.log("LocationPermissionContainer: Check Location Permission called()");
        }

        if (Platform.OS === 'ios'){
            const permission = parseInt(Platform.Version, 10) < 13
                            ? PERMISSIONS.IOS.LOCATION_ALWAYS
                            : PERMISSIONS.IOS.LOCATION_WHEN_IN_USE;

            request(permission).then( (result) => {
                props.getCurrentLocation()
            })
        }else{
            request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
            .then(result => {
                setRequestGeoLocationPermission(true)
                props.getCurrentLocation()
            })
        }
    }

    const checkNearbyDevicesPermission = () => {
        if (parseInt(getSystemVersion()) >= 12 ){
            setHaveRequestedNearbyPermission(true)
            requestMultiple(
                [ PERMISSIONS.ANDROID.BLUETOOTH_CONNECT,
                    PERMISSIONS.ANDROID.BLUETOOTH_SCAN,
                ]
            )
                .then( results => {
                    if (results['android.permission.BLUETOOTH_CONNECT']
                        && results['android.permission.BLUETOOTH_SCAN'] === 'granted'){
                        console.log('Android Nearby Devices Permissions: ', results)
                        
                        setNearbyDevicesPermission(true)
                    }else{
                        setNearbyDevicesPermission(false)
                    }
                })
                .catch( exception => {
                    crashlytics().recordError(exception)
                    console.log("Nearby Devices Permission Exception ", exception)
                    setNearbyDevicesPermission(false)
                })
        }

    }

    return (
        <Onboarding   onCancel={onCancel}
                        navigateToSignIn={navigateToSignIn}
                        checkLocationPermission={checkLocationPermission}
                        {...props} />
    )
}

const mapStateToProps = (state: RootState) => ({
    auth: AuthSelectors.auth(state),
    nextAppState: AppStateSelectors.nextAppState(state),
    geoLocation: PreparationSelectors.geoLocation(state),
    timestamp: PreparationSelectors.timestamp(state),
})

const mapDispatchToProps = (dispatch: RootDispatch) => ({
    onboardingLocationSuccess: () => dispatch(AppLaunchActions.onboardingLocationSuccess()),
    getCurrentLocation: () => dispatch(LocationActions.getCurrentLocation()),
})

const connector = connect(mapStateToProps, mapDispatchToProps)
interface Props extends ConnectedProps<typeof connector> {
    route: any
}

export default connector(OnboardingContainer)
