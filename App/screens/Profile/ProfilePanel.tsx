import React, { useState } from 'react'
import {
    View,
    TouchableOpacity,
    TouchableHighlight,
    Image,
    Switch,
    SectionList,
    Dimensions,
    Platform,
} from  'react-native'
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import { Container, SwipeablePanel, Header, Button, Text, TextInput, LoadingDialog } from '../../components'
import {strings} from "../../localization/strings";
import { images } from '../../constants'
import { Settings } from '../../models'
import { Colors } from '../../styles'
import {styles, commons, commonsAssets} from './Styles'
const { width, height } = Dimensions.get('window');

type Props = {
    toggleDrawer: () => void,
    onBack: () => void,
    onChangePin: () => void,
    onChangeEmail: () => void,
    onCancel: () => void,
    onSwitchPromos: () => void,
    onSwitchReminders: () => void, 
    setFirstname: (value: string) => void, 
    setLastname: (value: string) => void, 
    setPhoneNumber: (value: string) => void, 
    onSaveProfileDialog: () => void,
    onDeactivateAccount: () => void,

    shouldEdit: Boolean,
    firstname: string,
    lastname: string,
    emailAddress: string,
    phoneNumber: string,
    settings: Settings,
}

const Profile = (props: Props) =>{
    const insets = useSafeAreaInsets()

    const header = () => (
        <Header style={{ backgroundColor: Colors.red, }}
                left={Platform.OS === 'ios' ? (  <View style={{ height: 0 }}/> )
                :
                (
                   <TouchableOpacity    onPress={props.onBack}
                                        style={commons.headerTouchableOpacityStyle}>
                         <Text style={commons.headerTextStyle}>
                             Cancel
                         </Text>
                   </TouchableOpacity>
                )}
                leftContainerStyle={commons.leftContainerStyle}
        />
    )

    const [toggleKeyboard, setToggleKeyboard] = useState(false)

    const [isPanelActive, setIsPanelActive] = useState(true);

    const [panelProps, setPanelProps] = useState({
        fullWidth: true,
        openLarge: false,
        showCloseButton: false,
        onClose: () => null,
        // ...or any prop you want
        smallPanelHeight: (height - insets.top - insets.bottom) / 1.5,
    });

    const openPanel = () => {
        setIsPanelActive(true);
    };

    const closePanel = () => {
        setIsPanelActive(false);
    };

    const toggleUp = () => {
        setPanelProps({...panelProps, openLarge: true})
        closePanel()
        setTimeout(() => {
            openPanel()
        }, 1);
    }

    const logoMarginTop = () => { 
        return (props.phoneNumber != null && props.phoneNumber.length > 0 && !props.phoneNumber.includes('null')) ? styles.imagePanelMarginTopWithPhoneNumber : styles.imagePanelMarginTopWithoutPhoneNumber
    }

    return (
        <Container  safeAreaViewTopStyle={styles.safeArea}
                    statusBarStyle={'light'}
                    // scrollViewEnable={true}
                    header={header}
                    style={styles.container} >
                
            <Text style={commons.contentHeaderTextViewStyle}>{`${props.firstname} ${props.lastname}`}</Text>
            {(props.phoneNumber != null && props.phoneNumber.length > 0 && !props.phoneNumber.includes('null')) && 
                <Text style={commons.contentHeaderSubTextViewStyle}>
                    {`${props.phoneNumber}`}
                </Text> 
            }

            <View style={{ ...logoMarginTop()  }}>
                <Image resizeMode={'contain'} style={commons.contentLogoStyle} source={commonsAssets.contentImageSource} />
            </View>

            <SwipeablePanel titleValue={'myProfile'} style={commons.contentSwipePanelStyle} {...panelProps} isActive={isPanelActive}>
                    <View style={commons.contentInnerViewStyle}>
                         
                            <View style={{ ...commons.contentInnerContentViewStyle,  borderWidth: props.shouldEdit ? 0 : 0.5,}}>   
                                <View style={{ ...commons.contentInlineContentViewInnerHeaderViewStyle, borderBottomColor: props.shouldEdit ? Colors.red : Colors.darkgray,}}>
                                    <Text style={commons.contentInlineContentViewInnerHeaderTextViewStyle}>
                                    {strings.profile_information}
                                    </Text>
                                </View>
                                <View style={{ paddingTop: 10, borderBottomColor: props.shouldEdit ? Colors.red : Colors.darkgray, borderBottomWidth: 0.5, paddingVertical: 5, marginHorizontal: 8, }}>
                                    <Text style={{ ...commons.inlineContentTextViewStyle, color: props.shouldEdit ? Colors.red : Colors.black, paddingBottom: Platform.OS === 'ios' ? 5 : 0, }}>
                                        {`${strings.profile_firstname_placeholder}${props.shouldEdit ? ` *` : ``}`}
                                    </Text>
                                    <TextInput 
                                        placeholder={strings.profile_firstname_placeholder} 
                                        value={props.firstname} 
                                        placeholderTextColor={`${Colors.red}`} 
                                        keyboardType='email-address' 
                                        onChangeText={props.setFirstname} style={commons.contentInlineContextTextInputStyle}
                                        autoFocus={toggleKeyboard}
                                        onBlur={() => { setToggleKeyboard(false) }}
                                        onFocus={() => { 
                                            toggleUp()
                                            setToggleKeyboard(true)
                                        }}
                                        />
                                </View>
                                <View style={{ paddingTop: 10, borderBottomColor: props.shouldEdit ? Colors.red : Colors.darkgray, borderBottomWidth: 0.5, paddingVertical: 5, marginHorizontal: 8, }}>
                                    <Text style={{ ...commons.inlineContentTextViewStyle, color: props.shouldEdit ? Colors.red : Colors.black, paddingBottom: Platform.OS === 'ios' ? 5 : 0, }}>
                                        {`${strings.profile_lastname_placeholder}${props.shouldEdit ? ` *` : ``}`}
                                    </Text>
                                    <TextInput 
                                        placeholder={strings.profile_lastname_placeholder} 
                                        value={props.lastname} 
                                        placeholderTextColor={`${Colors.red}`} 
                                        keyboardType='email-address' 
                                        onChangeText={props.setLastname} 
                                        style={commons.contentInlineContextTextInputStyle}
                                        autoFocus={toggleKeyboard}
                                        onBlur={() => { setToggleKeyboard(false) }}
                                        onFocus={() => { 
                                            toggleUp()
                                            setToggleKeyboard(true)
                                        }}
                                                    />
                                </View>
                                <TouchableOpacity   style={{  borderBottomColor: props.shouldEdit ? Colors.red : Colors.darkgray, borderBottomWidth: 0.5, paddingVertical: 5, marginHorizontal: 8, }}
                                                    onPress={props.onChangePin}>
                                    <Text style={commons.contentInlineClickableTextViewStyle}>
                                        {strings.profile_change_pin_placeholder}
                                    </Text>
                                </TouchableOpacity>
                                <TouchableOpacity   style={{  borderBottomColor: props.shouldEdit ? Colors.red : Colors.darkgray, borderBottomWidth: 0.5, paddingVertical: 5, marginHorizontal: 8,}}
                                                    onPress={props.onChangeEmail}>
                                    <Text style={commons.contentInlineClickableTextViewStyle}>
                                        {props.emailAddress}
                                    </Text>
                                </TouchableOpacity>
                                <View style={{ paddingTop: 10, paddingVertical: 5, marginHorizontal: 8,}}>
                                    <Text style={{ ...commons.inlineContentTextViewStyle, color: props.shouldEdit ? Colors.red : Colors.black, paddingBottom: Platform.OS === 'ios' ? 5 : 0, }}>
                                        {`${strings.profile_telephone_placeholder}${props.shouldEdit ? ` *` : ``}`}
                                    </Text>
                                    <TextInput  
                                        placeholder="Phone Number" 
                                        value={`${(props.phoneNumber != null && props.phoneNumber.length > 0 && !props.phoneNumber.includes('null'))? props.phoneNumber : ''}`} 
                                        placeholderTextColor={`${Colors.red}`} 
                                        keyboardType='number-pad'
                                        onChangeText={props.setPhoneNumber} 
                                        style={commons.contentInlineContextTextInputStyle}
                                        autoFocus={toggleKeyboard}
                                        onBlur={() => { setToggleKeyboard(false) }}
                                        onFocus={() => { 
                                            toggleUp()
                                            setToggleKeyboard(true)
                                        }}
                                                    />
                                </View>
                            </View>

                            {!props.shouldEdit &&
                                <View style={{ ...commons.contentInnerContentViewStyle, paddingTop: 10, borderWidth: props.shouldEdit ? 0 : 0.5,}}>
                                    <View style={{ ...commons.contentInlineContentViewInnerHeaderViewStyle, borderBottomColor: props.shouldEdit ? Colors.red : Colors.darkgray,}}>
                                        <Text style={commons.contentInlineContentViewInnerHeaderTextViewStyle}>
                                            {strings.profile_settings.toUpperCase()}
                                        </Text>
                                    </View>
                                    <View style={[styles.settingsContainer, styles.settingsSeparator]}>
                                        <Text style={{ ...commons.contentInlineClickableTextViewStyle, color: Colors.lightgray}}>
                                            {strings.profile_email_promoos}
                                        </Text>
                                        <Switch disabled={true}
                                                trackColor={{ true: `${Colors.activeProgress}`,false: `${Colors.gray}`, }}
                                                ios_backgroundColor={!!(props.settings && props.settings.emailPromos) ? `${Colors.activeProgress}` : `${Colors.gray}`}
                                                thumbColor={(props.settings && props.settings.emailPromos) ? Colors.whiteOpacity : Colors.white}
                                                onValueChange={toggle => { props.onSwitchPromos() }}
                                                value={!!(props.settings && props.settings.emailPromos)}
                                                />
                                    </View>
                                    <View style={[styles.settingsContainer]}>
                                        <Text style={{ ...commons.contentInlineClickableTextViewStyle, color: Colors.lightgray}}>

                                            {strings.profile_earning_reminders}
                                        </Text>
                                        <Switch disabled={true}
                                                trackColor={{ true: `${Colors.activeProgress}`,false: `${Colors.gray}`, }}
                                                ios_backgroundColor={!!(props.settings && props.settings.smsReminders) ? `${Colors.activeProgress}` : `${Colors.gray}`}
                                                thumbColor={(props.settings && props.settings.smsReminders) ? Colors.whiteOpacity : Colors.white}
                                                onValueChange={toggle => { props.onSwitchReminders() }}
                                                value={!!(props.settings && props.settings.smsReminders)}
                                                />
                                    </View>
                                </View>
                            }
                            {!props.shouldEdit &&
                                <View style={{ ...commons.contentInnerContentViewStyle, paddingTop: 10, borderWidth: props.shouldEdit ? 0 : 0.5,}}>
                                    <View style={{ ...commons.contentInlineContentViewInnerHeaderViewStyle, borderBottomColor: props.shouldEdit ? Colors.red : Colors.darkgray,}}>
                                        <Text style={commons.contentInlineContentViewInnerHeaderTextViewStyle}>
                                            {strings.profile_account.toUpperCase()}
                                        </Text>
                                        <Button style={styles.deactivateAccountButton} 
                                            title={strings.deactivate_button_deactivate}
                                            onPress={props.onDeactivateAccount}/>
                                    </View>
                                    
                                </View>
                            }
                            {/* <View style={{ flex: 1, margin: 10, padding: 10, borderRadius: 15, borderWidth: 0.5, borderColor: Colors.lightgray }}>
                                <View style={{ alignItems: 'center', paddingTop: 10, borderBottomColor: Colors.darkgray, borderBottomWidth: 0.5, paddingVertical: 5, marginHorizontal: 8, }}>
                                    <Text style={{ color: Colors.black, paddingBottom: 5, fontSize: 16, }}>
                                        {strings.profile_promos_referrals.toUpperCase()}
                                    </Text>
                                </View>
                                <View style={{  paddingVertical: 5, marginHorizontal: 8, }}>
                                    <Text style={{ marginHorizontal: 15, fontSize: 15, color: Colors.black, paddingVertical: 8,}}>
                                        {strings.profile_promo_code}
                                    </Text>
                                </View>
                            </View> */}
                            {props.shouldEdit && 
                                <View style={{ flex: 1, paddingHorizontal: 25, paddingVertical: 20, flexDirection: 'row', justifyContent: 'space-around'}}>
                                    <View style={{ flex: 1, marginLeft: 10, marginRight: 5, }}>
                                        <TouchableOpacity   style={{ padding: 10, backgroundColor: Colors.red, borderRadius: 20,  justifyContent: 'center', alignItems: 'center', }}
                                                            onPress={props.onSaveProfileDialog}>
                                            <Text style={{ color: Colors.white, fontSize: 16, textAlign: 'center' }}>
                                                {strings.profile_save_button_text}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ flex: 1, marginLeft: 5, marginRight: 10, }}>
                                        <TouchableOpacity   style={{ padding: 10, borderColor: Colors.red, borderWidth: 0.5, borderRadius: 20,  justifyContent: 'center', alignItems: 'center', }}
                                                            onPress={props.onCancel}>
                                            <Text style={{ color: Colors.red, fontSize: 16, textAlign: 'center' }}>
                                                {strings.profile_cancel_button_text}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            }

                </View>
            </SwipeablePanel>
        </Container>
    )
}

export default Profile