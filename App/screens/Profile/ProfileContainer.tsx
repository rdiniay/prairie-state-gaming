import React, { useEffect, useState, useCallback } from 'react'
import { Alert, Keyboard } from 'react-native'
import { connect, ConnectedProps, } from 'react-redux'
import { RootState, RootDispatch } from '../../redux'
import ProfileLayout from './ProfilePanel'
import {StackActions ,useNavigation, DrawerActions , CommonActions, useFocusEffect} from '@react-navigation/native'
import {Screen as RootScreen} from '../../navigation/routes/rootRoutes'
import {Screen as MainScreen} from '../../navigation/routes/mainRoutes'
import { AuthSelectors } from '../../redux/auth-redux'
import ProfileActions, { ProfileSelectors } from '../../redux/profile-redux'
import { Settings, Profile } from '../../models'
import MenuActions, { MenuSelectors } from '../../redux/menu-redux'
import {strings} from "../../localization/strings";
import Toast from "react-native-toast-message";

const ProfileContainer = (props: Props) =>  {
    const navigation = useNavigation()
    const [shouldEdit, setShouldEdit] = useState(false)
    const [firstname, setFirst] = useState('')
    const [lastname, setLast] = useState('')
    const [emailAddress, setEmail] = useState('')
    const [phoneNumber, setPhone] = useState('')
    const [settings, setSetting] = useState(null)

    useEffect(() => {
        props.getProfile()
    }, [])

    useFocusEffect(
        useCallback ( ()=> {
            setDefaultProfile()
        }, [props.profile])
    )

    const onDeactivateAccount = () => {
        navigation.dispatch(StackActions.push(MainScreen.Deactivate));
    }

    const onBack = () => {
        navigation.dispatch(StackActions.pop());
    }

    const setDefaultProfile = () => {
        if (props.profile) {
            const profile = props.profile
            setFirst(profile.firstname)
            setLast(profile.lastname)
            setEmail(profile.emailAddress)
            setPhone(`${profile.phoneNumber}`)
            setSetting(profile.settings)
        }
        setShouldEdit(false)
    }

    const toggleDrawer = () => {
        navigation.dispatch(DrawerActions.toggleDrawer());
    }

    const setFirstname = (value: string) => {
        setShouldEdit(true)
        setFirst(value)
    }
    const setLastname = (value: string) => {
        setShouldEdit(true)
        setLast(value)
    }
    const setPhoneNumber = (value: string) => {
        setShouldEdit(true)
        var cleaned = ('' + value).replace(/\D/g, '')
        var match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/)
        if (match) {
            var intlCode = (match[1] ? '+1 ' : ''),
                number = [intlCode, '(', match[2], ') ', match[3], '-', match[4]].join('');

            setPhone(number)
            return;
        }
        setPhone(value)
    }
    const setSettings = (value: Settings) => {
        setShouldEdit(true)
        setSetting(value)
    }

    const onSwitchPromos = () => {
        const newSettings = { ...settings, emailPromos: !settings.emailPromos }
        setSettings(newSettings)
    }

    const onSwitchReminders = () => {
        const newSettings = { ...settings, smsReminders: !settings.smsReminders }
        setSettings(newSettings)
    }

    const onCancel = () => {
        setDefaultProfile()
        Keyboard.dismiss()
    }

    const onChangePin = () => {
        props.resetData()
        setTimeout(() => {
            navigation.dispatch(StackActions.push(RootScreen.ChangePinNavigators));
        }, 200)
    }

    const onChangeEmail = () => {
        props.resetData()
        setTimeout(() => {
            navigation.dispatch(StackActions.push(RootScreen.ChangeEmailNavigators));
        }, 200)
    }

    const onSaveProfileDialog = () => {
        Alert.alert(
            `Update Profile`,
            `Are you sure you want to update your profile?`,
            [
              {
                text: 'Yes',
                onPress: () => {
                    saveProfile()
                }
              },
              {  text: 'No', onPress: () => { } },
            ],
        );
    }

    const saveProfile = () => {
        const profile = props.profile
        if (profile) {
            const newProfile: Profile = {
                id: profile.id,
                firstname,
                lastname,
                phoneNumber: phoneNumber,
                emailAddress: profile.emailAddress,
                flags: profile.flags,
                verified: profile.verified,
                settings,
            }
            props.updateProfile(newProfile)
        }
    }



    useFocusEffect(
        useCallback(() => {
            console.log('Profile Container: ', props.error);
            if (props.error) {
                if (props.error.statusCode == 403 && props.error.signoutUser){
                    reset()
                    return
                }

                let errorMessage = props.error.genericError
                let errorStatus =  props.error.statusCode ? `Error: ${props.error.statusCode}` : ``
                if (props.error.backendError)
                    errorMessage = props.error.backendError

                if (props.error.connectionError)
                    errorMessage = props.error.connectionError


                if (__DEV__){
                    Toast.show({ type: 'custom', text1: `${errorMessage} ${errorStatus}`, })
                }else{
                    Toast.show({ type: 'custom', text1: `${errorMessage}`, })
                }

            }
        }, [props.error])
    )


    const reset = () => {
        props.signOut()
        setTimeout(() => {
            const resetAction = CommonActions.reset({
                index: 0,
                routes: [{ name: RootScreen.MainNavigators, params: {  } }]
            });
            navigation.dispatch(resetAction);

        }, 1000)
    }

    return (
        <ProfileLayout  toggleDrawer={toggleDrawer}
                        onBack={onBack}
                        onChangePin={onChangePin}
                        onChangeEmail={onChangeEmail}
                        onCancel={onCancel}
                        shouldEdit={shouldEdit}
                        firstname={firstname}
                        lastname={lastname}
                        emailAddress={emailAddress}
                        phoneNumber={phoneNumber}
                        settings={settings}
                        setFirstname={setFirstname}
                        setLastname={setLastname}
                        setPhoneNumber={setPhoneNumber}
                        onSwitchPromos={onSwitchPromos}
                        onSwitchReminders={onSwitchReminders}
                        onSaveProfileDialog={onSaveProfileDialog}
                        onDeactivateAccount={onDeactivateAccount}
                    { ...props } />
    )
}

const mapStateToProps = (state: RootState) => ({
    profile: ProfileSelectors.profile(state),
    error: ProfileSelectors.error(state),
})
const mapDispatchToProps = (dispatch: RootDispatch) => ({
    resetData: () => dispatch(ProfileActions.resetData()),
    getProfile: () => dispatch(ProfileActions.getProfile()),
    updateProfile: (profile: Profile) => dispatch(ProfileActions.updateProfile(profile)),
    signOut: () => dispatch(MenuActions.signOut()),
})

const connector = connect(mapStateToProps, mapDispatchToProps)
type Props = ConnectedProps<typeof connector>

export default connector(ProfileContainer)

