export enum Screen {
    Preparations = 'Preparations',
    LocationPermission = 'LocationPermission',
    NearbyDevicesPermission = 'NearbyDevicesPermission',
    BluetoothConnection = 'BluetoothConnection',
    NetworkConnection = 'NetworkConnection',
    OpenSettingsInstruction = 'OpenSettingsInstruction',
  }
