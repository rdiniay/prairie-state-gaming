import * as React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import TermsAndConditions from '../../screens/TermsAndConditions'
import { Screen } from '../routes/tosRoutes'

const Stack = createStackNavigator()

const TOSNavigators = () => (
    <Stack.Navigator>
        <Stack.Screen name={Screen.TOS} component={TermsAndConditions} 
                    options={{
                        headerShown:false,
                    }}/>
    </Stack.Navigator>
)

export default TOSNavigators
