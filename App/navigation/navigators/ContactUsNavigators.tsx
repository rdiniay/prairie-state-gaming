import * as React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import ContactUs from '../../screens/ContactUs'
import { Screen } from '../routes/contactUsRoutes'

const Stack = createStackNavigator()

const ContactUsNavigators = () => (
    <Stack.Navigator>
        <Stack.Screen name={Screen.ContactUs} component={ContactUs} 
                    options={{
                        headerShown:false,
                    }}/>
    </Stack.Navigator>
)

export default ContactUsNavigators
