import * as React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import DrawingRules from '../../screens/DrawingRules'
import { Screen } from '../routes/drawingRules'

const Stack = createStackNavigator()

const ContactUsNavigators = () => (
    <Stack.Navigator>
        <Stack.Screen name={Screen.DrawingRules} component={DrawingRules} 
                    options={{
                        headerShown:false,
                    }}/>
    </Stack.Navigator>
)

export default ContactUsNavigators
