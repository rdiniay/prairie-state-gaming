import * as React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import ReferralCode from '../../screens/ReferralCode'
import { Screen } from '../routes/referralCodeRoutes'

const Stack = createStackNavigator()

const ContactUsNavigators = () => (
    <Stack.Navigator>
        <Stack.Screen name={Screen.ReferralCode} component={ReferralCode} 
                    options={{
                        headerShown:false,
                    }}/>
    </Stack.Navigator>
)

export default ContactUsNavigators
