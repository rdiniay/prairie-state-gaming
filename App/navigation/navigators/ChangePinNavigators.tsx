import * as React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import ChangePin from '../../screens/ChangePin'
import { Screen } from '../routes/changePinRoutes'

const Stack = createStackNavigator()

const ContactUsNavigators = () => (
    <Stack.Navigator>
        <Stack.Screen name={Screen.ChangePin} component={ChangePin} 
                    options={{
                        headerShown:false,
                    }}/>
    </Stack.Navigator>
)

export default ContactUsNavigators
