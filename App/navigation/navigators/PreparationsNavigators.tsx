import * as React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import Preparations from '../../screens/Preparations'
import LocationPermission from '../../screens/Preparations/LocationPermission'
import BluetoothConnection from '../../screens/Preparations/BluetoothConnection'
import NetworkConnection from '../../screens/Preparations/NetworkConnection'
import OpenSettingsInstruction from '../../screens/Preparations/OpenSettings'
import NearbyDevicesPermission from '../../screens/Preparations/NearbyDevicePermission'

import { Screen } from '../routes/preparationsRoutes'
const Stack = createStackNavigator()

const PreparationsNavigators = (props: any) => (
    <Stack.Navigator>
        <Stack.Screen name={Screen.Preparations} component={(components: any) => 
                                                <Preparations {...components} settings={props.route?.params?.settings} />}
                    options={{
                        headerShown:false,
                    }}/>
        <Stack.Screen name={Screen.LocationPermission} component={LocationPermission}
                    options={{
                        headerShown:false,
                    }}/>
        <Stack.Screen name={Screen.NearbyDevicesPermission} component={NearbyDevicesPermission}
                    options={{
                        headerShown:false,
                    }}/>
        <Stack.Screen name={Screen.BluetoothConnection} component={BluetoothConnection}
                    options={{
                        headerShown:false,
                    }}/>
        <Stack.Screen name={Screen.NetworkConnection} component={NetworkConnection}
                    options={{
                        headerShown:false,
                    }}/>
        <Stack.Screen name={Screen.OpenSettingsInstruction} component={OpenSettingsInstruction}
                    options={{
                        headerShown:false,
                    }}/>
    </Stack.Navigator>
)

export default PreparationsNavigators
