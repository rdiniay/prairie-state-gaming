import * as React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import About from '../../screens/About'

import { Screen } from '../routes/aboutRoutes'
const Stack = createStackNavigator()

const AboutNavigators = () => (
    <Stack.Navigator>
        <Stack.Screen name={Screen.About} component={About} 
                    options={{
                        headerShown:false,
                    }}/>
    </Stack.Navigator>
)

export default AboutNavigators
