import * as React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import Profile from '../../screens/Profile'

import { Screen } from '../routes/profileRoutes'
const Stack = createStackNavigator()

const AboutNavigators = () => (
    <Stack.Navigator>
        <Stack.Screen name={Screen.Profile} component={Profile} 
                    options={{
                        headerShown:false,
                    }}/>
    </Stack.Navigator>
)

export default AboutNavigators
