import React, { useEffect } from 'react'
import { Platform } from 'react-native'
import {connect, ConnectedProps} from 'react-redux'
import {RootState,RootDispatch} from '../../../redux'
import { AuthSelectors } from '../../../redux/auth-redux'
import AppStateActions from '../../../redux/app-state-redux'

import { createStackNavigator } from '@react-navigation/stack'
import DrawerNavigators from './DrawerNavigors'
import TabNavigators from './DrawerNavigors/TabNavigators'

import DrawingDetails from '../../../screens/DrawingDetails'
import Deactivate from '../../../screens/Deactivate'

import Onboarding from '../../../screens/Onboarding'
import Login from '../../../screens/Login'
import Registration from '../../../screens/Registration'
import TOS from '../../../screens/TermsAndConditions'
import Verification from '../../../screens/Verification'
import ForgotPin from '../../../screens/ForgotPin'
import ForgotPinVerify from '../../../screens/ForgotPin/ForgotPinVerify'
import ForgotPinUpdate from '../../../screens/ForgotPin/ForgotPinUpdate'

import { Screen } from '../../routes/mainRoutes'
import { AppLaunchSelectors } from '../../../redux/app-launch-redux'
const Stack = createStackNavigator()
const showHeader = false

const MainNavigator = (props: Props) => {
    const shouldDrawer = Platform.OS !== 'ios'

    return (
        <Stack.Navigator>
            {!props.onboardingSuccess &&
                <Stack.Screen  name={Screen.Onboarding} component={Onboarding} 
                                options={{
                                    headerShown:showHeader,
                                    animationEnabled: false
                                }}/>
            }
            <Stack.Screen  name={Screen.Login} component={Login} 
                            options={{
                                headerShown:showHeader,
                                animationEnabled: false
                            }}/>
            <Stack.Screen   name={Screen.Registration} component={Registration} 
                            options={{
                                headerShown:showHeader,
                            }}/>
            <Stack.Screen   name={Screen.TOS} component={TOS} 
                            options={{
                                headerShown:showHeader,
                            }}/>
            <Stack.Screen   name={Screen.Verification} component={Verification} 
                            options={{
                                headerShown:showHeader,
                            }}/>
            <Stack.Screen   name={Screen.ForgotPin} component={ForgotPin} 
                            options={{
                                headerShown:showHeader,
                            }}/>
            <Stack.Screen   name={Screen.ForgotPinVerify} component={ForgotPinVerify} 
                            options={{
                                headerShown:showHeader,
                            }}/>
            <Stack.Screen   name={Screen.ForgotPinUpdate} component={ForgotPinUpdate} 
                            options={{
                                headerShown:showHeader,
                            }}/>

            {/* CONTESTS SCREEN */}
            <Stack.Screen   name={Screen.DrawerNavigator} component={shouldDrawer ? DrawerNavigators : TabNavigators} 
                            options={{
                                headerShown:showHeader,
                                animationEnabled: false
                            }} />
            <Stack.Screen   name={Screen.DrawingDetails} component={DrawingDetails} 
                            options={{
                                headerShown:showHeader,
                            }}/>
            
            {/* PROFILE SCREEN */}
            <Stack.Screen   name={Screen.Deactivate} component={Deactivate} 
                            options={{
                                headerShown:showHeader,
                            }}/>
        </Stack.Navigator>
    )
}

const mapStateToProps = (state: RootState) => ({
    auth: AuthSelectors.auth(state),
    onboardingSuccess: AppLaunchSelectors.onboardingSuccess(state),
})

const mapDispatchToProps = (dispatch: RootDispatch) =>({
    subscribeAppStateChange: () => dispatch(AppStateActions.subscribeAppStateChange()),
    unsubscribeAppStateChange: () => dispatch(AppStateActions.unsubscribeAppStateChange()),
})

const connector = connect(mapStateToProps, mapDispatchToProps)
type Props = ConnectedProps<typeof connector>

export default connector(MainNavigator)