import * as React from 'react'
import { createDrawerNavigator } from '@react-navigation/drawer';
import TabNavigators from './TabNavigators'
import Home from '../../../../screens/Dashboard'
import Menu from '../../../../screens/Menu'

import { Screen } from '../../../routes/drawerRoutes'
const Drawer = createDrawerNavigator()

const DrawerNavigator = () => (
  <Drawer.Navigator drawerContent={props => <Menu {...props} />}>

    <Drawer.Screen name={Screen.Contests} component={Home} />
  </Drawer.Navigator>
)

export default DrawerNavigator
//https://dev.to/easybuoy/combining-stack-tab-drawer-navigations-in-react-native-with-react-navigation-5-da
