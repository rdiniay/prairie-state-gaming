import React, {useEffect,useState,} from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { NavigationContainer } from '@react-navigation/native'
import {connect, ConnectedProps} from 'react-redux'
import {RootState,RootDispatch} from '../../redux'
import reduxPersist from '../../config/reduxPersist'
import AppStateActions from '../../redux/app-state-redux'
import AppLaunchActions, { AppLaunchSelectors } from '../../redux/app-launch-redux'
import PreparationsNavigators from './PreparationsNavigators'
import MainNavigators from './MainNavigators'
import AboutNavigators from './AboutNavigators'
import ContactUsNavigators from './ContactUsNavigators'
import ChangePinNavigators from './ChangePinNavigators'
import ChangeEmailNavigators from './ChangeEmailNavigators'
import DrawingRulesNavigators from './DrawingRulesNavigators'
import TOSNavigators from './TOSNavigators'
import TipsNavigators from './TipsNavigators'
import ReferralCodeNavigators from './ReferralCodeNavigators'
import ProfileNavigators from './ProfileNavigators'
import LocationNavigators from './LocationNavigators'
import HelpAndSupportNavigators from './HelpAndSupportNavigators'


import { AuthSelectors } from '../../redux/auth-redux'
import { Screen } from '../routes/rootRoutes'
import BackgroundGeolocation, {
  Config,
  Location,
  Subscription
} from "react-native-background-geolocation";

const Stack = createStackNavigator()

// MODAL SCREENS
const RootNavigator = (props: Props) => {

  useEffect(() => {
      props.subscribeAppStateChange()
      
      if (!reduxPersist.active)
        props.appLaunch()

      return () => {
        props.unsubscribeAppStateChange()
      };
  }, [])

  return (
    <NavigationContainer>
        {props.didAppLaunch && 
            <Stack.Navigator mode="modal">
                <Stack.Screen name={Screen.MainNavigators} component={MainNavigators} 
                                options={{
                                  headerShown:false,
                                }}/>
                <Stack.Screen name={Screen.PreparationsNavigators} component={PreparationsNavigators} 
                              options={{  
                                headerShown:false,
                                gestureEnabled: false,
                              }}/>
                <Stack.Screen name={Screen.AboutNavigators} component={AboutNavigators} 
                      options={{
                        headerShown:false,
                        gestureEnabled: false,
                      }}/>
                <Stack.Screen name={Screen.ContactUsNavigators} component={ContactUsNavigators} 
                      options={{
                        headerShown:false,
                        gestureEnabled: false,
                      }}/>
                <Stack.Screen name={Screen.ChangePinNavigators} component={ChangePinNavigators} 
                      options={{
                        headerShown:false,
                        gestureEnabled: false,
                      }}/>
                <Stack.Screen name={Screen.ChangeEmailNavigators} component={ChangeEmailNavigators} 
                      options={{
                        headerShown:false,
                        gestureEnabled: false,
                      }}/>   
                <Stack.Screen name={Screen.DrawingRulesNavigators} component={DrawingRulesNavigators} 
                      options={{
                        headerShown:false,
                        gestureEnabled: false,
                      }}/>    
                <Stack.Screen name={Screen.TOSNavigators} component={TOSNavigators} 
                      options={{
                        headerShown:false,
                        gestureEnabled: false,
                      }}/>    
                <Stack.Screen name={Screen.TipsNavigators} component={TipsNavigators} 
                      options={{
                        headerShown:false,
                        gestureEnabled: false,
                      }}/>
                <Stack.Screen name={Screen.ReferralCodeNavigators} component={ReferralCodeNavigators} 
                      options={{
                        headerShown:false,
                        gestureEnabled: false,
                      }}/>
                <Stack.Screen name={Screen.ProfileNavigators} component={ProfileNavigators} 
                      options={{
                        headerShown:false,
                        gestureEnabled: false,
                      }}/>
                <Stack.Screen name={Screen.LocationNavigators} component={LocationNavigators} 
                      options={{
                        headerShown:false,
                        gestureEnabled: false,
                      }}/>
                <Stack.Screen name={Screen.HelpAndSupportNavigators} component={HelpAndSupportNavigators} 
                      options={{
                        headerShown:false,
                        gestureEnabled: false,
                      }}/>
            </Stack.Navigator>
        }
    </NavigationContainer>
  )
}

const mapStateToProps = (state: RootState) => ({
    didAppLaunch: AppLaunchSelectors.didAppLaunch(state)
})

const mapDispatchToProps = (dispatch: RootDispatch) =>({
    appLaunch: () => dispatch(AppLaunchActions.appLaunch()),
    subscribeAppStateChange: () => dispatch(AppStateActions.subscribeAppStateChange()),
    unsubscribeAppStateChange: () => dispatch(AppStateActions.unsubscribeAppStateChange()),
})

const connector = connect(mapStateToProps, mapDispatchToProps)
type Props = ConnectedProps<typeof connector>

export default connector(RootNavigator)
