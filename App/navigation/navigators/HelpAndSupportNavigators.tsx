import * as React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import HelpAndSupport from '../../screens/HelpAndSupport'

import { Screen } from '../routes/helpAndSupportRoutes'
const Stack = createStackNavigator()

const HelpAndSupportNavigators = () => (
    <Stack.Navigator>
        <Stack.Screen name={Screen.HelpAndSupport} component={HelpAndSupport} 
                    options={{
                        headerShown:false,
                    }}/>
    </Stack.Navigator>
)

export default HelpAndSupportNavigators