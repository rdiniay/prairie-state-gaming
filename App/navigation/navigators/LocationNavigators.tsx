import * as React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import Location from '../../screens/Location'
import DrawingDetails from '../../screens/DrawingDetails'

import { Screen } from '../routes/locationRoutes'
const Stack = createStackNavigator()

const LocationNavigators = () => (
    <Stack.Navigator>
        <Stack.Screen name={Screen.Location} component={Location} 
                    options={{
                        headerShown:false,
                    }}/>
        <Stack.Screen   name={Screen.DrawingDetails} component={DrawingDetails} 
                        options={{
                            headerShown:false,
                        }}/>
    </Stack.Navigator>
)

export default LocationNavigators
