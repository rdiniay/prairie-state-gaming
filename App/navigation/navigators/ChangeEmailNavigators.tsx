import * as React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import ChangeEmail from '../../screens/ChangeEmail'
import { Screen } from '../routes/changeEmailRoutes'

const Stack = createStackNavigator()

const ContactUsNavigators = () => (
    <Stack.Navigator>
        <Stack.Screen name={Screen.ChangeEmail} component={ChangeEmail} 
                    options={{
                        headerShown:false,
                    }}/>
    </Stack.Navigator>
)

export default ContactUsNavigators
