import * as React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import Tips from '../../screens/Tips'
import { Screen } from '../routes/tipsRoutes'

const Stack = createStackNavigator()

const TipsNavigators = () => (
    <Stack.Navigator>
        <Stack.Screen name={Screen.Tips} component={Tips} 
                    options={{
                        headerShown:false,
                    }}/>
    </Stack.Navigator>
)

export default TipsNavigators
