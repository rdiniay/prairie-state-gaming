fastlane documentation
----

# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```sh
xcode-select --install
```

For _fastlane_ installation instructions, see [Installing _fastlane_](https://docs.fastlane.tools/#installing-fastlane)

# Available Actions

### increment_ios_build_number

```sh
[bundle exec] fastlane increment_ios_build_number
```



----


## iOS

### ios dev

```sh
[bundle exec] fastlane ios dev
```

Setup iOS configuration and environment variables

### ios staging

```sh
[bundle exec] fastlane ios staging
```



### ios production

```sh
[bundle exec] fastlane ios production
```



### ios upload_s3

```sh
[bundle exec] fastlane ios upload_s3
```

Upload to s3

----


## Android

### android dev

```sh
[bundle exec] fastlane android dev
```

Setup Android configuration and environment variables

Android dev, stage and prod releases

### android staging

```sh
[bundle exec] fastlane android staging
```



### android production

```sh
[bundle exec] fastlane android production
```



----

This README.md is auto-generated and will be re-generated every time [_fastlane_](https://fastlane.tools) is run.

More information about _fastlane_ can be found on [fastlane.tools](https://fastlane.tools).

The documentation of _fastlane_ can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
