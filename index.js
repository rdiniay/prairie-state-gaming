/**
 * @format
 */
import './App/config/reactotronConfig'
import {AppRegistry, Platform} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import BackgroundFetch from "react-native-background-fetch";
import BackgroundTimer from 'react-native-background-timer';

// Android shows an issue where it cannot save state when going into background mode since this is used
// in redux-persist setInterval and setInterval only works with foreground or active state
// As a temporary workaround we need to poly-fill global setTimeout and clearTimeout using react-native-background-timer
// https://github.com/rt2zz/redux-persist/issues/1136#issuecomment-662543947
(() => {
    global.setTimeout = BackgroundTimer.setTimeout.bind(BackgroundTimer);
    global.clearTimeout = BackgroundTimer.clearTimeout.bind(BackgroundTimer);
})();

let MyHeadlessTask = async (event) => {
    // Get task id from event {}:
    let taskId = event.taskId;
    let isTimeout = event.timeout;  // <-- true when your background-time has expired.
    if (isTimeout) {
        // This task has exceeded its allowed running-time.
        // You must stop what you're doing immediately finish(taskId)
        console.log('[BackgroundFetch] Headless TIMEOUT:', taskId);
        BackgroundFetch.finish(taskId);
        return;
    }
    console.log('[BackgroundFetch HeadlessTask] start: ', taskId);


    // Required:  Signal to native code that your task is complete.
    // If you don't do this, your app could be terminated and/or assigned
    // battery-blame for consuming too much time in background.
    BackgroundFetch.finish(taskId);
}

// Register your BackgroundFetch HeadlessTask
BackgroundFetch.registerHeadlessTask(MyHeadlessTask);
AppRegistry.registerComponent(appName, () => App);
