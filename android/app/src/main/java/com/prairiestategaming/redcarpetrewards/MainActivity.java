package com.prairiestategaming.redcarpetrewards;

import com.facebook.react.ReactActivity;
import android.os.Bundle;

public class MainActivity extends ReactActivity {

  /**
   * Returns the name of the main component registered from JavaScript. This is used to schedule
   * rendering of the component.
   */
  @Override
  protected String getMainComponentName() {
    return "PrairieStateGaming";
  }

  /**
   * No point restoring savedInstanceState since we are saving data in react native js
   * https://github.com/software-mansion/react-native-screens/issues/17#issuecomment-424704067
   * @param savedInstanceState
   */
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(null);
  }
}
